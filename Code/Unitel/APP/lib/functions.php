<?php

class CallInfo {

    public $msisdn;
    public $maxRetries;
    public $retryTime;
    public $waitTime;
    public $context;
    public $extension;
    public $priority;
    public $callerId;
    public $set;

}
function createCallFile($callInfo, $filePath) {
    $content = "Channel: SIP/{$callInfo->msisdn}" .
            "\nMaxRetries: {$callInfo->maxRetries}" .
            "\nRetryTime: {$callInfo->retryTime}" .
            "\nWaitTime: {$callInfo->waitTime}" .
            "\nContext: {$callInfo->context}" .
            "\nExtension: {$callInfo->extension}" .
            "\nPriority: {$callInfo->priority}" .
            "\nCallerID: {$callInfo->callerId}" .
            "\nSet: {$callInfo->set}" .
            "\nAlwaysDelete: Yes";

    if (file_exists("{$filePath}.call"))
        unlink("{$filePath}.call");

    file_put_contents("{$filePath}.call", $content);
}


function getFileName($name){
    return substr( $name, 0, strrpos( $name, '.' ) );
}


function getPathStorage($rootDir, $itemId, $itemPerDir, $makeDir)
{
    $dir1 = floor($itemId / ($itemPerDir * $itemPerDir * $itemPerDir));
    $dir2 = floor(($itemId - $dir1 * $itemPerDir * $itemPerDir * $itemPerDir)/($itemPerDir * $itemPerDir));
    $temp = floor($itemId / ($itemPerDir * $itemPerDir));
    $dir3 = floor(($itemId -  $temp * $itemPerDir * $itemPerDir)/$itemPerDir);

    $path = $dir1 . "/" . $dir2 . "/" .$dir3;

    if (!file_exists($rootDir. "/" . $path) && ($makeDir))
    {
            if (!file_exists($rootDir. "/" . $dir1 . "/" . $dir2))
            {
                    if (!file_exists($rootDir. "/" . $dir1))
                    {
                            @mkdir($rootDir. "/" . $dir1, true);
                    }
                    @mkdir($rootDir. "/" . $dir1 . "/" . $dir2, true);
                    chmod($rootDir. "/" . $dir1, 0777 );
            }
            @mkdir($rootDir. "/" . $dir1 . "/" . $dir2, true);
            chmod($rootDir. "/" . $dir1 . "/" . $dir2, 0777 );
    }
    @mkdir($rootDir. "/" . $dir1 . "/" . $dir2 . "/" . $dir3, true);
    chmod($rootDir. "/" . $dir1 . "/" . $dir2 . "/" . $dir3, 0777 );
    return $path;
}
?>
