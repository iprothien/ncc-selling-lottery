<?php

require_once("lib/config.php");
require_once("lib/sbMysqlPDO.class.php");
try {
    $conn = new sbMysqlPDO($server, $user, $password, $db);
} catch (Exception $e) {
    exit();
}

$sql = 'SELECT * FROM draw_time WHERE  is_check=0 && status=2 && delete_status!=1 order by id limit 5';
$listDraw = $conn->doSelect($sql);

foreach ($listDraw as $item) {
    $sqlUpdateLotto = 'UPDATE draw_time SET is_check=1 WHERE id=' . $item['id'];
    $conn->doUpdate($sqlUpdateLotto);

    echo "\nDraw ID: " . $item['id'];
    $minId = $item['id'];
    $sqlgetMember = " SELECT * FROM member_draw "
                    ." WHERE ((code= '{$item['three_digit']}' AND type=2) || (code='{$item['two_digit']}' AND type=1) )"
                    ." AND status=1 AND draw_id= '{$item['id']}' ";
    $data = $conn->doSelect($sqlgetMember);
    echo $sqlgetMember;
    if ($data) {
        $totalMoney = 0;
        foreach ($data as $a) {
            if ($a['code'] == $item['three_digit'])
                $totalMoney = $a['money'] * 500;
            else if ($a['code'] == $item['two_digit'])
                $totalMoney = $a['money'] * 60;
            $sqlInsert = 'INSERT INTO winner(msisdn,buyer_id,transaction_id,total_money,inform_status) 
                          VALUES("' . $a['msisdn'] . '","' . $a['id'] . '","' . $item['id'] . '","' . $totalMoney . '","1")';
            $conn->doUpdate($sqlInsert);

            $smsContent = "Congratulation, you win IVR lottery with digits " . $a['code'] . " and amount " . $a['money'] . ", you will get " . $totalMoney . " KIP. Thank you for using our services.";
            $sqlMt = 'INSERT INTO sms_mt(short_code,msisdn,status,content,created_datetime) 
                        VALUES("8899","' . $a['msisdn'] . '","0","' . $smsContent . '",now())';

            $conn->doUpdate($sqlMt);

            $sqlUpdateMember = 'UPDATE member_draw SET win_status=1 WHERE id=' . $a['id'];
            $conn->doUpdate($sqlUpdateMember);
        }
    }
}

?>