<?php
/**
 * User: dovv<dovv1987@gmail.com>
 * DateTime: 7/21/14 11:13 AM
 */

require_once('lib/sbMysqlPDO.class.php');
require_once('lib/config.php');
require_once('lib/functions_unitel.php');

$conn = new sbMysqlPDO($server, $user, $password, $db);

$msg = 'Error Message From Unitel <br />';

// Check Connecting
$connect = $conn->getConnection();
if(empty($connect)){
    $msg .= 'Can not connect to Unitel mysql server <br />';
}else{
    echo 'Ok Mysql';
}

// Check Asterisk
$statusAsterisk = exec('/etc/init.d/asterisk status');
$strAs = "is running";
if (strpos($statusAsterisk,$strAs) > 0) {
    echo 'Ok Asterisk';
}else{
    $msg .= 'Error Asterisk Or Stop Asterisk <br />';
}

// Ping server
$ping = exec('ping -c 3 10.78.17.17'); //10.78.17.17
if(empty($ping)){
    echo 'Error ping';
    $msg .= 'Can not ping to Unitel server <br />';
}else{
    echo 'Ok ping';
}

// Ping server smsc 1
$pingSmsc1 = exec('ping -c 3 10.20.3.1');
if(empty($pingSmsc1)){
    $msg .= 'Can not ping smsc1 ip: 10.20.3.1 <br />';
}else{
    echo 'Ok ping smsc1';
}

// Ping server smsc 2
$pingSmsc2 = exec('ping -c 3 10.20.3.17');
if(empty($pingSmsc2)){
    $msg .= 'Can not ping smsc2 ip: 10.20.3.17 <br />';
}else{
    echo 'Ok ping smsc2';
}

// Ping server smsc 3
$pingSmsc3 = exec('ping -c 3 10.20.5.1');
if(empty($pingSmsc3)){
    $msg .= 'Can not ping smsc3 ip: 10.20.5.1 <br />';
}else{
    echo 'Ok ping smsc3';
}

// Ping server smsc 4
$pingSmsc4 = exec('ping -c 3 10.20.5.17');
if(empty($pingSmsc4)){
    $msg .= 'Can not ping smsc1 ip: 10.20.5.17 <br />';
}else{
    echo 'Ok ping smsc4';
}

// Check Info
$info = viewInfo($_msisdn);
if($info[3] == $_msisdn){
    echo '0k info';
}else{
    $msg .= 'Can not get phone number information <br />';
}

// Check Payment
$payment = charge($_msisdn,$_money,$_shortcode,$_feeType,$subType=1);

if($payment[0] =='0'){
    echo 'Ok payment';
}else{
    $msg .= 'Can not charge money in Payment gw <br />';
}

if(empty($connect) || strpos($statusAsterisk,$strAs) < 0 || empty($ping) || $info[0] == 22 || empty($payment) || empty($pingSmsc1) || empty($pingSmsc2) || empty($pingSmsc3) || empty($pingSmsc4)){
    sendSmsToPhone($_phone,$msg);
    sendEmail($_email,$msg);
}else{
    echo 'Ok';
}

function getConnect(){
    global $server, $user, $password, $db;
    return new sbMysqlPDO($server, $user, $password, $db);
}

function sendSmsToPhone($phone = array(), $msg){
    $cnn = getConnect();
    $total_sms = floor(strlen($msg)/160);
    $date = date('Y-m-d H:i:s');
    foreach($phone as $p){
        $sql = "INSERT INTO sms_mt SET `short_code` = '8899', `msisdn`= '{$p}', `status` = 0, `type` = 1,`content` = '{$msg}',`created_datetime` = '{$date}', `updated_datetime` = '{$date}', `total_sms` = '{$total_sms}', `draw_no` =0";
        $cnn->doUpdate($sql);
    }
    return true;
}


function sendEmail($mailList =array(),$body){

    require_once('mailer/class.phpmailer.php');
    require_once('mailer/class.smtp.php');
    try {
        $mail = new PHPMailer(true);                //New instance, with exceptions enabled
        $mail->IsSMTP();                            // tell the class to use SMTP
        $mail->SMTPAuth   = true;                   // enable SMTP authentication
        $mail->Port       = 465;                     // set the SMTP server port
        $mail->Host       = "smtp.gmail.com";       // SMTP server
	 $mail->SMTPSecure = 'ssl'; 
        $mail->Username   = "dovv@iprotech.vn";     // SMTP server username
        $mail->Password   = "Do1987@123";           // SMTP server password

        $mail->From       = "dovv@iprotech.vn";
        $mail->FromName   = "NCC monitoring";

        foreach($mailList as $to){
            $mail->AddAddress($to);
        }

        $mail->Subject  = "NCC monitoring Unitel";
        $mail->MsgHTML($body);
        $mail->IsHTML(true);
        $mail->Send();
	 echo 'Email Ok';
    } catch (phpmailerException $e) {
        echo $e->errorMessage();
    }
    return true;
}
