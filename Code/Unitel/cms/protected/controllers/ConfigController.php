<?php

class ConfigController extends AccessController {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';
    public $module = 'config';

    public function accessRules() {
        return array(
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('view', 'create', 'update', 'admin', 'delete', 'export', 'admin2'),
                'users' => array('@'),
                'expression' => (string) $this->authenticate($this->module)
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Config;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        $model->attributes = $_POST['Config'];

        if ($model->save())
            $this->redirect(array('view', 'id' => $model->id));

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Config'])) {
            $model->attributes = $_POST['Config'];
            $file = CUploadedFile::getInstanceByName('Config[value]');
            if ($model->group == 1) {
                if (!isset($file)) {
                    $model->value = $_POST['old_value'];
                } else {
                    $file->saveAs('uploads/cms/config/' . $file->name);
                    rename('uploads/cms/config/' . $file->name, 'uploads/cms/config/' . $model->value);
                    $file_rename = $model->value;
                    $model->value = $file_rename;
                    $model->convert_status = '0';
                }
            } else {
                $model->time_start  = date('Y-m-d H:i:s',strtotime($_POST['Config']['time_start']));
                $model->time_end    = date('Y-m-d H:i:s',strtotime($_POST['Config']['time_end']));
            }

            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        /*
          $this->loadModel($id)->delete();

          // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
          if (!isset($_GET['ajax']))
          $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
         */
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Config('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Config']))
            $model->attributes = $_GET['Config'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function actionAdmin2() {
        $model = new Config('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Config']))
            $model->attributes = $_GET['Config'];

        $this->render('admin2', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = Config::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'config-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
