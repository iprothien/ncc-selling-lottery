<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');
// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'NCC Selling lottery system',
    // preloading 'log' component
    'preload' => array('log'),
    // autoloading model and component classes
    'import' => array(
        'application.models.*',
        'application.components.*',
    ),
    'modules' => array(
        // uncomment the following to enable the Gii tool

        'gii' => array(
            'class' => 'system.gii.GiiModule',
            'password' => '1',
            // If removed, Gii defaults to localhost only. Edit carefully to taste.
            'ipFilters' => array('127.0.0.1', '::1'),
        ),
    ),
    // application components
    'components' => array(
        'user' => array(
            // enable cookie-based authentication
            'class' => 'UWebUser'
        ),

        'db' => array(
            'connectionString' => 'mysql:host=localhost;dbname=lotto',
            'emulatePrepare' => true,
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8',
        ),
        'errorHandler' => array(
            // use 'site/error' action to display errors
            'errorAction' => 'site/error',
        ),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning',
                ),
            // uncomment the following to show log messages on web pages
            /*
              array(
              'class'=>'CWebLogRoute',
              ),
             */
            ),
        ),
    ),
    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params' => array(
        // this is used in contact page
        'audioType' => 'mp3, acc, wma, ac3, aif, aifc, aiff, amr, au, snd, dts, mka, mid, midi, rmi, oga, mpc, ofr, ofs, opus, ra, tak, tta, wav, wv, wax',
        'adminEmail' => 'hungnv@iprotech.vn',
        'module' => array(
            'user' => array(
                'name' => 'User',
                'url' => array('/user/admin')
            ),
            'drawResult' => array(
                'name' => 'Lottery result',
                'url' => array('/drawResult/admin')
            ),
            'drawTime' => array(
                'name' => 'Draw Time',
                'url' => array('/drawTime/admin')
            ),
            'memberDraw' => array(
                'name' => 'Buyer',
                'url' => array('/memberDraw/admin')
            ),
	     'winner' => array(
                'name' => 'Winner',
                'url' => array('/winner/admin')
            ),
            'chargingHistory' => array(
                'name' => 'Charging History',
                'url' => array('/chargingHistory/admin')
            ),
            'config' => array(
                'name' => 'Configuration',
                'url' => array('/config/admin&group=1')
            ),
            'smsMt' => array(
                'name' => ' SMS MT',
                'url' => array('/smsMt/admin')
            ),
            'cdr' => array(
                'name' => 'Cdr',
                'url' => array('/cdr/admin')
            )
        )
    ),
);