<?php

/**
 * This is the model class for table "sms_mt".
 *
 * The followings are the available columns in table 'sms_mt':
 * @property integer $id
 * @property integer $short_code
 * @property string $msisdn
 * @property integer $status
 * @property integer $type
 * @property string $content
 * @property string $created_datetime
 * @property string $updated_datetime
 * @property string $phonenumber
 */
class SmsMt extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return SmsMt the static model class
     */
    public function getStatus() {
        switch ($this->status) {
            case 1:
                return 'Completed';
                break;
            case 0:
                return 'Pending';
                break;
            default:
                return 'Error';
                break;
        }
    }
    
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'sms_mt';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('short_code, status, type', 'numerical', 'integerOnly' => true),
            array('msisdn, phonenumber', 'length', 'max' => 16),
            array('content', 'length', 'max' => 512),
            array('created_datetime, updated_datetime', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, msisdn, status, created_datetime, time_start, time_end', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'short_code' => 'Sender',
            'msisdn' => 'Msisdn',
            'status' => 'Status',
            'type' => 'Type',
            'content' => 'Content',
            'created_datetime' => 'Created Datetime',
            'updated_datetime' => 'Updated Datetime',
            'phonenumber' => 'Phonenumber',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public $time_start;
    public $time_end;
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('msisdn', $this->msisdn, true);
        $criteria->compare('status', $this->status);
        $criteria->compare('created_datetime', $this->created_datetime, true);
        if($this->time_start)
            $criteria->compare ('created_datetime', '>'.$this->time_start);
        if($this->time_end)
            $criteria->compare ('created_datetime', '<'.$this->time_end);
        $criteria->order    = 'id DESC';

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                    'pagination' => array('pageSize' => 25)
                ));
    }

}