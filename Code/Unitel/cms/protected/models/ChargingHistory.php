<?php

/**
 * This is the model class for table "charging_history".
 *
 * The followings are the available columns in table 'charging_history':
 * @property integer $id
 * @property string $msimdn
 * @property string $time_charge
 * @property string $money
 */
class ChargingHistory extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return ChargingHistory the static model class
     */
    public $time_start;
    public $time_end;

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'charging_history';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('msisdn, time_charge, money', 'required'),
            array('money', 'length', 'max' => 20),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, msisdn, time_start, time_end, time_charge, money', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'msisdn' => 'Msisdn',
            'time_charge' => 'Time Charge',
            'money' => 'Money',
            'time_start' => 'Start Charge Date',
            'time_end' => 'End Charge Date',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('msisdn', $this->msisdn, true);
        $criteria->compare('time_charge', $this->time_charge, true);
        if ($this->time_start)
            $criteria->compare('time_charge', '>' . $this->time_start);
        if ($this->time_end)
            $criteria->compare('time_charge', '<' . $this->time_end);
        $criteria->order = 'id DESC';

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                    'pagination' => array('pageSize' => '25'),
                ));
    }

}