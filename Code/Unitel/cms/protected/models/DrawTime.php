<?php

/**
 * This is the model class for table "draw_time".
 *
 * The followings are the available columns in table 'draw_time':
 * @property integer $id
 * @property string $start_time
 * @property string $end_time
 * @property string $three_digit
 * @property string $two_digit
 * @property integer $result_status
 */
class DrawTime extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    
    public function getStatus() {
        switch($this->status) {
            case 1:
                return 'Continue';
                break;
            case 2:
                return 'Finish';
                break;
            default:
                return 'Disable';
        }
    }
    
    public function tableName() {
        return 'draw_time';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('start_time, end_time, draw_no, result_status', 'required'),
            array('draw_no', 'numerical', 'integerOnly' => true),
            array('draw_no', 'length', 'max' => 10),
            array('draw_no', 'validateDrawNo'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, start_time, end_time', 'safe', 'on' => 'search'),
        );
    }
    
    public function validateDrawNo($attribute, $params) {
        if(isset($this->draw_no) && $this->draw_no && $this->isNewRecord) {
            if($this->findByAttributes(array('draw_no' => $this->draw_no)))
                $this->addError($attribute, 'Draw No "'.$this->draw_no.'" already exits!');
        }
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'start_time' => 'Start Time',
            'end_time' => 'End Time',
            'three_digit' => 'Three Digit',
            'two_digit' => 'Two Digit',
            'result_status' => 'Result Status'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('start_time', $this->start_time, true);
        $criteria->compare('end_time', $this->end_time, true);
        $criteria->order = 'id DESC';

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return DrawTime the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
