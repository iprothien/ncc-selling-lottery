<?php

/**
 * This is the model class for table "winner".
 *
 * The followings are the available columns in table 'winner':
 * @property integer $id
 * @property string $msisdn
 * @property integer $buyer_id
 * @property integer $transaction_id
 * @property integer $inform_status
 * @property string $created_datetime
 */
class Winner extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public $time_start, $time_end;
    
    public function getStatus() {
        switch ($this->inform_status) {
            case 1:
                'Complete';
                break;
            case 0:
                'Processing';
                break;
            default:
                return '';
                break;
        }
    }
    
    public function tableName() {
        return 'winner';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('msisdn, buyer_id, transaction_id, inform_status', 'required'),
            array('buyer_id, transaction_id, inform_status', 'numerical', 'integerOnly' => true),
            array('msisdn', 'length', 'max' => 20),
            array('created_datetime', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, msisdn, time_start, time_end, buyer_id, transaction_id, inform_status, created_datetime', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'msisdn' => 'Msisdn',
            'buyer_id' => 'Buyer',
            'transaction_id' => 'Transaction',
            'inform_status' => 'Inform Status',
            'created_datetime' => 'Created Datetime',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public $draw_no;
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;
        $criteria->alias    = 'a';
        $criteria->select   = 'a.*,b.draw_no';
        $criteria->join = 'INNER JOIN draw_time AS b ON a.transaction_id=b.id';

        $criteria->compare('id', $this->id);
        $criteria->compare('msisdn', $this->msisdn, true);
        $criteria->compare('buyer_id', $this->buyer_id);
        $criteria->compare('transaction_id', $this->transaction_id);
        $criteria->compare('inform_status', $this->inform_status);
        
        if($this->time_start)
            $criteria->compare ('created_datetime', '>'.$this->time_start);
        if($this->time_end)
            $criteria->compare ('created_datetime', '<'.$this->time_end);

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Winner the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
