<?php
/* @var $this DrawTimeController */
/* @var $model DrawTime */

$this->breadcrumbs=array(
	'Draw Times'=>array('admin'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'Create DrawTime', 'url'=>array('create')),
	array('label'=>'View DrawTime', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage DrawTime', 'url'=>array('admin')),
);
?>

<h1>Update DrawTime <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>