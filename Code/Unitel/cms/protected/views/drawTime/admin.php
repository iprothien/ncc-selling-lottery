<?php
/* @var $this DrawTimeController */
/* @var $model DrawTime */

$this->breadcrumbs=array(
	'Draw Times'=>array('admin'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Create DrawTime', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#draw-time-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Draw Times</h1>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'draw-time-grid',
	'dataProvider'=>$model->search(),
	'columns'=>array(
		'id',
                'draw_no',
		'start_time',
		'end_time',
                'three_digit',
                'two_digit',
                array('name'=>'status','value'=>'$data->getStatus()'),
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
