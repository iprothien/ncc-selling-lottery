<?php
/* @var $this DrawTimeController */
/* @var $model DrawTime */

$this->breadcrumbs=array(
	'Draw Times'=>array('admin'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Create DrawTime', 'url'=>array('create')),
	array('label'=>'Update DrawTime', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete DrawTime', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage DrawTime', 'url'=>array('admin')),
);
?>

<h1>View DrawTime #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
                'draw_no',
		'start_time',
		'end_time',
                'status',
                'three_digit',
                'two_digit',
                'result_status'
	),
)); ?>
