<?php
/* @var $this DrawTimeController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Draw Times',
);

$this->menu=array(
	array('label'=>'Create DrawTime', 'url'=>array('create')),
	array('label'=>'Manage DrawTime', 'url'=>array('admin')),
);
?>

<h1>Draw Times</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
