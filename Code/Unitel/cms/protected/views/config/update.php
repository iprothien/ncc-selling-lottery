<?php
/* @var $this ConfigController */
/* @var $model Config */

$this->breadcrumbs=array(
	'Configs'=>array('admin'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'View Config', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Audio Config', 'url'=>array('admin', 'group' => 1)),
        array('label'=>'Time to Draw', 'url'=>array('admin2','group' => 2)),
);
?>

<h4>Update Config <?php echo $model->id; ?></h4>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>