<?php
/* @var $this DrawResultController */
/* @var $model DrawResult */

$this->breadcrumbs=array(
	'Draw Results'=>array('admin'),
	'Create',
);

$this->menu=array(
	array('label'=>'Manage DrawResult', 'url'=>array('admin')),
);
?>

<h1>Create DrawResult</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>