<?php
/* @var $this DrawResultController */
/* @var $model DrawResult */

$this->breadcrumbs=array(
	'Draw Results'=>array('admin'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Create DrawResult', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('draw-result-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Draw Results</h1>

<div class="search-form">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'draw-result-grid',
	'dataProvider'=>$model->search(),
	'columns'=>array(
		'id',
		'code',
		'created_datetime',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
