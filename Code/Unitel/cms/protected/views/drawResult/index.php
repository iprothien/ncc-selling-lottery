<?php
/* @var $this DrawResultController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Draw Results',
);

$this->menu=array(
	array('label'=>'Create DrawResult', 'url'=>array('create')),
	array('label'=>'Manage DrawResult', 'url'=>array('admin')),
);
?>

<h1>Draw Results</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
