<?php
/* @var $this ChargingHistoryController */
/* @var $data ChargingHistory */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('msisdn')); ?>:</b>
	<?php echo CHtml::encode($data->msimdn); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('time_charge')); ?>:</b>
	<?php echo CHtml::encode($data->time_charge); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('money')); ?>:</b>
	<?php echo CHtml::encode($data->money); ?>
	<br />


</div>