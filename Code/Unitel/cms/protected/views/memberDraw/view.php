<?php
/* @var $this MemberDrawController */
/* @var $model MemberDraw */

$this->breadcrumbs=array(
	'Member Draws'=>array('admin'),
	$model->id,
);

$this->menu=array(
	//array('label'=>'Create MemberDraw', 'url'=>array('create')),
	//array('label'=>'Update MemberDraw', 'url'=>array('update', 'id'=>$model->id)),
	//array('label'=>'Delete MemberDraw', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage MemberDraw', 'url'=>array('admin')),
);
?>

<h1>View MemberDraw #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'msisdn',
		'code',
		'money',
		'created_datetime',
		'type',
		'status',
	),
)); ?>
