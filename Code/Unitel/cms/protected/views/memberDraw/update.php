<?php
/* @var $this MemberDrawController */
/* @var $model MemberDraw */

$this->breadcrumbs=array(
	'Member Draws'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List MemberDraw', 'url'=>array('index')),
	array('label'=>'Create MemberDraw', 'url'=>array('create')),
	array('label'=>'View MemberDraw', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage MemberDraw', 'url'=>array('admin')),
);
?>

<h1>Update MemberDraw <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>