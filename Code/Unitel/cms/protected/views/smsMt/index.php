<?php
/* @var $this SmsMtController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Sms Mts',
);

$this->menu=array(
	array('label'=>'Create SmsMt', 'url'=>array('create')),
	array('label'=>'Manage SmsMt', 'url'=>array('admin')),
);
?>

<h1>Sms Mts</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
