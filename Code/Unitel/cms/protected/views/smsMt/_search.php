<?php
/* @var $this SmsMtController */
/* @var $model SmsMt */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php
    Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
    $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row" style="width:50%;float:left">
		<?php echo $form->label($model,'msisdn'); ?>
		<?php echo $form->textField($model,'msisdn',array('size'=>16,'maxlength'=>16)); ?>
	</div>

	<div class="row" style="width:50%;float:left">
		<?php echo $form->label($model,'status'); ?>
		<?php echo $form->dropDownlist($model,'status',array(''=>'Select...',0=>'Pending',1=>'Completed'),array('style'=>'width:100px')); ?>
	</div>
    
        <div class="row" style="width:50%;float:left">
		<?php echo $form->label($model,'time_start'); ?>
		<?php 
                    $this->widget('CJuiDateTimePicker',array(
                    'model'=>$model, //Model object
                    'attribute'=>'time_start', //attribute name
                    'mode'=>'datetime', //use "time","date" or "datetime" (default)
                    'options'=>array("dateFormat"=>'yy/mm/dd'), // jquery plugin options
                    'language' => ''
            )); ?>
	</div>
    
        <div class="row" style="width:50%;float:left;">
		<?php echo $form->label($model,'time_end'); ?>
		<?php 
                    $this->widget('CJuiDateTimePicker',array(
                    'model'=>$model, //Model object
                    'attribute'=>'time_end', //attribute name
                    'mode'=>'datetime', //use "time","date" or "datetime" (default)
                    'options'=>array("dateFormat"=>'yy/mm/dd'), // jquery plugin options
                    'language' => ''
            )); ?>
	</div>
        
        <div style="clear:both;"></div>

	<div class="row buttons" style="width:25%;float:left">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->