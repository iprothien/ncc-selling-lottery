<?php
/* @var $this CdrController */
/* @var $data Cdr */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('start')); ?>:</b>
	<?php echo CHtml::encode($data->start); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('callerid')); ?>:</b>
	<?php echo CHtml::encode($data->callerid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('src')); ?>:</b>
	<?php echo CHtml::encode($data->src); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dst')); ?>:</b>
	<?php echo CHtml::encode($data->dst); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dcontext')); ?>:</b>
	<?php echo CHtml::encode($data->dcontext); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('channel')); ?>:</b>
	<?php echo CHtml::encode($data->channel); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('dstchannel')); ?>:</b>
	<?php echo CHtml::encode($data->dstchannel); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lastapp')); ?>:</b>
	<?php echo CHtml::encode($data->lastapp); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lastdata')); ?>:</b>
	<?php echo CHtml::encode($data->lastdata); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('duration')); ?>:</b>
	<?php echo CHtml::encode($data->duration); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('billsec')); ?>:</b>
	<?php echo CHtml::encode($data->billsec); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('disposition')); ?>:</b>
	<?php echo CHtml::encode($data->disposition); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('amaflags')); ?>:</b>
	<?php echo CHtml::encode($data->amaflags); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('accountcode')); ?>:</b>
	<?php echo CHtml::encode($data->accountcode); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('userfield')); ?>:</b>
	<?php echo CHtml::encode($data->userfield); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('uniqueid')); ?>:</b>
	<?php echo CHtml::encode($data->uniqueid); ?>
	<br />

	*/ ?>

</div>