<?php
/* @var $this CdrController */
/* @var $model Cdr */

$this->breadcrumbs=array(
	'Cdrs'=>array('admin'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Update Cdr', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Cdr', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Cdr', 'url'=>array('admin')),
);
?>

<h4>View Cdr #<?php echo $model->id; ?></h4>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'start',
		'callerid',
		'src',
		'dst',
		'dcontext',
		'channel',
		'dstchannel',
		'lastapp',
		'lastdata',
		'duration',
		'billsec',
		'disposition',
		'amaflags',
		'accountcode',
		'userfield',
		'uniqueid',
	),
)); ?>
