<?php
/* @var $this WinnerController */
/* @var $model Winner */

$this->breadcrumbs=array(
	'Winners'=>array('admin'),
	'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-form form').submit(function(){
	$('#winner-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Winners</h1>

<div class="search-form">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'winner-grid',
	'dataProvider'=>$model->search(),
	'columns'=>array(
		'id',
		'msisdn',
		'draw_no',
		'transaction_id',
		array(
			'class'=>'CButtonColumn',
                        'template'=>'{view}{delete}'
		),
	),
)); ?>
