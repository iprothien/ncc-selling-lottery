<?php
/* @var $this WinnerController */
/* @var $model Winner */

$this->breadcrumbs=array(
	'Winners'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Winner', 'url'=>array('index')),
	array('label'=>'Create Winner', 'url'=>array('create')),
	array('label'=>'View Winner', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Winner', 'url'=>array('admin')),
);
?>

<h1>Update Winner <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>