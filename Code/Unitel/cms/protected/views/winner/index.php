<?php
/* @var $this WinnerController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Winners',
);

$this->menu=array(
	array('label'=>'Create Winner', 'url'=>array('create')),
	array('label'=>'Manage Winner', 'url'=>array('admin')),
);
?>

<h1>Winners</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
