<?php
/* @var $this WinnerController */
/* @var $model Winner */

$this->breadcrumbs=array(
	'Winners'=>array('admin'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Delete Winner', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Winner', 'url'=>array('admin')),
);
?>

<h1>View Winner #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'msisdn',
		'buyer_id',
		'transaction_id',
		'inform_status',
		'created_datetime',
	),
)); ?>
