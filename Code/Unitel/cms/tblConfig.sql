-- MySQL dump 10.11
--
-- Host: localhost    Database: lotto
-- ------------------------------------------------------
-- Server version	5.0.95

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `config`
--

DROP TABLE IF EXISTS `config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `config` (
  `id` int(4) NOT NULL auto_increment,
  `key` varchar(255) collate utf8_unicode_ci NOT NULL,
  `value` varchar(255) collate utf8_unicode_ci NOT NULL,
  `note` varchar(255) collate utf8_unicode_ci NOT NULL,
  `convert_status` tinyint(2) NOT NULL,
  `time_start` datetime default NULL,
  `time_end` datetime default NULL,
  `group` tinyint(2) NOT NULL default '1',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `config`
--

LOCK TABLES `config` WRITE;
/*!40000 ALTER TABLE `config` DISABLE KEYS */;
INSERT INTO `config` VALUES (1,'Welcome','welcome.wav','Welcome to our mobile lottery services',1,NULL,NULL,1),(2,'Main Menu','main_menu.wav','Press 1 to buy lottery through IVR, Press 2 to buy lottery via our staff',1,NULL,NULL,1),(3,'Promotion','promotion.wav','Right Now we don’t have any promotion Pls. Check again later',1,NULL,NULL,1),(4,'Method to Buy','method.wav','To buy lottery through IVR call to 8899,then press 1, then enter the digit and amount that you want to buy, press 1 to confirm then wait for the confirmation message.',1,NULL,NULL,1),(5,'Continue Buy','continue_buy.wav','Press 1 to continue to buy, Press 2 to go back to main menu, Press 3 to end this call',1,NULL,NULL,1),(6,'Enter the number','number_buy.wav','Please enter the digit that you want to buy',1,NULL,NULL,1),(7,'Enter the amount','amount_buy.wav','Please enter the amount that you want to buy',1,NULL,NULL,1),(8,'Invalid','invalid.wav','Sorry the digit that you entered is invalid',1,NULL,NULL,1),(9,'You want to buy','youwant.wav','You want to buy the digits…(digit1,digit2,digit3)',1,NULL,NULL,1),(10,'With','with.wav','With the amount of…(amount in kip)',1,NULL,NULL,1),(11,'Confirm','confirm.wav','Press 1 to confirm.Press 2 if you want to buy and change the number.Press 3 to go back to the main menu',1,NULL,NULL,1),(14,'Not Enough','notenough.wav','Sorry, You do not have enough balance to buy the lottery, Please  refill your  balance and try again.',1,NULL,NULL,1),(15,'System close','close.wav','The current lot is now closed for drawing. The next lot will be open  immediately after this drawing.',1,NULL,NULL,1),(16,'Menu results','menu_result.wav','Press 1 to get the latest result, Press 2 to enter the date that you want to get the result for, Press 3 to go back to main menu',1,NULL,NULL,1),(20,'Input date','input_date.wav','Please input the  date that you want in the following format dd-mm-yyyy',1,NULL,NULL,1),(21,'We have no draw on this day','no_draw.wav','Sorry but we have no draw on the  date that you entered. Pls. try again',1,NULL,NULL,1),(22,'Send Result','result.wav','The result will be sent to you via sms after this call',1,NULL,NULL,1),(23,'Charge 1000kips','charge1000.wav','Sorry you did not purchase the lottery for that draw date, You will be charge 1000 kip. Press 1 to continue, Press 2 to cancel',1,NULL,NULL,1),(24,'Number Full Bought','full_bought.wav','Sorry, the number you are trying to buy has reach the maximum times of being bought. Please try another number instead',1,NULL,NULL,1),(25,'Post paid','postpaid.wav','Sorry, right now our system supports prepaid customer only',1,NULL,NULL,1),(26,'Cant charge','cant_charge.wav','Sorry, right now our system cannot deduct money from your account, pls. try again later',1,NULL,NULL,1),(57,'Invalid amount money','invalid_amount.wav','Sorry you enter an invalid amount, you can only by 1000 kip or higher and should be whole amount only',1,NULL,NULL,1),(58,'Not Press any key','not_press.wav','Sorry you did not press anything within 5 seconds, this call will now end. Pls.try again later, Thank You!',1,NULL,NULL,1),(60,'Invalid date','invalid_date.wav','You have enter an invalid format. Pls enter in the following format. Date: 2 digits, month: 2 digits and year: 4 digits',1,'2014-01-21 00:00:00','2014-03-31 00:00:00',1),(61,'Success amount','success_amount.wav','With the amount of…(amount in kip)',1,NULL,NULL,1),(62,'Success digits','success_digit.wav','You have successfully bought the digits xxx',1,NULL,NULL,1),(63,'Sub Menu','sub_menu.wav','Press 1 to buy 2 or 3 digit lottery, Press 2 to buy Millionaire\'s lottery',1,NULL,NULL,1),(64,'Millionaire','millionaire.wav','Sorry, this service is not yet available pls. try again later',1,NULL,NULL,1),(65,'Main Result','main_result.wav','Press 1 to get draw results, Press 2 to know the method to buy lottery, Press 3 to learn about promotions, Press 4 to connect to call center',1,NULL,NULL,1);
/*!40000 ALTER TABLE `config` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-04-28 16:34:20
