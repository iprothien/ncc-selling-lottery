<?php

function sayLaoCurrency($number,$agi){
    $path = "/var/www/html/lotto/uploads/outbond/digits/";
    $result = array();
    if($number==1000000){
        $agi->say_digits(1);
        $agi->stream_file($path."1000000");
    }else if($number>1000000){
        $result['1000000'] = intval($number/1000000);
        $number = $number-$result['1000000']*1000000;
        $result['100000'] = intval($number/100000);
        $result['1000'] = intval(($number-$result['100000']*100000)/1000);
        $remainNum = $number-$result['100000']*100000;
        $agi->say_digits($result['1000000']);
        $agi->stream_file($path."1000000");
        $agi->say_digits($result['100000']);
        $agi->stream_file($path."100000");
        if($remainNum) $agi->say_number($remainNum);
    }else if($number>100000&&$number<1000000){
        $result['100000'] = intval($number/100000);
        $result['1000'] = intval(($number-$result['100000']*100000)/1000);
        $remainNum = $number-$result['100000']*100000;
        $agi->say_digits($result['100000']);
        $agi->stream_file($path."100000");
        if($remainNum) $agi->say_number($remainNum);
    }else if($number==100000){
        $agi->say_digits(1);
        $agi->stream_file($path."100000");
    }else{
        $agi->say_number($number);
    }
}

function getFileName($name) {
    return substr($name, 0, strrpos($name, '.'));
}

function encodeDL($id,$type){
    return base64_encode($id."|".$type);
}

function decodeDL($code){
    $code = explode("|",base64_decode($code));
    return $code[0];
}

function viewInfo($msisdn){
    global $_vasgateway;
    $postVar = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:web="http://webservice.bccsgw.viettel.com/">
                    <soapenv:Header/>
                    <soapenv:Body>
                       <web:gwOperation>
                          <Input>
                            <username>'.$_vasgateway['username'].'</username>
                            <password>'.$_vasgateway['password'].'</password>
                            <wscode>viewinfonew</wscode>
                            <!--1 or more repetitions:-->
                            <param name="GWORDER" value="1"/>
                            <param name="msisdn" value="'.$msisdn.'"/>
                          </Input>
                       </web:gwOperation>
                    </soapenv:Body>
                 </soapenv:Envelope>
               ';
    //sfContext::getInstance()->getLogger()->err("Charge:SOAPXML:".$postVar);
    //open connection
    $ch = curl_init();
    //set the url, number of POST vars, POST data
    $options = array(
            CURLOPT_URL	       => $_vasgateway['address'],
            CURLOPT_RETURNTRANSFER => true,     // return web page
            CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
            CURLOPT_TIMEOUT        => 120,      // timeout on response
            CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects {$channelInfo['static_name']}
            CURLOPT_POST           => true,
            CURLOPT_POSTFIELDS     => $postVar,
            CURLOPT_HTTPHEADER	   => array('Content-Type: text/xml; charset=utf-8', 'Content-Length: '.strlen($postVar) ),
    );

    //open connection
    $ch = curl_init();
    curl_setopt_array( $ch, $options );
    $err     = curl_errno( $ch );
    $errmsg  = curl_error( $ch );
    $header  = curl_getinfo( $ch );
    $responeContent = curl_exec($ch);
    //var_dump($responeContent);
    $result = array();
    if($err || !$responeContent)
    {
        $result[0] = 201;
    }
    else
    {
        //$responeContent = '77|The isdn does not exist';
        $subStr = substr($responeContent, strpos($responeContent, '<return>')+8);
        $responeContent = substr($subStr, 0, strpos($subStr, '</return>'));
        $result = explode('|', $responeContent);
    }
    return $result;
}


function charge($msisdn,$money,$shortcode,$feeType,$subType=1){
    global $_vasgateway;
    //$money = "100";
    $postVar = '
                    <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:web="http://webservice.bccsgw.viettel.com/">
                        <soapenv:Header/>
                        <soapenv:Body>
                           <web:gwOperation>
                              <Input>
                                <username>'.$_vasgateway['username'].'</username>
                                <password>'.$_vasgateway['password'].'</password>
                                <wscode>chargenew</wscode>
                                <!--1 or more repetitions:-->
                                <param name="GWORDER" value="1"/>
                                <param name="input" value="'.$msisdn.'|'.$shortcode.'|'.$feeType.'|'.$subType.'|'.$money.'|0"/>
                              </Input>
                           </web:gwOperation>
                        </soapenv:Body>
                     </soapenv:Envelope>
               ';
    //sfContext::getInstance()->getLogger()->err("Charge:SOAPXML:".$postVar);
    //open connection
    $ch = curl_init();
    //set the url, number of POST vars, POST data
    $options = array(
            CURLOPT_URL	       => $_vasgateway['address'],
            CURLOPT_RETURNTRANSFER => true,     // return web page
            CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
            CURLOPT_TIMEOUT        => 120,      // timeout on response
            CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects {$channelInfo['static_name']}
            CURLOPT_POST           => true,
            CURLOPT_POSTFIELDS     => $postVar,
            CURLOPT_HTTPHEADER	   => array('Content-Type: text/xml; charset=utf-8', 'Content-Length: '.strlen($postVar) ),
    );

    //open connection
    $ch = curl_init();
    curl_setopt_array( $ch, $options );
    $err     = curl_errno( $ch );
    $errmsg  = curl_error( $ch );
    $header  = curl_getinfo( $ch );
    $responeContent = curl_exec($ch);
    //var_dump($responeContent);
    $result = 0;
    if($err || !$responeContent)
    {
        $result[0] = 201;
    }
    else
    {
        //$responeContent = '77|The isdn does not exist';
        $subStr = substr($responeContent, strpos($responeContent, '<return>')+8);
        $responeContent = substr($subStr, 0, strpos($subStr, '</return>'));
        $result = explode('|',$responeContent);
    }
    return $result;
}

function addOrder($MobileNo, $digits, $amount) {
    try {
        $soapClient = new SoapClient('http://ncclaos.net:8208/lldserviceIVR.asmx?WSDL', array('trace' => true));
    } catch (Exception $e) {
        var_dump($e);
    }

    $QueryUserEvt->PassKey = 'mo246d0262c3925617b0c72bb20aac2d';
    $QueryUserEvt->AccountNo = '1448';
    $QueryUserEvt->MobileNo = $MobileNo;
    $QueryUserEvt->mDigit = $digits;
    $QueryUserEvt->mAmount = $amount;
    $QueryUserEvt->isFirstRecord = true;

    try {
        $resutl = $soapClient->AddOrder($QueryUserEvt);
        //var_dump($resutl);
        if(@$resutl->AddOrderResult) return $resutl->AddOrderResult;
        else return 0;
    } catch (Exception $e) {
        //var_dump($e);
    }
    return $resutl;
}

function SubmitBuying($MobileNo, $TotalAmount, $PaymentNo) {
    try {
        $soapClient = new SoapClient('http://ncclaos.net:8208/lldserviceIVR.asmx?WSDL', array('trace' => true));
    } catch (Exception $e) {
        var_dump($e);
    }

    $submitOrder->PassKey = 'mo246d0262c3925617b0c72bb20aac2d';
    $submitOrder->ProviderID = '7';
    $submitOrder->MobileNo = $MobileNo;
    $submitOrder->PaymentNo = $PaymentNo;
    $submitOrder->TotalAmount = $TotalAmount;
    $submitOrder->isSuccess = true;

    try {
        $resut = $soapClient->SubmitBuying($submitOrder);
        //var_dump($resut);
    } catch (Exception $e) {
        //var_dump($e);
    }
    return $resut;
}

function convertDurationToMoney($duration){
    $block = 1;
    return round(ceil($duration/$block) * 800/60);
}

function convertMoneyToDuration($money){
   $block = 1;
   return round((ceil($money/(800/60))-1)*$block);
}


function shortContent($str,$lenght){
    return substr($str,0,strripos(substr($str,0,$lenght)," "));
}



function explodeContent($content){
    $resutls = array();
    $contents = explode("#",$content);
    foreach($contents as $content){
        if(strlen($content)<=140){
            $results[] = $content;
        }else{
            while(1){
               $shortContent = shortContent($content,140);
               $results[] = $shortContent;
               $content = substr($content,strlen($shortContent)+1);
               if(strlen($content)<140){
                   $results[] = $content;
                   break;
               }
            }
        }
    }
    return $results;
}


function microtime_float()
{
    list($usec, $sec) = explode(" ", microtime());
    return ((float)$usec + (float)$sec);
}

function sendSMS($sender,$receiver,$content){
    $postVar = "";
    //$_url_send_sms .= "&from={$data['service']}&to=855{$data['receiver']}&text={$data['content']}";
    $msisdn = (int)$data['msisdn'];
    $_url_send_sms = "http://localhost:13002/sendsms?username=thiendv&password=thiendv&from={$sender}&to=$receiver&text={$content}";
    //$url = str_replace( "&amp;", "&", urldecode(trim($url)) );
    //$url = str_replace( " ", "&amp;", urldecode(trim($url)) );
    //$url = urlencode($url);
    $_url_send_sms = str_replace(" ","%20",$_url_send_sms);
    $cookie = tempnam ("/tmp", "CURLCOOKIE");
    $ch = curl_init();
    curl_setopt( $ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; rv:1.7.3) Gecko/20041001 Firefox/0.10.1" );
    curl_setopt( $ch, CURLOPT_URL, $_url_send_sms );
    curl_setopt( $ch, CURLOPT_COOKIEJAR, $cookie );
    curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, true );
    curl_setopt( $ch, CURLOPT_ENCODING, "" );
    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
    curl_setopt( $ch, CURLOPT_AUTOREFERER, true );
    curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );    # required for https urls
    curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT, $timeout );
    curl_setopt( $ch, CURLOPT_TIMEOUT, $timeout );
    curl_setopt( $ch, CURLOPT_MAXREDIRS, 10 );
    $content = curl_exec( $ch );
    $response = curl_getinfo( $ch );
    curl_close ( $ch );
    $responArr = explode(":",$content);
    if(!$responArr[0]&&$responArr[1]){
        $status = 1;
    }else{
        $status = $responArr[0];
    }
    if(!$status) $status=99;

    return $status;

}



function delRBT($msisdn,$rbtId,$rbtCode){
    //View inform
    try{
       $soapClient = new SoapClient( 'http://10.78.10.29:8090/mc/ToneProvide_laos_Unitel.wsdl', array('trace' => true));
    }catch(Exception $e){
       file_put_contents("error_log.txt",$e."|".date("Y-m-d H:i:s",time())."\n",FILE_APPEND);
       return false;
    }

    //OPEN TONE
    $QueryUserEvt->portalAccount = 'gw';
    $QueryUserEvt->portalPwd = 'gw';
    $QueryUserEvt->portalType = 0;
    $QueryUserEvt->role = 3;
    $QueryUserEvt->roleCode = 0;
    $QueryUserEvt->type = 1;
    $QueryUserEvt->resourceID = $rbtId;
    $QueryUserEvt->operateType = 2;
    try{
        $resutl = $soapClient->hiddenAndResumeTone($QueryUserEvt);
        $rbtStatus=true;
    }catch(Exception $e){
        $rbtStatus = false;
    }

    if($rbtStatus){
        try{
           $userToneClient = new SoapClient( 'http://10.78.10.29:8090/mc/UserToneManage_laos_Unitel.wsdl', array('trace' => true));
        }catch(Exception $e){
           file_put_contents("error_log.txt",$e."|".date("Y-m-d H:i:s",time())."\n",FILE_APPEND);
           return false;
        }
        //FAKE SMS
        $sendSMSstatus = sendSMS("856{$msisdn}","119","D {$rbtCode}");
        if($sendSMSstatus==1){
            //OK
            $returnValue = true;
            sleep(2);
        }else{
            //NOT OK
            $returnValue = false ;
        }


        //HIDDEN TONE
        try{
           $soapClient = new SoapClient( 'http://10.78.10.29:8090/mc/ToneProvide_laos_Unitel.wsdl', array('trace' => true));
        }catch(Exception $e){
           file_put_contents("error_log.txt",$e."|".date("Y-m-d H:i:s",time())."\n",FILE_APPEND);
           return false;
        }
        $QueryUserEvt = null;
        $QueryUserEvt->portalAccount = 'gw';
        $QueryUserEvt->portalPwd = 'gw';
        $QueryUserEvt->portalType = 0;
        $QueryUserEvt->role = 3;
        $QueryUserEvt->roleCode = 0;
        $QueryUserEvt->type = 1;
        $QueryUserEvt->phoneNumber = $msisdn;
        $QueryUserEvt->resourceID = $rbtId;
        $QueryUserEvt->operateType = 1;
        try{
            $resutl = $soapClient->hiddenAndResumeTone($QueryUserEvt);
            if($resutl->returnCode=='000000') $rbtStatus=true;
            else $rbtStatus = false;
        }catch(Exception $e){
            $rbtStatus = false;
        }

        if(!$rbtStatus){
            $returnValue = 99;
        }
        return $returnValue;
    }else{
        //FAIL
        return false;
    }
}


function checkRegisterCRBT($msisdn){
    $subInfo = viewInfo($msisdn);
    if($subInfo[0]==22){
        $subType = 0;
    }else{
        $subType = 1;
    }
    try{
       $soapClient = new SoapClient( 'http://10.78.10.28:8090/mc/UserManage_Laos_Unitel.wsdl', array('trace' => true));
    }catch(Exception $e){
       file_put_contents("error_log.txt",$e."|".date("Y-m-d H:i:s",time())."\n",FILE_APPEND);
       return false;
    }
    $QueryUserEvt->portalAccount = 'gw';
    $QueryUserEvt->portalPwd = 'gw';
    $QueryUserEvt->portalType = 0;
    $QueryUserEvt->role = 3;
    $QueryUserEvt->roleCode = 0;
    $QueryUserEvt->type = $subType;
    $QueryUserEvt->phoneNumber = $msisdn;
    try{
        $resutl = $soapClient->query($QueryUserEvt);
        if($resutl->returnCode=='000000'){
            $users = $resutl->userInfos;
            foreach($users as $user){
                if($user->phoneNumber==$msisdn){
                    return true;
                }
            }
            return false;
        }else{
            return false;
        }
    }catch(Exception $e){
        return true;
    }
    return true;
}

function registerRBT($msisdn){
    try{
       $soapClient = new SoapClient( 'http://10.78.10.29:8090/mc/UserManage_Laos_Unitel.wsdl', array('trace' => true));
    }catch(Exception $e){
       file_put_contents("error_log.txt",$e."|".date("Y-m-d H:i:s",time())."\n",FILE_APPEND);
       return false;
    }
    $QueryUserEvt->portalAccount = 'gw';
    $QueryUserEvt->portalPwd = 'gw';
    $QueryUserEvt->portalType = 0;
    $QueryUserEvt->role = 3;
    $QueryUserEvt->roleCode = 0;
    $QueryUserEvt->type = 1;
    $QueryUserEvt->phoneNumber = $msisdn;
    try{
        $resutl = $soapClient->subscribe($QueryUserEvt);
        $returnValue = $resutl->returnCode;
        if($returnValue=='000000'){
            $returnValue=true;
        }else{
            return checkRegisterCRBT($msisdn);
        }
    }catch(Exception $e){
        $returnValue = false;
    }
    return $returnValue;
}


function downloadRBT($msisdn,$rbtId,$rbtCode){
    //View inform
    try{
       $soapClient = new SoapClient( 'http://10.78.10.29:8090/mc/ToneProvide_laos_Unitel.wsdl', array('trace' => true));
    }catch(Exception $e){
       file_put_contents("error_log.txt",$e."|".date("Y-m-d H:i:s",time())."\n",FILE_APPEND);
       return false;
    }

    //OPEN TONE
    $QueryUserEvt->portalAccount = 'gw';
    $QueryUserEvt->portalPwd = 'gw';
    $QueryUserEvt->portalType = 0;
    $QueryUserEvt->role = 3;
    $QueryUserEvt->roleCode = 0;
    $QueryUserEvt->type = 1;
    $QueryUserEvt->resourceID = $rbtId;
    $QueryUserEvt->operateType = 2;
    try{
        $resutl = $soapClient->hiddenAndResumeTone($QueryUserEvt);
        $rbtStatus=true;
    }catch(Exception $e){
        $rbtStatus = false;
    }

    if($rbtStatus){
        try{
           $userToneClient = new SoapClient( 'http://10.78.10.29:8090/mc/UserToneManage_laos_Unitel.wsdl', array('trace' => true));
        }catch(Exception $e){
           file_put_contents("error_log.txt",$e."|".date("Y-m-d H:i:s",time())."\n",FILE_APPEND);
           return false;
        }

        /*
        //DOWNLOAD TONE
        $QueryUserEvt = null;
        $QueryUserEvt->portalAccount = 'gw';
        $QueryUserEvt->portalPwd = 'gw';
        $QueryUserEvt->portalType = 0;
        $QueryUserEvt->role = 3;
        $QueryUserEvt->roleCode = 0;
        $QueryUserEvt->type = 1;
        $QueryUserEvt->resourceType=1;
        $QueryUserEvt->phoneNumber=$msisdn;
        $QueryUserEvt->resourceCode = $rbtCode;
        try{
            $resutl = $userToneClient->orderTone($QueryUserEvt);
            if($resutl->returnCode=='000000'||$resutl->returnCode=='302011') $downloadStatus=true;
            else $downloadStatus = false;
        }catch(Exception $e){
            $downloadStatus = false;
        }

        if($downloadStatus){
            //OKIE
            $returnValue = true;
        }else{
            //FAILSE
            $returnValue=false;
        }
         *
         */

        //FAKE SMS
        $sendSMSstatus = sendSMS("856{$msisdn}","119","O {$rbtCode}");
        if($sendSMSstatus==1){
            //OK
            $returnValue = true;
            sleep(2);
        }else{
            //NOT OK
            $returnValue = false ;
        }


        //HIDDEN TONE
        try{
           $soapClient = new SoapClient( 'http://10.78.10.29:8090/mc/ToneProvide_laos_Unitel.wsdl', array('trace' => true));
        }catch(Exception $e){
           file_put_contents("error_log.txt",$e."|".date("Y-m-d H:i:s",time())."\n",FILE_APPEND);
           return false;
        }
        $QueryUserEvt = null;
        $QueryUserEvt->portalAccount = 'gw';
        $QueryUserEvt->portalPwd = 'gw';
        $QueryUserEvt->portalType = 0;
        $QueryUserEvt->role = 3;
        $QueryUserEvt->roleCode = 0;
        $QueryUserEvt->type = 1;
        $QueryUserEvt->phoneNumber = $msisdn;
        $QueryUserEvt->resourceID = $rbtId;
        $QueryUserEvt->operateType = 1;
        try{
            $resutl = $soapClient->hiddenAndResumeTone($QueryUserEvt);
            if($resutl->returnCode=='000000') $rbtStatus=true;
            else $rbtStatus = false;
        }catch(Exception $e){
            $rbtStatus = false;
        }

        if(!$rbtStatus){
            $returnValue = 99;
        }
        return $returnValue;
    }else{
        //FAIL
        return false;
    }
}

?>
