#!/usr/bin/php -q
<?php
set_time_limit(30);
require('lib/phpagi.php');
require('lib/sbMysqlPDO.class.php');
require('lib/sbMssql.class.php');
require('lib/functions.php');
require('lib/config.php');
$agi = new AGI();
$conn = new sbMysqlPDO($server, $user, $password, $db);

$caller = $agi->get_variable("CALLERID(num)");
$caller = $caller['data'];

$keyConfirm = $agi->get_variable("keyConfirm");
$keyConfirm = $keyConfirm['data'];


$code = $agi->get_variable("code");
$code = $code['data'];

$money = $agi->get_variable("money");
$money = $money['data'];

switch ($keyConfirm) {
    case 1:
	 //Check Time Avaiable
        $sqlgetTime = "SELECT * FROM draw_time WHERE NOW() > start_time && NOW() < end_time && status=1";
        $getTime = $conn->doSelectOne($sqlgetTime);
        
        if(!$getTime) {
            //Invalid Time
            $agi->stream_file($_system_path['close']);
            $agi->exec_goto('lotto-menu',$dial,1);
            exit;
        }
	
        //Check Balance
        $CheckBalence = TRUE;
        $Time_Buy = date('Y-m-d H:i:s', time());
        if (strlen($code) > 2)
            $Type = 2;
        else
            $Type = 1;
        $info = viewInfo($caller);
        if ($info[0]!=22) {
            $info = viewInfo($caller);
            if (intval($info[9]) >= $money) {
                $rsAddOrder = addOrder($caller, $code, $money);
                //$agi->say_digits($rsAddOrder);
                if ($rsAddOrder == $money){
                    //$result = charge($caller, $money);
                    $result = charge($caller,$money,$_shortcode,$_feeType,$subType=1);
                }else {
                    $agi->stream_file($_system_path['full_bought']);
                    $agi->exec_goto('lotto-buy', $dial, 1);
                    $result = false;
                    exit;
                }

                //$result = 1;
                 if(!$result[0]) {
                    SubmitBuying($caller, $money, $code);
                    //Insert Into member_draw
                    $status = 1;
                    $sqlInsert = "INSERT INTO member_draw(
                                                           msisdn,code,money,created_datetime,type,status,draw_id 
                                               ) VALUES(
                                                            '{$caller}','{$code}','{$money}','{$Time_Buy}','{$Type}', '{$status}', '{$getTime['id']}'
                                               )";
                    $memberId = $conn->doUpdate($sqlInsert);
                    if ($memberId) {
                        $sqlInsertCharging = " 
                                    INSERT INTO charging_history(msisdn,time_charge,money,member_id,digit)
                                    VALUES ('{$caller}',now(),'{$money}','{$memberId}','{$code}')
                                 ";
                        $conn->doUpdate($sqlInsertCharging);

                        $smsInform = "You successfuly bought lottery with the following information:"."\n"
                                     ."Draw No: ".$getTime['draw_no']."\n"
                                     ."Draw Date " . date('d/m/Y',strtotime($getTime['end_time'])) . "\n"
                                     . "Digit: " . $code . "\n"
                                     . "Amount: " . $money." KIP";
                        
                        $sqlInsertSMS = "
                                INSERT INTO sms_mt(short_code,msisdn,status,content,created_datetime)
                                VALUES('8899','{$caller}','0','{$smsInform}',now())
                            ";
                        $conn->doUpdate($sqlInsertSMS);
                        $agi->stream_file($_system_path['success_digit']);
                        $agi->say_digits($code);
                        $agi->stream_file($_system_path['success_amount']);
                        $agi->say_number($money);
			   $agi->stream_file($_system_path['kip']);

                        $i = 1;
                        while (1) {
                            if ($i++ > 3) {
                                $agi->stream_file($_system_path['not_press']);
                                exit;
                            }
                            $dtConfirm = $agi->get_data($_system_path['continue_buy'], $max_timeout, 1);
                            $keyConfirm = $dtConfirm['result'];
                            switch ($keyConfirm) {
                                case "1":
                                    $agi->exec_goto('lotto-buy', $dial, 1);
                                    exit;
                                    break;
                                case "2":
                                    $agi->exec_goto('lotto-menu', $dial, 1);
                                    exit;
                                    break;
                            }
                        }
                    }
                } else {
                    $status = 3;
                    $sqlInsert = "INSERT INTO member_draw(
                                                           msisdn,code,money,created_datetime,type,status 
                                               ) VALUES(
                                                            '{$caller}','{$code}','{$money}','{$Time_Buy}','{$Type}', '{$status}'
                                 )";
                    $conn->doUpdate($sqlInsert);
                    $agi->stream_file($_system_path['cant_charge']);
                    $agi->exec_goto('lotto-buy', $dial, 1);
                }
            } else {
                $agi->stream_file($_system_path['notenough']);
                $agi->exec_goto('lotto-buy', $dial, 1);
                exit;
            }
        } else {
            $agi->stream_file($_system_path['postpaid']);
            $agi->exec_goto('lotto-menu', $dial, 1);
            exit;
        }
        break;
    case 2:
        $agi->exec_goto('lotto-buy', $dial, 1);
        exit;
        break;
    case 3:
        $agi->exec_goto("lotto-menu", $dial, 1);
        exit;
        break;
}
?>