<?php
ob_start();
error_reporting(false);
require('lib/config.php');
require('lib/functions.php');
require('lib/sbMysqlPDO.class.php');
require('lib/sbMssql.class.php');
$conn = new sbMysqlPDO($server, $user, $password, $db);

$return = array();
$ip = $_SERVER['REMOTE_ADDR'];
$transaction_id = time();

$logInfo = $ip."|".date("Y-m-d H:i:s",time());
foreach($_REQUEST as $key=>$value){
    $logInfo .= "|$key<>$value";
}
$logInfo .= "\n";
file_put_contents("data/".date("Y_m_d").".log",$logInfo,FILE_APPEND);

if ($ip == '103.240.242.59' || $ip == '27.118.30.2') {
    if (isset($_GET['msisdn']) && $_GET['msisdn'])
        $msisdn = $_GET['msisdn'];

    if (isset($_GET['code']) && $_GET['code'])
        $listcode = $_GET['code'];

    if (isset($_GET['money']) && $_GET['money'])
        $listmoney = $_GET['money'];

    if (isset($_GET['draw_id']) && $_GET['draw_id'])
        $draw_id = $_GET['draw_id'];

    if (isset($_GET['agent_id']) && $_GET['agent_id'])
        $agent_id = $_GET['agent_id'];

    if ($msisdn && $listcode && $listmoney && $draw_id && $agent_id) {

        $code = explode(':', $listcode);
        $money = explode(':', $listmoney);

	// edit
	if($msisdn == '2095743524'){
       	$money = 1;
       }

        $i = 0;
        $count = count($code);
        $msgMt = array();
        for ($i; $i < $count; $i++) {
            $memberId = 0;
            $submitResultContent = '';

            $sqlgetTime = "SELECT * FROM draw_time WHERE ref_id=" . $draw_id;
            $getTime = $conn->doSelectOne($sqlgetTime);

            if (!$getTime) {
                //Invalid Time
                $status = 2;
                $statusError[] = $code[$i] . ':' . $money[$i] . ':' . $status;
            } else {
                $info = viewInfo($msisdn);
                if ($info[0] != 22) {
                    if ($info[9] >= $money[$i]) {
                        $rsAddOrder = addOrder($msisdn, $code[$i], $money[$i]);

                        if ($rsAddOrder == $money[$i]) {
                            
                            $timeRef = time();
                            $contentLog = date("Y-m-d H:i:s",time())."|$timeRef|$msisdn|{$money[$i]}|{$code[$i]}\n";
                            file_put_contents("data/".date("Y-m-d")."_before_charge.log",$contentLog,FILE_APPEND);                      
                            $result = charge($msisdn, $money[$i], $_shortcode, $_feeType, $subType = 1);
                            
                            
                            //LOG AFTER
                            $contentLog = date("Y-m-d H:i:s",time())."|$timeRef|$msisdn|{$money[$i]}|{$code[$i]}|{$result[0]}|{$result[1]}\n";
                            file_put_contents("data/".date("Y-m-d")."_charge.log",$contentLog,FILE_APPEND);
                            
                            
                            $logCharging = date('Y-m-d H:i:s',time())."|".$msisdn."|".$money[$i]."|".$_shortcode."|".$_feeType."|".$subType;
                            foreach($result as $key=>$value){
                                $logCharging .= "|$key<>$value";
                            }
                            $logCharging .= "\n";
                            file_put_contents("data/".date("Y_m_d")."_charging.log",$logCharging,FILE_APPEND);				

                            if ("{$result[0]}"=="0") {
                                $sqlInsertMember = 'INSERT INTO member_draw(msisdn,code,money,created_datetime,type,status,draw_no,draw_id,buy_type,agent_id,transaction_id)
                                            VALUES ("' . $msisdn . '",
                                                    "' . $code[$i] . '",
                                                    "' . $money[$i] . '",
                                                    now(),
                                                    "' . (strlen($code[$i]) - 1) . '",
                                                    "1",
                                                    "' . $getTime['draw_no'] . '",
                                                    "' . $getTime['id'] . '",
                                                    "1",
                                                    "' . $agent_id . '", 
							   "' . $transaction_id . '"
                                                    )';

                                $memberId = $conn->doUpdate($sqlInsertMember);
                                if ($memberId) {
                                    $submitResult = SubmitBuying($msisdn, $money[$i], $code[$i]);
                                    if (!substr_count(@$submitResult->SubmitBuyingResult, "Successfull-IVR")) {
                                        //SUBMIT NOT OK
                                        $status = 1;
                                        $statusError[] = $code[$i] . ':' . $money[$i] . ':' . $status;
                                    } else {
                                        //TRU TIEN VA MUA THANH CONG
                                        $status = 1;
                                        $statusError[] = $code[$i] . ':' . $money[$i] . ':' . $status;
                                    }
                                    $submitResultContent = htmlentities(@$submitResult->SubmitBuyingResult, ENT_QUOTES);

                                    //InsertCharging
                                    $sqlInsertCharging = " 
                                                INSERT INTO charging_history(msisdn,time_charge,money,member_id,digit,trans_id,trans_date,draw_no)
                                                VALUES ('{$msisdn}',now(),'{$money[$i]}','{$memberId}','{$code[$i]}','{$result['trans_id']}','{$result['trans_date']}','{$getTime['draw_no']}')
                                             ";
                                    $conn->doUpdate($sqlInsertCharging);

                                    $msgMt[] = 'Digit: ' . $code[$i] . ', Amount: ' . $money[$i] . " KIP";
                                }
                            } else {
                                //TRU TIEN THAT BAI
                                $status = 6;
                                $statusError[] = $code[$i] . ':' . $money[$i] . ':' . $status;
                            }
                        } else {
                            //FULL BOUGHT
                            $status = 5;
                            $statusError[] = $code[$i] . ':' . $money[$i] . ':' . $status;
                        }
                    } else {
                        //Not Enough
                        $status = 4;
                        $statusError[] = $code[$i] . ':' . $money[$i] . ':' . $status;
                    }
                } else {
                    //POSTPAID
                    $status = 3;
                    $statusError[] = $code[$i] . ':' . $money[$i] . ':' . $status;
                }
            }

            $inforAfter = viewInfo($msisdn);
            $tBalance[] = $info[9];
            $sBalance[] = $inforAfter[9];
            $submitContent[] = $submitResultContent;

            if (isset($status) && $status) {
                $sqlInsertTrans = 'INSERT INTO transaction_log
                                    (
                                    msisdn,code,money,created_datetime,type,status,draw_id,buy_type,agent_id,content,balance_before,balance_after,member_id
                                    ) 
                                    VALUES ("' . $msisdn . '",
                                            "' . $code[$i] . '",
                                            "' . $money[$i] . '",
                                            "' . date('Y-m-d H:i:s', time()) . '",
                                            "' . (strlen($code[$i]) - 1) . '",
                                            "' . $status . '",
                                            "' . $getTime['id'] . '",
                                            "1",
                                            "' . $agent_id . '",
                                            "' . $submitResultContent . '",
                                            "' . $info[9] . '",
                                            "' . $inforAfter[9] . '",
                                            "' . $memberId . '"
                                            )';

                $conn->doUpdate($sqlInsertTrans);
            }
        }

        /* INSERT INTO SMS_MT MULTI LOTTO  */
        if (isset($msgMt) && count($msgMt) > 0) {
            $smsInform = "You successfuly bought lottery with the following information:" . "\n"
                . "Draw No: " . $getTime['draw_no'] . "\n"
                . "Draw Date " . date('d/m/Y', strtotime($getTime['end_time'])) . "\n"
                . "Timing buy: " . date('H:i d/m/Y', time()) . "\n"
                . implode("\n", $msgMt);
            $totalSMS = ceil(strlen($smsInform) / 160);
            $sqlInsertSMS = "
                            INSERT INTO sms_mt(short_code,msisdn,status,content,created_datetime,total_sms,draw_no, transaction_id)
                            VALUES('8899','{$msisdn}','0','{$smsInform}',now(),'{$totalSMS}','{$getTime['draw_no']}','{$transaction_id}')
                        ";
            $conn->doUpdate($sqlInsertSMS);
        }

        if (isset($statusError) && $statusError)
            $statusReturn = implode(',', $statusError);
        $return[] = array('status' => $statusReturn, 'content' => implode('|', $submitContent), 'balance_before' => implode(',', $tBalance), 'balance_after' => implode(',', $sBalance));
        ob_clean();
        echo json_encode($return);
    }
}
?>