<?php
/* @var $this MemberDrawController */
/* @var $model MemberDraw */

$this->breadcrumbs=array(
	'Member Draws'=>array('admin'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Create MemberDraw', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('member-draw-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Member Draws</h1>

<div class="search-form">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'member-draw-grid',
	'dataProvider'=>$model->search(),
	'columns'=>array(
		'id',
		'msisdn',
		'code',
		'money',
		'created_datetime',
                array('name'=>'type','value'=>'$data->getType()'),
		/*
		'status',
		*/
		array(
			'class'=>'CButtonColumn',
                        'template'=>'{view}'
		),
	),
)); ?>
