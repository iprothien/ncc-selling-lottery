<?php
/* @var $this DrawTimeController */
/* @var $model DrawTime */
/* @var $form CActiveForm */
?>

<div class="form">

<?php
    Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
    $form=$this->beginWidget('CActiveForm', array(
	'id'=>'draw-time-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'start_time'); ?>
		<?php 
                    $this->widget('CJuiDateTimePicker',array(
                    'model'=>$model, //Model object
                    'attribute'=>'start_time', //attribute name
                    'mode'=>'datetime', //use "time","date" or "datetime" (default)
                    'options'=>array("dateFormat"=>'yy/mm/dd'), // jquery plugin options
                    'language' => ''
            )); ?>
		<?php echo $form->error($model,'start_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'end_time'); ?>
		<?php 
                    $this->widget('CJuiDateTimePicker',array(
                    'model'=>$model, //Model object
                    'attribute'=>'end_time', //attribute name
                    'mode'=>'datetime', //use "time","date" or "datetime" (default)
                    'options'=>array("dateFormat"=>'yy/mm/dd'), // jquery plugin options
                    'language' => ''
            )); ?>
		<?php echo $form->error($model,'end_time'); ?>
	</div>
        <?php if(!$model->isNewRecord): ?>
        <div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->dropDownlist($model,'status',array(''=>'Select...',0=>'Disable',1=>'Enable')); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>
        <?php endif; ?>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->