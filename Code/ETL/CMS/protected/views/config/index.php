<?php
/* @var $this ConfigController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Configs',
);

$this->menu=array(
	array('label'=>'Manage Config', 'url'=>array('admin')),
);
?>

<h4>Configs</h4>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
