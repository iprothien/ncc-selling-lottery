<?php
/* @var $this ConfigController */
/* @var $model Config */
/* @var $form CActiveForm */
?>

<div class="form">

<?php
    Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
    $form=$this->beginWidget('CActiveForm', array(
	'id'=>'config-form',
        'htmlOptions'=>array('enctype'=>'multipart/form-data'),
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>
        
        <?php if($model->group==1) { ?>
	<div class="row">
		<?php echo $form->labelEx($model,'key'); ?>
		<?php echo $form->textField($model,'key',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'key'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'value'); ?>
		<?php echo $form->fileField($model,'value',array('size'=>60,'maxlength'=>255)); echo '<span class="file_exits">'.$model->value.'</span>'; ?>
                <?php echo $form->hiddenField($model,'value',array('type'=>'hidden','size'=>2,'name'=>'old_value')); ?>
		<?php echo $form->error($model,'value'); ?>
	</div>
        <?php } ?>
        
        <?php if($model->group==2) { ?>
        <div class="row">
		<?php echo $form->labelEx($model,'time_start'); ?>
		<?php 
                    $this->widget('CJuiDateTimePicker',array(
                    'model'=>$model, //Model object
                    'attribute'=>'time_start', //attribute name
                    'mode'=>'datetime', //use "time","date" or "datetime" (default)
                    'options'=>array("dateFormat"=>'yy/mm/dd'), // jquery plugin options
                    'language' => ''
            )); ?>
		<?php echo $form->error($model,'time_start'); ?>
	</div>
        
        <div class="row">
		<?php echo $form->labelEx($model,'time_end'); ?>
		<?php 
                    $this->widget('CJuiDateTimePicker',array(
                    'model'=>$model, //Model object
                    'attribute'=>'time_end', //attribute name
                    'mode'=>'datetime', //use "time","date" or "datetime" (default)
                    'options'=>array("dateFormat"=>'yy/mm/dd'), // jquery plugin options
                    'language' => ''
            )); ?>
		<?php echo $form->error($model,'time_end'); ?>
	</div>
        <?php } ?>
        
        <?php if($model->group==1) { ?>
        <div class="row">
		<?php echo $form->labelEx($model,'note'); ?>
		<?php echo $form->textField($model,'note',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'note'); ?>
	</div>
        <?php } ?>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->