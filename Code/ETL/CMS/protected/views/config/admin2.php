<?php
/* @var $this ConfigController */
/* @var $model Config */

$this->breadcrumbs=array(
	'Configs'=>array('admin'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Audio Config', 'url'=>array('admin', 'group' => 1)),
        array('label'=>'Time to Draw', 'url'=>array('admin2','group' => 2)),
);


Yii::app()->clientScript->registerScript('search', "
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('config-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Configs</h1>

<?php
    $group  = intval($_GET['group']);
    $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'config-grid',
	'dataProvider'=>$model->search($group),
	'columns'=>array(
		'id',
		'key',
                'time_start',
                'time_end',
                //'note',
		array(
			'class'=>'CButtonColumn',
			'template' => '{update}'
		),
	),
)); ?>