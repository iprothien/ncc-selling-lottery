<?php
/* @var $this ConfigController */
/* @var $model Config */

$this->breadcrumbs=array(
	'Configs'=>array('admin'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Update Config', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Audio Config', 'url'=>array('admin', 'group' => 1)),
        array('label'=>'Time to Draw', 'url'=>array('admin2','group' => 2)),
);
?>

<h4>View Config #<?php echo $model->id; ?></h4>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'key',
		'value',
                'note',
                'time_start',
                'time_end',
		'convert_status',
	),
)); ?>
