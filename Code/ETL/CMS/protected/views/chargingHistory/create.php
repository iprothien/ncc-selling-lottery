<?php
/* @var $this ChargingHistoryController */
/* @var $model ChargingHistory */

$this->breadcrumbs=array(
	'Charging Histories'=>array('admin'),
	'Create',
);

$this->menu=array(
	array('label'=>'Manage ChargingHistory', 'url'=>array('admin')),
);
?>

<h1>Create ChargingHistory</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>