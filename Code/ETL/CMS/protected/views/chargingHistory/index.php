<?php
/* @var $this ChargingHistoryController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Charging Histories',
);

$this->menu=array(
	array('label'=>'Manage ChargingHistory', 'url'=>array('admin')),
);
?>

<h4>Charging Histories</h4>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
