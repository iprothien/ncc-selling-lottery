<?php
/* @var $this CdrController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Cdrs',
);

$this->menu=array(
	array('label'=>'Manage Cdr', 'url'=>array('admin')),
);
?>

<h4>Cdrs</h4>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
