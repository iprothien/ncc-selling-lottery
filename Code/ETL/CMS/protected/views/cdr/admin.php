<?php
/* @var $this CdrController */
/* @var $model Cdr */

$this->breadcrumbs = array(
    'Cdrs' => array('admin'),
    'Manage',
);

$this->menu = array(
    array('label' => 'Create Cdr', 'url' => array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('cdr-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h4>Manage Cdrs</h4>

<?php // echo CHtml::link('#','#',array('class'=>'search-button')); ?>
<div class="search-form">
    <?php
    $this->renderPartial('_search', array(
        'model' => $model,
    ));
    ?>
</div><!-- search-form -->

<div class="thongke">
    <p>
        Total Call:
        <span><?php echo $model->getTotal(); ?></span>
    </p>
    <p>
        Total Block:
        <span><?php echo $model->getCountBlock(); ?></span>
    </p>
</div>

<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'cdr-grid',
    'dataProvider' => $model->search(),
    'columns' => array(
        'id',
        'start',
        'src',
        'duration',
        'billsec',
        /*
          'channel',
          'dstchannel',
          'lastapp',
          'lastdata',
          'duration',
          'billsec',
          'disposition',
          'amaflags',
          'accountcode',
          'userfield',
          'uniqueid',
         */
        array(
            'class' => 'CButtonColumn',
        ),
    ),
));
?>
