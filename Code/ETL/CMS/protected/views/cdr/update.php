<?php
/* @var $this CdrController */
/* @var $model Cdr */

$this->breadcrumbs=array(
	'Cdrs'=>array('admin'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'View Cdr', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Cdr', 'url'=>array('admin')),
);
?>

<h1>Update Cdr <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>