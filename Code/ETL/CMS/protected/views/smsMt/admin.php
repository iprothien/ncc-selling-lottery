<?php
/* @var $this SmsMtController */
/* @var $model SmsMt */

$this->breadcrumbs=array(
	'Sms Mts'=>array('admin'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Create SmsMt', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('sms-mt-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Sms Mts</h1>

<div class="search-form">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'sms-mt-grid',
	'dataProvider'=>$model->search(),
	'columns'=>array(
		'id',
		'short_code',
		'msisdn',
		'content',
                'created_datetime',
                'type',
                array('name'=>'status','value'=>'$data->getStatus()'),
		/*
		'created_datetime',
		'updated_datetime',
		'phonenumber',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
