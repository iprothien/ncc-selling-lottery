<?php

/**
 * This is the model class for table "draw_result".
 *
 * The followings are the available columns in table 'draw_result':
 * @property integer $id
 * @property string $code
 * @property string $created_datetime
 * @property integer $type
 */
class DrawResult extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return DrawResult the static model class
     */
    public $time_start;
    public $time_end;
    
    public function getType() {
        switch ($this->type) {
            case 1:
                return '2 Number';
                break;
            case 2:
                return '3 Number';
                break;
            default:
                break;
        }
    }
    
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'draw_result';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('code, created_datetime', 'required'),
            array('code', 'length', 'max' => 1024),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, time_start, time_end, code, created_datetime', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'code' => 'Code',
            'created_datetime' => 'Draw date',
            'type' => 'Type',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('code', $this->code, true);
        $criteria->compare('type', $this->type);
        if ($this->time_start)
            $criteria->compare('created_datetime', '>' . $this->time_start);
        if ($this->time_end)
            $criteria->compare('created_datetime', '<' . $this->time_end);
        $criteria->order = 'id DESC';

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                    'pagination' => array('pageSize' => '25'),
                ));
    }

}