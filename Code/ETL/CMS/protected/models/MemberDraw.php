<?php

/**
 * This is the model class for table "member_draw".
 *
 * The followings are the available columns in table 'member_draw':
 * @property integer $id
 * @property string $msisdn
 * @property string $code
 * @property integer $money
 * @property string $created_datetime
 * @property integer $type
 * @property integer $status
 */
class MemberDraw extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return MemberDraw the static model class
     */
    public $time_start;
    public $time_end;

    public function getType() {
        switch ($this->type) {
            case 1:
                return '2 Number';
                break;
            case 2:
                return '3 Number';
                break;
            default:
                break;
        }
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'member_draw';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('msisdn, code, money, created_datetime, type, status', 'required'),
            array('money, type, status', 'numerical', 'integerOnly' => true),
            array('msisdn', 'length', 'max' => 16),
            array('code', 'length', 'max' => 3),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, msisdn, code, created_datetime, status, time_start, time_end', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'msisdn' => 'Msisdn',
            'code' => 'Code',
            'money' => 'Money',
            'created_datetime' => 'Created Datetime',
            'type' => 'Type',
            'status' => 'Status',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('msisdn', $this->msisdn, true);
        $criteria->compare('code', $this->code, true);
        $criteria->compare('status', 1);
        if ($this->time_start)
            $criteria->compare('created_datetime', '>' . $this->time_start);
        if ($this->time_end)
            $criteria->compare('created_datetime', '<' . $this->time_end);
        $criteria->order = 'id DESC';

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                    'pagination' => array('pageSize'=>'25'),
                ));
    }

}