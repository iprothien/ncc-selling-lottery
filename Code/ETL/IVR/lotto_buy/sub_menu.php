#!/usr/bin/php -q
<?php
ob_start();
error_reporting(false);
set_time_limit(120);
require('lib/phpagi.php');
require('lib/sbMysqlPDO.class.php');
require('lib/sbMssql.class.php');
require('lib/functions.php');
require('lib/config.php');
$agi = new AGI();
$conn = new sbMysqlPDO($server, $user, $password, $db);

$caller = $agi->get_variable("CALLERID(num)");
$caller = $caller['data'];

$i = 1;
while (1) {
    $countMenu = $agi->get_variable("count_menu", true);
    if ($i++ > 2 || $countMenu >= 2) {
        $agi->stream_file($_system_path['not_press']);
        break;
    }

    $result = $agi->get_data($_system_path['sub_menu'], $max_timeout, "1");
    $pressSub = $result['result'];
    switch ($pressSub) {
        case '1':
            $agi->exec_goto("8899-buy", $dial, 1);
            exit;
            break;
        case '2':
            $agi->stream_file($_system_path['millionaire']);
            $agi->exec_goto("8899-menu", $dial, 1);
            exit;
            break;
    }
}
ob_clean();
exit;
?>