#!/usr/bin/php -q
<?php
ob_start();
error_reporting(false);
set_time_limit(120);
require('lib/phpagi.php');
require('lib/sbMysqlPDO.class.php');
require('lib/sbMssql.class.php');
require('lib/functions.php');
require('lib/config.php');
$agi = new AGI();
$conn = new sbMysqlPDO($server, $user, $password, $db);

$caller = $agi->get_variable("CALLERID(num)");
$caller = $caller['data'];

$keyConfirm = $agi->get_variable("keyConfirm");
$keyConfirm = $keyConfirm['data'];

$code = $agi->get_variable("code");
$code = $code['data'];

$money = $agi->get_variable("money");
$money = $money['data'];
//edit
if($caller == '2029830292'){
	$money = 1;					
}

switch ($keyConfirm) {
    case 1:
        //Check Time Avaiable
        $sqlgetTime = "SELECT * FROM draw_time WHERE NOW() > start_time && NOW() < end_time && status=1";
        $getTime = $conn->doSelectOne($sqlgetTime);
        if (strlen($code) > 2)
            $type = 2;
        else
            $type = 1;
        if (!$getTime) {
            //Invalid Time
            $tranStatus = 2;
            $sqlInsertTransaction = "INSERT INTO transaction_log (msisdn,code,money,type,status,draw_id,buy_type,content,created_datetime) VALUES ('{$caller}','{$code}','{$money}','{$type}','{$tranStatus}','{$getTime['id']}',0,'',now()) ";
            $conn->doUpdate($sqlInsertTransaction);
            $agi->stream_file($_system_path['close']);
            exit;
        }

        $timeBuy = date('Y-m-d H:i:s', time());
        $info = viewInfo($caller);
        if ($info[0] == 2000) {
            if ($info[1] >= $money) {
                $rsAddOrder = addOrder($caller, $code, $money);
                if ($rsAddOrder == $money) {
                    $timeRef = time();
                    $contentLog = date("Y-m-d H:i:s",time())."|$timeRef|$caller|$money|$code\n";
                    $result = charge($caller, $money);
                    $contentLog = date("Y-m-d H:i:s",time())."|$timeRef|$caller|$money|$code|{$result[0]}|{$result['trans_content']}\n";
                    file_put_contents("/var/www/log/".date("Y-m-d")."_charge.log",$contentLog,FILE_APPEND);
                } else {
                    $tranStatus = 5;
                    $sqlInsertTransaction = " INSERT INTO transaction_log (msisdn,code,money,type,status,draw_id,buy_type,content,created_datetime) VALUES ('{$caller}','{$code}','{$money}','{$type}','{$tranStatus}','{$getTime['id']}',0,'{$rsAddOrder}',now()) ";
                    $conn->doUpdate($sqlInsertTransaction);
                    $agi->stream_file($_system_path['full_bought']);
                    $agi->exec_goto('8899-buy', $dial, 1);
                    exit;
                }
                if ($result[0] == $caller) {
                    //Insert Into member_draw
                    $status = 1;
                    $sqlInsert = "INSERT INTO member_draw(msisdn,code,money,created_datetime,type,status,draw_no,draw_id) VALUES('{$caller}','{$code}','{$money}','{$timeBuy}','{$type}', '{$status}', '{$getTime['draw_no']}', '{$getTime['id']}')";
                    $memberId = $conn->doUpdate($sqlInsert);
                    $infoAfter = viewInfo($caller);
                    if ($memberId) {
                        
                        //CHARGING & SMS
                        $sqlInsertCharging = "INSERT INTO charging_history(msisdn,time_charge,money,member_id,digit,trans_id,trans_date,draw_no) VALUES ('{$caller}',now(),'{$money}','{$memberId}','{$code}','{$result['trans_id']}','{$result['trans_date']}', '{$getTime['draw_no']}')";
                        $conn->doUpdate($sqlInsertCharging);

                        $smsInform = "You successfuly bought lottery with the following information:" . "\n"
                            . "Draw No: " . $getTime['draw_no'] . "\n"
                            . "Draw Date " . date('d/m/Y', strtotime($getTime['end_time'])) . "\n"
                            . "Digit: " . $code . "\n"
                            . "Amount: " . $money . " KIP";
                        $totalSMS = ceil(strlen($smsInform) / 160);
                        $sqlInsertSMS = "INSERT INTO sms_mt(short_code,msisdn,status,content,created_datetime,total_sms) VALUES('8899','{$caller}','0','{$smsInform}',now(),'{$totalSMS}')";
                        $conn->doUpdate($sqlInsertSMS);
                        
                        #### SUBMIT BUYING
                        $submitResult = SubmitBuying($caller, $money, $code);
                        $submitResultContent = htmlentities(@$submitResult->SubmitBuyingResult, ENT_QUOTES);
                        if (!substr_count(@$submitResult->SubmitBuyingResult, "Successfull-IVR")) {
                            //SUBMIT NOT OK
                            $submitStatus = 2;
                            $tranStatus = 7;
                            $sqlInsertTransaction = "INSERT INTO transaction_log (msisdn,code,money,type,status,draw_id,buy_type,content,created_datetime,balance_before,balance_after,member_id) VALUES ('{$caller}','{$code}','{$money}','{$type}','{$tranStatus}','{$getTime['id']}',0,'{$submitResultContent}',now(),'{$info[1]}','{$infoAfter[1]}','{$memberId}')";
                            $conn->doUpdate($sqlInsertTransaction);
                        } else {
                            $submitStatus = 1;
                            $tranStatus = 1;
                            $sqlInsertTransaction = "INSERT INTO transaction_log (msisdn,code,money,type,status,draw_id,buy_type,content,created_datetime,balance_before,balance_after,member_id) VALUES ('{$caller}','{$code}','{$money}','{$type}','{$tranStatus}','{$getTime['id']}',0,'{$submitResultContent}',now(),'{$info[1]}','{$infoAfter[1]}','{$memberId}') ";
                            $conn->doUpdate($sqlInsertTransaction);
                        }
                        
                        $agi->stream_file($_system_path['success_digit']);
                        $agi->say_digits($code);
                        $agi->stream_file($_system_path['success_amount']);
                        $agi->say_number($money);
			   $agi->stream_file($_system_path['kip']);

                        $i = 1;
                        while (1) {
                            if ($i++ > 3) {
                                $agi->stream_file($_system_path['not_press']);
                                exit;
                            }

                            $dtConfirm = $agi->get_data($_system_path['continue_buy'], $max_timeout, 1);
                            $keyConfirm = $dtConfirm['result'];
                            switch ($keyConfirm) {
                                case "1":
                                    $agi->exec_goto('8899-buy', $dial, 1);
                                    exit;
                                    break;
                                case "2":
                                    $agi->exec_goto('8899-menu', $dial, 1);
                                    exit;
                                    break;
                                case "3":
                                    exit;
                                    break;
                            }
                        }
                    } else {
                        $content = " {$caller},{$code},{$money},{$type},{$tranStatus},{$getTime['draw_no']},0,{$submitResultContent}," . date("Y-m-d H:i:s", time()) . ",{$info[1]},{$infoAfter[1]},{$memberId}\n";
                        file_put_contents("/var/www/log/" . date("Y-m-d") . ".log", $content, FILE_APPEND);
                    }
                } else {
                    $tranStatus = 6;
                    $infoAfter = viewInfo($caller);
                    $sqlInsertTransaction = " INSERT INTO transaction_log (msisdn,code,money,type,status,draw_id,buy_type,content,created_datetime,balance_before,balance_after,member_id) VALUES ('{$caller}','{$code}','{$money}','{$type}','{$tranStatus}','{$getTime['id']}',0,'{$result['trans_content']}',now(),'{$info[1]}','{$infoAfter[1]}','{$memberId}') ";
                    $conn->doUpdate($sqlInsertTransaction);
                    $agi->stream_file($_system_path['cant_charge']);
                    $agi->exec_goto('8899-buy', $dial, 1);
                    exit;
                }
            } else {
                $tranStatus = 4;
                $sqlInsertTransaction = " INSERT INTO transaction_log (msisdn,code,money,type,status,draw_id,buy_type,content,created_datetime,balance_before) VALUES ('{$caller}','{$code}','{$money}','{$type}','{$tranStatus}','{$getTime['id']}',0,'',now(),'{$info[1]}') ";
                $conn->doUpdate($sqlInsertTransaction);
                $agi->stream_file($_system_path['notenough']);
                $agi->exec_goto('8899-buy', $dial, 1);
                exit;
            }
        } else {
            $tranStatus = 3;
            $sqlInsertTransaction = " INSERT INTO transaction_log (msisdn,code,money,type,status,draw_id,buy_type,content,created_datetime) VALUES ('{$caller}','{$code}','{$money}','{$type}','{$tranStatus}','{$getTime['id']}',0,'System close',now()) ";
            $conn->doUpdate($sqlInsertTransaction);
            $agi->stream_file($_system_path['postpaid']);
            exit;
        }
        break;
    case 2:
        $agi->exec_goto('8899-buy', $dial, 1);
        exit;
        break;
    case 3:
        $agi->exec_goto("8899-menu", $dial, 1);
        exit;
        break;
}
ob_clean();
?>