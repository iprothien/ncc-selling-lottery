#!/usr/bin/php -q
<?php
ob_start();
error_reporting(false);
set_time_limit(120);
require('lib/phpagi.php');
require('lib/sbMysqlPDO.class.php');
require('lib/config.php');
$agi = new AGI();
$conn = new sbMysqlPDO($server, $user, $password, $db);

// Lay ra so dien thoai cua nguoi goi den
$msisdn = $agi->get_variable("CALLERID(num)", true);
// Webcome
$agi->stream_file('read_bought_number_last_week', 8899, 1);
// check thoi gian
$dateStart = date('Y-m-d H:i:s', strtotime('-7 day', date('Y-m-d H:i:s')));
$dateEnd = date('Y-m-d H:i:s');

$sql = "SELECT * FROM member_draw WHERE `msisdn` = '{$msisdn}' AND created_datetime >= '{$dateStart}' AND created_datetime <= '{$dateEnd}' ORDER BY id ASC";
$listLotto = $conn->doSelect($sql);
$i = 0;
while (1) {
    if (empty($listLotto)) {
        $agi->stream_file($_system_path['no_number_week']);
        $agi->exec_goto("8899-menu", $dial, 1);
    } else {
        foreach ($listLotto as $lotto) {
            // Ngay
            $agi->stream_file($_system_path['lotto_day']);
            $day = date('d', strtotime(date($lotto['created_datetime'])));
            $agi->say_number($day);
            // Thang
            $agi->stream_file($_system_path['lotto_mouth']);
            $mouth = date('m', strtotime(date($lotto['created_datetime'])));
            $agi->say_number($mouth);
            // Nam
            //$agi->stream_file($_system_path['lotto_year']);
            $year = date('Y', strtotime(date($lotto['created_datetime'])));
            $agi->say_number($year);
            // Mua so  number_buy
            $agi->stream_file($_system_path['number_buy']);
            $agi->say_number($lotto['code']);
            // So tien
            $agi->stream_file($_system_path['amount_buy']);
            $agi->say_number($lotto['amount']);
        }

        $result = $agi->get_data($_system_path['main_menu'], $max_timeout, "1");
        $pressKey = $result['result'];
        switch($pressKey){
            case 0:
                $agi->exec_goto("8899-menu", $dial, 1);
                break;
            default:
                exit;
                break;
        }
    }
}
ob_clean();
exit;
?>