#!/usr/bin/php -q
<?php
ob_start();
error_reporting(false);
set_time_limit(120);
require('lib/phpagi.php');
require('lib/sbMysqlPDO.class.php');
require('lib/sbMssql.class.php');
require('lib/functions.php');
require('lib/config.php');
$agi = new AGI();
$agi->stream_file($_system_path['welcome'], "1");
$agi->exec_goto("8899-menu", $dial, 1);
ob_clean();
exit;
?>