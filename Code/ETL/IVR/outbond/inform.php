#!/usr/bin/php -q
<?php
ob_start();
error_reporting(false);
set_time_limit(120);
require('lib/phpagi.php');
require('lib/sbMysqlPDO.class.php');
require('lib/sbMssql.class.php');
require('lib/functions.php');
require('lib/config.php');
error_reporting(0);
$agi = new AGI();
$conn = new sbMysqlPDO($server, $user, $password, $db);

$callerInfo = explode("-",$agi->get_variable("info",true));
$callId = $callerInfo[0];
$msisdn = $callerInfo[1];
$callDetailId = $callerInfo[2];

//$callDetailId=15;

$sqlSelectCall = " SELECT call_service.*,call_detail.msisdn,call_detail.id as detail_id,call_detail.prize,call_detail.digit FROM call_service "
                ." INNER JOIN call_detail ON call_service.id=call_detail.call_service_id "
                ." WHERE call_detail.id={$callDetailId} "
                ." ORDER BY id ASC LIMIT 1 ";
$call = $conn->doSelectOne($sqlSelectCall);
//echo $sqlSelectCall;
//var_dump($call);
$sqlUpdateCall =  "UPDATE call_detail SET time_called = now(),status=1 WHERE id='{$call['detail_id']}'";
$conn->doUpdate($sqlUpdateCall);

sleep(2);

switch($call['call_type']){
    case "2":
        //Media
        $agi->stream_file($_media_path.getFileName($call['media_path']));
		//$agi->stream_file($_media_path."promotion");
        break;
    case "3":
        if($call['prize']){
            $agi->stream_file($_real_path."outbond_winner_inform1");
            //$agi->say_number(intval($call['prize']));
			sayLaoCurrency(intval($call['prize']),$agi);
            $agi->stream_file($_real_path."outbond_winner_inform2");
        }
        break;
    default:
        $sqlSelectDraw = " SELECT * FROM draw_time WHERE draw_no='{$call['draw_no']}' AND delete_status=0 ORDER BY id DESC ";
        //echo $sqlSelectDraw;
        $draw = $conn->doSelectOne($sqlSelectDraw);
        //var_dump($draw);
	$agi->stream_file($_real_path."outbond_intro");
        $agi->stream_file($_real_path."outbond_draw_no");
        $agi->say_digits($draw['draw_no']);
        $agi->stream_file($_real_path."outbond_date_1");
        $agi->say_digits(intval(date('d',strtotime($draw['end_time']))));
        $agi->stream_file($_real_path."outbond_date_2");
        $agi->say_digits(intval(date('m',strtotime($draw['end_time']))));
        $agi->stream_file($_real_path."outbond_2_digits");
        $agi->say_digits(intval($draw['two_digit']));
        $agi->stream_file($_real_path."outbond_3_digits");
        $agi->say_digits(intval($draw['three_digit']));
        
        //$sqlSelectWinner = " SELECT * FROM winner WHERE msisdn='{$call['msisdn']}' AND draw_no='{$call['draw_no']}' ";
        //$winner = $conn->doSelectOne($sqlSelectWinner);
        if($call['prize']){
            $agi->stream_file($_real_path."outbond_winner_inform1");
            $agi->say_number(intval($call['prize']));
            $agi->stream_file($_real_path."outbond_winner_inform2");
        }else{
            $agi->stream_file($_real_path."outbond_end");
        }
        //Inform result & winner
}
ob_clean();
?>