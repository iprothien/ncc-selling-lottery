#!/usr/bin/php -q
<?php
set_time_limit(30);
require('lib/phpagi.php');
require('lib/sbMysqlPDO.class.php');
require('lib/sbMssql.class.php');
require('lib/functions.php');
require('lib/config.php');
$agi = new AGI();
$conn = new sbMysqlPDO($server, $user, $password, $db);

$caller = $agi->get_variable("CALLERID(num)");
$caller = $caller['data'];

$option = $agi->get_variable("option");
$option = $option['data'];

$pressDay = $agi->get_variable("pressDay");
$pressDay = $pressDay['data'];

$sqlSelecResult = "SELECT draw_result.*,day(created_datetime) as day, month(created_datetime) as month FROM draw_result ORDER BY created_datetime DESC LIMIT 2 ";
$resultDate = $conn->doSelect($sqlSelecResult);
$lastDay = $resultDate[0]['day'];
$lastMonth = $resultDate[0]['month'];
$lastDate = $resultDate[0]['created_datetime'];

$preLastDay = $resultDate[1]['day'];
$preLastMonth = $resultDate[1]['month'];
$preLastDate = $resultDate[1]['created_datetime'];

if ($option == 1) {
    $sms = $resultDate[0]['code'];
    $sqlCheckMem = " SELECT * FROM member_draw "
            . "WHERE created_datetime<='{$lastDate}' AND created_datetime>='$preLastDate' AND msisdn='{$caller}' ";
} else if ($option == 2) {
    $sms = $resultDate[1]['code'];
    $sqlCheckMem = " SELECT * FROM member_draw "
            . "WHERE created_datetime<='{$lastDate}' AND created_datetime>='$preLastDate' AND msisdn='{$caller}' ";
} else {
    $sqlResultOption = "SELECT code,created_datetime FROM draw_result WHERE created_datetime <='{$pressDay}' order by created_datetime DESC limit 2";
    $ResultOption = $conn->doSelect($sqlResultOption);
    $sms = $ResultOption[0]['code'];

    if (!$ResultOption[1])
        $ResultOption[1]['created_datetime'] = 0;
    $sqlCheckMem = " SELECT * FROM member_draw "
            . "WHERE created_datetime<='{$pressDay}' AND created_datetime>='{$ResultOption[1]['created_datetime']}' AND msisdn='{$caller}' ";
}

$CheckMember = $conn->doSelectOne($sqlCheckMem);

$Time_Created = date('Y-m-d H:i:s', time());
if ($CheckMember) {
    $agi->stream_file($_system_path['result']);
    /* Send SMS */
    $sqlInsertMt = "INSERT INTO sms_mt(
                                        short_code,msisdn,status,content,created_datetime
                                        ) VALUES(
                                        '8899','{$caller}','0','{$sms}','{$Time_Created}'
                                        )";
    $conn->doSelectOne($sqlInsertMt);
    /* End */
    $agi->exec_goto('lotto-menu', $dial, 1);
    exit;
} else {
    $i = 1;
    while (1) {
        if ($i++ > 3) {
            $agi->stream_file($_system_path['not_press']);
            exit;
        }

        $result = $agi->get_data($_system_path['charge1000'], $max_timeout, "1");
        $pressKey = $result['result'];
        switch ($pressKey) {
            case '1':
                $breakStatus = 1;
                break;
            case '2':
                $agi->exec_goto("lotto-result", $dial, 1);
                exit;
                break;
        }
        if ($breakStatus)
            break;
    }

    $CheckBalence = charge($caller, 1000);
    if ($CheckBalence[0] && $breakStatus) {
        $sqlInsertCharging = " 
                                INSERT INTO charging_history(msisdn,time_charge,money,member_id,digit,charging_type)
                                VALUES ('{$caller}',now(),'1000','0','0','1')
                             ";
        $conn->doUpdate($sqlInsertCharging);
        $sqlInsertMt = "INSERT INTO sms_mt(
                                                    short_code,msisdn,status,content,created_datetime
                                                    ) VALUES(
                                                    '{$dial}','{$caller}','0','{$sms}',now()
                                                    )";
        $conn->doSelectOne($sqlInsertMt);
        $agi->stream_file($_system_path['result']);
        $agi->exec_goto('lotto-menu', $dial, 1);
        exit;
    } else {
        $agi->stream_file($_system_path['cant_charge']);
        $agi->exec_goto('lotto-menu', $dial, 1);
        exit;
    }
}
?>