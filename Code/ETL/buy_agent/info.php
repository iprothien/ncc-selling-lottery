<?php
ob_start();
error_reporting(false);
require('lib/config.php');
require('lib/functions.php');
require('lib/sbMysqlPDO.class.php');
require('lib/sbMssql.class.php');
$conn = new sbMysqlPDO($server, $user, $password, $db);

$result = array();
$rsMember = array();

$ip = $_SERVER['REMOTE_ADDR'];
if ($ip == '103.240.242.59' || $ip == '27.118.30.2' || $ip == '42.112.93.23') {
    if (isset($_GET['msisdn']) && $_GET['msisdn']) {
        $msisdn = $_GET['msisdn'];

        $info = viewInfo($msisdn);
        if ($info[0] == 2000)
            $current = $info[1];
        else
            $current = 'postpaid';


        $sqlSelect = 'SELECT a.* FROM member_draw AS a INNER JOIN draw_time AS b ON a.draw_id=b.id && a.status=1 && a.msisdn="'.$msisdn.'" ORDER BY a.id DESC';
        $data = $conn->doSelect($sqlSelect);
        if ($data) {
            foreach ($data as $item) {
                $rsMember[] = $item;
            }
        }

        $sqlTotalBuy = 'SELECT COUNT(*) as t1 FROM member_draw WHERE msisdn="' . $msisdn . '" && status=1';
        $rsTotalBuy = $conn->doSelectOne($sqlTotalBuy);

        $sqlTotalMoney = 'SELECT SUM(money) as t2 FROM member_draw WHERE msisdn="' . $msisdn . '" && status=1';
        $rsTotalMoney = $conn->doSelectOne($sqlTotalMoney);

        $sqlTotalWin = 'SELECT COUNT(*) as t3 FROM member_draw WHERE msisdn="' . $msisdn . '" && status=1 && win_status=1';
        $rsTotalWin = $conn->doSelectOne($sqlTotalWin);

        $result[] = array(
            'infoMoney' => $current,
            'totalBuy' => $rsTotalBuy['t1'],
            'totalMoney' => $rsTotalMoney['t2'],
            'totalWin' => $rsTotalWin['t3'],
            'list' => $rsMember
        );
        ob_clean();
        echo json_encode($result);
    }
}
?>