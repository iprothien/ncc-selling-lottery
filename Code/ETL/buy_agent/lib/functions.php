<?php

function getFileName($name) {
    return substr($name, 0, strrpos($name, '.'));
}

function viewInfo($msisdn) {
    try{
        //$_url_send_sms = "http://10.10.1.155:3636/nccivrlottery/NccQuerySubscriberTypeIvrSmsEtl.php?msisdn={$msisdn}&&TranId=123pw=nccsms2014";
        $_url_send_sms = "http://10.10.1.155:3636/nccivrlottery/NccQuerySubscriberTypeIvrSmsEtl.php?msisdn={$msisdn}&TranId=123&pw=nccsms2014";
        $cookie = tempnam("/tmp", "CURLCOOKIE");
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; rv:1.7.3) Gecko/20041001 Firefox/0.10.1");
        curl_setopt($ch, CURLOPT_URL, $_url_send_sms);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_ENCODING, "");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_AUTOREFERER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);    # required for https urls
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        $responeContent = curl_exec($ch);
        $response = curl_getinfo($ch);
        curl_close($ch);
        //var_dump($responeContent);
        $result = explode(";", $responeContent);
        return $result;
    }catch(Exception $e){
        return array();
    }
}

function charge($msisdn, $money) {
    //$money=1;
    $url = "http://10.10.1.155:8282/mobile/services/Paymentgw?wsdl";
    $user = "Ncclotty";
    $pass = "nccsms123";
    try {
        $soapClient = new SoapClient($url, array('trace' => true));
        $param = array('username' => $user, 'password' => $pass);
        
        $Login = $soapClient->__soapCall('login', $param);
        
        //var_dump($Login);
        $tokeyKey = $Login->loginInfo->token->tokenKey;
        $TimePay = date('mdHis', time());
        $TimePay = round(microtime(true) * 1000);
        $TimeCharge = date('Ymd', time());
        $serviceCode = '30';
        $xmlRequest = '<isomsg>
                        <field id="0" value="0200"/>
                        <field id="3" value="070000"/>
                        <field id="7" value="0528092955"/>
                        <field id="42" value="' . $TimePay . '"/>
                        <field id="61" value="' . $msisdn . '|' . $money . '"/>
                        <field id="73" value="' . $TimeCharge . '"/>
                        <field id="94" value="' . $serviceCode . '"/>
                       </isomsg>';
        //echo $xmlRequest;
        $option = array('commandXML' => $xmlRequest, 'tokenKey' => $tokeyKey);
        $Paid = $soapClient->__soapCall('execute', $option);
        //var_dump($Paid);
        //var_dump($Paid);
        preg_match('/id\=\"39\" value\=\"(.*?)\"/', $Paid, $RespondCode);
        $result = 0;
        if ($RespondCode[1] == '00') {
            preg_match('/id\=\"61\" value\=\"(.*?)\"/', $Paid, $infoCharge);
            $result = explode('|', $infoCharge[1]);
        } else {
            $result = array();
        }
        $result['trans_id']=$TimePay;
        $result['trans_date']=$TimeCharge;
        return $result;
    } catch (Exception $e) {
        return array();
    }
    
}

function addOrder($MobileNo, $digits, $amount) {
    try {
        $soapClient = new SoapClient('http://ncclaos.net:8208/lldserviceIVR.asmx?WSDL', array('trace' => true));
        $QueryUserEvt->PassKey = 'mo246d0262c3925617b0c72bb20aac2d';
        $QueryUserEvt->AccountNo = '1448';
        $QueryUserEvt->MobileNo = $MobileNo;
        $QueryUserEvt->mDigit = $digits;
        $QueryUserEvt->mAmount = $amount;
        $QueryUserEvt->isFirstRecord = true;
        $resutl = $soapClient->AddOrder($QueryUserEvt);
        if(@$resutl->AddOrderResult){
            return $resutl->AddOrderResult;
        }else{
            return 0;
        }
    } catch (Exception $e) {
        return 0;
    }

    
}

function SubmitBuying($MobileNo, $TotalAmount, $PaymentNo) {
    $result->SubmitBuyingResult = "";
    try {
        $soapClient = new SoapClient('http://ncclaos.net:8208/lldserviceIVR.asmx?WSDL', array('trace' => false));
        $submitOrder->PassKey = 'mo246d0262c3925617b0c72bb20aac2d';
        $submitOrder->ProviderID = '7';
        $submitOrder->MobileNo = $MobileNo;
        $submitOrder->PaymentNo = $PaymentNo;
        $submitOrder->TotalAmount = $TotalAmount;
        $submitOrder->isSuccess = true;
        $result = $soapClient->SubmitBuying($submitOrder);
        return $result;
    } catch (Exception $e) {
        $result->SubmitBuyingResult = $e->getMessage();
	return $result;
    }
}

function checkDigitAvailable($digit, $amout) {
    return TRUE;
}

function getPathStorage($rootDir, $itemId, $itemPerDir, $makeDir) {
    $dir1 = floor($itemId / ($itemPerDir * $itemPerDir * $itemPerDir));
    $dir2 = floor(($itemId - $dir1 * $itemPerDir * $itemPerDir * $itemPerDir) / ($itemPerDir * $itemPerDir));
    $temp = floor($itemId / ($itemPerDir * $itemPerDir));
    $dir3 = floor(($itemId - $temp * $itemPerDir * $itemPerDir) / $itemPerDir);

    $path = $dir1 . "/" . $dir2 . "/" . $dir3;

    if (!file_exists($rootDir . "/" . $path) && ($makeDir)) {
        if (!file_exists($rootDir . "/" . $dir1 . "/" . $dir2)) {
            if (!file_exists($rootDir . "/" . $dir1)) {
                @mkdir($rootDir . "/" . $dir1, true);
            }
            @mkdir($rootDir . "/" . $dir1 . "/" . $dir2, true);
            chmod($rootDir . "/" . $dir1, 0777);
        }
        @mkdir($rootDir . "/" . $dir1 . "/" . $dir2, true);
        chmod($rootDir . "/" . $dir1 . "/" . $dir2, 0777);
    }
    @mkdir($rootDir . "/" . $dir1 . "/" . $dir2 . "/" . $dir3, true);
    chmod($rootDir . "/" . $dir1 . "/" . $dir2 . "/" . $dir3, 0777);
    return $path;
}

?>
