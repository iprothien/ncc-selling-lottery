<?php
/**
 * User: dovv<dovv1987@gmail.com>
 * DateTime: 7/21/14 11:13 AM
 */

require_once('lib/sbMysqlPDO.class.php');
require_once('lib/config.php');
require_once('lib/functions_etl.php');

$conn = new sbMysqlPDO($server, $user, $password, $db);
$msg = 'Error Message From Etl'.'<br />';

// Check Connecting
$connect = $conn->getConnection();
if(empty($connect)){
    $msg .= 'Can not connect to ETL mysql server <br />';
}else{
    echo 'Ok Mysql';
}

// Check Asterisk
$statusAsterisk = exec('/etc/init.d/asterisk status');
$strAs = "is running";
if (strpos($statusAsterisk,$strAs) > 0) {
    echo 'Ok Asterisk';
}else{
    $msg .= 'Error Asterisk Or Stop Asterisk <br />';
}

// Ping server
$ping = exec('ping -c 3 10.10.1.155');
if(empty($ping)){
    $msg .= 'Can not ping to Etl server <br />';
}else{
    echo 'Ok ping';
}

// Ping server smsc
$pingSmsc = exec('ping -c 3 169.169.100.30');
if(empty($pingSmsc)){
    $msg .= 'Can not ping smsc ip: 169.169.100.30 <br />';
}else{
    echo 'Ok ping smsc';
}

// Check Info
$info = viewInfo($_msisdn);
if($info[0] == 2000){
    echo '0k info';
}else{
    $msg .= 'Can not get phone number information <br />';
}

// Check Payment
$payment = charge($_msisdn,$_money);
if($payment[0] == $_msisdn){
    echo 'Ok payment';
}else{
    $msg .= 'Can not charge money in Payment gw <br />';
}
// || $payment[0] != $_msisdn
if(empty($connect) || strpos($statusAsterisk,$strAs) < 0 || empty($ping) || empty($pingSmsc) || $info[0] != 2000){
    sendSmsToPhone($_phone,$msg);
    sendEmail($_email,$msg);
}else{
    echo 'Ok';
}

function getConnect(){
    global $server, $user, $password, $db;
    return new sbMysqlPDO($server, $user, $password, $db);
}

function sendSmsToPhone($phone = array(), $msg){
    $cnn = getConnect();
    $total_sms = floor(strlen($msg)/160);
    $date = date('Y-m-d H:i:s');
    foreach($phone as $p){
        $sql = "INSERT INTO sms_mt SET `short_code` = '8899', `msisdn`= '{$p}', `status` = 0, `type` = 1,`content` = '{$msg}',`created_datetime` = '{$date}', `updated_datetime` = '{$date}', `total_sms` = '{$total_sms}', `draw_no` =0";
        $cnn->doUpdate($sql);
    }
    return true;
}


function sendEmail($mailList =array(),$body){
    require_once('mailer/class.phpmailer.php');
    require_once('mailer/class.smtp.php');
    try {
        $mail = new PHPMailer(true);                //New instance, with exceptions enabled
        $mail->IsSMTP();                            // tell the class to use SMTP
        $mail->SMTPAuth   = true;                   // enable SMTP authentication
        $mail->Port       = 465;                     // set the SMTP server port
        $mail->Host       = "smtp.gmail.com";       // SMTP server
	 $mail->SMTPSecure = 'ssl'; 
        $mail->Username   = "dovv@iprotech.vn";     // SMTP server username
        $mail->Password   = "Do1987@123";           // SMTP server password

        //$mail->IsSendmail();
        $mail->From       = "dovv@iprotech.vn";
        $mail->FromName   = "NCC monitoring";

        foreach($mailList as $to){
            $mail->AddAddress($to);
	    echo "Email".$to;
        }
        
	//$to = "dovv@iprotech.vn";
        $mail->Subject  = "NCC monitoring ETL";
        $mail->MsgHTML($body);
        $mail->IsHTML(true);
        $mail->Send();
    } catch (phpmailerException $e) {
        echo $e->errorMessage();
    }
    return true;
}
