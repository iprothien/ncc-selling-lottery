<?php
class CallInfo {

    public $msisdn;
    public $maxRetries;
    public $retryTime;
    public $waitTime;
    public $context;
    public $extension;
    public $priority;
    public $callerId;
    public $set;

}
function createCallFile($callInfo, $filePath) {
    $content = "Channel: SIP/{$callInfo->msisdn}" .
            "\nMaxRetries: {$callInfo->maxRetries}" .
            "\nRetryTime: {$callInfo->retryTime}" .
            "\nWaitTime: {$callInfo->waitTime}" .
            "\nContext: {$callInfo->context}" .
            "\nExtension: {$callInfo->extension}" .
            "\nPriority: {$callInfo->priority}" .
            "\nCallerID: {$callInfo->callerId}" .
            "\nSet: {$callInfo->set}" .
            "\nAlwaysDelete: Yes";

    if (file_exists("{$filePath}.call"))
        unlink("{$filePath}.call");

    file_put_contents("{$filePath}.call", $content);
	chmod("{$filePath}.call",0777);
}

function sendSMS($shortcode,$msisdn,$content){
    $url = "http://10.10.1.155:8082/SmsProcessDeliveryReportManagementProcessing/services/SmsApiWS?wsdl";
    $postVar = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:sms="http://SmsApiWS.fadao.com">
        <soapenv:Header/>
        <soapenv:Body>
           <sms:BulkBroadcastingMessaging>
              <sms:SpId>1100223121</sms:SpId>
              <sms:SpPassword>NccSMS@2user</sms:SpPassword>
              <sms:ServicID>1100002201</sms:ServicID>
              <sms:TransactionID>'.substr(time(),0,9).'</sms:TransactionID>
              <sms:SrcAddr>8899</sms:SrcAddr>
              <sms:DestAddrss>'.$msisdn.'</sms:DestAddrss>
              <sms:BodyMsg>'.$content.'</sms:BodyMsg>
              <sms:reportRequire>1</sms:reportRequire>
              <sms:validityPeriod>0</sms:validityPeriod>
           </sms:BulkBroadcastingMessaging>
        </soapenv:Body>
     </soapenv:Envelope>';
     $ch = curl_init();
     //set the url, number of POST vars, POST data
     $options = array(
                     CURLOPT_URL	       => $url,
                     CURLOPT_RETURNTRANSFER => true,     // return web page
                     CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
                     CURLOPT_TIMEOUT        => 120,      // timeout on response
                     CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects {$channelInfo['static_name']}
                     CURLOPT_POST           => true,
                     CURLOPT_POSTFIELDS     => $postVar,
                     CURLOPT_HTTPHEADER	   => array('Content-Type: text/xml; charset=utf-8', 'Content-Length: '.strlen($postVar) ),
     );

     //open connection
     $ch = curl_init();
     curl_setopt_array( $ch, $options );
     $err     = curl_errno( $ch );
     $errmsg  = curl_error( $ch );
     $header  = curl_getinfo( $ch );
     $responeContent = curl_exec($ch);
     return 1;
    
    
}

function shortContent($str,$lenght){
    return substr($str,0,strripos(substr($str,0,$lenght)," "));
}

function explodeContent($content,$lenght=480){
    $resutls = array();
    $contents = explode("#",$content);
    foreach($contents as $content){
        if(strlen($content)<=$lenght){
            $results[] = $content;
        }else{
            while(1){
               $shortContent = shortContent($content,$lenght);
               $results[] = $shortContent;
               $content = substr($content,strlen($shortContent)+1);
               if(strlen($content)<$lenght){
                   $results[] = $content;
                   break;
               }
            }
        }
    }
    return $results;
}


function microtime_float()
{
    list($usec, $sec) = explode(" ", microtime());
    return ((float)$usec + (float)$sec);
}


function getFileName($name) {
    return substr($name, 0, strrpos($name, '.'));
}

?>
