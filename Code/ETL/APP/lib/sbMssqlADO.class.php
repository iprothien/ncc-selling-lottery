<?php

class sbMssqlADO{

        private $conn;
        private $rs;

        public function __construct($server,$user,$password,$db){
                $this->conn = new COM ("ADODB.Connection",NULL,CP_UTF8) or die("Cannot start ADO");
                $connStr = "PROVIDER=SQLOLEDB;SERVER=".$server.";UID=".$user.";PWD=".$password.";DATABASE=".$db;
                $this->conn->open($connStr);
        }

        public function query($query){
                $this->rs = $this->conn->execute($query);
        }

        public function fetchAll(){
            $results = array();
            $index   = 0;
            $num_columns = $this->rs->Fields->Count();
            for ($i=0; $i < $num_columns; $i++) {
                    $fld[$i] = $this->rs->Fields($i);
            }
            while (!$this->rs->EOF)  //carry on looping through while there are records
            {
                    for ($i=0; $i < $num_columns; $i++) {
                            $results[$index][$fld[$i]->name] = $fld[$i]->value;
                    }
                    $index++;
                    $this->rs->MoveNext(); //move on to the next record
            }
            return $results;
        }

        public function fetch(){
            $results = array();
            $index   = 0;
            $num_columns = $this->rs->Fields->Count();
            for ($i=0; $i < $num_columns; $i++) {
                    $fld[$i] = $this->rs->Fields($i);
            }
            while (!$this->rs->EOF)  //carry on looping through while there are records
            {
                for ($i=0; $i < $num_columns; $i++) {
                        $results[$fld[$i]->name] = $fld[$i]->value;
                }
                break;
            }
            if(count($results)) return $results;
            else return false;
        }

        public function doSelect($query){
            $this->query($query);
            return $this->fetchAll();
        }

        public function doSelectOne($query){
            $this->query($query);
            return $this->fetch();
        }

        public function doUpdate($query){
            $this->query($query);
        }

        public function closeConnection(){
            $this->rs->Close();
            $this->conn->Close();
            $this->rs = null;
            $this->conn = null;
        }
}
?>