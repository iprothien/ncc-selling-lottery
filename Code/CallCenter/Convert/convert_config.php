<?php
require_once("lib/config.php");
require_once("lib/functions.php");
require_once("lib/sbMysqlPDO.class.php");
$conn = NULL;

try {
    if (!$conn) {
        try {
            $conn = new sbMysqlPDO($server, $user, $password, $db);
        } catch (Exception $e) {
            file_put_contents("/var/www/service/log.txt", "\nError when connect to database server|" . date("Y-m-d H:i:s", time()));
            sleep(5);
        }
    } {
        $sqlSelect = " SELECT * FROM config WHERE convert_status=0 ORDER BY id ASC LIMIT 10 ";
        $ListConfig = $conn->doSelect($sqlSelect);
        foreach ($ListConfig as $item) {
            if (!strpos($item['value'], 'mp3')) {
                $OrgPath = "/var/www/backend/uploads/cms/config/" . $item['value'];
                $fileMp3 = "/var/www/backend/uploads/cms/config/" . getFileName($item['value']) . ".mp3";
                exec("/usr/bin/ffmpeg -i $OrgPath -y -vn -ar 44100 -ac 2 -ab 192 -f  mp3 $fileMp3");
            }

            $fileOrgPath = "/var/www/backend/uploads/cms/config/" . $item['value'];
            $fileDestPath = "/var/www/backend/uploads/system/config/" . $item['value'];
            exec("/usr/bin/ffmpeg -i $fileOrgPath -acodec pcm_alaw -ab 64k -ar 8000 -ac 1 $fileDestPath");
            rename($fileOrgPath.$item['value'], $fileDestPath.getFileName($item['value']) . ".alaw");

            $sqlUpdate = " UPDATE config SET convert_status=1 WHERE id='{$item['id']}' ";
            $conn->doUpdate($sqlUpdate);

            sleep(5);
        }
        sleep(5);
        $conn = null;
    }
} catch (Exception $e) {
    
}
?>