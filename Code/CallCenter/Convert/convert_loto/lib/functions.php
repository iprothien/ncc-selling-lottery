<?php

function getFileName($name){
    return substr( $name, 0, strrpos( $name, '.' ) );
}

function getPathStorage($rootDir, $itemId, $itemPerDir, $makeDir)
{
    $dir1 = floor($itemId / ($itemPerDir * $itemPerDir * $itemPerDir));
    $dir2 = floor(($itemId - $dir1 * $itemPerDir * $itemPerDir * $itemPerDir)/($itemPerDir * $itemPerDir));
    $temp = floor($itemId / ($itemPerDir * $itemPerDir));
    $dir3 = floor(($itemId -  $temp * $itemPerDir * $itemPerDir)/$itemPerDir);

    $path = $dir1 . "/" . $dir2 . "/" .$dir3;

    if (!file_exists($rootDir. "/" . $path) && ($makeDir))
    {
            if (!file_exists($rootDir. "/" . $dir1 . "/" . $dir2))
            {
                    if (!file_exists($rootDir. "/" . $dir1))
                    {
                            @mkdir($rootDir. "/" . $dir1, true);
                    }
                    @mkdir($rootDir. "/" . $dir1 . "/" . $dir2, true);
                    chmod($rootDir. "/" . $dir1, 0777 );
            }
            @mkdir($rootDir. "/" . $dir1 . "/" . $dir2, true);
            chmod($rootDir. "/" . $dir1 . "/" . $dir2, 0777 );
    }
    @mkdir($rootDir. "/" . $dir1 . "/" . $dir2 . "/" . $dir3, true);
    chmod($rootDir. "/" . $dir1 . "/" . $dir2 . "/" . $dir3, 0777 );
    return $path;
}
?>
