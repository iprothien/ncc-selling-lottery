<?php
require_once("lib/config.php");
require_once("lib/functions.php");
require_once("lib/sbMysqlPDO.class.php");
$conn = NULL;

try {
    if (!$conn) {
        try {
            $conn = new sbMysqlPDO($server, $user, $password, $db);
        } catch (Exception $e) {
            file_put_contents("/var/www/service/log.txt", "\nError when connect to database server|" . date("Y-m-d H:i:s", time()));
            sleep(5);
        }
    } {
        $sqlSelect = " SELECT * FROM departments WHERE convert_status=0 ORDER BY id ASC LIMIT 10 ";
        $ListConfig = $conn->doSelect($sqlSelect);
        foreach ($ListConfig as $item) {
            if (!strpos($item['path_wellcome_file'], 'mp3')) {
                $OrgPath = "/var/www/html/NCC/uploads/cms/departments/" . $item['path_wellcome_file'];
                $fileMp3 = "/var/www/html/NCC/uploads/cms/departments/" . getFileName($item['path_wellcome_file']) . ".mp3";
                exec("/usr/bin/ffmpeg -i $OrgPath -y -vn -ar 44100 -ac 2 -ab 192 -f  mp3 $fileMp3");
            }

            $fileOrgPath = "/var/www/html/NCC/uploads/cms/departments/" . $item['path_wellcome_file'];
            $fileDestPath = "/var/www/html/NCC/uploads/system/departments/" . $item['path_wellcome_file'];
            exec("/usr/bin/ffmpeg -i $fileOrgPath -acodec pcm_alaw -ab 64k -ar 8000 -ac 1 $fileDestPath");
            rename("/var/www/html/NCC/uploads/system/departments/" . $item['path_wellcome_file'], "/var/www/html/NCC/uploads/system/departments/" . getFileName($item['path_wellcome_file']) . ".alaw");

            $sqlUpdate = " UPDATE departments SET convert_status=1 WHERE id='{$item['id']}' ";
            $conn->doUpdate($sqlUpdate);

            sleep(5);
        }
        sleep(5);
        $conn = null;
    }
} catch (Exception $e) {
    
}
?>