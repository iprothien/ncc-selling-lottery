#!/bin/bash
# Set a value that we can use for a datestamp
# Our Base backup directory
BASEBACKUP="/backup/daily"

for DATABASE in `cat /backup/db-list.txt`
do
        # This is where we throw our backups.
        FILEDIR="$BASEBACKUP/$DATABASE"

        # Test to see if our backup directory exists.
        # If not, create it.
        if [ ! -d $FILEDIR ]
        then
                mkdir -p $FILEDIR
        fi

        echo -n "Exporting database:  $DATABASE"
        mysqldump --host=localhost --user=root --opt $DATABASE | gzip -c -9 > $FILEDIR/$DATABASE-`date +%Y-%m-%d`.sql.gz
        echo "      ......[ Done Exporting to local backup, now exporting for remote backup] "
        #cp $FILEDIR/$DATABASE-$DATE.sql.gz /backup/uploads/$DATABASE.sql.gz
        echo "      .......[Done]"
done

HOST=103.1.209.39  #This is the FTP servers host or IP address. 
USER=vasbackup             #This is the FTP user that has access to the server. 
PASS=backup@2014          #This is the password for the FTP user. 

# Call 1. Uses the ftp command with the -inv switches.  -i turns off interactive prompting. -n Restrains FTP from attempting the auto-login feature. -v enables verbose and progress.  

ftp -inv $HOST << EOF 

# Call 2. Here the login credentials are supplied by calling the variables. 

user $USER $PASS 

# Call 3. Here you will change to the directory where you want to put or get 
cd /home/vasbackup 
binary
# Call4.  Here you will tell FTP to put or get the file. 
put $FILEDIR/$DATABASE-`date +%Y-%m-%d`.sql.gz $DATABASE-CALLCENTER-`date +%Y-%m-%d`.sql.gz

# or 
bye 
EOF