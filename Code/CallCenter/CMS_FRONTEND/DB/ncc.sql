-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 07, 2014 at 12:11 PM
-- Server version: 5.5.32
-- PHP Version: 5.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ncc`
--
CREATE DATABASE IF NOT EXISTS `ncc` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `ncc`;

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

CREATE TABLE IF NOT EXISTS `account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL DEFAULT '',
  `canreinvite` char(3) DEFAULT 'yes',
  `context` varchar(80) DEFAULT NULL,
  `dtmfmode` varchar(7) DEFAULT NULL,
  `host` varchar(31) NOT NULL DEFAULT '',
  `port` varchar(5) NOT NULL DEFAULT '',
  `secret` varchar(80) DEFAULT NULL,
  `type` varchar(6) NOT NULL DEFAULT 'friend',
  `username` varchar(80) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `name_2` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `module` varchar(255) NOT NULL,
  `permision` tinyint(2) NOT NULL DEFAULT '1',
  `status` tinyint(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `account_id`, `username`, `password`, `module`, `permision`, `status`) VALUES
(1, 0, 'admin', '202cb962ac59075b964b07152d234b70', 'user,account,nccAgentAnswer,staff,config,statistic,memberDraw,drawTime,drawResult,serviceStatus,cdr,callCenter', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `call_note`
--

CREATE TABLE IF NOT EXISTS `call_note` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `msisdn` varchar(20) NOT NULL,
  `name` varchar(150) NOT NULL,
  `address` varchar(255) NOT NULL,
  `age` tinyint(1) NOT NULL,
  `content` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `cdr`
--

CREATE TABLE IF NOT EXISTS `cdr` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `start` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `callerid` varchar(80) NOT NULL DEFAULT '',
  `src` varchar(80) NOT NULL DEFAULT '',
  `dst` varchar(80) NOT NULL DEFAULT '',
  `dcontext` varchar(80) NOT NULL DEFAULT '',
  `channel` varchar(80) NOT NULL DEFAULT '',
  `dstchannel` varchar(80) NOT NULL DEFAULT '',
  `lastapp` varchar(80) NOT NULL DEFAULT '',
  `lastdata` varchar(80) NOT NULL DEFAULT '',
  `duration` int(11) NOT NULL DEFAULT '0',
  `billsec` int(11) NOT NULL DEFAULT '0',
  `disposition` varchar(45) NOT NULL DEFAULT '',
  `amaflags` int(11) NOT NULL DEFAULT '0',
  `accountcode` varchar(20) NOT NULL DEFAULT '',
  `userfield` varchar(255) NOT NULL DEFAULT '',
  `uniqueid` varchar(255) NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `src` (`src`),
  KEY `channel` (`channel`),
  KEY `dcontext` (`dcontext`),
  KEY `start` (`start`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `cdr`
--

INSERT INTO `cdr` (`id`, `start`, `callerid`, `src`, `dst`, `dcontext`, `channel`, `dstchannel`, `lastapp`, `lastdata`, `duration`, `billsec`, `disposition`, `amaflags`, `accountcode`, `userfield`, `uniqueid`, `status`) VALUES
(1, '0000-00-00 00:00:00', '', '2023047852', '', '', '', 'SIP/1901-2093018', '', '', 150, 0, '', 0, '', '', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `config`
--

CREATE TABLE IF NOT EXISTS `config` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `note` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `convert_status` tinyint(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `config`
--

INSERT INTO `config` (`id`, `key`, `value`, `note`, `convert_status`) VALUES
(1, 'No phone', 'no_phone.mp3', 'phone number does not exist', 0),
(2, 'Require enter', 'require_enter.mp3', 'Require enter phone number', 1),
(3, 'Require re-enter', 'require_re_enter.mp3', 'Require re-enter phone number', 1),
(4, 'Not connect', 'not_connect.mp3', 'Can not connect to phone number', 1),
(5, 'Agent Login', 'agent-login.mp3', 'Agent Login File', 1),
(6, 'Agent Logout', 'agent-logout.mp3', 'Agent Logout File', 1);

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE IF NOT EXISTS `departments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shortcode` int(10) unsigned NOT NULL,
  `name` varchar(50) NOT NULL,
  `path_wellcome_file` varchar(50) NOT NULL,
  `convert_status` tinyint(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `shortcode`, `name`, `path_wellcome_file`, `convert_status`) VALUES
(1, 8090, 'iprotech', '1_month.mp3', 1),
(2, 444, 'Phong Kinh Doanh', '2_month.mp3', 1),
(10, 3333, 'test', '10_month.mp3', 1);

-- --------------------------------------------------------

--
-- Table structure for table `draw_time`
--

CREATE TABLE IF NOT EXISTS `draw_time` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time_start` datetime NOT NULL,
  `time_end` datetime NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `draw_time`
--

INSERT INTO `draw_time` (`id`, `time_start`, `time_end`, `status`) VALUES
(1, '2014-03-12 00:00:00', '2014-03-13 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `extensions`
--

CREATE TABLE IF NOT EXISTS `extensions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `context` varchar(20) NOT NULL DEFAULT '',
  `exten` varchar(20) NOT NULL DEFAULT '',
  `priority` tinyint(4) NOT NULL DEFAULT '0',
  `app` varchar(20) NOT NULL DEFAULT '',
  `appdata` varchar(128) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `miss_call`
--

CREATE TABLE IF NOT EXISTS `miss_call` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `msisdn` varchar(20) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `miss_call`
--

INSERT INTO `miss_call` (`id`, `msisdn`, `status`, `created`) VALUES
(1, '20265649897', 2, '2014-03-27 03:06:45'),
(2, '20214234124', 1, '2014-03-24 02:46:52'),
(3, '2023047852', 1, '2014-03-24 09:27:14');

-- --------------------------------------------------------

--
-- Table structure for table `ncc_agent`
--

CREATE TABLE IF NOT EXISTS `ncc_agent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL COMMENT 'Biết Agent thuộc group nào',
  `phonenumber` varchar(15) NOT NULL COMMENT 'Số ĐT trong bảng SIP',
  `name` varchar(128) NOT NULL COMMENT 'Tên Agent',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0: Online; 1: Offline',
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `status` (`status`),
  KEY `group_id` (`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table lưu thông tin Agent' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ncc_agent_answer`
--

CREATE TABLE IF NOT EXISTS `ncc_agent_answer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `agent_id` int(11) NOT NULL COMMENT 'Mã Agent',
  `path_record` varchar(50) NOT NULL COMMENT 'File ghi âm Agent trả lời khách hàng  ',
  `cv_status` tinyint(4) NOT NULL DEFAULT '0',
  `msisdn_agent` varchar(15) DEFAULT NULL COMMENT 'Số điện thoại của Agent',
  `msisdn_member` varchar(15) NOT NULL COMMENT 'Số điện thoại Member gọi đến',
  `call_time` datetime DEFAULT NULL COMMENT 'Thời điểm gọi đến của Member tới Agent',
  `duration` int(11) DEFAULT NULL COMMENT 'Thời gian đàm thoại',
  `as_status` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `note` varchar(255) NOT NULL,
  `customer` varchar(150) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`,`agent_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Table này ghi log cuộc gọi' AUTO_INCREMENT=13 ;

--
-- Dumping data for table `ncc_agent_answer`
--

INSERT INTO `ncc_agent_answer` (`id`, `agent_id`, `path_record`, `cv_status`, `msisdn_agent`, `msisdn_member`, `call_time`, `duration`, `as_status`, `status`, `note`, `customer`, `created`, `updated`) VALUES
(6, 12, 'NULL', 0, '1901', '2023047852', '2014-02-19 04:00:23', 150, 1, 0, 'dsd', NULL, '2014-03-24 09:37:44', '2014-03-24 10:37:44'),
(11, 0, '78', 0, NULL, '201212', '2014-03-17 07:16:00', NULL, 1, 0, '', NULL, '2014-03-24 09:26:04', NULL),
(12, 3, 'NULL', 0, '800', '20265649897', '2014-03-27 04:06:45', 0, 0, 1, '', 'ssss', '2014-03-27 03:06:45', '2014-03-27 04:06:45');

-- --------------------------------------------------------

--
-- Table structure for table `ncc_agent_status`
--

CREATE TABLE IF NOT EXISTS `ncc_agent_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `agent_id` int(11) NOT NULL COMMENT 'Mã của Agent',
  `agent_login_time` datetime DEFAULT NULL COMMENT 'Thời gian Agent nhập',
  `agent_logout_time` datetime DEFAULT NULL COMMENT 'Thời gian Agent Logout',
  `duration` int(6) NOT NULL COMMENT 'Thời Agent Login trởi lại',
  PRIMARY KEY (`id`),
  KEY `id` (`id`,`agent_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Table theo dõi Agent login, logout' AUTO_INCREMENT=7 ;

--
-- Dumping data for table `ncc_agent_status`
--

INSERT INTO `ncc_agent_status` (`id`, `agent_id`, `agent_login_time`, `agent_logout_time`, `duration`) VALUES
(1, 1, '2013-10-11 11:13:10', '2013-10-12 11:13:14', 130),
(2, 1, '2014-03-17 05:48:52', '2014-03-17 06:14:39', 1547),
(4, 1, '2014-03-17 06:13:31', '2014-03-17 06:14:39', 1547),
(5, 1, '2014-03-17 06:13:38', '2014-03-17 06:14:39', 1547),
(6, 1, '2014-03-17 06:14:32', '2014-03-17 06:14:39', 1547);

-- --------------------------------------------------------

--
-- Table structure for table `ncc_group`
--

CREATE TABLE IF NOT EXISTS `ncc_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL COMMENT 'Tên group',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0: Active; 1: Disable',
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Table này phân nhóm hỗ trợ khách hàng (Sales, Support, QA...' AUTO_INCREMENT=4 ;

--
-- Dumping data for table `ncc_group`
--

INSERT INTO `ncc_group` (`id`, `name`, `status`) VALUES
(1, 'Sales', 0),
(2, 'Support', 0),
(3, 'Q&A', 0);

-- --------------------------------------------------------

--
-- Table structure for table `ncc_note`
--

CREATE TABLE IF NOT EXISTS `ncc_note` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `agent_id` int(11) NOT NULL,
  `agent_answer_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL COMMENT '0: Đã xử lý, 1: Đang xử lý, 2: Chưa xử lý',
  `name` varchar(255) NOT NULL COMMENT 'Tên note',
  `timer` datetime NOT NULL COMMENT 'Thời gian',
  `content` varchar(128) NOT NULL COMMENT 'Nội dung',
  `process_content` varchar(128) NOT NULL,
  `group_id` int(11) NOT NULL COMMENT 'Thuộc nhóm nội dung nào',
  PRIMARY KEY (`id`),
  KEY `id` (`id`,`agent_id`),
  KEY `status` (`status`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Table này ghi lại note cuộc gọi ' AUTO_INCREMENT=2 ;

--
-- Dumping data for table `ncc_note`
--

INSERT INTO `ncc_note` (`id`, `agent_id`, `agent_answer_id`, `status`, `name`, `timer`, `content`, `process_content`, `group_id`) VALUES
(1, 9, 1, 0, 'testthoi', '2013-10-12 09:46:20', 'abc', 'xyz', 2);

-- --------------------------------------------------------

--
-- Table structure for table `online_customer`
--

CREATE TABLE IF NOT EXISTS `online_customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `msisdn` varchar(20) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `online_customer`
--

INSERT INTO `online_customer` (`id`, `msisdn`, `status`, `created`) VALUES
(1, '412314123', 1, '2014-03-24 02:18:24');

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE IF NOT EXISTS `staff` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `full_name` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `departments_id` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
