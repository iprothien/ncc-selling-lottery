-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 07, 2014 at 12:10 PM
-- Server version: 5.5.32
-- PHP Version: 5.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `lotto`
--
CREATE DATABASE IF NOT EXISTS `lotto` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `lotto`;

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `module` varchar(255) NOT NULL,
  `status` tinyint(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`, `module`, `status`) VALUES
(1, 'admin', '202cb962ac59075b964b07152d234b70', 'user,drawResult,memberDraw,chargingHistory,config,smsMt,cdr', 1),
(2, 'unitel', '14a03de8ffad6c632266c37d60707896', 'user,member,chargingHistory,category,news,config,cdr', 1);

-- --------------------------------------------------------

--
-- Table structure for table `cdr`
--

CREATE TABLE IF NOT EXISTS `cdr` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `start` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `callerid` varchar(80) NOT NULL DEFAULT '',
  `src` varchar(80) NOT NULL DEFAULT '',
  `dst` varchar(80) NOT NULL DEFAULT '',
  `dcontext` varchar(80) NOT NULL DEFAULT '',
  `channel` varchar(80) NOT NULL DEFAULT '',
  `dstchannel` varchar(80) NOT NULL DEFAULT '',
  `lastapp` varchar(80) NOT NULL DEFAULT '',
  `lastdata` varchar(80) NOT NULL DEFAULT '',
  `duration` int(11) NOT NULL DEFAULT '0',
  `billsec` int(11) NOT NULL DEFAULT '0',
  `disposition` varchar(45) NOT NULL DEFAULT '',
  `amaflags` int(11) NOT NULL DEFAULT '0',
  `accountcode` varchar(20) NOT NULL DEFAULT '',
  `userfield` varchar(255) NOT NULL DEFAULT '',
  `uniqueid` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `src` (`src`),
  KEY `channel` (`channel`),
  KEY `dcontext` (`dcontext`),
  KEY `start` (`start`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `charging_history`
--

CREATE TABLE IF NOT EXISTS `charging_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `msisdn` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `time_charge` datetime NOT NULL,
  `money` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `config`
--

CREATE TABLE IF NOT EXISTS `config` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `note` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `convert_status` tinyint(2) NOT NULL,
  `time_start` datetime DEFAULT NULL,
  `time_end` datetime DEFAULT NULL,
  `group` tinyint(2) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=26 ;

--
-- Dumping data for table `config`
--

INSERT INTO `config` (`id`, `key`, `value`, `note`, `convert_status`, `time_start`, `time_end`, `group`) VALUES
(1, 'Main Menu', 'main_menu.mp3', 'Press 1 to buy Lotto, 2 to get Draw Result, 3 to know method buy...', 0, NULL, NULL, 1),
(2, 'Welcome', 'welcome.mp3', 'Welcome to Lotto', 0, NULL, NULL, 1),
(3, 'Promotion', 'promotion.mp3', 'Promotion Voice', 0, NULL, NULL, 1),
(4, 'Method to Buy', 'method.mp3', 'Method to Buy Lotto', 0, NULL, NULL, 1),
(5, 'Introduce', 'introduce.mp3', 'Introduce how to press buy lotto', 0, NULL, NULL, 1),
(6, 'Invalid', 'invalid.mp3', 'Invalid, press wrong syntax to buy Lotto...', 0, NULL, NULL, 1),
(7, 'You want to buy', 'youwant.mp3', 'You want to buy (number abc)...', 0, NULL, NULL, 1),
(8, 'With (Money for Code)', 'with.mp3', 'With (Money for Code)...', 0, NULL, NULL, 1),
(9, 'Confirm', 'confirm.wav', 'kips. Please press 1 to confirm, press 2 to Cancel', 0, NULL, NULL, 1),
(10, 'You Buy', 'youbuy.mp3', 'You have buy number xxx...', 0, NULL, NULL, 1),
(11, 'Successful', 'buysuccess.mp3', 'number XXX successful', 0, NULL, NULL, 1),
(12, 'Not Enough', 'notenough.wav', 'Your Balance not enough money to buy lotto', 0, NULL, NULL, 1),
(13, 'System close', 'close.mp3', 'System close until the draw completed...', 0, NULL, NULL, 1),
(14, 'get the lastest result on...', 'menu_result_1.mp3', 'Press 1 to get the lastest result on...', 0, NULL, NULL, 1),
(15, 'Press 2 to get result on dd-mm-yyyy', 'menu_result_2.mp3', 'Press 2 to get result on dd-mm-yyyy', 0, NULL, NULL, 1),
(16, 'Press 3 to input the date...', 'menu_result_3.mp3', 'Press 3 to input the date you want. Press 0 to back main menu', 0, NULL, NULL, 1),
(17, 'Month', 'month.wav', 'Month (dd-mm-yy)', 0, NULL, NULL, 1),
(18, 'Please Input date you want', 'input_date.mp3', 'Please Input date you want. Press * to back', 0, NULL, NULL, 1),
(19, 'No draw', 'no_draw.wav', 'We have no draw on this day', 0, NULL, NULL, 1),
(20, 'Result', 'result.mp3', 'The result will be send to you by sms after this call', 0, NULL, NULL, 1),
(21, 'Charge 1000kips', 'charge1000.mp3', 'You do not have code for this draw.you will be charge 1000 kip. Press 1 co ok, 2 to cancel', 0, NULL, NULL, 1),
(22, 'Early Time To Draw', '', 'Set time_start and time_end to play draw', 0, '2013-10-09 00:00:00', '2013-10-10 00:00:00', 2),
(23, 'Number Full Bought', 'full_bought.mp3', 'That Number is full bought', 0, NULL, NULL, 1),
(24, 'abc', 'abc', 'abc', 1, NULL, NULL, 1),
(25, 'def', 'def', 'def', 1, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `draw_result`
--

CREATE TABLE IF NOT EXISTS `draw_result` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(1024) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'so trung thuong',
  `created_datetime` datetime NOT NULL COMMENT 'ngày quay xổ số',
  `type` tinyint(2) NOT NULL COMMENT '1.loại 2 sô 2.loại 3 số',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `member_draw`
--

CREATE TABLE IF NOT EXISTS `member_draw` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `msisdn` varchar(16) NOT NULL,
  `code` varchar(3) NOT NULL COMMENT 'Mã xổ số KH đăng ký mua',
  `money` int(10) NOT NULL,
  `created_datetime` datetime NOT NULL,
  `type` tinyint(2) NOT NULL COMMENT '1.loại 2 sô 2.loại 3 số',
  `status` tinyint(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `member_draw`
--

INSERT INTO `member_draw` (`id`, `msisdn`, `code`, `money`, `created_datetime`, `type`, `status`) VALUES
(1, '900', '012', 3000, '2013-10-08 15:14:39', 2, 1),
(2, '900', '123', 5000, '2014-01-21 00:00:00', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `quocta`
--

CREATE TABLE IF NOT EXISTS `quocta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `max_two` int(11) NOT NULL,
  `max_three` int(11) NOT NULL,
  `time_start` datetime NOT NULL,
  `time_end` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `quocta`
--

INSERT INTO `quocta` (`id`, `max_two`, `max_three`, `time_start`, `time_end`) VALUES
(2, 10000, 50000, '2014-01-22 16:32:41', '2014-02-01 01:01:01');

-- --------------------------------------------------------

--
-- Table structure for table `sms_mt`
--

CREATE TABLE IF NOT EXISTS `sms_mt` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `short_code` int(11) DEFAULT NULL,
  `msisdn` varchar(16) DEFAULT NULL,
  `status` int(4) DEFAULT NULL,
  `type` int(4) DEFAULT NULL COMMENT '1. text; 2. wappush',
  `content` varchar(512) DEFAULT NULL,
  `created_datetime` datetime DEFAULT NULL,
  `updated_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
