<style>
    .loader {
        position: fixed;
        left: 0px;
        top: 0px;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: url('img/page-loader.gif') 50% 50% no-repeat rgb(249,249,249);
    }
</style>
<?php
    mysql_connect('localhost', 'root', '');
    mysql_select_db('lotto');
    $sell   = array();
    
    $sql1   = mysql_query('SELECT SUM(money) as t1 FROM member_draw WHERE type=1');
    $rs1    = mysql_fetch_assoc($sql1);
    if($rs1)
        $sell['t1'] = $rs1['t1'];
    else
        $sell['t1'] = 0;
    
    $sql2  = mysql_query('SELECT SUM(money) as t2 FROM member_draw WHERE type=2');
    $rs2    = mysql_fetch_assoc($sql2);
    if($rs2)
        $sell['t2'] = $rs2['t2'];
    else
        $sell['t2'] = 0;
    
    $sqlmax = mysql_query('SELECT max_two,max_three FROM quocta WHERE time_start<"'.date('Y-m-d H:i:s',time()).'" AND time_end>"'.date('Y-m-d H:i:s',time()).'"');
    $rs3    = mysql_fetch_assoc($sqlmax);
    if($rs3) {
        $tmax = number_format($rs3['max_two'] + $rs3['max_three']);
        $t2 = number_format($rs3['max_two']);
        $t3 = number_format($rs3['max_three']);
    } else {
        $tmax = number_format(20000000);
        $t2 = number_format(10000000);
        $t3 = number_format(10000000);
    }
    
?>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/JavaScript">
    $(window).load(function() {
        $(".loader").fadeOut("slow");
    })
</script>
<div class="loader"></div>
<div id="selling_status">
    <p style="font-weight: bold;color:#636363">Selling Status</p>
    <div class="line">
        <span class="group1">Total</span>
        <span class="group2"><?php echo $smax = number_format($sell['t1'] + $sell['t2']); ?></span>
        <span class="group3"><?php echo round($smax/$tmax*100); ?> %</span>
    </div>
    <div class="line">
        <span class="group1">2digit</span>
        <span class="group2"><?php echo $s2 = number_format($sell['t1']); ?></span>
        <span class="group3"><?php echo round($s2/$t2*100); ?> %</span>
    </div>
    <div class="line">
        <span class="group1">3digit</span>
        <span class="group2"><?php echo $s3 = number_format($sell['t2']); ?></span>
        <span class="group3"><?php echo round($s3/$t3*100); ?> %</span>
    </div>
</div>