<?php
$ip = $_SERVER['REMOTE_ADDR'];

if(!substr_count($ip,'192.168') && $ip != '103.240.242.59' && $ip != '27.118.30.2' && $ip!="183.182.127.195" && $ip!="115.84.84.209") exit();

// change the following paths if necessary
$yii=dirname(__FILE__).'/framework/yii.php';
$config=dirname(__FILE__).'/protected/config/main.php';

// remove the following lines when in production mode
defined('YII_DEBUG') or define('YII_DEBUG',true);
// specify how many levels of call stack should be shown in each log message
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);
define('YII_ENABLE_ERROR_HANDLER', false);
define('YII_ENABLE_EXCEPTION_HANDLER', false);

require_once($yii);
Yii::createWebApplication($config)->run();
