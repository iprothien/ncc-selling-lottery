<?php

class SiteController extends Controller {

    /**
     * Declares class-based actions.
     */
    public $defaultAction = 'login';

    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex() {
        $this->render('index');
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError() {
        $this->render('error');
    }
    
    public function actionSetlang() {
        unset(Yii::app()->request->cookies['_langhandle']);
        $lang   = Yii::app()->request->getQuery('lang');
        Yii::app()->request->cookies['_langhandle'] = new CHttpCookie('_langhandle', $lang);
        $this->redirect($_SERVER['HTTP_REFERER']);
    }

    /**
     * Displays the login page
     */
    public function actionLogin() {
        $model = new LoginForm;

        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        if (isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];
            if ($model->validate() && $model->login())
                $this->redirect(Yii::app()->user->returnUrl);
        }
        $this->render('login', array('model' => $model));
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout() {
        Yii::app()->request->cookies->clear();
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }

}