<?php

class SellingController extends AccessController {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';
    public $module = 'selling';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    public function accessRules() {
        return array(
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('buy', 'test'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionBuy() {
        $this->menuActive   = 'callCenter';
        $msisdn = Yii::app()->request->getParam('msisdn');
	 $msisdn = base64_decode($msisdn);
        $model = Selling::model()->findByAttributes(array('msisdn' => $msisdn));

        if (!$model) {
            $model = new Selling;
            $model->name = '';
            $model->address = '';
        }

        if (substr($msisdn, 0, 3) == '202' || substr($msisdn, 0, 3) == '302') {
            //Etl
            $urlInfo = 'http://202.62.111.233/buy_agent/info.php?msisdn=' . $msisdn;
            $info = Selling::model()->viewInfo($urlInfo);
            if ($info)
                $rsInfor = $info;
            else
                Yii::app()->user->setFlash('statusConnect', "Status: Error (Error connect to server)");
        } else if (substr($msisdn, 0, 3) == '207' || substr($msisdn, 0, 3) == '307') {
            //Beeline
            $urlInfo = "http://115.84.105.43:8888/buy_agent/info.php?msisdn=".$msisdn;		
		//echo $urlInfo;
            $info = Selling::model()->viewInfo($urlInfo);
		//var_dump($info);
            if ($info)
                $rsInfor = $info;
            else
                Yii::app()->user->setFlash('statusConnect', "Status: Error (Error connect to server)");
        } else if (substr($msisdn, 0, 3) == '209' || substr($msisdn, 0, 3) == '309') {
            //Etl
            $urlInfo = 'http://183.182.100.131/buy_agent/info.php?msisdn=' . $msisdn;
            $info = Selling::model()->viewInfo($urlInfo);
            if ($info)
                $rsInfor = $info;
            else
                Yii::app()->user->setFlash('statusConnect', "Status: Error (Error connect to server)");
        }

        $sqlAgent = 'SELECT account.username FROM admin INNER JOIN account ON admin.account_id=account.id && admin.id=' . Yii::app()->user->id;
        $cmdAgent = Yii::app()->db->createCommand($sqlAgent);
        $rsAgent = $cmdAgent->queryRow();

        $j = 0;
        for ($j; $j < 6; $j++) {
            if (isset($_POST['listCode'][$j]) && $_POST['listCode'][$j])
                $listCode[] = $_POST['listCode'][$j];
            if (isset($_POST['listAmount'][$j]) && $_POST['listAmount'][$j])
                $listAmount[] = $_POST['listAmount'][$j];
        }


        $code = '';
        $amount = '';
        if (isset($listCode) && isset($listAmount)) {

            $code = implode(':', $listCode);
            $amount = implode(':', $listAmount);

            if ($code != '' && $amount != '') {
                $urlBuy = '';
                $rsDraw = ServiceStatus::model()->getTime();
                if (isset($rsDraw) && $rsDraw) {
                    if (substr($msisdn, 0, 3) == '202' || substr($msisdn, 0, 3) == '302') {
                        $urlBuy = 'http://202.62.111.233/buy_agent/charge.php?msisdn=' . $msisdn . '&code=' . $code . '&money=' . $amount . '&draw_id=' . $rsDraw['id'] . '&agent_id=' . $rsAgent['username'];
                        $rsBuy = Selling::model()->buyLotto($urlBuy);
                    } else if (substr($msisdn, 0, 3) == '207' || substr($msisdn, 0, 3) == '307') {
                        $urlBuy = 'http://115.84.105.43:8888/buy_agent/charge.php?msisdn=' . $msisdn . '&code=' . $code . '&money=' . $amount . '&draw_id=' . $rsDraw['id'] . '&agent_id=' . $rsAgent['username'];
                        $rsBuy = Selling::model()->buyLotto($urlBuy);
                    } else if (substr($msisdn, 0, 3) == '209' || substr($msisdn, 0, 3) == '309') {
                        $urlBuy = 'http://183.182.100.131/buy_agent/charge.php?msisdn=' . $msisdn . '&code=' . $code . '&money=' . $amount . '&draw_id=' . $rsDraw['id'] . '&agent_id=' . $rsAgent['username'];
                        $rsBuy = Selling::model()->buyLotto($urlBuy);
                    }

                    if (isset($rsBuy) && $rsBuy) {
                        $listStatus = explode(',', $rsBuy[0]->status);
                        $listContent = explode(',', $rsBuy[0]->content);
                        $tBalance = explode(',', $rsBuy[0]->balance_before);
                        $sBalance = explode(',', $rsBuy[0]->balance_after);

                        $count = count($listStatus);
                        $i = 0;
                        $viewSuccess = '';
                        $viewError = '';
                        for ($i; $i < $count; $i++) {
                            $memberId = 0;
                            $result = explode(':', $listStatus[$i]);

                            if ($result[2] == 1) {
                                $sqlInsertMember = 'INSERT INTO member_draw(msisdn,code,money,created_datetime,type,status,draw_id,buy_type,agent_id)
                                                        VALUES ("' . $msisdn . '",
                                                        "' . $result[0] . '",
                                                        "' . $result[1] . '",
                                                        now(),
                                                        "' . (strlen($result[0]) - 1) . '",
                                                        "1",
                                                        "' . $rsDraw['id'] . '",
                                                        "1",
                                                        "' . $rsAgent['username'] . '"
                                                        )';

                                Yii::app()->db->createCommand($sqlInsertMember)->execute();

                                $memberId = Yii::app()->db->createCommand('SELECT MAX(id) as maxId FROM member_draw WHERE msisdn="' . $msisdn . '" && status=1')->queryScalar();

                                $smsSuccess[] = 'Digit: ' . $result[0] . ', Amount: ' . $result[1] . ' KIP';
                            } else {
                                if ($result[2] == 2)
                                    $text = "The current lot is now closed for drawing";
                                else if ($result[2] == 3)
                                    $text = "The postpaid phone number can't buy lottery";
                                else if ($result[2] == 4)
                                    $text = "The phone number is not enough money in balance to buy lottory";
                                else if ($result[2] == 5)
                                    $text = "That Number has been sold out";
                                else if ($result[2] == 6)
                                    $text = "System can't charge money";
                                else if ($result[2] == 7)
                                    $text = "Connect to SMS Lottery fail";

                                $smsError[] = 'Digit: ' . $result[0] . ', Amount: ' . $result[1] . ' KIP, Reason: ' . $text;
                            }

                            if (isset($rsBuy) && $rsBuy && isset($memberId) && $memberId) {
                                $sqlInsertTrans = 'INSERT INTO transaction_log(msisdn,code,money,created_datetime,type,status,draw_id,buy_type,agent_id,content,balance_before,balance_after,member_id)
                                                        VALUES ("' . $msisdn . '",
                                                        "' . $result[0] . '",
                                                        "' . $result[1] . '",
                                                        now(),
                                                        "' . (strlen($result[0]) - 1) . '",
                                                        "' . $result[2] . '",
                                                        "' . $rsDraw['id'] . '",
                                                        "1",
                                                        "' . $rsAgent['username'] . '",
                                                        "' . $listContent[$i] . '",
                                                        "' . $tBalance[$i] . '",
                                                        "' . $sBalance[$i] . '",
                                                        "' . $memberId . '"
                                                        )';
                                Yii::app()->db->createCommand($sqlInsertTrans)->execute();
                            }
                        }
                        if (isset($smsSuccess) && $smsSuccess)
                            $viewSuccess = "<span style='color:#219648'>You successfuly bought lottery with:<br />"
                                    . implode("<br />", $smsSuccess)."</span>";
                        if (isset($smsError) && $smsError)
                            $viewError = "<br />And Error bought lottery with: <br />"
                                    . implode("<br />", $smsError);

                        Yii::app()->user->setFlash('submitBuying', $viewSuccess . $viewError);
                    }

                    Yii::app()->controller->refresh();
                } else {
                    Yii::app()->user->setFlash('submitBuying', "The current lot is now closed for drawing");
                }
            }
        }
        
        $quota  = ServiceStatus::model()->getQuota();

        if (isset($_POST['Selling'])) {
            $model->code = $_POST['Selling']['code'];
            $model->amount = $_POST['Selling']['amount'];
            $rsDraw = ServiceStatus::model()->getTime();
        }

        if (isset($rsInfor) && $rsInfor)
            $this->render('test', array(
                'model' => $model,
                'msisdn' => $msisdn,
                'rsInfor' => $rsInfor,
                'quota' => $quota
            ));
        else
            $this->render('test', array(
                'model' => $model,
                'msisdn' => $msisdn,
                'quota' => $quota
            ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = Selling::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'member-draw-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
