<?php
class CallCenterController extends AccessController {

    public function __construct($id, $module = null) {

        if ($this->hasCookie('_langhandle'))
            $lang = $this->getCookie('_langhandle');
        else
            $lang = Yii::app()->user->getLang();

        if ($lang && in_array($lang, CLocale::getLocaleIDs())) {
            Yii::app()->setLanguage($lang);
        }
        return parent::__construct($id, $module);
    }

    public $layout='//layouts/column2';
    public $module = 'callCenter';

    public function actionLoadProvince(){
        $loadType = isset($_POST['loadType']) ? $_POST['loadType'] : 0;
        $loadId =  isset($_POST['loadId']) ? $_POST['loadId'] : 0;
        if($loadType=="district"){
            $sql = "SELECT * FROM province WHERE `type`=2 AND `parent_id` =".$loadId;
        }else if($loadType=="ward"){
            $sql = "SELECT * FROM province WHERE `type`=3 AND `parent_id` =".$loadId;
        }else if($loadType=="village"){
            $sql = "SELECT * FROM province WHERE `type`=4 AND `parent_id` =".$loadId;
        }

        $result = Yii::app()->db->createCommand($sql)->queryAll();
        if($result){
            $html = '';
            foreach($result as $row){
                $html .="<option value='".$row['id']."'>".$row['name']."</option>";
            }
            echo $html;
        }
    }

    public function actionAdmin(){
$this->menuActive   = 'callCenter';

        $limit=20;
        $currentPage = isset($_GET['page']) ? $_GET['page'] : 1;
        $startRecord = ($currentPage - 1) * $limit;
        $callCenter = CallCenter::model();
        $msisdn = isset($_REQUEST['m_search'])?$_REQUEST['m_search']:NULL;
        if(isset($_POST['history'])){
            $count = $callCenter->countHistoryCall($msisdn);
            $numberPage = ceil($count / $limit);
            $history = $callCenter->findHistoryCall($msisdn,$startRecord,$limit);
        }else{
            $count = $callCenter->countHistoryCall(NULL);
            $numberPage = ceil($count / $limit);
            $history = $callCenter->findHistoryCall(NULL,$startRecord,$limit);
        }

        if(isset($_POST['current'])){
            $current = $callCenter->findOnlineCall($msisdn);
        }else{
            $current = $callCenter->findOnlineCall();
        }

        if(isset($_POST['missed'])){
            $miss = $callCenter->findMissCall($msisdn,0,5);
        }else{
            $miss = $callCenter->findMissCall(NULL,0,5);
        }

        $this->render('admin',array(
            'history' => $history,
            'current' => $current,
            'miss' => $miss,
            'currentPage'   => $currentPage,
            'numberPage'    =>  $numberPage,
            'startRecord'   => $startRecord,
        ));
    }
    
    public function  actionOnline(){
	    $this->layout='//layouts/blank';
        $msisdn = isset($_REQUEST['m_search'])?$_REQUEST['m_search']:NULL;
        $callCenter = CallCenter::model();
        if(isset($_POST['current'])){$current = $callCenter->findOnlineCall($msisdn);}else{$current = $callCenter->findOnlineCall();}

        $this->render('_online',array(
            'current'=> $current,
            'msisdn' => $msisdn,
        ));
    }

    public function  actionMiss(){
	 $this->layout='//layouts/blank';
        $msisdn = isset($_REQUEST['m_search'])?$_REQUEST['m_search']:NULL;

        if(isset($_POST['missed'])){
            $miss = CallCenter::model()->findMissCall($msisdn,0,5);
        }else{
            $miss = CallCenter::model()->findMissCall(NULL,0,5);
        }
        $this->render('_miss',array(
            'miss'=>$miss,
            'msisdn'=>$msisdn,
        ));
    }

    public function actionSms() {
$this->menuActive   = 'callCenter';

        $msisdn = isset($_REQUEST['m_search'])?$_REQUEST['m_search']:NULL;
        $callCenter = CallCenter::model();

        if(isset($_POST['current'])){
            $current = $callCenter->findOnlineCall($msisdn);
        }else{
            $current = $callCenter->findOnlineCall();
        }

        if(isset($_POST['missed'])){
            $miss = $callCenter->findMissCall($msisdn,0,5);
        }else{
            $miss = $callCenter->findMissCall(NULL,0,5);
        }

        $this->render('sms',array(
            'current' => $current,
            'miss' => $miss,
        ));
    }

    public function actionIvr() {
$this->menuActive   = 'callCenter';

        $msisdn = isset($_GET['ServiceStatus']['msisdn']) ? $_GET['ServiceStatus']['msisdn'] : NULL;

        if (substr($msisdn, 0, 3) == '207' || substr($msisdn, 0, 3) == '307') {
            if (empty($_GET['ServiceStatus2']['msisdn'])) {
                $_GET['ServiceStatus2']['msisdn'] = $msisdn;
            }
            $model = new ServiceStatus2('search');
            $model->unsetAttributes();  // clear any default values
            if (isset($_GET['ServiceStatus2'])) {
                $model->attributes = $_GET['ServiceStatus2'];
            }
        } elseif (substr($msisdn, 0, 3) == '202' || substr($msisdn, 0, 3) == '203') {
            if (empty($_GET['ServiceStatus3']['msisdn'])) {
                $_GET['ServiceStatus3']['msisdn'] = $msisdn;
            }
            $model = new ServiceStatus3('search');
            $model->unsetAttributes();  // clear any default values
            if (isset($_GET['ServiceStatus3'])) {
                $model->attributes = $_GET['ServiceStatus3'];
            }
        } elseif (substr($msisdn, 0, 3) == '209' || substr($msisdn, 0, 3) == '309') {
            if (empty($_GET['ServiceStatus4']['msisdn'])) {
                $_GET['ServiceStatus4']['msisdn'] = $msisdn;
            }
            $model = new ServiceStatus4('search');
            $model->unsetAttributes();  // clear any default values
            if (isset($_GET['ServiceStatus4'])) {
                $model->attributes = $_GET['ServiceStatus4'];
            }
        } else {
            $model = new ServiceStatus('search');
            $model->unsetAttributes();  // clear any default values
        }


        $sell = array();
        $rs_time = ServiceStatus::model()->getTime();
        if($rs_time) {
            $sell['start_time'] = $rs_time['start_time'];
            $sell['end_time']   = $rs_time['end_time'];
        }

        $rsBeeline = ServiceStatus2::model()->getSellingStatus();
        $rsEtl = ServiceStatus3::model()->getSellingStatus();
        $rsUnitel   = ServiceStatus4::model()->getSellingStatus();

        $sell['t1'] = $rsBeeline['t1'];
        $sell['t2'] = $rsBeeline['t2'];
        $sell['t3'] = $rsEtl['t3'];
        $sell['t4'] = $rsEtl['t4'];
        $sell['t5'] = $rsUnitel['t1'];
        $sell['t6'] = $rsUnitel['t2'];

        $rs_max = ServiceStatus::model()->getQuota();
        if ($rs_max) {
            $sell['max'] = $rs_max['max_two'] + $rs_max['max_three'];
            $sell['max_two'] = $rs_max['max_two'];
            $sell['max_three'] = $rs_max['max_three'];
        } else {
            $sell['max'] = 20000000;
            $sell['max_two'] = 10000000;
            $sell['max_three'] = 10000000;
        }

	    $searchMsisdn = isset($_REQUEST['m_search'])?$_REQUEST['m_search']:NULL;
        $callCenter = CallCenter::model();

        if(isset($_POST['current'])){
            $current = $callCenter->findOnlineCall($searchMsisdn);
        }else{
            $current = $callCenter->findOnlineCall();
        }

        if(isset($_POST['missed'])){
            $miss = $callCenter->findMissCall($searchMsisdn,0,5);
        }else{
            $miss = $callCenter->findMissCall(NULL,0,5);
        }

        $this->render('ivr', array(
            'model' => $model,
            'current' => $current,
            'miss' => $miss,
            'sell' => $sell,
            'msisdn' => $msisdn,
        ));
    }

    public function actionMissCall(){
$this->menuActive   = 'callCenter';

        $searchMsisdn = isset($_REQUEST['m_search'])?$_REQUEST['m_search']:NULL;
        $callCenter = CallCenter::model();

        if(isset($_POST['current'])){
            $current = $callCenter->findOnlineCall($searchMsisdn);
        }else{
            $current = $callCenter->findOnlineCall();
        }

        if(isset($_POST['missed'])){
            $miss = $callCenter->findMissCall($searchMsisdn,0,5);
        }else{
            $miss = $callCenter->findMissCall(NULL,0,5);
        }

        $limit=20;
        $currentPage = isset($_GET['page']) ? $_GET['page'] : 1;
        $startRecord = ($currentPage - 1) * $limit;
        $historyMiss = isset($_REQUEST['sText'])?$_REQUEST['sText']:NULL;
        $startDate = isset($_REQUEST['start'])?$_REQUEST['start']:NULL;
        $endDate = isset($_REQUEST['end'])?$_REQUEST['end']:NULL;
        $formatStartDate = $startDate.' '.'00:00:00';
        $formatEndDate = $endDate.' '.'23:59:59';

        if(isset($_POST['missCall']) && $historyMiss && $startDate && $endDate){
            $count = $callCenter->countMissCall($historyMiss,$formatStartDate,$formatEndDate);
            $numberPage = ceil($count / $limit);
            $hMissCall = $callCenter->searchMissCall($historyMiss,$formatStartDate,$formatEndDate,$startRecord,$limit);
        }elseif(isset($_POST['missCall']) && $startDate && $endDate){
            $count = $callCenter->countMissCall(NULL,$formatStartDate,$formatEndDate);
            $numberPage = ceil($count / $limit);
            $hMissCall = $callCenter->searchMissCall(NULL,$formatStartDate,$formatEndDate,$startRecord,$limit);
        }elseif(isset($_POST['missCall']) && $startDate){
            $count = $callCenter->countMissCall(NULL,$formatStartDate,NULL);
            $numberPage = ceil($count / $limit);
            $hMissCall = $callCenter->searchMissCall(NULL,$formatStartDate,NULL,$startRecord,$limit);
        }elseif(isset($_POST['missCall']) && $endDate){
            $count = $callCenter->countMissCall(NULL,NULL,$formatEndDate);
            $numberPage = ceil($count / $limit);
            $hMissCall = $callCenter->searchMissCall(NULL,NULL,$formatEndDate,$startRecord,$limit);
        }elseif($startDate){
            $count = $callCenter->countMissCall(NULL,$formatStartDate,NULL);
            $numberPage = ceil($count / $limit);
            $hMissCall = $callCenter->searchMissCall(NULL,$formatStartDate,NULL,$startRecord,$limit);
        }elseif($endDate){
            $count = $callCenter->countMissCall(NULL,NULL,$formatEndDate);
            $numberPage = ceil($count / $limit);
            $hMissCall = $callCenter->searchMissCall(NULL,NULL,$formatEndDate,$startRecord,$limit);
        }elseif(isset($_POST['missCall']) && $historyMiss){
            $count = $callCenter->countMissCall($historyMiss,NULL,NULL);
            $numberPage = ceil($count / $limit);
            $hMissCall = $callCenter->searchMissCall($historyMiss,NULL,NULL,$startRecord,$limit);
        } else{
            $count = $callCenter->countMissCall(NULL);
            $numberPage = ceil($count / $limit);
            $hMissCall = $callCenter->searchMissCall(NULL,NULL,NULL,$startRecord,$limit);
        }


        $this->render('historyNote', array(
            'current' => $current,
            'miss' => $miss,
            'hMissCall' => $hMissCall,
            'currentPage'   => $currentPage,
            'numberPage'    =>  $numberPage,
            'startRecord'   => $startRecord,
	     'start' => $startDate,
            'end' => $endDate,
        ));
    }

    public function actionNote(){
$this->menuActive   = 'callCenter';

        $callCenter = CallCenter::model();
        $sms = new Service();
        $callCenter1 = CallCenterDB3::model();
        $callCenter2 = CallCenterDB4::model();
        $callCenter3 = CallCenterDB5::model();
        $message='';
        $provinceId='';
        $id_agent = Yii::app()->user->getId();
        $id = isset($_REQUEST['id'])?$_REQUEST['id']:NULL;
        $hack = $callCenter->checkHackMissCall($id);
        if($hack){
            $province = $callCenter->getProvince();
            $msisdn = $callCenter->getMissCall($id);
            $checkMsisdn = substr($msisdn['msisdn'],0,3);
            if($checkMsisdn=='207' || $checkMsisdn=='307'){
                $countPurchare = $callCenter1->countHistoryCallPur($msisdn['msisdn']);
                $sumPurchare = $callCenter1->sumHistoryCallPur($msisdn['msisdn']);
                $historyLotto = $callCenter1->getHistory($msisdn['msisdn']);
            }elseif($checkMsisdn=='202' || $checkMsisdn=='203'){
                $countPurchare = $callCenter2->countHistoryCallPur($msisdn['msisdn']);
                $sumPurchare = $callCenter2->sumHistoryCallPur($msisdn['msisdn']);
                $historyLotto = $callCenter2->getHistory($msisdn['msisdn']);
            }elseif($checkMsisdn=='209' || $checkMsisdn=='309'){
                $countPurchare = $callCenter3->countHistoryCallPur($msisdn['msisdn']);
                $sumPurchare = $callCenter3->sumHistoryCallPur($msisdn['msisdn']);
                $historyLotto = $callCenter3->getHistory($msisdn['msisdn']);
            }else{
                $countPurchare = $callCenter->countHistoryCallPur($msisdn['msisdn']);
                $sumPurchare = $callCenter->sumHistoryCallPur($msisdn['msisdn']);
                $historyLotto = $callCenter->getHistory($msisdn['msisdn']);
            }

            $detailNote = $callCenter->getCallNote($msisdn['msisdn']);
            $edProvince = $callCenter->findProvince($msisdn['msisdn']);
            $purchaseSMS = explode('|',$sms->SaleHistory($msisdn['msisdn'])->SaleHistoryResult);
            $agent = $callCenter->getPhoneAgent();

            if($_SERVER['REQUEST_METHOD'] == 'POST'){
                $name = isset($_REQUEST['name'])?$_REQUEST['name']:NULL;
                $address = isset($_REQUEST['address'])?$_REQUEST['address']:NULL;
                $age = isset($_REQUEST['age'])?$_REQUEST['age']:NULL;
                $content = isset($_REQUEST['content'])?$_REQUEST['content']:NULL;
                $status = isset($_REQUEST['status'])?$_REQUEST['status']:NULL;
                $comment = isset($_REQUEST['note'])?$_REQUEST['note']:NULL;

                $provinceNote = isset($_REQUEST['province']) ? $_REQUEST['province'] : NULL;
                $districtNote = isset($_REQUEST['district']) ? $_REQUEST['district'] : NULL;
                $wardNote = isset($_REQUEST['ward']) ? $_REQUEST['ward'] : NULL;
                $villageNote = isset($_REQUEST['village']) ? $_REQUEST['village'] : NULL;
                if(empty($status)){$error[]='Status not Empty';}
                /// Submit form
                if(!empty($error)){
                    $message = '<p style="font-size: 16px;color: chocolate;">Message Error:</p><ul>';
                    foreach($error as $e) {$message .= '<li>'.$e.'</li>';}
                    $message .= '</ul>';
                }else{
                    if(!empty($provinceNote) && !empty($districtNote) && !empty($wardNote) && !empty($villageNote)){
                        $provinceId = $provinceNote.','.$districtNote.','.$wardNote.','.$villageNote;
                    }elseif(!empty($provinceNote) && !empty($districtNote) && !empty($wardNote)){
                        $provinceId = $provinceNote.','.$districtNote.','.$wardNote;
                    }elseif(!empty($provinceNote) && !empty($districtNote)){
                        $provinceId = $provinceNote.','.$districtNote;
                    }elseif(!empty($provinceNote)){
                        $provinceId = $provinceNote;
                    }
                    if($status==2){
                        $callCenter->createAnswerMiss($id_agent,$comment,$msisdn['msisdn'],$agent,$name);
                        $callCenter->updateMissCall($id);
                    }
                    $checkNote = $callCenter->checkNote($msisdn['msisdn']);
                    if($checkNote){
                        $callCenter->updateNote($msisdn['msisdn'],$name,$address,$age,$content,$status,$provinceId);
                        $this->redirect(Yii::app()->createUrl('callCenter/admin'));
                    }else{
                        $callCenter->createNote($msisdn['msisdn'],$name,$address,$age,$content,$status,$id,NULL,$provinceId);
                        $this->redirect(Yii::app()->createUrl('callCenter/admin'));
                    }
                }
            }
        }else{
            $this->redirect(Yii::app()->createUrl('callCenter/Hack'));
        }

        $this->render('note',array(
                'historyLotto'  => $historyLotto,
                'message'       => $message,
                'detail'        => $detailNote,
                'count'         => $countPurchare,
                'sum'           => $sumPurchare,
                'msisdn'        => $msisdn['msisdn'],
                'purchaseSMS'   => $purchaseSMS,
                'id'            => $id,
                'province'      => $province,
                'edProvince' => $edProvince,
        ));
    }

    public function actionMissNote(){
$this->menuActive   = 'callCenter';

        $callCenter = CallCenter::model();
        $sms = new Service();
        $callCenter1 = CallCenterDB3::model();
        $callCenter2 = CallCenterDB4::model();
        $callCenter3 = CallCenterDB5::model();
        $message='';
        $id_agent = Yii::app()->user->getId();
        $id = isset($_REQUEST['id'])?$_REQUEST['id']:NULL;
        $hack = $callCenter->checkHackMissCall($id);
        if($hack){
            $province = $callCenter->getProvince();
            $msisdn = $callCenter->getMissCall($id);
            $checkMsisdn = substr($msisdn['msisdn'],0,3);
            if($checkMsisdn=='207' || $checkMsisdn=='307'){
                $countPurchare = $callCenter1->countHistoryCallPur($msisdn['msisdn']);
                $sumPurchare = $callCenter1->sumHistoryCallPur($msisdn['msisdn']);
                $historyLotto = $callCenter1->getHistory($msisdn['msisdn']);
            }elseif($checkMsisdn=='202' || $checkMsisdn=='203'){
                $countPurchare = $callCenter2->countHistoryCallPur($msisdn['msisdn']);
                $sumPurchare = $callCenter2->sumHistoryCallPur($msisdn['msisdn']);
                $historyLotto = $callCenter2->getHistory($msisdn['msisdn']);
            }elseif($checkMsisdn=='209' || $checkMsisdn=='309'){
                $countPurchare = $callCenter3->countHistoryCallPur($msisdn['msisdn']);
                $sumPurchare = $callCenter3->sumHistoryCallPur($msisdn['msisdn']);
                $historyLotto = $callCenter3->getHistory($msisdn['msisdn']);
            }else{
                $countPurchare = $callCenter->countHistoryCallPur($msisdn['msisdn']);
                $sumPurchare = $callCenter->sumHistoryCallPur($msisdn['msisdn']);
                $historyLotto = $callCenter->getHistory($msisdn['msisdn']);
            }

            $detailNote = $callCenter->getCallNote($id);
            $purchaseSMS = explode('|',$sms->SaleHistory($msisdn['msisdn'])->SaleHistoryResult);
            $agent = $callCenter->getPhoneAgent();
            $historyNote = $callCenter->getHistoryNote($msisdn['msisdn']);
            $edProvince = $callCenter->findProvince($msisdn['msisdn']);

            if($_SERVER['REQUEST_METHOD'] == 'POST'){
                $name = isset($_REQUEST['name'])?$_REQUEST['name']:NULL;
                $address = isset($_REQUEST['address'])?$_REQUEST['address']:NULL;
                $age = isset($_REQUEST['age'])?$_REQUEST['age']:NULL;
                $content = isset($_REQUEST['content'])?$_REQUEST['content']:NULL;
                $status = isset($_REQUEST['status'])?$_REQUEST['status']:NULL;
                $comment = isset($_REQUEST['note'])?$_REQUEST['note']:NULL;

                $provinceNote = isset($_REQUEST['province']) ? $_REQUEST['province'] : NULL;
                $districtNote = isset($_REQUEST['district']) ? $_REQUEST['district'] : NULL;
                $wardNote = isset($_REQUEST['ward']) ? $_REQUEST['ward'] : NULL;
                $villageNote = isset($_REQUEST['village']) ? $_REQUEST['village'] : NULL;

                if(empty($status)){$error[]='Status not Empty';}
                /// Submit form
                if(!empty($error)){
                    $message = '<p style="font-size: 16px;color: chocolate;">Message Error:</p><ul>';
                    foreach($error as $e) {$message .= '<li>'.$e.'</li>';}
                    $message .= '</ul>';
                }else{
                    if(!empty($provinceNote) && !empty($districtNote) && !empty($wardNote) && !empty($villageNote)){
                        $provinceId = $provinceNote.','.$districtNote.','.$wardNote.','.$villageNote;
                    }elseif(!empty($provinceNote) && !empty($districtNote) && !empty($wardNote)){
                        $provinceId = $provinceNote.','.$districtNote.','.$wardNote;
                    }elseif(!empty($provinceNote) && !empty($districtNote)){
                        $provinceId = $provinceNote.','.$districtNote;
                    }elseif(!empty($provinceNote)){
                        $provinceId = $provinceNote;
                    }
                    if($status==2){
                        $callCenter->createAnswerMiss($id_agent,$comment,$msisdn['msisdn'],$agent,$name);
                        $callCenter->updateMissCall($id);
                    }
                    $checkNote = $callCenter->checkNote($id);
                    if($checkNote){
                        $callCenter->updateNote($msisdn['msisdn'],$name,$address,$age,$content,$status,$provinceId);
                        $this->redirect(Yii::app()->createUrl('callCenter/admin'));
                    }else{
                        $callCenter->createNote($msisdn['msisdn'],$name,$address,$age,$content,$status,$id,NULL,$provinceId);
                        $this->redirect(Yii::app()->createUrl('callCenter/admin'));
                    }
                }
            }
        }else{
            $this->redirect(Yii::app()->createUrl('callCenter/Hack'));
        }

        $this->render('noteHistorys',array(
            'historyLotto'  => $historyLotto,
            'message'       => $message,
            'detail'        => $detailNote,
            'count'         => $countPurchare,
            'sum'           => $sumPurchare,
            'msisdn'       => $msisdn['msisdn'],
            'purchaseSMS' => $purchaseSMS,
            'id'           => $id,
            'historyNote' => $historyNote,
            'province' => $province,
            'edProvince' => $edProvince,
        ));
    }

    public function actionHistory(){
$this->menuActive   = 'callCenter';

        $callCenter = CallCenter::model();
        $callCenter1 = CallCenterDB3::model();
        $callCenter2 = CallCenterDB4::model();
        $callCenter3 = CallCenterDB5::model();
        $sms = new Service();
        $message='';
        $id = isset($_REQUEST['id'])?$_REQUEST['id']:NULL;
        $hack = $callCenter->checkHackAnswer($id);
        if($hack){
            $province = $callCenter->getProvince();
            $msisdn = $callCenter->getHistory($id);
            $msi = isset($msisdn['msisdn_member'])?$msisdn['msisdn_member']:0;
            $edProvince = $callCenter->findProvince($msi);
            $checkMsisdn = substr($msisdn['msisdn_member'],0,3);
            if($checkMsisdn=='207' || $checkMsisdn=='307'){
                $countPurchare = $callCenter1->countHistoryCallPur($msisdn['msisdn_member']);
                $sumPurchare = $callCenter1->sumHistoryCallPur($msisdn['msisdn_member']);
                $historyLotto = $callCenter1->getHistory($msisdn['msisdn_member']);
            }elseif($checkMsisdn=='202' || $checkMsisdn=='203'){
                $countPurchare = $callCenter2->countHistoryCallPur($msisdn['msisdn_member']);
                $sumPurchare = $callCenter2->sumHistoryCallPur($msisdn['msisdn_member']);
                $historyLotto = $callCenter2->getHistory($msisdn['msisdn_member']);
            }elseif($checkMsisdn=='209' || $checkMsisdn=='309'){
                $countPurchare = $callCenter3->countHistoryCallPur($msisdn['msisdn_member']);
                $sumPurchare = $callCenter3->sumHistoryCallPur($msisdn['msisdn_member']);
                $historyLotto = $callCenter3->getHistory($msisdn['msisdn_member']);
            }else{
                $countPurchare = $callCenter->countHistoryCallPur($msisdn['msisdn_member']);
                $sumPurchare = $callCenter->sumHistoryCallPur($msisdn['msisdn_member']);
                $historyLotto = $callCenter->getHistory($msisdn['msisdn_member']);
            }

            $detailNote = $callCenter->getCallNote($msisdn['msisdn_member']);
            $noteCall = $callCenter->getAnswerNote($id);
            $purchaseSMS = explode('|',$sms->SaleHistory($msisdn['msisdn_member'])->SaleHistoryResult);

            if($_SERVER['REQUEST_METHOD'] == 'POST'){
                $error=array();
                $name = isset($_REQUEST['name'])?$_REQUEST['name']:NULL;
                $address = isset($_REQUEST['address'])?$_REQUEST['address']:NULL;
                $age = isset($_REQUEST['age'])?$_REQUEST['age']:NULL;
                $content = isset($_REQUEST['content'])?$_REQUEST['content']:NULL;
                $status = isset($_REQUEST['status'])?$_REQUEST['status']:NULL;
                $comment = isset($_REQUEST['note'])?$_REQUEST['note']:NULL;

                $provinceNote = isset($_REQUEST['province']) ? $_REQUEST['province'] : NULL;
                $districtNote = isset($_REQUEST['district']) ? $_REQUEST['district'] : NULL;
                $wardNote = isset($_REQUEST['ward']) ? $_REQUEST['ward'] : NULL;
                $villageNote = isset($_REQUEST['village']) ? $_REQUEST['village'] : NULL;

                if(empty($status)){$error[]='Status not Empty';}
                if(!empty($error)){
                    $message = '<p style="font-size: 16px;color: chocolate;">Message Error:</p><ul>';
                    foreach($error as $e) {$message .= '<li>'.$e.'</li>';}
                    $message .= '</ul>';
                }else{
                    if(!empty($provinceNote) && !empty($districtNote) && !empty($wardNote) && !empty($villageNote)){
                        $provinceId = $provinceNote.','.$districtNote.','.$wardNote.','.$villageNote;
                    }elseif(!empty($provinceNote) && !empty($districtNote) && !empty($wardNote)){
                        $provinceId = $provinceNote.','.$districtNote.','.$wardNote;
                    }elseif(!empty($provinceNote) && !empty($districtNote)){
                        $provinceId = $provinceNote.','.$districtNote;
                    }elseif(!empty($provinceNote)){
                        $provinceId = $provinceNote;
                    }
                    if(empty($provinceId)){
                        $provinceId = NULL;
                    }
                    if($status==2){
                        $callCenter->updateAgentAnswer($id,$comment,$name);
                    }
                    $checkNote = $callCenter->checkNote($msi);

                    if($checkNote){
                        $callCenter->updateNote($msi,$name,$address,$age,$content,$status,$provinceId);
                        $this->redirect(Yii::app()->createUrl('callCenter/admin'));
                    }else{
                        $callCenter->createNote($msi,$name,$address,$age,$content,$status,NULL,$id,$provinceId);
                        $this->redirect(Yii::app()->createUrl('callCenter/admin'));
                    }
                }
            }
        }else{
            $this->redirect(Yii::app()->createUrl('callCenter/Hack'));
        }
        $this->render('history',array(
            'historyLotto'  => $historyLotto,
            'message'       => $message,
            'detail'        => $detailNote,
            'count'         => $countPurchare,
            'sum'           => $sumPurchare,
            'msisdn'        => $msisdn['msisdn_member'],
            'id'            => $id,
            'noteCall'      => $noteCall,
            'purchaseSMS' => $purchaseSMS,
            'province' => $province,
            'edProvince' => $edProvince,
        ));
    }

    public function actionNoteHistory(){
$this->menuActive   = 'callCenter';

        $callCenter = CallCenter::model();
        $callCenter1 = CallCenterDB3::model();
        $callCenter2 = CallCenterDB4::model();
        $callCenter3 = CallCenterDB5::model();
        $message='';
        $id = isset($_REQUEST['id'])?$_REQUEST['id']:NULL;
        $hack = $callCenter->checkHackAnswer($id);
        if($hack){
            $province = $callCenter->getProvince();
            $msisdn = $callCenter->getHistory($id);
            $checkMsisdn = substr($msisdn['msisdn_member'],0,3);
            $edProvince = $callCenter->findProvince($msisdn['msisdn_member']);

            if($checkMsisdn=='207' || $checkMsisdn=='307'){
                $countPurchare = $callCenter1->countHistoryCallPur($msisdn['msisdn_member']);
                $sumPurchare = $callCenter1->sumHistoryCallPur($msisdn['msisdn_member']);
                $historyLotto = $callCenter1->getHistory($msisdn['msisdn_member']);
            }elseif($checkMsisdn=='202' || $checkMsisdn=='203'){
                $countPurchare = $callCenter2->countHistoryCallPur($msisdn['msisdn_member']);
                $sumPurchare = $callCenter2->sumHistoryCallPur($msisdn['msisdn_member']);
                $historyLotto = $callCenter2->getHistory($msisdn['msisdn_member']);
            }elseif($checkMsisdn=='209' || $checkMsisdn=='309'){
                $countPurchare = $callCenter3->countHistoryCallPur($msisdn['msisdn_member']);
                $sumPurchare = $callCenter3->sumHistoryCallPur($msisdn['msisdn_member']);
                $historyLotto = $callCenter3->getHistory($msisdn['msisdn_member']);
            }else{
                $countPurchare = $callCenter->countHistoryCallPur($msisdn['msisdn_member']);
                $sumPurchare = $callCenter->sumHistoryCallPur($msisdn['msisdn_member']);
                $historyLotto = $callCenter->getHistory($msisdn['msisdn_member']);
            }

            $detailNote = $callCenter->getCallNote($msisdn['msisdn_member']);
            $noteCall = $callCenter->getAnswerNote($id);
            $historyNote = $callCenter->getHistoryNote($msisdn['msisdn_member']);

            if($_SERVER['REQUEST_METHOD'] == 'POST'){
                $error=array();
                $name = isset($_REQUEST['name'])?$_REQUEST['name']:NULL;
                $address = isset($_REQUEST['address'])?$_REQUEST['address']:NULL;
                $age = isset($_REQUEST['age'])?$_REQUEST['age']:NULL;
                $content = isset($_REQUEST['content'])?$_REQUEST['content']:NULL;
                $status = isset($_REQUEST['status'])?$_REQUEST['status']:NULL;
                $comment = isset($_REQUEST['note'])?$_REQUEST['note']:NULL;

                $provinceNote = isset($_REQUEST['province']) ? $_REQUEST['province'] : NULL;
                $districtNote = isset($_REQUEST['district']) ? $_REQUEST['district'] : NULL;
                $wardNote = isset($_REQUEST['ward']) ? $_REQUEST['ward'] : NULL;
                $villageNote = isset($_REQUEST['village']) ? $_REQUEST['village'] : NULL;

                if(empty($status)){$error[]='Status not Empty';}
                if(!empty($error)){
                    $message = '<p style="font-size: 16px;color: chocolate;">Message Error:</p><ul>';
                    foreach($error as $e) {$message .= '<li>'.$e.'</li>';}
                    $message .= '</ul>';
                }else{
                    if(!empty($provinceNote) && !empty($districtNote) && !empty($wardNote) && !empty($villageNote)){
                        $provinceId = $provinceNote.','.$districtNote.','.$wardNote.','.$villageNote;
                    }elseif(!empty($provinceNote) && !empty($districtNote) && !empty($wardNote)){
                        $provinceId = $provinceNote.','.$districtNote.','.$wardNote;
                    }elseif(!empty($provinceNote) && !empty($districtNote)){
                        $provinceId = $provinceNote.','.$districtNote;
                    }elseif(!empty($provinceNote)){
                        $provinceId = $provinceNote;
                    }
                    if($comment || $status==2){$callCenter->updateAgentAnswer($id,$comment,$name);}
                    $checkNote = $callCenter->checkNote($msisdn['msisdn_member']);
                    if($checkNote){
                        $callCenter->updateNote($msisdn['msisdn_member'],$name,$address,$age,$content,$status,$provinceId);
                        $this->redirect(Yii::app()->createUrl('callCenter/admin'));
                    }else{
                        $callCenter->createNote($msisdn['msisdn_member'],$name,$address,$age,$content,$status,NULL,$id,$provinceId);
                        $this->redirect(Yii::app()->createUrl('callCenter/admin'));
                    }
                }
            }
        }else{
            $this->redirect(Yii::app()->createUrl('callCenter/Hack'));
        }
        
        $this->render('notehistory',array(
            'historyLotto'  => $historyLotto,
            'message'       => $message,
            'detail'        => $detailNote,
            'count'         => $countPurchare,
            'sum'           => $sumPurchare,
            'msisdn'        => $msisdn['msisdn_member'],
            'id'            => $id,
            'noteCall'      => $noteCall,
            'historyNote' => $historyNote,
            'province' => $province,
            'edProvince' => $edProvince,
        ));
    }

    public function actionEditonline(){
$this->menuActive   = 'callCenter';

        $callCenter = CallCenter::model();
        $callCenter1 = CallCenterDB3::model();
        $callCenter2 = CallCenterDB4::model();
        $callCenter3 = CallCenterDB5::model();
        $sms = new Service();
        $message='';
        $province = $callCenter->getProvince();
        $msisdn = isset($_REQUEST['msisdn']) ? $_REQUEST['msisdn'] : NULL;
        $hack = $callCenter->checkHackOnline($msisdn);
        if($hack){
            $province = $callCenter->getProvince();
            $checkMsisdn = substr($msisdn,0,3);
            if($checkMsisdn=='207' || $checkMsisdn=='307'){
                $countPurchare = $callCenter1->countHistoryCallPur($msisdn);
                $sumPurchare = $callCenter1->sumHistoryCallPur($msisdn);
                $historyLotto = $callCenter1->getHistory($msisdn);
            }elseif($checkMsisdn=='202' || $checkMsisdn=='203'){
                $countPurchare = $callCenter2->countHistoryCallPur($msisdn);
                $sumPurchare = $callCenter2->sumHistoryCallPur($msisdn);
                $historyLotto = $callCenter2->getHistory($msisdn);
            }elseif($checkMsisdn=='209' || $checkMsisdn=='309'){
                $countPurchare = $callCenter3->countHistoryCallPur($msisdn);
                $sumPurchare = $callCenter3->sumHistoryCallPur($msisdn);
                $historyLotto = $callCenter3->getHistory($msisdn);
            }else{
                $countPurchare = $callCenter->countHistoryCallPur($msisdn);
                $sumPurchare = $callCenter->sumHistoryCallPur($msisdn);
                $historyLotto = $callCenter->getHistory($msisdn);
            }

            $detailNote = $callCenter->getCallNote($msisdn);
            $noteCall = $callCenter->getAnswerNote($msisdn);
            $purchaseSMS = explode('|',$sms->SaleHistory($msisdn)->SaleHistoryResult);

            if($_SERVER['REQUEST_METHOD'] == 'POST'){
                $error=array();
                $name = isset($_REQUEST['name'])?$_REQUEST['name']:NULL;
                $status = isset($_REQUEST['status'])?$_REQUEST['status']:NULL;
                $comment = isset($_REQUEST['note'])?$_REQUEST['note']:NULL;
                if(empty($status)){$error[]='Status not Empty';}
                if(!empty($error)){
                    $message = '<p style="font-size: 16px;color: chocolate;">Message Error:</p><ul>';
                    foreach($error as $e) {$message .= '<li>'.$e.'</li>';}
                    $message .= '</ul>';
                }else{
                    $callCenter->updateOnline($msisdn,$comment,$name);
                    $callCenter->updateTableOnline($msisdn);
                    $this->redirect(Yii::app()->createUrl('callCenter/admin'));
                }
            }
        }else{
            $this->redirect(Yii::app()->createUrl('callCenter/Hack'));
        }

        $this->render('online',array(
            'historyLotto'  => $historyLotto,
            'message'       => $message,
            'detail'        => $detailNote,
            'count'         => $countPurchare,
            'sum'           => $sumPurchare,
            'msisdn'        => $msisdn,
            'noteCall'      => $noteCall,
            'purchaseSMS'   => $purchaseSMS,
            'province'      => $province,
        ));
    }

    public function actionOlHistory(){
$this->menuActive   = 'callCenter';

        $callCenter = CallCenter::model();
        $callCenter1 = CallCenterDB3::model();
        $callCenter2 = CallCenterDB4::model();
        $callCenter3 = CallCenterDB5::model();
        $sms = new Service();
        $message='';
        $msisdn = isset($_REQUEST['msisdn']) ? $_REQUEST['msisdn'] : NULL;
        $hack = $callCenter->checkHackOnline($msisdn);
        if($hack){
            $province = $callCenter->getProvince();
            $checkMsisdn = substr($msisdn,0,3);
            if($checkMsisdn=='207' || $checkMsisdn=='307'){
                $countPurchare = $callCenter1->countHistoryCallPur($msisdn);
                $sumPurchare = $callCenter1->sumHistoryCallPur($msisdn);
                $historyLotto = $callCenter1->getHistory($msisdn);
            }elseif($checkMsisdn=='202' || $checkMsisdn=='203'){
                $countPurchare = $callCenter2->countHistoryCallPur($msisdn);
                $sumPurchare = $callCenter2->sumHistoryCallPur($msisdn);
                $historyLotto = $callCenter2->getHistory($msisdn);
            }elseif($checkMsisdn=='209' || $checkMsisdn=='309'){
                $countPurchare = $callCenter3->countHistoryCallPur($msisdn);
                $sumPurchare = $callCenter3->sumHistoryCallPur($msisdn);
                $historyLotto = $callCenter3->getHistory($msisdn);
            }else{
                $countPurchare = $callCenter->countHistoryCallPur($msisdn);
                $sumPurchare = $callCenter->sumHistoryCallPur($msisdn);
                $historyLotto = $callCenter->getHistory($msisdn);
            }

            $detailNote = $callCenter->getCallNote($msisdn);
            $noteCall = $callCenter->getAnswerNote($msisdn);
            $purchaseSMS = explode('|',$sms->SaleHistory($msisdn)->SaleHistoryResult);
            $historyNote = $callCenter->getHistoryNote($msisdn);
            if($_SERVER['REQUEST_METHOD'] == 'POST'){
                $error=array();
                $name = isset($_REQUEST['name'])?$_REQUEST['name']:NULL;
                $status = isset($_REQUEST['status'])?$_REQUEST['status']:NULL;
                $comment = isset($_REQUEST['note'])?$_REQUEST['note']:NULL;
                if(empty($status)){$error[]='Status not Empty';}
                if(!empty($error)){
                    $message = '<p style="font-size: 16px;color: chocolate;">Message Error:</p><ul>';
                    foreach($error as $e) {$message .= '<li>'.$e.'</li>';}
                    $message .= '</ul>';
                }else{
                    $callCenter->updateOnline($msisdn,$comment,$name);
                    $callCenter->updateTableOnline($msisdn);
                    $this->redirect(Yii::app()->createUrl('callCenter/admin'));
                }
            }
        }else{
            $this->redirect(Yii::app()->createUrl('callCenter/Hack'));
        }

        $this->render('olHistory',array(
            'historyLotto'  => $historyLotto,
            'message'       => $message,
            'detail'        => $detailNote,
            'count'         => $countPurchare,
            'sum'           => $sumPurchare,
            'msisdn'        => $msisdn,
            'noteCall'      => $noteCall,
            'purchaseSMS'   => $purchaseSMS,
            'historyNote'   => $historyNote,
            'province'      => $province,
        ));
    }


    public function actionExcel(){
        $id = isset($_REQUEST['id'])?$_REQUEST['id']:NULL;
        $callCenter = CallCenter::model();
        $callCenter1 = CallCenterDB3::model();
        $callCenter2 = CallCenterDB4::model();
        $callCenter3 = CallCenterDB5::model();
        $service = new Service();

        $msisdn = $callCenter->getMissCall($id);
        $note = $callCenter->getCallNote($msisdn['msisdn']);
        $sms = $service->getSmsAmount($msisdn['msisdn']);

        $checkMsisdn = substr($msisdn['msisdn'],0,3);
        if($checkMsisdn=='207' || $checkMsisdn=='307'){
            $countPurchare = $callCenter1->countHistoryCallPur($msisdn['msisdn']);
            $sumPurchare = $callCenter1->sumHistoryCallPur($msisdn['msisdn']);
            $historyLotto = $callCenter1->getHistory($msisdn['msisdn']);
        }elseif($checkMsisdn=='202' || $checkMsisdn=='203'){
            $countPurchare = $callCenter2->countHistoryCallPur($msisdn['msisdn']);
            $sumPurchare = $callCenter2->sumHistoryCallPur($msisdn['msisdn']);
            $historyLotto = $callCenter2->getHistory($msisdn['msisdn']);
        }elseif($checkMsisdn=='209' || $checkMsisdn=='309'){
            $countPurchare = $callCenter3->countHistoryCallPur($msisdn['msisdn']);
            $sumPurchare = $callCenter3->sumHistoryCallPur($msisdn['msisdn']);
            $historyLotto = $callCenter3->getHistory($msisdn['msisdn']);
        }else{
            $countPurchare = $callCenter->countHistoryCallPur($msisdn['msisdn']);
            $sumPurchare = $callCenter->sumHistoryCallPur($msisdn['msisdn']);
	        $historyLotto = $callCenter->getHistory($msisdn['msisdn']);
        }

        $content = $this->renderPartial(
            "excel",
            array(
                'id' => $id,
                'detail' => $note,
                'sms' => $sms,
                'hitory' => $historyLotto,
                'count' => $countPurchare,
                'sum'   => $sumPurchare,
            ),
            true
        );
        Yii::app()->request->sendFile(date('YmdHis').'.xls',$content);
    }

    public function actionExcelhistory(){

        $id = isset($_REQUEST['id'])?$_REQUEST['id']:NULL;
        $callCenter = CallCenter::model();
        $callCenter1 = CallCenterDB3::model();
        $callCenter2 = CallCenterDB4::model();
        $callCenter3 = CallCenterDB5::model();
        $service = new Service();

        $msisdn = $callCenter->getHistory($id);
        $note = CallCenter::model()->getCallNote($msisdn['msisdn_member']);
        $sms = $service->getSmsAmount($msisdn['msisdn_member']);
        $checkMsisdn = substr($msisdn['msisdn_member'],0,3);
        if($checkMsisdn=='207' || $checkMsisdn=='307'){
            $countPurchare = $callCenter1->countHistoryCallPur($msisdn['msisdn_member']);
            $sumPurchare = $callCenter1->sumHistoryCallPur($msisdn['msisdn_member']);
            $historyLotto = $callCenter1->getHistory($msisdn['msisdn_member']);
        }elseif($checkMsisdn=='202' || $checkMsisdn=='203'){
            $countPurchare = $callCenter2->countHistoryCallPur($msisdn['msisdn_member']);
            $sumPurchare = $callCenter2->sumHistoryCallPur($msisdn['msisdn_member']);
            $historyLotto = $callCenter2->getHistory($msisdn['msisdn_member']);
        }elseif($checkMsisdn=='209' || $checkMsisdn=='309'){
            $countPurchare = $callCenter3->countHistoryCallPur($msisdn['msisdn_member']);
            $sumPurchare = $callCenter3->sumHistoryCallPur($msisdn['msisdn_member']);
            $historyLotto = $callCenter3->getHistory($msisdn['msisdn_member']);
        }else{
            $countPurchare = $callCenter->countHistoryCallPur($msisdn['msisdn_member']);
            $sumPurchare = $callCenter->sumHistoryCallPur($msisdn['msisdn_member']);
            $historyLotto = $callCenter->getHistory($msisdn['msisdn_member']);
        }

        $content = $this->renderPartial(
            "excelhistory",
            array(
                'id' => $id,
                'detail' => $note,
                'sms' => $sms,
                'hitory' => $historyLotto,
                'count' => $countPurchare,
                'sum'   => $sumPurchare,
            ),
            true
        );
        Yii::app()->request->sendFile(date('YmdHis').'.xls',$content);
    }

    public function actionHack(){
        $this->render('hack_error');
    }
} 