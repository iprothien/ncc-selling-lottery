<?php

class ServiceStatusController extends AccessController {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public function __construct($id, $module = null) {

        if ($this->hasCookie('_langhandle'))
            $lang = $this->getCookie('_langhandle');
        else
            $lang = Yii::app()->user->getLang();

        if ($lang && in_array($lang, CLocale::getLocaleIDs())) {
            Yii::app()->setLanguage($lang);
        }
        return parent::__construct($id, $module);
    }

    public $layout = '//layouts/column2';
    public $module = 'serviceStatus';

    public function actionView($id) {
        $this->layout = '//layouts/blank';
        $sell = array();
        $rs_time = ServiceStatus::model()->getTime();
        if ($rs_time) {
            $sell['start_time'] = $rs_time['start_time'];
            $sell['end_time'] = $rs_time['end_time'];
        }

        $rsBeeline = ServiceStatus2::model()->getSellingStatus();
        $rsEtl = ServiceStatus3::model()->getSellingStatus();
        $rsUnitel = ServiceStatus4::model()->getSellingStatus();

        $sell['t1'] = $rsBeeline['t1'];
        $sell['t2'] = $rsBeeline['t2'];
        $sell['t3'] = $rsEtl['t3'];
        $sell['t4'] = $rsEtl['t4'];
        $sell['t5'] = $rsUnitel['t1'];
        $sell['t6'] = $rsUnitel['t2'];

        $rs_max = ServiceStatus::model()->getQuota();
        if ($rs_max) {
            $sell['max'] = $rs_max['max_two'] + $rs_max['max_three'];
            $sell['max_two'] = $rs_max['max_two'];
            $sell['max_three'] = $rs_max['max_three'];
        } else {
            $sell['max'] = 20000000;
            $sell['max_two'] = 10000000;
            $sell['max_three'] = 10000000;
        }

        $this->render('view', array('sell' => $sell));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $this->menuActive   = 'serviceStatus';
        $msisdn = isset($_GET['ServiceStatus']['msisdn']) ? $_GET['ServiceStatus']['msisdn'] : NULL;

        if (substr($msisdn, 0, 3) == '207' || substr($msisdn, 0, 3) == '307') {
            if (empty($_GET['ServiceStatus2']['msisdn'])) {
                $_GET['ServiceStatus2']['msisdn'] = $msisdn;
            }
            $model = new ServiceStatus2('search');
            $model->unsetAttributes();  // clear any default values
            if (isset($_GET['ServiceStatus2'])) {
                $model->attributes = $_GET['ServiceStatus2'];
            }
        } elseif (substr($msisdn, 0, 3) == '202' || substr($msisdn, 0, 3) == '203') {
            if (empty($_GET['ServiceStatus3']['msisdn'])) {
                $_GET['ServiceStatus3']['msisdn'] = $msisdn;
            }
            $model = new ServiceStatus3('search');
            $model->unsetAttributes();  // clear any default values
            if (isset($_GET['ServiceStatus3'])) {
                $model->attributes = $_GET['ServiceStatus3'];
            }
        } elseif (substr($msisdn, 0, 3) == '209' || substr($msisdn, 0, 3) == '309') {
            if (empty($_GET['ServiceStatus4']['msisdn'])) {
                $_GET['ServiceStatus4']['msisdn'] = $msisdn;
            }
            $model = new ServiceStatus4('search');
            $model->unsetAttributes();  // clear any default values
            if (isset($_GET['ServiceStatus4'])) {
                $model->attributes = $_GET['ServiceStatus4'];
            }
        } else {
            $model = new ServiceStatus('search');
            $model->unsetAttributes();  // clear any default values
        }


        $sell = array();
        $rs_time = ServiceStatus::model()->getTime();
        if ($rs_time) {
            $sell['start_time'] = $rs_time['start_time'];
            $sell['end_time'] = $rs_time['end_time'];
        }

        $rsBeeline = ServiceStatus2::model()->getSellingStatus();
        $rsEtl = ServiceStatus3::model()->getSellingStatus();
        $rsUnitel = ServiceStatus4::model()->getSellingStatus();

        $sell['t1'] = $rsBeeline['t1'];
        $sell['t2'] = $rsBeeline['t2'];
        $sell['t3'] = $rsEtl['t3'];
        $sell['t4'] = $rsEtl['t4'];
        $sell['t5'] = $rsUnitel['t1'];
        $sell['t6'] = $rsUnitel['t2'];

        $rs_max = ServiceStatus::model()->getQuota();
        if ($rs_max) {
            $sell['max'] = $rs_max['max_two'] + $rs_max['max_three'];
            $sell['max_two'] = $rs_max['max_two'];
            $sell['max_three'] = $rs_max['max_three'];
        } else {
            $sell['max'] = 20000000;
            $sell['max_two'] = 10000000;
            $sell['max_three'] = 10000000;
        }

        $this->render('admin', array(
            'model' => $model,
            'sell' => $sell,
            'msisdn' => $msisdn,
        ));
    }

    /*
      public function actionAdmin() {
      $model = new ServiceStatus('search');
      $model->unsetAttributes();  // clear any default values
      if (isset($_GET['ServiceStatus']))
      $model->attributes = $_GET['ServiceStatus'];

      $this->render('admin', array(
      'model' => $model,
      ));
      }
     */

    /**
     * Manages all models.
     */
    public function actionSms() {
        $this->menuActive   = 'serviceStatus';
        $model = new ServiceStatus('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['ServiceStatus'])) {
            $model->attributes = $_GET['ServiceStatus'];
        }

        $sell = array();
        $rs_time = ServiceStatus::model()->getTime();
        if ($rs_time) {
            $sell['start_time'] = $rs_time['start_time'];
            $sell['end_time'] = $rs_time['end_time'];
        }

        $rsBeeline = ServiceStatus2::model()->getSellingStatus();
        $rsEtl = ServiceStatus3::model()->getSellingStatus();
        $rsUnitel = ServiceStatus4::model()->getSellingStatus();

        $sell['t1'] = $rsBeeline['t1'];
        $sell['t2'] = $rsBeeline['t2'];
        $sell['t3'] = $rsEtl['t3'];
        $sell['t4'] = $rsEtl['t4'];
        $sell['t5'] = $rsUnitel['t1'];
        $sell['t6'] = $rsUnitel['t2'];

        $rs_max = ServiceStatus::model()->getQuota();
        if ($rs_max) {
            $sell['max'] = $rs_max['max_two'] + $rs_max['max_three'];
            $sell['max_two'] = $rs_max['max_two'];
            $sell['max_three'] = $rs_max['max_three'];
        } else {
            $sell['max'] = 20000000;
            $sell['max_two'] = 10000000;
            $sell['max_three'] = 10000000;
        }

        $this->render('sms', array(
            'model' => $model,
            'sell' => $sell
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = ServiceStatus::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'service-status-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
