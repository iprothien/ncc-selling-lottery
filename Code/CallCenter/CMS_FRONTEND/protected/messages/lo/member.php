<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
return array(
    'phoneNo' => 'Phone No',
    'name' => 'Name',
    'address' => 'Address',
    'province' => 'Province',
    'district' => 'District',
    'ward' => 'Ward',
    'village' => 'Village',
    'gender' => 'Gender',
    'contact' => 'Contact for',
    'memberFrom' => 'Member from',
    'bought' => 'Lotto bought',
    'totalCharge' => 'Total charge',
    'noteOfCall' => 'Note Of Call',
    'statusOfCall' => 'Status Of Call',
    'number' => 'Number',
    'amount' => 'Amount',
    'buyType' => 'Buy Tupe',
    'dateTime' => 'Date Time',
    'note' => 'Note',
    'agent' => 'Agent',
    'status' => 'Status',
    'time' => 'Time'
);
?>
