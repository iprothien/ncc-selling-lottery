<?php
return array(
    'history' => 'Laos History Call',
    'ivr' => 'Laos IVR History',
    'sms' => 'Laos SMS History',
    'miss' => 'Laos All Missed',
    /*NOTE*/
    'edit' => 'Laos Edit member info',
    'download' => 'Laos Download xls',
    'cancel' => 'Laos Cancel',
    'save' => 'Laos Save & Close',
    'phone' => 'Laos Phone no.',
    'name' => 'Laos Name',
    'address' => 'Laos Address',
    'Province' => 'Laos Province',
    'District' => 'Laos District',
    'Ward' => 'Laos Ward',
    'Village' => 'Laos Village',
    'Gender' => 'Laos Gender',
    'contact' => 'Laos Contact for',
    'member' => 'Laos Member from.',
    'bought' => 'Laos Lotto bought',
    'charge' => 'Laos Total charge',
    'note_of_call' => 'Laos Note of call',
    'status_of_call' => 'Laos Status of call',
    'purchase_history' => 'Laos Purchase history',
    'history_note' => 'Laos History Note',
    'number_amount' => 'Laos Number',
    'amount' => 'Laos Amount',
    'buy_type' => 'Laos Buy type',
    'date_time' => 'Laos Date time',



);
