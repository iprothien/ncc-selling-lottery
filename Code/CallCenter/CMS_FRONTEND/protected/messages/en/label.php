<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
return array(
    'serviceStatus' => 'Service Status',
    'callCenter' => 'Call Center',
    'quocta' => 'Quocta',
    'total' => 'Total',
    '2digit' => '2 digits',
    '3digit' => '3 digits',
    'sellingStatus' => 'Selling Status',
    'search' => 'Search',
    'currentCall' => 'Current Call',
    'missCall' => 'Missed Call',
    'start' =>'Start',
    'end' => 'End',
    'purchaseHistory' => 'Purchase history',
    'historyNote' => 'History Note',
    'ivrHistory' => 'IVR History',
    'smsHistory' => 'SMS History',
    'historyCall' => 'History Call',
    'allMiss' => 'All Missed',
    'viewAll' => 'View All',
    'editMember' => 'Edit Member Infor',
    'download' => 'Download xls',
    'cancel' => 'Cancel',
    'save' => 'Save and Close',
    'selected' => 'Selected'
);
?>
