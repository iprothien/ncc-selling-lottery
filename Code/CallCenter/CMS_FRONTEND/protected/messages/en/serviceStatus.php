<?php

return array(
    'id' => 'Laos ID',
    'msisdn' => 'Laos Msisdn',
    'code' => 'Laos Code',
    'money' => 'Laos Money',
    'created_datetime' => 'Laos Created Datetime',
    'type' => 'Laos Type',
    'status' => 'Laos Status',
    'draw_no' => 'Laos Draw No',
    'drawTime' => 'Laos Draw Time',
    'win_status' => 'Laos Win Status'
);
?>