<?php
/**
 * Description of CWebUser
 *
 */
class UWebUser extends CWebUser {
    
    const SESSION_LANG = "__lang";
    private $account;
    public $returnUrl = 'index.php?r=callCenter/admin';

    public function getGroup2() {
        $account = $this->getAccount();
        $menu = array();
        if ($account) {
            if (strlen(strstr($account->module, 'serviceStatus')) > 0)
                $menu[] = array('label' => 'Service Status', 'url' => Yii::app()->createUrl('/serviceStatus/admin'), 'alias'=>'serviceStatus');
            if (strlen(strstr($account->module, 'callCenter')) > 0)
                $menu[] = array('label' => 'Call Center', 'url' => Yii::app()->createUrl('/callCenter/admin'), 'alias'=>'callCenter');
        }
        return $menu;
    }

    public function getAccount() {
        if (!$this->account)
            $this->account = User::model()->findByAttributes(array('username' => $this->name));
        return $this->account;
    }
    
    public function login($identity, $duration = 0) {
        $result = parent::login($identity, $duration);
        //$this->setLang($identity->lang);
	 $this->setLang('en');
        return $result;
    }
    
    public function getLang() {
        if ($this->hasState(self::SESSION_LANG)) {
            return $this->getState(self::SESSION_LANG);
        }
        return NULL;
    }
    
    public function setLang($value) {
        $this->setState(self::SESSION_LANG, $value);
    }
}

?>