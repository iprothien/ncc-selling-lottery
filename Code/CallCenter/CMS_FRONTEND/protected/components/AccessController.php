<?php

/**
 * Description of AccessController
 */
class AccessController extends Controller {

    public $layout = '//layouts/column2';
    public $defaultAction = 'admin';
    public $module;
    public $account;

    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }
    
    public function accessRules() {
        return array(
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin','ivr','sms','historyNote','note','history','excelhistory','MissCall','NoteHistory','excel','online','miss','editonline','Error','Hack','MissNote','OlHistory','LoadData','loadProvince'),
                'users' => array('@'),
                'expression' => (string) $this->authenticate($this->module)
            ),
            array('allow',
                'actions' => array('delete', 'update'),
                'users' => array('@'),
                'expression' => (string) $this->full()
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function authenticate($module) {
        $account = $this->getAccount();
        if ($account) {
            $accountModules = explode(',', $account->module);
            return in_array($module, $accountModules);
        }
        return FALSE;
    }

    public function full() {
        $account = $this->getAccount();
        if ($account) {
            if ($account->permision==2 || $account->permision==1)
                return TRUE;
        }
        return FALSE;
    }

    public function getAccount() {
        if (!$this->account)
            $this->account = User::model()->findByAttributes(array('username' => Yii::app()->user->name, 'status' => 1));
        return $this->account;
    }
    
    public function hasCookie($name) {
        return !empty(Yii::app()->request->cookies[$name]->value);
    }

    public function getCookie($name) {
        return (string) Yii::app()->request->cookies[$name];
    }

    public function removeCookie($name) {
        unset(Yii::app()->request->cookies[$name]);
    }
}

?>