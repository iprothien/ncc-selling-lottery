<?php

class CallCenter extends CActiveRecord{

    public static function model($className=__CLASS__){
        return parent::model($className);
    }

    public function findMissCall($msisdn=NULL){
        $sql = "SELECT miss.`id`,miss.`msisdn`,miss.`created`,note.`name` FROM `miss_call` AS miss LEFT OUTER JOIN customer AS note ON miss.`msisdn` = note.`msisdn` WHERE miss.`status`= 1";
        if($msisdn){$sql .=" AND miss.`msisdn` LIKE '%".$msisdn."%'";}
        $sql .=" ORDER BY miss.`id` DESC LIMIT 5";
        $result = Yii::app()->db->createCommand($sql)->queryAll();
        return $result;
    }

    public function searchMissCall($msisdn=NULL,$startDate=NULL,$endDate=NULL,$startRecord=NULL, $limit=NULL){
        $sql = "SELECT miss.`id`,miss.`msisdn`,miss.`created`,note.`name` FROM `miss_call` AS miss LEFT OUTER JOIN customer AS note ON miss.`msisdn` = note.`msisdn` WHERE miss.`status`= 1";
        if($msisdn){$sql .=" AND miss.`msisdn` LIKE '%".$msisdn."%'";}
        if($startDate && $endDate){
            $sql .= " AND miss.`created` >='".$startDate."' AND miss.`created` <='".$endDate."'";
        }elseif($startDate){
            $sql .= " AND miss.`created` >='".$startDate."'";
        }elseif($endDate){
            $sql .= " AND miss.`created` <='".$endDate."'";
        }

        $sql .=" ORDER BY miss.`id` DESC LIMIT $startRecord,$limit";
        $result = Yii::app()->db->createCommand($sql)->queryAll();
        return $result;
    }

    public function countMissCall($msisdn=NULL,$startDate=NULL,$endDate=NULL){
        $sql = "SELECT COUNT(`id`) FROM `miss_call` WHERE `status`= 1";
        if($msisdn){$sql .=" AND `msisdn` LIKE '%".$msisdn."%'";}
        if($startDate && $endDate){
            $sql .= " AND `created` >='".$startDate."' AND `created` <='".$endDate."'";
        }elseif($startDate){
            $sql .= " AND `created` >='".$startDate."'";
        }elseif($endDate){
            $sql .= " AND `created` <='".$endDate."'";
        }
        $result = Yii::app()->db->createCommand($sql)->queryScalar();
        return $result;
    }

    public function findOnlineCall($msisdn=NULL){
        $sql = "SELECT ol.`msisdn`,ol.`ol_status`,ol.`created`,note.`name` FROM `online_customer` AS ol LEFT OUTER JOIN customer AS note ON ol.`msisdn` = note.`msisdn` WHERE ol.`status`=1";
        if($msisdn){
            $sql .=" AND ol.`msisdn` LIKE '%".$msisdn."%'";
        }
        $sql .=" ORDER BY ol.`id` DESC";
        $result = Yii::app()->db->createCommand($sql)->queryAll();
        return $result;
    }

    public function getMissCall($id=NULL){
        $sql = "SELECT * FROM `miss_call` WHERE `status`=1 AND `id`=$id";
        $result = Yii::app()->db->createCommand($sql)->queryRow();
        return $result;
    }

    public function getHistory($id=NULL){
        $sql = "SELECT * FROM `ncc_agent_answer` WHERE `id`=$id";
        $result = Yii::app()->db->createCommand($sql)->queryRow();
        return $result;
    }

    public function getHistoryNote($msisdn = NULL){
        $sql = "SELECT ag.name, ans.call_time,ans.note,cus.status FROM ncc_agent_answer AS ans INNER JOIN customer AS cus ON  ans.msisdn_member = cus.msisdn LEFT OUTER JOIN ncc_agent AS ag ON ans.msisdn_agent=ag.phonenumber WHERE ans.msisdn_member=".$msisdn;
        $result = Yii::app()->db->createCommand($sql)->queryAll();
        return $result;
    }

    public function findHistoryCall($msisdn=NULL,$startRecord=NULL, $limit=NULL){
        $sql = "SELECT a.id,a.customer,a.ol_status,a.msisdn_agent,a.msisdn_member,a.duration,a.call_time,ag.name,a.`status`,note.`name` as name_cus FROM ncc_agent_answer as a INNER JOIN ncc_agent as ag ON a.msisdn_agent = ag.phonenumber LEFT JOIN customer AS note ON a.msisdn_member = note.msisdn";
        if($msisdn){
            $sql .=" WHERE a.`msisdn_member` LIKE '%".$msisdn."%'";
        }
        $sql .=" ORDER BY a.id DESC LIMIT $startRecord,$limit";
        $result = Yii::app()->db->createCommand($sql)->queryAll();
        return $result;
    }

    public function countHistoryCall($msisdn=NULL){
        $sql = "SELECT COUNT(a.id) FROM ncc_agent_answer as a INNER JOIN ncc_agent as ag ON a.msisdn_agent = ag.phonenumber";
        if($msisdn){
            $sql .=" WHERE a.`msisdn_member` LIKE '%".$msisdn."%'";
        }
        $result = Yii::app()->db->createCommand($sql)->queryScalar();
        return $result;
    }


    public function countHistoryCallPur($msisdn){
        $sql = "SELECT COUNT(id) FROM member_draw WHERE `status`=1 AND `msisdn`=$msisdn";
        $result = Yii::app()->db->createCommand($sql)->queryScalar();
        return $result;
    }

    public function sumHistoryCallPur($msisdn){
        $sql = "SELECT SUM(money) FROM member_draw WHERE `status`=1 AND `msisdn`=$msisdn";
        $result = Yii::app()->db->createCommand($sql)->queryScalar();
        return $result;
    }

    public function createNote($msisdn,$name,$address,$age,$content,$status,$miss_id=NULL,$ans_id=NULL,$province_id=NULL){
        $sql = "INSERT INTO `customer` SET `miss_id`='{$miss_id}',`ans_id`='{$ans_id}', `province_id`='{$province_id}' ,`msisdn`='{$msisdn}',`name`='{$name}',`address`='{$address}',`age`='{$age}',`content`='{$content}',`status`='{$status}'";
        $result = Yii::app()->db->createCommand($sql)->execute();
        return $result;
    }

    public function updateNote($msisdn,$name,$address,$age,$content,$status,$province_id=NULL){
        $sql = "UPDATE `customer` SET `province_id`='{$province_id}' , `name`='{$name}',`address`='{$address}',`age`='{$age}',`content`='{$content}',`status`='{$status}' WHERE `msisdn`='{$msisdn}'";
        $result = Yii::app()->db->createCommand($sql)->execute();
        return $result;
    }

    public function getCallNote($msisdn){
        $sql = "SELECT * FROM `customer` WHERE `msisdn`= $msisdn";
        $result = Yii::app()->db->createCommand($sql)->queryRow();
        return $result;
    }

    public function checkNote($msisdn){
        $sql = "SELECT COUNT(id) FROM `customer` WHERE `msisdn`=$msisdn";
        $result = Yii::app()->db->createCommand($sql)->queryScalar();
        return $result;
    }

    public function updateMissCall($id){
        $sql = "UPDATE `miss_call` SET `status`=2 WHERE `id`='{$id}'";
        $result = Yii::app()->db->createCommand($sql)->execute();
        return $result;
    }

    public function updateAgentAnswer($id,$note,$customer){
        $sql = "UPDATE `ncc_agent_answer` SET `note`='{$note}',`customer`='{$customer}' WHERE `id`='{$id}'";
        $result = Yii::app()->db->createCommand($sql)->execute();
        return $result;
    }

    public function updateOnline($msisdn,$note,$customer){
        $sql = "UPDATE `ncc_agent_answer` SET `note`='{$note}',`customer`='{$customer}', `ol_status` = 1 WHERE `msisdn_member`='{$msisdn}' AND `ol_status` = 0";
        $result = Yii::app()->db->createCommand($sql)->execute();
        return $result;
    }

    public function updateTableOnline($msisdn){
        $sql = "UPDATE `online_customer` SET `ol_status` = 1 WHERE `msisdn`='{$msisdn}' AND `ol_status` = 0";
        $result = Yii::app()->db->createCommand($sql)->execute();
        return $result;
    }

    public function createAnswerMiss($agent_id,$note,$msisdn,$phone_agent,$customer){
        $date = date('Y-m-d H:i:s');
        $sql = "INSERT INTO `ncc_agent_answer` SET `agent_id`='{$agent_id}',`path_record`='NULL',`msisdn_agent`='{$phone_agent}',`msisdn_member`='{$msisdn}',`call_time`='{$date}',`duration`=0,`status`=1,`note`='{$note}',`customer`='{$customer}',`updated`='{$date}'";
        $result = Yii::app()->db->createCommand($sql)->execute();
        return $result;
    }

    public function getInfo($msisdn){
        $sql = "SELECT * FROM `customer` WHERE `msisdn`=$msisdn";
        $result = Yii::app()->db->createCommand($sql)->queryRow();
        return $result;
    }

    public function getAnswerNote($id){
        $sql = "SELECT `note` FROM `ncc_agent_answer` WHERE `id`=$id";
        $result = Yii::app()->db->createCommand($sql)->queryRow();
        return $result;
    }

    public function getPhoneAgent(){
        $sql = "SELECT acc.`username` FROM `account` AS acc INNER JOIN `admin` AS ad ON ad.account_id = acc.id WHERE ad.`id`=".Yii::app()->user->getId();
        $result = Yii::app()->db->createCommand($sql)->queryRow();
        return $result['username'];
    }

    public function checkHackOnline($msisdn){
        $sql = "SELECT COUNT(id) FROM `online_customer` WHERE msisdn = $msisdn AND ol_status=0";
        $result = Yii::app()->db->createCommand($sql)->queryScalar();
        return $result;
    }

    public function checkHackMissCall($id){
        $sql = "SELECT COUNT(id) FROM `miss_call` WHERE id = $id AND `status`=1";
        $result = Yii::app()->db->createCommand($sql)->queryScalar();
        return $result;
    }

    public function checkHackAnswer($id){
        $sql = "SELECT COUNT(id) FROM `ncc_agent_answer` WHERE id = $id";
        $result = Yii::app()->db->createCommand($sql)->queryScalar();
        return $result;
    }

    public function getProvince(){
        $sql = "SELECT id, `name`, `type` FROM province WHERE type=1 ORDER BY id ASC";
        $result = Yii::app()->db->createCommand($sql)->queryAll();
        return $result;
    }

    public function getDistrict($provinceID){
        $sql = "SELECT * FROM province WHERE type=2 AND parent_id=".$provinceID." ORDER BY id ASC";
        $result = Yii::app()->db->createCommand($sql)->queryAll();
        return $result;
    }

    public function getWard($districtID){
        $sql = "SELECT * FROM province WHERE type=3 AND parent_id=".$districtID." ORDER BY id ASC";
        $result = Yii::app()->db->createCommand($sql)->queryAll();
        return $result;
    }

    public function getVillage($wardID){
        $sql = "SELECT * FROM province WHERE type=4 AND parent_id=".$wardID." ORDER BY id ASC";
        $result = Yii::app()->db->createCommand($sql)->queryAll();
        return $result;
    }

    public function findProvince($msisdn){
        $sqlProvinceId = "SELECT province_id FROM customer WHERE msisdn=".$msisdn;
        $resultProvince = Yii::app()->db->createCommand($sqlProvinceId)->queryRow();
        if($resultProvince){
            $expProvince = explode(',',$resultProvince['province_id']);
            $proFist = isset($expProvince[0])?$expProvince[0]:0;
            $proTow = isset($expProvince[1])?$expProvince[1]:0;
            $proThree = isset($expProvince[2])?$expProvince[2]:0;
            $proFore = isset($expProvince[3])?$expProvince[3]:0;
            if($proFist){
                $sqlProvince = "SELECT id,name FROM province WHERE type = 1 AND id = ".$expProvince[0];
                $resultProvince = Yii::app()->db->createCommand($sqlProvince)->queryRow();
            }
            if($proTow){
                $sqlDistrict = "SELECT id,name FROM province WHERE type = 2 AND id = ".$expProvince[1];
                $resultDistrict = Yii::app()->db->createCommand($sqlDistrict)->queryRow();
            }
            if($proThree){
                $sqlWard = "SELECT id,name FROM province WHERE type = 3 AND id = ".$expProvince[2];
                $resultWard = Yii::app()->db->createCommand($sqlWard)->queryRow();
            }
            if($proFore){
                $sqlVillage = "SELECT id,name FROM province WHERE type = 4 AND id = ".$expProvince[3];
                $resultVillage = Yii::app()->db->createCommand($sqlVillage)->queryRow();
            }
            return $list =array(
                'province' => array(
                    'id' => isset($resultProvince['id'])?$resultProvince['id']:NULL,
                    'name' => isset($resultProvince['name'])?$resultProvince['name']:NULL,
                ),
                'district' => array(
                    'id' => isset($resultDistrict['id'])?$resultDistrict['id']:NULL,
                    'name' => isset($resultDistrict['name'])?$resultDistrict['name']:NULL,
                ),
                'ward' => array(
                    'id' => isset($resultWard['id'])?$resultWard['id']:NULL,
                    'name' => isset($resultWard['name'])?$resultWard['name']:NULL,
                ),
                'village' => array(
                    'id' => isset($resultVillage['id'])?$resultVillage['id']:NULL,
                    'name' => isset($resultVillage['name'])?$resultVillage['name']:NULL,
                ),
            );
        }
    }
}

class CallCenterDB3 extends CActiveRecord{

    public static function model($className=__CLASS__){
        return parent::model($className);
    }

    public function getDbConnection() {
        return Yii::app()->db3;
    }

    public function countHistoryCallPur($msisdn){
        $sql = "SELECT COUNT(id) FROM member_draw WHERE `status`=1 AND `msisdn`=$msisdn";
        $result = Yii::app()->db3->createCommand($sql)->queryScalar();
        return $result;
    }

    public function sumHistoryCallPur($msisdn){
        $sql = "SELECT SUM(money) FROM member_draw WHERE `status`=1 AND `msisdn`=$msisdn";
        $result = Yii::app()->db3->createCommand($sql)->queryScalar();
        return $result;
    }

    public function getHistory($msisdn){
        $sql = "SELECT * FROM member_draw WHERE `status`=1 AND `msisdn`=$msisdn ORDER BY `id` DESC";
        $result = Yii::app()->db3->createCommand($sql)->queryAll();
        return $result;
    }
}

class CallCenterDB4 extends CActiveRecord{
    public static function model($className=__CLASS__){
        return parent::model($className);
    }

    public function getDbConnection() {
        return Yii::app()->db4;
    }

    public function countHistoryCallPur($msisdn){
        $sql = "SELECT COUNT(id) FROM member_draw WHERE `status`=1 AND `msisdn`=$msisdn";
        $result = Yii::app()->db4->createCommand($sql)->queryScalar();
        return $result;
    }

    public function sumHistoryCallPur($msisdn){
        $sql = "SELECT SUM(money) FROM member_draw WHERE `status`=1 AND `msisdn`=$msisdn";
        $result = Yii::app()->db4->createCommand($sql)->queryScalar();
        return $result;
    }

    public function getHistory($msisdn){
        $sql = "SELECT * FROM member_draw WHERE `status`=1 AND `msisdn`=$msisdn ORDER BY `id` DESC";
        $result = Yii::app()->db4->createCommand($sql)->queryAll();
        return $result;
    }
}

class CallCenterDB5 extends CActiveRecord{
    public static function model($className=__CLASS__){
        return parent::model($className);
    }

    public function getDbConnection() {
        return Yii::app()->db5;
    }

    public function countHistoryCallPur($msisdn){
        $sql = "SELECT COUNT(id) FROM member_draw WHERE `status`=1 AND `msisdn`=$msisdn";
        $result = Yii::app()->db5->createCommand($sql)->queryScalar();
        return $result;
    }

    public function sumHistoryCallPur($msisdn){
        $sql = "SELECT SUM(money) FROM member_draw WHERE `status`=1 AND `msisdn`=$msisdn";
        $result = Yii::app()->db5->createCommand($sql)->queryScalar();
        return $result;
    }

    public function getHistory($msisdn){
        $sql = "SELECT * FROM member_draw WHERE `status`=1 AND `msisdn`=$msisdn ORDER BY `id` DESC";
        $result = Yii::app()->db5->createCommand($sql)->queryAll();
        return $result;
    }
}