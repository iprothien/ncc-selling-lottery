<?php

/**
 * This is the model class for table "member_draw".
 *
 * The followings are the available columns in table 'member_draw':
 * @property integer $id
 * @property string $msisdn
 * @property string $code
 * @property integer $money
 * @property string $created_datetime
 * @property integer $type
 * @property integer $status
 */
class ServiceStatus extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return MemberDraw the static model class
     */
    
    public function getType() {
        switch ($this->type) {
            case 1:
                return '2 digits';
                break;
            default:
                return '3 digits';
                break;
        }
    }
    
    public function getQuota() {
        $cmd    = Yii::app()->db->createCommand();
        $cmd->select('max_two, max_three')
                ->from('quocta')
                ->where('time_start<=:t1 AND time_end>=:t1',array(':t1'=>date('Y-m-d H:i:s')));
        $data   = $cmd->queryRow();
        return $data;
    }
    
    public function getSellingStatus() {
        $data   = array();
        $cmd    = Yii::app()->db->createCommand('SELECT SUM(money) as t1 FROM member_draw WHERE type=1');
        $data1  = $cmd->queryRow();
        if($data1)
            $data['t1'] = $data1['t1'];
        else
            $data['t1'] = 0;
        $cmd1   = Yii::app()->db->createCommand('SELECT SUM(money) as t2 FROM member_draw WHERE type=2');
        $data2  = $cmd1->queryRow();
        if($data2)
            $data['t2'] = $data2['t2'];
        else
            $data['t2'] = 0;
        return $data;
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'member_draw';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return array(
            array('msisdn', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'msisdn' => 'Msisdn',
            'code' => 'Code',
            'money' => 'Money',
            'created_datetime' => 'Created Datetime',
            'type' => 'Type',
            'status' => 'Status',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('msisdn', $this->msisdn, true);
        $criteria->compare('code', $this->code, true);
        $criteria->order = 'id DESC';

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                    'pagination' => array('pageSize'=>'25'),
                ));
    }

}