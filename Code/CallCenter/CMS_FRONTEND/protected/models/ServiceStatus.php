<?php

/**
 * This is the model class for table "member_draw".
 *
 * The followings are the available columns in table 'member_draw':
 * @property integer $id
 * @property string $msisdn
 * @property string $code
 * @property integer $money
 * @property string $created_datetime
 * @property integer $type
 * @property integer $status
 */
class ServiceStatus extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return MemberDraw the static model class
     */
    public $option;
    
    public function getTime() {
        $data   = array();
        $cmd    = Yii::app()->db->createCommand('SELECT * FROM draw_time WHERE NOW() > start_time && NOW() < end_time && status=1');
        $data     = $cmd->queryRow();
        if($data)
            return $data;
        else
            return FALSE;
    }

    public function getQuota() {
        $cmd = Yii::app()->db->createCommand('SELECT max_two,max_three FROM quocta WHERE 1');
        $data = $cmd->queryRow();
        return $data;
    }

public function getType() {
        switch ($this->type) {
            case 1:
                return Yii::t('label', '2digit');
                break;
            default:
                return Yii::t('label', '3digit');
                break;
        }
    }

public function getWinStatus() {
        switch ($this->win_status) {
            case 1:
                return 'Yes';
                break;
            case 0:
                return 'No';
                break;
            default:
                return 'No';
                break;
        }
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'member_draw';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return array(
            array('msisdn, status', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'msisdn' => 'Msisdn',
            'code' => 'Code',
            'money' => 'Money',
            'created_datetime' => 'Created Datetime',
            'type' => 'Type',
            'status' => 'Status',
            'draw_no' => 'Draw No',
            'drawTime' => 'Draw Time',
            'win_status' => 'Win Status'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public $draw_no, $drawTime;
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;
        $criteria->alias    = 'a';
        $criteria->join     = 'INNER JOIN draw_time AS b ON a.draw_id=b.id';
        $criteria->select   = 'a.*,b.draw_no,b.end_time as drawTime';

        $criteria->compare('id', $this->id);
        $criteria->compare('msisdn', $this->msisdn, true);
        $criteria->compare('code', $this->code, true);
 	 $criteria->compare('status', $this->status);
        $criteria->order = 'id DESC';

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                    'pagination' => array('pageSize' => '25'),
                ));
    }

}