<?php

class MemberDraw extends CActiveRecord {
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'member_draw';
    }

    public function historyLotto($msisdn){
        $sql = "SELECT * FROM `member_draw` WHERE `msisdn`=$msisdn ORDER BY id DESC";
        $result = Yii::app()->db->createCommand($sql)->queryAll();
        if($result === false){
            header(Yii::app()->createUrl('site/error'));
        }
        return $result;
    }
}