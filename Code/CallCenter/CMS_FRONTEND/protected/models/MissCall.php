<?php

class MissCall extends CActiveRecord{
    public static function model($className=__CLASS__){
        return parent::model($className);
    }

    public function tableName(){
        return 'miss_call';
    }

    public function getDbConnection() {
        return Yii::app()->db2;
    }

    public function findMissCall($msisdn=NULL){
        $sql = "SELECT miss.`id`,miss.`msisdn`,miss.`created`,note.`name` FROM `miss_call` AS miss LEFT OUTER JOIN call_note AS note ON miss.`msisdn` = note.`msisdn` WHERE miss.`status`= 1";
        if($msisdn){
            $sql .=" AND miss.`msisdn` LIKE '%".$msisdn."%'";
        }
        $sql .=" ORDER BY miss.`id` DESC";
        $result = Yii::app()->db2->createCommand($sql)->queryAll();
        return $result;
    }

    public function findOnlineCall($msisdn=NULL){
        $sql = "SELECT ol.`msisdn`,ol.`created`,note.`name` FROM `online_customer` AS ol LEFT OUTER JOIN call_note AS note ON ol.`msisdn` = note.`msisdn` WHERE ol.`status`=1";
        if($msisdn){
            $sql .=" AND ol.`msisdn` LIKE '%".$msisdn."%'";
        }
        $sql .=" ORDER BY ol.`id` DESC";
        $result = Yii::app()->db2->createCommand($sql)->queryAll();
        return $result;
    }

    public function getMissCall($id=NULL){
        $sql = "SELECT * FROM `miss_call` WHERE `status`=1 AND `id`=$id";
        $result = Yii::app()->db2->createCommand($sql)->queryRow();
        return $result;
    }

    public function getHistory($id=NULL){
        $sql = "SELECT * FROM `ncc_agent_answer` WHERE `id`=$id";
        $result = Yii::app()->db2->createCommand($sql)->queryRow();
        return $result;
    }

    public function findHistoryCall($msisdn=NULL,$startRecord=NULL, $limit=NULL){
        $sql = "SELECT a.id,a.msisdn_agent,a.msisdn_member,a.duration,a.call_time,ag.name,a.`status` FROM ncc_agent_answer as a INNER JOIN ncc_agent as ag ON a.msisdn_agent = ag.phonenumber";
        if($msisdn){
            $sql .=" WHERE a.`msisdn_member` LIKE '%".$msisdn."%'";
        }
        $sql .=" ORDER BY a.id DESC LIMIT $startRecord,$limit";
        $result = Yii::app()->db2->createCommand($sql)->queryAll();
        return $result;
    }

    public function countHistoryCall($msisdn=NULL){
        $sql = "SELECT COUNT(a.id) FROM ncc_agent_answer as a INNER JOIN ncc_agent as ag ON a.msisdn_agent = ag.phonenumber";
        if($msisdn){
            $sql .=" WHERE a.`msisdn_member` LIKE '%".$msisdn."%'";
        }
        $result = Yii::app()->db2->createCommand($sql)->queryScalar();
        return $result;
    }


    public function countHistoryCallPur($msisdn){
        $sql = "SELECT COUNT(id) FROM member_draw WHERE `status`=1 AND `msisdn`=$msisdn";
        $result = Yii::app()->db->createCommand($sql)->queryScalar();
        return $result;
    }

    public function sumHistoryCallPur($msisdn){
        $sql = "SELECT SUM(money) FROM member_draw WHERE `status`=1 AND `msisdn`=$msisdn";
        $result = Yii::app()->db->createCommand($sql)->queryScalar();
        return $result;
    }
} 