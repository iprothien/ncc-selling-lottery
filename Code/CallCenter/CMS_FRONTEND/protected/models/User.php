<?php

/**
 * This is the model class for table "admin".
 *
 * The followings are the available columns in table 'admin':
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property string $module
 * @property integer $status
 */
class User extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return User the static model class
     */
    public $oldPassword;
    public $rePassword;
    public $isChangePassword = FALSE;

    public function beforeSave() {
        if ($this->isChangePassword)
            $this->password = md5($this->password);
        return parent::beforeSave();
    }

    public function afterSave() {
        $this->password = NULL;
        return parent::afterSave();
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'admin';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        if ($this->isChangePassword) {
            $rules[] = array('rePassword', 'compare', 'compareAttribute' => 'password');
            $rules[] = array('oldPassword', 'checkOldPassword');
            $rules[] = array('oldPassword, password', 'required');
        }
        return $rules;
    }

    public function checkOldPassword($attribute) {
        if (!$this->findAllByAttributes(array('id' => $this->id, 'password' => md5($this->oldPassword)))) {
            $this->addError($attribute, 'Old Password invalid.');
        }
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'username' => 'Username',
            'password' => 'Password',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('username', $this->username, true);

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                ));
    }

}