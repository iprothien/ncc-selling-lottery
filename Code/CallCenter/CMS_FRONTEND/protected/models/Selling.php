<?php

class Selling extends CActiveRecord {

    public $money;
    public $totalBuy;
    public $totalMoney;
    public $totalWin;
    public $amount;
    public $code;
    public $name;
    public $address;
    
    public $_vasgateway = array(
        "address" => "http://10.78.17.17:6868/BCCSGatewayWS/BCCSGatewayWS?wsdl",
        "username" => "1fd69023f8000d79",
        "password" => "3b9b581c1ba670875abb7d22177c9d1e"
    );

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function viewInfo($url) {
	 //echo $url;
        $content    = file_get_contents($url);
	 //var_dump($content);
        $content = json_decode($content);
        return $content;
    }

    public function buyLotto($url) {
        $content    = file_get_contents($url);
        $content = json_decode($content);
        return $content;
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'customer';
    }
    
    public function rules() {
        return array(
            array('code, amount', 'required'),
            array('code, amount', 'numerical', 'integerOnly' => true),
            array('code', 'length', 'max' => 3),
            array('amount', 'validateMoney'),
        );
    }
    
    public function validateMoney($attribute) {
        if($this->amount%1000 != 0) {
            $this->addError($attribute, 'Invalid Amount');
        }
    }
}