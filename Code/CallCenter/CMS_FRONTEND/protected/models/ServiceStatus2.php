<?php

/**
 * This is the model class for table "member_draw".
 *
 * The followings are the available columns in table 'member_draw':
 * @property integer $id
 * @property string $msisdn
 * @property string $code
 * @property integer $money
 * @property string $created_datetime
 * @property integer $type
 * @property integer $status
 */
class ServiceStatus2 extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return MemberDraw the static model class
     */
    
    public function getDbConnection() {
            return Yii::app()->db3;
    }
    
    public function getType() {
        switch ($this->type) {
            case 1:
                return '2 digits';
                break;
            default:
                return '3 digits';
                break;
        }
    }
    
    public function getWinStatus() {
        switch ($this->win_status) {
            case 1:
                return 'Yes';
                break;
            case 0:
                return 'No';
                break;
            default:
                return 'No';
                break;
        }
    }
    
    public function getTime() {
        $data   = array();
        $cmd    = Yii::app()->db3->createCommand('SELECT * FROM draw_time WHERE NOW() > start_time && NOW() < end_time && status=1');
        $rs     = $cmd->queryRow();
        if($rs)
            return $rs;
        else
            return FALSE;
    }
    
    public function getSellingStatus() {
        $sell = array();
        $sub1 = ' && status=1';
        
        $rs_time    = $this->getTime();
        if ($rs_time)
            $sub1 .= ' && created_datetime > "' . $rs_time['start_time'] . '" && created_datetime < "' . $rs_time['end_time'] . '"';

        $cmd = Yii::app()->db3->createCommand('SELECT SUM(money) as t1 FROM member_draw WHERE type=1' . $sub1);
        $data = $cmd->queryRow();
        if ($data)
            $sell['t1'] = $data['t1'];
        else
            $sell['t1'] = 0;
        $cmd1 = Yii::app()->db3->createCommand('SELECT SUM(money) as t2 FROM member_draw WHERE type=2' . $sub1);
        $data1 = $cmd1->queryRow();
        if ($data1)
            $sell['t2'] = $data1['t2'];
        else
            $sell['t2'] = 0;
        return $sell;
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'member_draw';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return array(
            array('msisdn,status', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'msisdn' => 'Msisdn',
            'code' => 'Code',
            'money' => 'Money',
            'created_datetime' => 'Created Datetime',
            'type' => 'Type',
            'status' => 'Status',
            'draw_no' => 'Draw No',
            'drawTime' => 'Draw Time',
            'win_status' => 'Win Status'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public $draw_no, $drawTime;
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;
        $criteria->alias    = 'a';
        $criteria->join     = 'INNER JOIN draw_time AS b ON a.draw_id=b.id';
        $criteria->select   = 'a.*,b.draw_no,b.end_time as drawTime';

        $criteria->compare('id', $this->id);
        $criteria->compare('msisdn', $this->msisdn, true);
        $criteria->compare('code', $this->code, true);
	 $criteria->compare('status', $this->status);
        $criteria->order = 'id DESC';

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                    'pagination' => array('pageSize'=>'25'),
                ));
    }

}