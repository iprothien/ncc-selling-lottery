<?php

class Paging {
    public static function showPageNavigation($currentPage, $maxPage, $path = '',$object = '',$first = '',$last = '') {
        $nav = array('left' => 2,'right' => 2,);
        if ($maxPage < $currentPage){$currentPage = $maxPage;}
        $max = $nav['left'] + $nav['right'];
        if ($max >= $maxPage) {
            $start = 1;
            $end = $maxPage;
        } elseif ($currentPage - $nav['left'] <= 0) {
            $start = 1;
            $end = $max + 1;
        } elseif (($right = $maxPage - ($currentPage + $nav['right'])) <= 0) {
            $start = $maxPage - $max;
            $end = $maxPage;
        } else {
            $start = $currentPage - $nav['left'];
            if ($start == 2) {
                $start = 1;
            }
            $end = $start + $max;
            if ($end == $maxPage - 1) {
                ++$end;
            }
        }
        $navig = '';
        if($currentPage > 1){
            $navig .= '<li><a href="' . $path . 'page'.$object.'=' . ceil(1) . '"><<</a></li>';
        }else{
            $navig ='';
        }
        if ($currentPage >= 2) {
            $navig .= '<li><a href="' . $path . 'page'.$object.'=' . ceil($currentPage - 1) . '"><</a></li>';
            if ($currentPage >= $nav['left']) {
                if ($currentPage - $nav['left'] > 2 && $max < $maxPage) {
                    $navig .= '<li><a href="' . $path . 'page'.$object.'=1' . '">'. $first .'1'. $last .'</a></li>';
                    $navig .= '<li>...</li>';
                }
            }
        }
        for ($i = $start; $i <= $end; $i++) {
            if ($i == $currentPage) {
                $navig .= '<li><a href="' . $path . 'page'.$object.'=' . $i . '" class="paging-active">'. $first . $i . $last .'</a></li>';
            }else {
                $navig .= '<li><a href="' . $path . 'page'.$object.'=' . $i . '">'. $first . $i . $last .'</a></li>';
            }
        }
        if ($currentPage <= $maxPage - 1) {
            if ($currentPage + $nav['right'] < $maxPage - 1 && $max + 1 < $maxPage) {
                $navig .= '<li>...</li>';
                $navig .= '<li><a href="' . $path . 'page'.$object.'=' . $maxPage . '">'. $first . $maxPage . $last .'</a></li>';
            }
            $navig .= '<li><a href="' . $path . 'page'.$object.'=' . ($currentPage + 1) . '">></a></li>';
        }
        if($currentPage <= $maxPage - 1){
            $navig .= '<li><a href="' . $path . 'page'.$object.'=' . ($maxPage) . '">>></a></li>';
        }
        return $navig;
    }
}