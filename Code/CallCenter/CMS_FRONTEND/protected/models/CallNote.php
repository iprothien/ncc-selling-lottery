<?php

class CallNote extends CActiveRecord{
    public static function model($className=__CLASS__){
        return parent::model($className);
    }

    public function tableName(){
        return 'call_note';
    }

    public function getDbConnection() {
        return Yii::app()->db2;
    }

    public function createNote($msisdn,$name,$address,$age,$content,$status){
        $sql = "INSERT INTO `call_note` SET `msisdn`='{$msisdn}',`name`='{$name}',`address`='{$address}',`age`='{$age}',`content`='{$content}',`status`='{$status}'";
        $result = Yii::app()->db2->createCommand($sql)->execute();
        return $result;
    }

    public function updateNote($msisdn,$name,$address,$age,$content,$status){
        $sql = "UPDATE `call_note` SET `name`='{$name}',`address`='{$address}',`age`='{$age}',`content`='{$content}',`status`='{$status}' WHERE `msisdn`='{$msisdn}'";
        $result = Yii::app()->db2->createCommand($sql)->execute();
        return $result;
    }

    public function updateMissCall($id){
        $sql = "UPDATE `miss_call` SET `status`=2 WHERE `id`='{$id}'";
        $result = Yii::app()->db2->createCommand($sql)->execute();
        return $result;
    }

    public function updateAgentAnswer($id,$note){
        $sql = "UPDATE `ncc_agent_answer` SET `note`='{$note}' WHERE `id`='{$id}'";
        $result = Yii::app()->db2->createCommand($sql)->execute();
        return $result;
    }

    public function createAnswerMiss($agent_id,$note,$msisdn,$phone_agent){
        $date = date('Y-m-d H:i:s');
        $sql = "INSERT INTO `ncc_agent_answer` SET `agent_id`='{$agent_id}',`path_record`='NULL',`msisdn_agent`='{$phone_agent}',`msisdn_member`='{$msisdn}',`call_time`='{$date}',`duration`=0,`status`=1,`note`='{$note}',`updated`='{$date}'";
        $result = Yii::app()->db2->createCommand($sql)->execute();
        return $result;
    }

    public function getInfo($msisdn){
        $sql = "SELECT * FROM `call_note` WHERE `msisdn`=$msisdn";
        $result = Yii::app()->db2->createCommand($sql)->queryRow();
        return $result;
    }

    public function getAnswerNote($id){
        $sql = "SELECT `note` FROM `ncc_agent_answer` WHERE `id`=$id";
        $result = Yii::app()->db2->createCommand($sql)->queryRow();
        return $result;
    }

    public function getCallNote($msisdn){
        $sql = "SELECT * FROM `call_note` WHERE `msisdn`=$msisdn";
        $result = Yii::app()->db2->createCommand($sql)->queryRow();
        return $result;
    }

    public function getPhoneAgent($id){
        $sql = "SELECT * FROM `ncc_agent` WHERE `id`=$id";
        $result = Yii::app()->db2->createCommand($sql)->queryRow();
        return $result;
    }

    public function checkNote($msisdn){
        $sql = "SELECT COUNT(id) FROM `call_note` WHERE `msisdn`=$msisdn";
        $result = Yii::app()->db2->createCommand($sql)->queryScalar();
        return $result;
    }
} 