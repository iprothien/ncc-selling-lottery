<?php
/* @var $this MemberDrawController */
/* @var $model MemberDraw */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php
    Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
    $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row" style="width:48%;float:left">
		<?php echo $form->label($model,'msisdn'); ?>
		<?php echo $form->textField($model,'msisdn',array('size'=>20,'maxlength'=>16)); ?>
	</div>

	<div class="row" style="width:50%;float:left;">
		<?php echo $form->label($model,'code',array('style'=>'width:110px;')); ?>
		<?php echo $form->textField($model,'code',array('size'=>20,'maxlength'=>3)); ?>
	</div>

	<div class="row" style="width:48%;float:left">
		<?php echo $form->label($model,'time_start'); ?>
		<?php 
                    $this->widget('CJuiDateTimePicker',array(
                    'model'=>$model, //Model object
                    'attribute'=>'time_start', //attribute name
                    'mode'=>'datetime', //use "time","date" or "datetime" (default)
                    'options'=>array("dateFormat"=>'yy/mm/dd'), // jquery plugin options
                    'language' => ''
            )); ?>
	</div>
    
        <div class="row" style="width:50%;float:left;">
		<?php echo $form->label($model,'time_end',array('style'=>'width:110px;')); ?>
		<?php 
                    $this->widget('CJuiDateTimePicker',array(
                    'model'=>$model, //Model object
                    'attribute'=>'time_end', //attribute name
                    'mode'=>'datetime', //use "time","date" or "datetime" (default)
                    'options'=>array("dateFormat"=>'yy/mm/dd'), // jquery plugin options
                    'language' => ''
            )); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->