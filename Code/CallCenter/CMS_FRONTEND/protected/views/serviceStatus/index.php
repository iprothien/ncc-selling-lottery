<?php
/* @var $this MemberDrawController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Member Draws',
);

$this->menu=array(
	array('label'=>'Create MemberDraw', 'url'=>array('create')),
	array('label'=>'Manage MemberDraw', 'url'=>array('admin')),
);
?>

<h1>Member Draws</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
