
<?php
    $sell   = array();
    $cmd    = Yii::app()->db->createCommand('SELECT SUM(money) as t1 FROM member_draw WHERE type=1');
    $data1  = $cmd->queryRow();
    if($data1)
        $data['t1'] = $data1['t1'];
    else
        $data['t1'] = 0;
    $cmd1   = Yii::app()->db->createCommand('SELECT SUM(money) as t2 FROM member_draw WHERE type=2');
    $data2  = $cmd1->queryRow();
    if($data2)
        $data['t2'] = $data2['t2'];
    else
        $data['t2'] = 0;
    $sell   = $data;

    $data   = $model->getQuota();
    
    $tmax = number_format($data['max_two'] + $data['max_three']);
    $t2 = number_format($data['max_two']);
    $t3 = number_format($data['max_three']);
?>
<div id="selling_status">
    <p style="font-weight: bold;color:#636363">Selling Status</p>
    <div class="line">
        <span class="group1">Total</span>
        <span class="group2"><?php echo $smax = number_format($sell['t1'] + $sell['t2']); ?></span>
        <span class="group3"><?php echo round($smax/$tmax*100); ?> %</span>
    </div>
    <div class="line">
        <span class="group1">2digit</span>
        <span class="group2"><?php echo $s2 = number_format($sell['t1']); ?></span>
        <span class="group3"><?php echo round($s2/$t2*100); ?> %</span>
    </div>
    <div class="line">
        <span class="group1">3digit</span>
        <span class="group2"><?php echo $s3 = number_format($sell['t2']); ?></span>
        <span class="group3"><?php echo round($s3/$t3*100); ?> %</span>
    </div>
</div>