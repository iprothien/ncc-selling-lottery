<?php
$form = $this->beginWidget('CActiveForm', array(
    'action' => Yii::app()->createUrl($this->route),
    'method' => 'get',
));
?>
<?php echo $form->textField($model, 'msisdn', array('size' => 20, 'maxlength' => 16, 'placeholder'=>'Phone Number')); ?>

<?php echo CHtml::submitButton('Search'); ?>
<?php $this->endWidget(); ?>