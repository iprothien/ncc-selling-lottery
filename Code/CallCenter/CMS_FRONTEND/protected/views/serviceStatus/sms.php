<?php
    /* @var $this MemberDrawController */
    /* @var $model MemberDraw */

    Yii::app()->clientScript->registerScript('search', "
        $('.statistic-form form').submit(function(){
            $.fn.yiiGridView.update('service-status-grid', {
                data: $(this).serialize()
            });
            return false;
        });
    ");

    $data = $model->getQuota();
    if(!$data) {
        $data['max_two']    = 100000000;
        $data['max_three']  = 10000000;
    }
    ?>
    <script>
        $(document).ready(function(){
            setInterval(function() {
                $("#selling_status").load('index.php?r=serviceStatus/view&id=1');
            }, 5000);
        });
    </script>
    <div id="quota">
        <p style="font-weight: bold;color:#636363">Quota</p>
        <div class="line">
            <span class="group1">Total</span>
            <span class="group2"><?php echo $tmax = number_format($data['max_two'] + $data['max_three']); ?></span>
            <span class="group3">LAK</span>
        </div>
        <div class="line">
            <span class="group1">2digit</span>
            <span class="group2"><?php echo $t2 = number_format($data['max_two']); ?></span>
            <span class="group3">LAK</span>
        </div>
        <div class="line">
            <span class="group1">3digit</span>
            <span class="group2"><?php echo $t3 = number_format($data['max_three']); ?></span>
            <span class="group3">LAK</span>
        </div>
    </div>

    <?php

    $this->renderPartial('_sellingstatus', array(
        'sell' => $sell,
    ));
?>

<div style="float: left;width: 100%;margin-top: 10px;margin-bottom: 10px;border-bottom: 3px solid #4F81BD;">
    <a href="<?php echo Yii::app()->createUrl('serviceStatus/admin'); ?>" class="tab">IVR History</a>
    <a  href="<?php echo Yii::app()->createUrl('serviceStatus/sms'); ?>" class="tab active">SMS History</a>
</div>

<div id="sms-history">
    <?php $this->renderPartial('_sms', array()); ?>
</div>