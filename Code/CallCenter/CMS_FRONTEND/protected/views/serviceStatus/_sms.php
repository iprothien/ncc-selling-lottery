<form method="POST" name="current">
<div class="sms-main">
    <div class="sms" style="width: 95% !important;">
        <div class="sms-history">
            <div class="search">
                <span class="title-search">&nbsp;</span>
                <input class="input-search" type="text" name="sText" placeholder="Mobile number" />
                <input class="bottom-search" type="submit" value="Search">
            </div>
            <table class="table" style="border: 1px solid #D0E3EF;border-collapse: collapse;width: 100%;">
                <tbody>
                <tr class="border_bottom tr">
                    <th class="th" style="width: 20%;">Phone Number</th>
                    <th class="th" style="width: 20%;">Number</th>
                    <th class="th" style="width: 20%;">Amount</th>
                    <th class="th" style="border-right: 0px;background: #FFF8E6;width: 40%;">Date time</th>
                </tr>
                <?php
                    $sms = new Service();
                    $i=0;
                    if($_SERVER['REQUEST_METHOD'] == 'POST'){
                        $msisdn = isset($_REQUEST['sText'])?$_REQUEST['sText']:0;
                        if($msisdn){
                            $pSms = explode('|',$sms->SaleHistory($msisdn)->SaleHistoryResult);
                        }else{
                            $pSms = $sms->getSmsAmount(0);
                        }
                ?>
                    <?php if($pSms){ ?>
                        <?php
                        for($i=0;$i<count($pSms)-1;$i++){
                            $item = $g = explode(';',$pSms[$i]);
                            $digits = explode(';',$sms->SaleHistoryDetail($item[2])->SaleHistoryDetailResult);
                            ?>
                            <tr class="td-border tr">
                                <td class="td" style="width: 20%;"><?php echo $msisdn; ?></td>
                                <td class="td" style="width: 20%;"><?php echo $digits[0]; ?></td>
                                <td class="td" style="width: 20%;font-weight: bold;"><?php echo $item[3]; ?></td>
                                <?php $dateSMS = explode('/',$item[1]); ?>
                                <td class="td" style="border-right: none;background: #FFF8E6;width: 40%;"><?php echo $dateSMS[0].'-'.$dateSMS[1].'-'.$dateSMS[2].' '.$item[6]; ?></td>
                            </tr>
                        <?php } ?>
                    <?php } ?>
                <?php } ?>
                <?php if(empty($pSms)){ ?>
                    <tr class="td-border tr">
                        <td class="td" colspan="4" style="color: red;"><?php echo 'No data result'; ?></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

</form>