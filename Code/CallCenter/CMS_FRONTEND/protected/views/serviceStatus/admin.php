<?php
/* @var $this MemberDrawController */
/* @var $model MemberDraw */

Yii::app()->clientScript->registerScript('search', "
$('.statistic-form form').submit(function(){
	$.fn.yiiGridView.update('service-status-grid', {
		data: $(this).serialize()
	});
	return false;
});
");

?>
<script>
    $(document).ready(function(){
        setInterval(function() {
	     $("#selling_status").load('index.php?r=serviceStatus/view&id=1');
        }, 5000);
    });
</script>
<div id="quota">
    <p style="font-weight: bold;color:#636363"><?php echo Yii::t('label', 'quocta'); ?></p>
    <div class="line">
        <span class="group1"><?php echo Yii::t('label', 'total'); ?></span>
        <span class="group2"><?php echo $tmax = number_format($sell['max_two'] + $sell['max_three']); ?></span>
        <span class="group3">LAK</span>
    </div>
    <div class="line">
        <span class="group1"><?php echo Yii::t('label', '2digit'); ?></span>
        <span class="group2"><?php echo $t2 = number_format($sell['max_two']); ?></span>
        <span class="group3">LAK</span>
    </div>
    <div class="line">
        <span class="group1"><?php echo Yii::t('label', '3digit'); ?></span>
        <span class="group2"><?php echo $t3 = number_format($sell['max_three']); ?></span>
        <span class="group3">LAK</span>
    </div>
</div>

<?php

    $this->renderPartial('_sellingstatus', array(
        'sell' => $sell,
    ));
?>

<div style="float: left;width: 100%;margin-top: 10px;margin-bottom: 10px;border-bottom: 3px solid #4F81BD;">
    <a href="<?php echo Yii::app()->createUrl('serviceStatus/admin'); ?>" class="tab active" name="tab1"><?php echo Yii::t('label', 'ivrHistory'); ?></a>
    <a href="<?php echo Yii::app()->createUrl('serviceStatus/sms'); ?>" class="tab" name="tab2"><?php echo Yii::t('label', 'smsHistory'); ?></a>
</div>

<div class="clear"></div>
<div id="wrapper-statistic">
    <div class="statistic-form">
        <?php
        $this->renderPartial('_search', array(
            'model' => $model,
        ));
        ?>
    </div><!-- search-form -->

    <?php
    if(isset($msisdn) && $msisdn):
    $this->widget('zii.widgets.grid.CGridView', array(
        'id' => 'service-status-grid',
        'dataProvider' => $model->search(),
        'ajaxUpdate' => false,
        'template' => '{items}{pager}',
        'pager' => array(
            'header' => ''
        ),
        'summaryText' => '',
        'columns' => array(
            'id',
            'msisdn',
            array('name' => 'code', 'value' => '$data->code', 'type' => 'raw', 'htmlOptions' => array('style' => 'font-weight:bold;')),
            array('name' => 'money', 'value' => '$data->money', 'type' => 'raw', 'htmlOptions' => array('style' => 'font-weight:bold;')),
            array('name' => 'type', 'value' => '$data->getType()', 'type' => 'raw', 'htmlOptions' => array('style' => 'font-weight:bold;')),
            array('name' => 'draw_no', 'value' => '$data->draw_no'),
            'drawTime',
            array('name' => 'created_datetime', 'value' => '$data->created_datetime', 'type' => 'raw', 'htmlOptions' => array('class' => 'td_private')),
            array('name' => 'win_status', 'value' => '$data->getWinStatus()')
        ),
    ));
    endif;
    ?>
</div>



