<?php
/* @var $this MemberDrawController */
/* @var $model MemberDraw */

$this->breadcrumbs=array(
	'Member Draws'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List MemberDraw', 'url'=>array('index')),
	array('label'=>'Manage MemberDraw', 'url'=>array('admin')),
);
?>

<h1>Create MemberDraw</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>