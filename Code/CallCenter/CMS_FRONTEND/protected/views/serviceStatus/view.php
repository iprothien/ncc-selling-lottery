<p style="font-weight: bold;color:#636363"><?php echo Yii::t('label', 'sellingStatus'); ?>: 
    <?php if (isset($sell['start_time']) && $sell['start_time']): ?>
        <span style="font-weight: normal;">  From <span class="red"><?php echo date('H:i:s d/m/Y', strtotime($sell['start_time'])); ?></span> To 
            <span class="red"><?php echo date('H:i:s d/m/Y', strtotime($sell['end_time'])); ?></span>
        </span>
    <?php endif; ?>
</p>
<div class="line">
    <span class="group1"><?php echo Yii::t('label', 'total'); ?></span>
    <span class="group2"><?php echo number_format($sell['t1'] + $sell['t2'] + $sell['t3'] + $sell['t4'] + $sell['t5'] + $sell['t6']); ?></span>
    <span class="group3"><?php echo round((($sell['t1'] + $sell['t2'] + $sell['t3'] + $sell['t4'] + $sell['t5'] + $sell['t6']) / $sell['max']) * 100, 3); ?>%</span>
</div>
<div class="line">
    <span class="group1"><?php echo Yii::t('label', '2digit'); ?></span>
    <span class="group2"><?php echo number_format($sell['t1'] + $sell['t3']); ?></span>
    <span class="group3"><?php echo round(100 * ($sell['t1'] + $sell['t3']) / $sell['max_two'], 3); ?> %</span>
</div>
<div class="line">
    <span class="group1"><?php echo Yii::t('label', '3digit'); ?></span>
    <span class="group2"><?php echo number_format($sell['t2'] + $sell['t4']); ?></span>
    <span class="group3"><?php echo round(100 * ($sell['t2'] + $sell['t4']) / $sell['max_three'], 3); ?> %</span>
</div>

<div style="clear:both; margin: 0 auto; height:1px; border-top: 1px solid #636363; width:70%; margin-top: 10px;"></div>
<p style="font-weight: bold;color:#636363; margin: 12px 0 5px;">Beeline<span style="float:right; padding-right: 45%;">ETL</span></p>
<div class="half">
    <div class="line">
        <span class="group1"><?php echo Yii::t('label', 'total'); ?></span>
        <span class="group4"><?php echo number_format($sell['t1'] + $sell['t2']); ?></span>
        <span class="group3"><?php echo round(100 * ($sell['t1'] + $sell['t2']) / $sell['max'], 3); ?> %</span>
    </div>
    <div class="line">
        <span class="group1"><?php echo Yii::t('label', '2digit'); ?></span>
        <span class="group4"><?php echo $s2 = number_format($sell['t1']); ?></span>
        <span class="group3"><?php echo round(100 * $sell['t1'] / $sell['max_two'], 3); ?> %</span>
    </div>
    <div class="line">
        <span class="group1"><?php echo Yii::t('label', '3digit'); ?></span>
        <span class="group4"><?php echo number_format($sell['t2']); ?></span>
        <span class="group3"><?php echo round(100 * $sell['t2'] / $sell['max_three'], 3); ?> %</span>
    </div>
</div>

<div class="half">
    <div class="line">
        <span class="group1"><?php echo Yii::t('label', 'total'); ?></span>
        <span class="group4"><?php echo number_format($sell['t3'] + $sell['t4']); ?></span>
        <span class="group3"><?php echo round(100 * ($sell['t3'] + $sell['t4']) / $sell['max'], 3); ?> %</span>
    </div>
    <div class="line">
        <span class="group1"><?php echo Yii::t('label', '2digit'); ?></span>
        <span class="group4"><?php echo number_format($sell['t3']); ?></span>
        <span class="group3"><?php echo round(100 * $sell['t3'] / $sell['max_two'], 3); ?> %</span>
    </div>
    <div class="line">
        <span class="group1"><?php echo Yii::t('label', '3digit'); ?></span>
        <span class="group4"><?php echo number_format($sell['t4']); ?></span>
        <span class="group3"><?php echo round(100 * $sell['t4'] / $sell['max_three'], 3); ?> %</span>
    </div>
</div>

<div class="clear"></div>
<p style="font-weight: bold;color:#636363; margin: 12px 0 5px;">Unitel<span style="float:right; padding-right: 45%;"></span></p>
<div class="half">
    <div class="line">
        <span class="group1"><?php echo Yii::t('label', 'total'); ?></span>
        <span class="group4"><?php echo number_format($sell['t5'] + $sell['t6']); ?></span>
        <span class="group3"><?php echo round(100 * ($sell['t5'] + $sell['t6']) / $sell['max'], 3); ?> %</span>
    </div>
    <div class="line">
        <span class="group1"><?php echo Yii::t('label', '2digit'); ?></span>
        <span class="group4"><?php echo number_format($sell['t5']); ?></span>
        <span class="group3"><?php echo round(100 * $sell['t5'] / $sell['max_two'], 3); ?> %</span>
    </div>
    <div class="line">
        <span class="group1"><?php echo Yii::t('label', '3digit'); ?></span>
        <span class="group4"><?php echo number_format($sell['t6']); ?></span>
        <span class="group3"><?php echo round(100 * $sell['t6'] / $sell['max_three'], 3); ?> %</span>
    </div>
</div>