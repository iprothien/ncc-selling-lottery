<?php
$cmd_time   = Yii::app()->db4->createCommand('SELECT * FROM draw_time WHERE NOW() > start_time && NOW() < end_time');
$rs_time    = $cmd_time->queryRow();
$sub        = '';
if($rs_time)
    $sub    = ' && created_datetime > "'.$rs_time['start_time'].'" && created_datetime < "'.$rs_time['end_time'].'"';

$cmd_time1   = Yii::app()->db3->createCommand('SELECT * FROM draw_time WHERE NOW() > start_time && NOW() < end_time');
$rs_time1    = $cmd_time1->queryRow();
$sub1        = '';
if($rs_time1)
    $sub1    = ' && created_datetime > "'.$rs_time1['start_time'].'" && created_datetime < "'.$rs_time1['end_time'].'"';

$sell = array();
$cmd = Yii::app()->db3->createCommand('SELECT SUM(money) as t1 FROM member_draw WHERE type=1'.$sub1);
$data1 = $cmd->queryRow();
if ($data1)
    $data['t1'] = $data1['t1'];
else
    $data['t1'] = 0;
$cmd1 = Yii::app()->db3->createCommand('SELECT SUM(money) as t2 FROM member_draw WHERE type=2'.$sub1);
$data2 = $cmd1->queryRow();
if ($data2)
    $data['t2'] = $data2['t2'];
else
    $data['t2'] = 0;
$sell = $data;

/* beeline */
$cmd3 = Yii::app()->db4->createCommand('SELECT SUM(money) as t1 FROM member_draw WHERE type=1'.$sub);
$data3 = $cmd3->queryRow();
if ($data3)
    $data['t3'] = $data3['t1'];
else
    $data['t3'] = 0;
$cmd4 = Yii::app()->db4->createCommand('SELECT SUM(money) as t2 FROM member_draw WHERE type=2'.$sub);
$data4 = $cmd4->queryRow();
if ($data2)
    $data['t4'] = $data4['t2'];
else
    $data['t4'] = 0;
$sell = $data;


$data = $model->getQuota();

$cmd = Yii::app()->db->createCommand('SELECT max_two,max_three FROM quocta WHERE 1');
$rs_max = $cmd->queryRow();
if ($rs_max) {
    $tmax = ($rs_max['max_two'] + $rs_max['max_three']);
    $t2 = ($rs_max['max_two']);
    $t3 = ($rs_max['max_three']);
} else {
    $tmax = 20000000;
    $t2 = 10000000;
    $t3 = 10000000;
}
?>
<div id="selling_status">
    <p style="font-weight: bold;color:#636363">Selling Status: 
	<span style="font-weight: normal;">  From <span class="red"><?php if(isset($rs_time)): echo date('H:i:s d/m/Y',strtotime($rs_time['start_time'])); ?></span> To 
       <span class="red"><?php echo date('H:i:s d/m/Y',strtotime($rs_time['end_time'])); endif; ?></span></span>
    </p>
    <div class="line">
        <span class="group1">Total</span>
        <span class="group2"><?php echo $smax = number_format($sell['t1'] + $sell['t2'] + $sell['t3'] + $sell['t4']); ?></span>
        <span class="group3"><?php echo round((($sell['t1'] + $sell['t2'] + $sell['t3'] + $sell['t4']) / $tmax) * 100,3); ?>%</span>
    </div>
    <div class="line">
        <span class="group1">2digit</span>
        <span class="group2"><?php echo $s2 = number_format($sell['t1'] + $sell['t3']); ?></span>
        <span class="group3"><?php echo round(100 * ($sell['t1'] + $sell['t3']) / $t2, 3); ?> %</span>
    </div>
    <div class="line">
        <span class="group1">3digit</span>
        <span class="group2"><?php echo $s3 = number_format($sell['t2'] + $sell['t4']); ?></span>
        <span class="group3"><?php echo round(100 * ($sell['t2'] + $sell['t4']) / $t3, 3); ?> %</span>
    </div>
    
    <div style="clear:both; margin: 0 auto; height:1px; border-top: 1px solid #636363; width:70%; margin-top: 10px;"></div>
    <p style="font-weight: bold;color:#636363; margin: 10px 0;">Beeline<span style="float:right; padding-right: 45%;">ETL</span></p>
    <div class="half">
        <div class="line">
            <span class="group1">Total</span>
            <span class="group4"><?php echo $smax = number_format($sell['t1'] + $sell['t2']); ?></span>
            <span class="group3"><?php echo round(100 * ($sell['t1'] + $sell['t2']) / $tmax, 3); ?> %</span>
        </div>
        <div class="line">
            <span class="group1">2digit</span>
            <span class="group4"><?php echo $s2 = number_format($sell['t1']); ?></span>
            <span class="group3"><?php echo round(100 * $sell['t1'] / $t2, 3); ?> %</span>
        </div>
        <div class="line">
            <span class="group1">3digit</span>
            <span class="group4"><?php echo $s3 = number_format($sell['t2']); ?></span>
            <span class="group3"><?php echo round(100 * $sell['t2'] / $t3, 3); ?> %</span>
        </div>
    </div>
    
    <div class="half">
        <div class="line">
            <span class="group1">Total</span>
            <span class="group4"><?php echo $smax = number_format($sell['t3'] + $sell['t4']); ?></span>
            <span class="group3"><?php echo round(100 * ($sell['t3'] + $sell['t4']) / $tmax, 3); ?> %</span>
        </div>
        <div class="line">
            <span class="group1">2digit</span>
            <span class="group4"><?php echo $s2 = number_format($sell['t3']); ?></span>
            <span class="group3"><?php echo round(100 * $sell['t3'] / $t2, 3); ?> %</span>
        </div>
        <div class="line">
            <span class="group1">3digit</span>
            <span class="group4"><?php echo $s3 = number_format($sell['t4']); ?></span>
            <span class="group3"><?php echo round(100 * $sell['t4'] / $t3, 3); ?> %</span>
        </div>
    </div>
</div>