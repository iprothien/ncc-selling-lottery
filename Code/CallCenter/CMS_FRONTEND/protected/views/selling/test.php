<div class="buy-content">
        <div class="buy-left">
            <?php if (isset($rsInfor) && $rsInfor){ ?>
            <div class="customer-info">
                <h3>Customer Infor</h3>
                <p><label>Number</label><span><?php echo $msisdn; ?></span></p>
                <p><label>Name</label><span><?php echo $model->name; ?></span></p>
                <p><label>Address</label><span><?php echo $model->address; ?></span></p>
                <p>
                    <label>Current Balance</label>
                    <span <?php if (!is_numeric($rsInfor[0]->infoMoney)): ?> style="color:red; font-weight: bold; text-transform: uppercase;" <?php endif; ?>>
                        <?php echo $rsInfor[0]->infoMoney; ?>&nbsp;
                        <?php if (!is_numeric($rsInfor[0]->infoMoney)): ?>
                            <span style="color:red; font-weight: bold;">(Can't buy lotto)</span>
                        <?php endif; ?>
                    </span>
                </p>
            </div>

            <div style="clear:both; margin: 0 auto; margin-bottom: 15px; margin-top: 20px; border-top: 1px dotted #CCC; width: 70%;"></div>

            <div class="purchase">
                <h3>Purchase History</h3>
                <p><label>Total Buy</label><span><?php echo $rsInfor[0]->totalBuy; ?></span></p>
                <p><label>Total Money</label><span><?php echo $rsInfor[0]->totalMoney; ?></span></p>
                <p><label>Win</label><span><?php echo $rsInfor[0]->totalWin; ?></span></p>
            </div>

            <p style="color: red; margin-top: 20px; font-weight: bold;">
                <?php if (Yii::app()->user->hasFlash('statusConnect')) { ?>
                    <?php echo Yii::app()->user->getFlash('statusConnect'); ?>
                <?php } else { ?>
                    <span style="color: #219648">Status: OK</span>
                <?php } ?>
            </p>
            <?php }else{ ?>
            <div class="customer-info">
                <h3>Customer Infor</h3>
                <p style="font-weight: bold; color: red;">Don't exits phone number. Please try again!</p>
            </div>
            <?php }; ?>
        </div>
    <?php if (isset($rsInfor) && $rsInfor): ?>
    <div class="buy">
        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'frmBuying',
            'enableAjaxValidation' => false,
                ));
        ?>
        <input type="text" value="<?php echo $rsInfor[0]->infoMoney; ?>" id="currentMoney" style="display:none" name="currentMoney" />
        <div class="head-buy">
            <h3 style="font-size: 20px;color: red;">Selling</h3>
            <?php echo CHtml::button('Remove', array('id' => 'removebuy', 'style' => 'background: #DA2F2A;margin-left: 5px;')); ?>
            <?php echo CHtml::button('Add', array('id' => 'addbuy')); ?>
        </div>
        <div class="clear"></div>
        <p class="error" style="display: block;">
            <label class="lbleft">3 Digits Quota</label><span>: <?php echo number_format($quota['max_three']).' KIP'; ?></span><span style="float: right;">100,000,000 KIP</span><label class="lbright">Remaining:</label>
            <br />
            <label class="lbleft">2 Digits Quota</label><span>: <?php echo number_format($quota['max_two']).' KIP'; ?></span><span style="float: right;">100,000,000 KIP</span><label class="lbright">Remaining:</label>
            <div class="clear"></div>
        </p>
        <div class="row-buy">
            <div style='margin: 10px 0;'>
                <?php echo $form->errorSummary($model); ?>
            </div>
            <input type="text" id="code0" onchange="javascript:validBuy('code', '1', '0')" placeholder="Number" name="listCode[0]" class="totalRecord" size="15" value="" />
            <input type="text" id="listAmount0" onchange="javascript:validBuy('listAmount', '2', '0')" placeholder="Amount of money" name="listAmount[0]" style="margin-left:13px;" size="20" value="" />
            <span class="errorBuy" id="errorBuy0"></span>
            <input type="button" id="reset0" value="Clear" onclick="javascript:clearAttr('reset', '0')" />
            <span id="addArea">
            </span>
            <?php if (Yii::app()->user->hasFlash('submitBuying')): ?>
                <p class="error">
                    <?php echo Yii::app()->user->getFlash('submitBuying'); ?>
                </p>
            <?php endif; ?>
        </div>

        <div class="submit-buy">
            <?php echo CHtml::submitButton('Buy'); ?>
            <?php echo CHtml::resetButton('Clear'); ?>
        </div>

        <?php $this->endWidget(); ?>
    </div>
    <?php endif; ?>
</div>

<div class="clear"></div>
<div id="content-buyagent" style="margin-top: 15px;">
    <table class="buy-agent">
        <thead>
            <tr>
                <th>ID</th>
                <th>Phone Number</th>
                <th>Time</th>
                <th>Lotto Number</th>
                <th>Money Charge</th>
                <th>Status</th>
            </tr>
        </thead>
        <?php if (isset($rsInfor[0]->list) && $rsInfor[0]->list): ?>
            <tbody>
                <?php foreach ($rsInfor[0]->list as $item): ?>
                    <tr>
                        <td><?php echo $item->id; ?></td>
                        <td><?php echo $item->msisdn; ?></td>
                        <td class="td_private"><?php echo $item->created_datetime; ?></td>
                        <td><b><?php echo $item->code; ?></b></td>
                        <td><b><?php echo $item->money; ?></b></td>
                        <td>OK</td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        <?php endif; ?>
    </table>

    <?php if (!$rsInfor[0]->list): ?>
        <p style="padding:10px;background: #FFF;font-style: italic;">No Result Found</p>
    <?php endif; ?>
</div>

<script type="text/javascript">
    var maxMoney = parseInt($("#currentMoney").val());
    function validBuy(id, type, number) {
        var sms = '';
        var max = parseInt($('input.totalRecord').length);
        if (type == '1') {
            if ($('#' + id + number).val().length > 3 || $('#' + id + number).val().length < 2 || isNaN($('#' + id + number).val())) {
                sms = 'Invalid Number';
            }
        } else {
            var code   = $("#code" + number).val();
            var money   = $('#' + id + number).val();
            if (isNaN(money) || 
                parseInt(money) % 1000!='0' || 
                parseInt(money) < 1000 || 
                (code.length==3 && parseInt(money)>2500000) || 
                (code.length==2 && parseInt(money)>500000)
        ) {
                sms = 'Invalid Amount';
            }
            var validMoney = 0;
            var i = 0;
            for (i; i < max; i++) {
                if ($('#' + id + i).val() != '')
                    validMoney = validMoney + parseInt($('#' + id + i).val());
            }
            if (validMoney > maxMoney)
                sms = 'Not Enough Money';
        }

        if (sms != '') {
            $("#errorBuy" + number).empty();
            $("#errorBuy" + number).append(sms);
            if (sms == 'Not Enough Money') {
                $('#code' + number).css({"color": "red", "border": "1px dotted red", "padding": "5px"});
                $('#' + id + number).css({"color": "red", "border": "1px dotted red", "padding": "5px"});
            } else
                $('#' + id + number).css({"color": "red", "border": "1px dotted red", "padding": "5px"});
        } else {
            $('#' + id + number).css({"color": "#000", "border": "none", "padding": "6px"});

            if ($('#errorBuy' + number).text() != 'Not Enough Money')
                $("#errorBuy" + number).empty();
            else {
                if ($('#code' + number).val().length > 3 || $('#code' + number).val().length < 2 || isNaN($('#code' + number).val())) {
                    $("#errorBuy" + number).empty();
                    $("#errorBuy" + number).append('Invalid Number');
                    $('#code' + number).css({"color": "red", "border": "1px dotted red", "padding": "5px"});
                } else {
                    $("#errorBuy" + number).empty();
                    $('#code' + number).css({"color": "#000", "border": "none", "padding": "6px"});
                }
            }
                            
            var j = 0;
            for (j; j < max; j++) {
                var borderCode = $('#code' + j).css("border");
                var borderAmount = $('#listAmount' + j).css("border");
                if (borderCode == '1px dotted red' && borderAmount == '1px dotted red') {
                    $('#listAmount' + j).css({"color": "#000", "border": "none", "padding": "6px"});
                    if ($('#code' + j).val().length > 3 || $('#code' + j).val().length < 2 || isNaN($('#code' + j).val())) {
                        $("#errorBuy" + j).empty();
                        $("#errorBuy" + j).append('Invalid Number');
                        $('#code' + j).css({"color": "red", "border": "1px dotted red", "padding": "5px"});
                    } else {
                        $("#errorBuy" + j).empty();
                        $('#code' + j).css({"color": "#000", "border": "none", "padding": "6px"});
                    }
                }
            }
        }
    }

    /**
     * Clear Attribute
     */
    function clearAttr(id, number) {
        $("#listAmount" + number).val('');
        $("#code" + number).val('');
    }
    
    $().ready(function() {
        var total = 1;
        $("#addbuy").click(function() {
            total = $("input.totalRecord").length;
            if (total < 6)
                $("#addArea").append('<div id="avg' + total + '" style="clear:both; margin-top:10px;">\n\
<input onchange=\'javascript:validBuy("code","1","' + total + '")\' id="code' + total + '" class="totalRecord" placeholder="Number" size="15" type="text" name="listCode[' + total + ']" value="" />\n\
<input onchange=\'javascript:validBuy("listAmount","2","' + total + '")\' id="listAmount' + total + '" size="20" style="margin-left:13px;" placeholder="Amount of Money" type="text" name="listAmount[' + total + ']" value="" />\n\
<input type="button" id="reset' + total + '" value="Clear" onclick=\'javascript:clearAttr("reset","' + total + '")\' />\n\
<span class="errorBuy" id="errorBuy' + total + '"></span></div>');
            });

            $("#removebuy").click(function() {
                var totalRecord = $("input.totalRecord").length;
                if (totalRecord > 1) {
                    $('#avg' + (totalRecord - 1)).remove();
                }
            });

            $("form#frmBuying").submit(function(e) {
                var i = 0;
                var j = 0;
                var empty = 0;
                var error = 0;
                var validMoney = 0;
                var str = '';
                var strAmount   = '';
                for (i; i < 6; i++) {
                    if($("#code" + i).length > 0) {
                        str = $('#code' + i).val();
                        strAmount   = $("#listAmount" + i).val();
                        if(str != '') {
                            if(strAmount != '') {
                                //HOP LE
                                if (isNaN(str) || 
                                    isNaN(strAmount) || 
                                    parseInt(strAmount)<1000 || 
                                    parseInt(strAmount)%1000!=0 || 
                                    str.length > 3 || 
                                    (str.length == 3 && parseInt(strAmount)>=2500000) || 
                                    (str.length == 2 && parseInt(strAmount)>500000)
                            ) {
                                    
                                    error++;
                                    $('#errorBuy' + i).empty();
                                    $('#errorBuy' + i).append('Invalid Content');
                                }
                            } else {
                                // Thieu 1 trong 2 gia tri
                                $('#errorBuy' + i).empty();
                                $('#errorBuy' + i).append('Invalid Content');
                                error++;
                            }
                        } else {
                            if(strAmount != '') {
                                // Thieu 1 trong 2 gia tri
                                $('#errorBuy' + i).empty();
                                $('#errorBuy' + i).append('Invalid Content');
                                error++;
                            } else {
                                // 1 ban ghi khong co gia tri
                                empty++;
                            }
                        }
                    }
                    
                    if(parseInt($("#listAmount" + i).val()) > 0)
                        validMoney = validMoney + parseInt($('#listAmount' + i).val());
                
                }
                
                if(empty == $("input.totalRecord").length) {
                    $("#errorBuy0").empty();
                    $("#errorBuy0").append('Empty Content');
                }
                
                if(validMoney > parseInt($("#currentMoney").val())) {
                    $("#errorBuy" + ($("input.totalRecord").length - 1)).empty();
                    $("#errorBuy" + ($("input.totalRecord").length - 1)).append('Not Enough Money');
                }
                
                //Khong co loi || tat ca cac ban ghi trong

                if (error == 0 && validMoney <= parseInt($("#currentMoney").val()) && empty < $("input.totalRecord").length) {
                    return true;
                }
                
                e.preventDefault();
            }); 
        });
</script>