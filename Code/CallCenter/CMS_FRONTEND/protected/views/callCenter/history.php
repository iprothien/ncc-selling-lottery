<form method="post">
    <div class="note-top">
        <span class="note-title">Edit member info</span>
        <div class="note-button-main" style="margin-top: -30px;">
            <a href="<?php echo Yii::app()->createUrl('callCenter/excelhistory',array('id'=>$id)); ?>" class="note-button-b">Download xls</a>
            <a href="<?php echo Yii::app()->createUrl('callCenter/admin'); ?>" class="note-button-b">Cancel</a>
            <input type="submit" class="note-button-b" value="Save & Close" style="padding-top: 5px;padding-bottom: 5px;">
        </div>
    </div>
    <div class="note-content">
        <div class="note-sub">
            <?php if($message){ ?>
                <div class="note-error"><?php echo $message; ?></div><br />
            <?php } ?>
            <span class="note-sub-title">Phone no.</span>
            <span class="note-sub-input" style="padding-left: 0px;font-weight: bold;color: #FFF;"><?php echo $msisdn; ?></span>
            <span class="note-sub-title">Name</span>
            <input type="text" class="note-sub-input" name="name" value="<?php if($detail){echo $detail['name'];}else{echo isset($_REQUEST['name'])?$_REQUEST['name']:'';} ?>" height="25" />
            <span class="note-sub-title">Address</span>
            <input type="text" class="note-sub-input" name="address" value="<?php if($detail){echo $detail['address'];}else{echo isset($_REQUEST['address'])?$_REQUEST['address']:'';} ?>" />

            <span class="note-sub-title">Province</span>
            <select name="province" style="height: 20px;" class="note-sub-input" onchange="selectDistrict(this.options[this.selectedIndex].value)">
                <?php if($edProvince['province']['id']){ ?>
                    <option value="<?php echo $edProvince['province']['id']; ?>"><?php echo $edProvince['province']['name']; ?></option>
                <?php } ?>
                <option value="">-- Select Province --</option>
                <?php foreach($province as $p): ?>
                    <option value="<?php echo $p['id'] ?>"><?php echo $p['name']; ?></option>
                <?php endforeach; ?>
            </select>

            <span class="note-sub-title">District</span>
            <select id="district_dropdown" name="district" style="height: 20px;" class="note-sub-input" onchange="selectWard(this.options[this.selectedIndex].value)">
                <?php if($edProvince['district']['id']){ ?>
                    <option value="<?php echo $edProvince['district']['id']; ?>"><?php echo $edProvince['district']['name']; ?></option>
                <?php } ?>
                <option value="">-- Select District --</option>
            </select>

            <span class="note-sub-title">Ward</span>
            <select id="ward_dropdown" name="ward" style="height: 20px;" class="note-sub-input" onchange="selectVillage(this.options[this.selectedIndex].value)">
                <?php if($edProvince['ward']['id']){ ?>
                    <option value="<?php echo $edProvince['ward']['id']; ?>"><?php echo $edProvince['ward']['name']; ?></option>
                <?php } ?>
                <option value="">-- Select Ward --</option>
            </select>

            <span class="note-sub-title">Village</span>
            <select id="village_dropdown" name="village" style="height: 20px;" class="note-sub-input">
                <?php if($edProvince['village']['id']){ ?>
                    <option value="<?php echo $edProvince['village']['id']; ?>"><?php echo $edProvince['village']['name']; ?></option>
                <?php } ?>
                <option value="">-- Select Village --</option>
            </select>

            <span class="note-sub-title">Gender</span>
            <select name="age" class="note-sub-input">
                <?php if($detail){?>
                    <option value="<?php if($detail['age']==1){echo 1;}else{echo 2;} ?>"><?php if($detail['age']==1){echo 'Male';}else{echo 'Female';} ?></option>
                    <option value="1">Male</option>
                    <option value="2">Female</option>
                <?php }else{ ?>
                    <option value="0">Selected</option>
                    <option value="1">Male</option>
                    <option value="2">Female</option>
                <?php } ?>
            </select>
            <span class="note-sub-title">Contact for</span>
            <textarea class="note-sub-input" name="content" rows="6" cols="10"><?php if($detail){echo $detail['content'];}else{echo isset($_REQUEST['content'])?$_REQUEST['content']:'';} ?></textarea>
        </div>
        <div class="note-sub" style="margin-left: 45px;">
            <span class="note-sub-title note-number-w">Member from.</span>
        <span class="note-sub-input note-number">
            <?php $d = new DateTime($detail['created']); ?>
            <?php if($detail['created']){echo $d->format('d.m.Y');}else{echo date('d.m.Y');} ?>
        </span>
            <span class="note-sub-title note-number-w">Lotto bought</span>
            <span class="note-sub-input note-number"><?php echo $count; ?></span>
            <span class="note-sub-title note-number-w">Total charge</span>
            <span class="note-sub-input note-number"><?php if($sum){echo $sum;}else{echo 0;} ?></span>

            <span class="note-sub-title note-number-w">Note of call</span>
            <textarea class="note-sub-input note-number" style="width: 50%;padding: 5px;color: #000;" name="note" rows="3" cols="10"><?php if($noteCall){echo $noteCall['note'];}else{echo isset($_REQUEST['note'])?$_REQUEST['note']:'';} ?></textarea>
            <span class="note-sub-title note-number-w">Status of call</span>
            <select name="status" style="width: 30%;" class="note-sub-input">
                <?php if($detail){ ?>
                    <option value="<?php if($detail['status']==1){echo 1;}else{echo 2;} ?>"><?php if($detail['status']==1){echo 'Pending';}else{echo 'Processed';} ?></option>
                    <option value="1">Pending</option>
                    <option value="2">Processed</option>
                <?php }else{ ?>
                    <option value="0">Selected</option>
                    <option value="1">Pending</option>
                    <option value="2">Processed</option>
                <?php } ?>
            </select>
        </div>
        <div class="history">

            <div style="float: left;width: 96%;margin-top: 10px;margin-bottom: 10px;border-bottom: 3px solid #4F81BD;">
                <a class="tab active" style="margin-left: 27px!important;">Purchase history</a>
                <a href="<?php echo Yii::app()->createUrl('callCenter/NoteHistory',array('id'=>$id)); ?>" class="tab">History Note</a>
            </div>

            <div class="history-list" style="margin-left: 3%;width: 93%!important;">
                <div class="history-list-title">
                    <span class="number font-title" style="border-right: 2px solid #3855A5;width: 20%!important;">Number</span>
                    <span class="number font-title" style="border-right: 2px solid #3855A5;width: 20%!important;">Amount</span>
                    <span class="number font-title" style="border-right: 2px solid #3855A5;width: 20%!important;">Type</span>
                    <span class="date font-title">Date time</span>
                </div>

                <?php if($historyLotto){ ?>
                    <?php foreach($historyLotto as $lotto): ?>
                        <div class="history-list-title" style="border-bottom: none;">
                            <span class="number" style="border-right: 2px solid #3855A5;width: 20%!important;"><?php echo $lotto['code']; ?></span>
                            <span class="number" style="border-right: 2px solid #3855A5;font-weight: bold;width: 20%!important;"><?php echo $lotto['money']; ?></span>
                            <span class="number" style="border-right: 2px solid #3855A5;font-weight: bold;width: 20%!important;">IVR</span>
                            <?php $date = new DateTime($lotto['created_datetime']); ?>
                            <span class="date"><?php  echo $date->format('d.m.Y - H:i'); ?></span>
                        </div>
                    <?php endforeach; ?>
                <?php } ?>
                <?php if($purchaseSMS){ ?>
                    <?php
                    $sms = new Service();
                    $i=0;
                    for($i=0;$i< count($purchaseSMS)-1;$i++){
                        $item = explode(';',$purchaseSMS[$i]);
                        $dateFomat = explode("/",$item[1]);
                        //echo $item[2];
                        $amountSms = explode(';',$sms->SaleHistoryDetail(floatval($item[2]))->SaleHistoryDetailResult);
                        //var_dump($sms->SaleHistoryDetail($item[2])->SaleHistoryDetailResult);
                        ?>
                        <div class="history-list-title" style="border-bottom: none;">
                            <span class="number" style="border-right: 2px solid #3855A5;width: 20%!important;"><?php echo $amountSms[0]; ?></span>
                            <span class="number" style="border-right: 2px solid #3855A5;font-weight: bold;width: 20%!important;"><?php echo $item[3]; ?></span>
                            <span class="number" style="border-right: 2px solid #3855A5;font-weight: bold;width: 20%!important;">SMS</span>
                            <span class="date"><?php echo date('d.m.Y - H:i',strtotime($dateFomat[0].'-'.$dateFomat[1].'-'.$dateFomat[2].' '.$item[6])); ?></span>
                        </div>
                    <?php } ?>
                <?php } ?>
                <?php if(empty($historyLotto) && empty($purchaseSMS)){ ?>
                    <div class="history-list-title" style="border-bottom: none;">
                        <?php echo 'No result!!!!'; ?>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
    <div class="note-button">
        <div class="note-button-main">
            <a href="<?php echo Yii::app()->createUrl('callCenter/excelhistory',array('id'=>$id)); ?>" class="note-button-b">Download xls</a>
            <a href="<?php echo Yii::app()->createUrl('callCenter/admin'); ?>" class="note-button-b">Cancel</a>
            <input type="submit" class="note-button-b" value="Save & Close" style="padding-top: 5px;padding-bottom: 5px;">
        </div>
    </div>
</form>