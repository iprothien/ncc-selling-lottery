
<script type="text/javascript">
    $(document).ready(function(){
        setInterval(function() {
            $("#caller").load('<?php echo Yii::app()->createUrl("callCenter/online") ?> #caller > *');
        }, 5000);
    });

    $(document).ready(function(){
        setInterval(function() {
            $("#missed").load('<?php echo Yii::app()->createUrl("callCenter/miss") ?> #missed > *');
        }, 5000);
    });
</script>

<div class="div-main">
    <div class="call-miss-main">
        <div class="call-miss">
            <form method="POST" name="current" style="padding: 0px;">
                <div class="search">
                    <span class="title-search">Current Call:</span>
                    <input class="input-search" type="text" name="m_search" />
                    <input class="bottom-search" type="submit" name="current" value="Search">
                </div>
            </form>
            <div id="caller">
                <table class="table" id="list1" style="width: 100%;border:1px solid #CCC;">
                    <tbody>
                    <tr class="border_bottom tr">
                        <th class="th" style="width: 25%;"><?php echo Yii::t('currentCall', 'phoneno'); ?></th>
                        <th class="th" style="width: 30%;"><?php echo Yii::t('currentCall', 'customer'); ?></th>
                        <th class="th" style="background: #FDFDC8;width: 20%;"><?php echo Yii::t('currentCall', 'time'); ?></th>
                        <th class="th" style="width: 15%;">Lotto</th>
                    </tr>
                    <?php if($current){ ?>
                        <?php foreach($current as $m): ?>
                            <tr class="td-border tr">
                                <td class="td">
                                    <?php if($m['ol_status']==0){ ?>
                                        <a href="<?php echo Yii::app()->createUrl('callCenter/editonline',array('msisdn'=>$m['msisdn'])); ?>"><?php echo $m['msisdn']; ?></a>
                                    <?php }else{ ?>
                                        <?php echo $m['msisdn'];
                                    } ?>
                                </td>
                                <td class="td" style="font-weight: bold;"><?php if($m['name']){echo $m['name'];} ?></td>
                                <td class="td" style="background: #FDFDC8;"><?php echo date('H:i:s',strtotime($m['created'])); ?></td>
                                <td class="td" style="font-weight: bold;"><a href="<?php echo Yii::app()->createUrl('selling/buy',array('msisdn'=>base64_encode($m['msisdn']))); ?>">Buy</a> </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php }else{ ?>
                        <tr class="td-border tr">
                            <td class="td" colspan="4" style="color: red;">No data result</td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="call-miss-main call-miss-left">
        <div class="call-miss">
            <form method="post" name="missed" style="padding: 0px;">
                <div class="search">
                    <span class="title-search">Missed Call:</span>
                    <input class="input-search" type="text" name="m_search" />
                    <input class="bottom-search" type="submit" name="missed" value="Search">
                </div>
            </form>
            <div id="missed">
                <table id="list2" class="table" style="width: 100%;border:1px solid #CCC;">
                    <tbody>
                    <tr class="border_bottom tr">
                        <th class="th">Phone No.</th>
                        <th class="th">Customer Information</th>
                        <th class="th" style="border-right: 0px;background: #FDFDC8;">Time</th>
                    </tr>
                    <?php if($miss){ ?>
                        <?php foreach($miss as $m): ?>
                            <tr class="td-border tr">
                                <td class="td"><a href="<?php echo Yii::app()->createUrl('callCenter/note',array('id'=>$m['id'])); ?>"><?php echo $m['msisdn']; ?></a></td>
                                <td class="td" style="font-weight: bold;"><?php if($m['name']){echo $m['name'];} ?></td>
                                <td class="td" style="border-right: none;background: #FDFDC8;"><?php echo date('H:i - d/m/Y',strtotime($m['created'])); ?></td>
                            </tr>
                        <?php endforeach; ?>
                    <?php }else{ ?>
                        <tr class="td-border tr">
                            <td class="td" colspan="3" style="color: red;">No data result</td>
                        </tr>
                    <?php } ?>
                    <tr class="td-border tr">
                        <td class="td" colspan="3" style="background: #fef1ec;color: #FFF;text-align: right;padding-right: 5px;font-weight: bold;"><a href="<?php echo Yii::app()->createUrl('callCenter/MissCall'); ?>">View All</a> </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div style="float: left;width: 96%;margin-top: 10px;margin-bottom: 10px;border-bottom: 3px solid #4F81BD;">
    <a href="<?php echo Yii::app()->createUrl('callCenter/admin'); ?>" class="tab"><?php echo Yii::t('title', 'history'); ?></a>
    <a href="<?php echo Yii::app()->createUrl('callCenter/ivr'); ?>" class="tab"><?php echo Yii::t('title', 'ivr'); ?></a>
    <a class="tab active"><?php echo Yii::t('title', 'sms'); ?></a>
    <a href="<?php echo Yii::app()->createUrl('callCenter/MissCall'); ?>" class="tab"><?php echo Yii::t('title', 'miss'); ?></a>
</div>

<div id="sms-history">
    <?php $this->renderPartial('_sms', array()); ?>
</div>