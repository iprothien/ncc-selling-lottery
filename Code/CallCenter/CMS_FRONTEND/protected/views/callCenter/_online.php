<div id="caller">
    <table class="table" id="list1" style="width: 100%;border:1px solid #CCC;">
        <tbody>
        <tr class="border_bottom tr">
            <th class="th" style="width: 25%;"><?php echo Yii::t('currentCall', 'phoneno'); ?></th>
            <th class="th" style="width: 30%;"><?php echo Yii::t('currentCall', 'customer'); ?></th>
            <th class="th" style="background: #FDFDC8;width: 20%;"><?php echo Yii::t('currentCall', 'time'); ?></th>
            <th class="th" style="width: 15%;">Lotto</th>
        </tr>
        <?php if($current){ ?>
            <?php foreach($current as $m): ?>
                <tr class="td-border tr">
                    <td class="td">
                        <?php if($m['ol_status']==0){ ?>
                            <a href="<?php echo Yii::app()->createUrl('callCenter/editonline',array('msisdn'=>$m['msisdn'])); ?>"><?php echo $m['msisdn']; ?></a>
                        <?php }else{ ?>
                            <?php echo $m['msisdn']; ?>
                        <?php } ?>
                    </td>
                    <td class="td" style="font-weight: bold;"><?php if($m['name']){echo $m['name'];} ?></td>
                    <td class="td" style="background: #FDFDC8;"><?php echo date('H:i:s',strtotime($m['created'])); ?></td>
                    <td class="td" style="font-weight: bold;"><a href="<?php echo Yii::app()->createUrl('selling/buy',array('msisdn'=>base64_encode($m['msisdn']))); ?>">Buy</a> </td>
                </tr>
            <?php endforeach; ?>
        <?php }else{ ?>
            <tr class="td-border tr">
                <td class="td" colspan="4" style="color: red;">No data result</td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>