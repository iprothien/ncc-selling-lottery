<form method="post">
    <div class="note-top">
        <span class="note-title">Edit member info</span>
        <div class="note-button-main" style="margin-top: -30px;">
            <a href="<?php echo Yii::app()->createUrl('callCenter/excelhistory',array('msisdn'=>$msisdn)); ?>" class="note-button-b">Download xls</a>
            <a href="<?php echo Yii::app()->createUrl('callCenter/admin'); ?>" class="note-button-b">Cancel</a>
            <input type="submit" class="note-button-b" value="Save & Close" style="padding-top: 5px;padding-bottom: 5px;">
        </div>
    </div>
    <div class="note-content">
        <div class="note-sub">
            <?php if($message){ ?>
                <div class="note-error"><?php echo $message; ?></div><br />
            <?php } ?>
            <span class="note-sub-title">Phone no.</span>
            <span class="note-sub-input" style="padding-left: 0px;font-weight: bold;color: #FFF;"><?php echo $msisdn; ?></span>
            <span class="note-sub-title">Name</span>
            <input type="text" class="note-sub-input" name="name" value="<?php if($detail){echo $detail['name'];}else{echo isset($_REQUEST['name'])?$_REQUEST['name']:'';} ?>" height="25" />
            <span class="note-sub-title">Address</span>
            <input type="text" class="note-sub-input" name="address" value="<?php if($detail){echo $detail['address'];}else{echo isset($_REQUEST['address'])?$_REQUEST['address']:'';} ?>" />

            <span class="note-sub-title">Province</span>
            <select name="province" style="height: 20px;" class="note-sub-input" onchange="selectDistrict(this.options[this.selectedIndex].value)">
                <option value="">-- Select Province --</option>
                <?php foreach($province as $p): ?>
                    <option value="<?php echo $p['id'] ?>"><?php echo $p['name']; ?></option>
                <?php endforeach; ?>
            </select>

            <span class="note-sub-title">District</span>
            <select id="district_dropdown" name="district" style="height: 20px;" class="note-sub-input" onchange="selectWard(this.options[this.selectedIndex].value)">
                <option value="">-- Select District --</option>
            </select>

            <span class="note-sub-title">Ward</span>
            <select id="ward_dropdown" name="ward" style="height: 20px;" class="note-sub-input" onchange="selectVillage(this.options[this.selectedIndex].value)">
                <option value="">-- Select Ward --</option>
            </select>

            <span class="note-sub-title">Village</span>
            <select id="village_dropdown" name="village" style="height: 20px;" class="note-sub-input">
                <option value="">-- Select Village --</option>
            </select>

            <span class="note-sub-title">Gender</span>
            <select name="age" class="note-sub-input">
                <?php if($detail){?>
                    <option value="<?php if($detail['age']==1){echo 1;}else{echo 2;} ?>"><?php if($detail['age']==1){echo 'Male';}else{echo 'Female';} ?></option>
                    <option value="1">Male</option>
                    <option value="2">Female</option>
                <?php }else{ ?>
                    <option value="0">Selected</option>
                    <option value="1">Male</option>
                    <option value="2">Female</option>
                <?php } ?>
            </select>
            <span class="note-sub-title">Contact for</span>
            <textarea class="note-sub-input" name="content" rows="6" cols="10"><?php if($detail){echo $detail['content'];}else{echo isset($_REQUEST['content'])?$_REQUEST['content']:'';} ?></textarea>
        </div>
        <div class="note-sub" style="margin-left: 45px;">
            <span class="note-sub-title note-number-w">Member from.</span>
        <span class="note-sub-input note-number">
            <?php $d = new DateTime($detail['created']); ?>
            <?php if($detail['created']){echo $d->format('d.m.Y');}else{echo date('d.m.Y');} ?>
        </span>
            <span class="note-sub-title note-number-w">Lotto bought</span>
            <span class="note-sub-input note-number"><?php echo $count; ?></span>
            <span class="note-sub-title note-number-w">Total charge</span>
            <span class="note-sub-input note-number"><?php if($sum){echo $sum;}else{echo 0;} ?></span>

            <span class="note-sub-title note-number-w">Note of call</span>
            <textarea class="note-sub-input note-number" style="width: 50%;padding: 5px;color: #000;" name="note" rows="3" cols="10"><?php if($noteCall){echo $noteCall['note'];}else{echo isset($_REQUEST['note'])?$_REQUEST['note']:'';} ?></textarea>
            <span class="note-sub-title note-number-w">Status of call</span>
            <select name="status" style="width: 30%;" class="note-sub-input">
                <?php if($detail){ ?>
                    <option value="<?php if($detail['status']==1){echo 1;}else{echo 2;} ?>"><?php if($detail['status']==1){echo 'Pending';}else{echo 'Processed';} ?></option>
                    <option value="1">Pending</option>
                    <option value="2">Processed</option>
                <?php }else{ ?>
                    <option value="0">Selected</option>
                    <option value="1">Pending</option>
                    <option value="2">Processed</option>
                <?php } ?>
            </select>
        </div>
        <div class="history">
            <div style="float: left;width: 96%;margin-top: 10px;margin-bottom: 10px;border-bottom: 3px solid #4F81BD;">
                <a href="<?php echo Yii::app()->createUrl('callCenter/editonline',array('msisdn'=>$msisdn)); ?>" class="tab" style="margin-left: 27px!important;">Purchase history</a>
                <a class="tab active">History Note</a>
            </div>

            <div class="history-list" style="margin-left: 3%;width: 93%!important;">
                <div class="history-list-title">
                    <span class="number font-title" style="border-right: 2px solid #3855A5;width: 40%!important;">Note</span>
                    <span class="number font-title" style="border-right: 2px solid #3855A5;width: 20%!important;">Agent</span>
                    <span class="number font-title" style="border-right: 2px solid #3855A5;width: 16%!important;">Status</span>
                    <span class="date font-title" style="width: 23%!important;">Time</span>
                </div>

                <?php if($historyNote){ ?>
                    <?php foreach($historyNote as $note): ?>
                        <div class="history-list-title" style="border-bottom: none;">
                            <span class="number" style="border-right: 2px solid #3855A5;width: 40%!important;"><?php if($note['note']){ echo $note['note']; }else{ echo 'No Note';} ?></span>
                            <span class="number" style="border-right: 2px solid #3855A5;font-weight: bold;width: 20%!important;"><?php if($note['name']){ echo $note['name']; }else{ echo 'No Agent (Miss call)';} ?></span>
                            <span class="number" style="border-right: 2px solid #3855A5;font-weight: bold;width: 16%!important;"><?php if($note['status']==1){echo 'Pending'; }elseif($note['status']==2){ echo 'Processed';}elseif(empty($note['status'])){ echo 'No';} ?></span>
                            <span class="date" style="width: 23%!important;"><?php  echo $note['call_time']; ?></span>
                        </div>
                    <?php endforeach; ?>
                <?php }else{ ?>
                    <?php echo 'No data'; ?>
                <?php } ?>
            </div>
        </div>
    </div>
    <div class="note-button">
        <div class="note-button-main">
            <a href="<?php echo Yii::app()->createUrl('callCenter/excelhistory',array('msisdn'=>$msisdn)); ?>" class="note-button-b">Download xls</a>
            <a href="<?php echo Yii::app()->createUrl('callCenter/admin'); ?>" class="note-button-b">Cancel</a>
            <input type="submit" class="note-button-b" value="Save & Close" style="padding-top: 5px;padding-bottom: 5px;">
        </div>
    </div>
</form>