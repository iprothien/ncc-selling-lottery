<div id="missed">
    <table id="list2" class="table" style="width: 100%;border:1px solid #CCC;">
        <tbody>
        <tr class="border_bottom tr">
            <th class="th"><?php echo Yii::t('missCall', 'phoneno'); ?></th>
            <th class="th"><?php echo Yii::t('missCall', 'customer'); ?></th>
            <th class="th" style="border-right: 0px;background: #FDFDC8;"><?php echo Yii::t('missCall', 'time'); ?></th>
        </tr>
        <?php if($miss){ ?>
            <?php foreach($miss as $m): ?>
                <tr class="td-border tr">
                    <td class="td"><a href="<?php echo Yii::app()->createUrl('callCenter/note',array('id'=>$m['id'])); ?>"><?php echo $m['msisdn']; ?></a></td>
                    <td class="td" style="font-weight: bold;"><?php if($m['name']){echo $m['name'];} ?></td>
                    <td class="td" style="border-right: none;background: #FDFDC8;"><?php echo date('H:i - d/m/Y',strtotime($m['created'])); ?></td>
                </tr>
            <?php endforeach; ?>
        <?php }else{ ?>
            <tr class="td-border tr">
                <td class="td" colspan="3" style="color: red;">No data result</td>
            </tr>
        <?php } ?>
        <tr class="td-border tr">
            <td class="td" colspan="3" style="background: #fef1ec;color: #FFF;text-align: right;padding-right: 5px;font-weight: bold;"><a href="<?php echo Yii::app()->createUrl('callCenter/MissCall'); ?>">View All</a> </td>
        </tr>
        </tbody>
    </table>
</div>