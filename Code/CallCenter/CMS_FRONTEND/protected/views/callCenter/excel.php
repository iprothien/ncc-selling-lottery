<?php if($detail){ ?>
    <table border="1">
        <tbody>
        <tr>
            <th style="background: tomato;" colspan="4">Information</th>
        </tr>
        <tr>
            <td style="background: plum;">Phone No</td>
            <td><?php echo $detail['msisdn']; ?></td>
            <td style="background: plum;">Member from</td>
            <?php $date = new DateTime($detail['created']); ?>
            <td><?php echo $date->format('d-m-Y H:i:s'); ?></td>
        </tr>
        <tr>
            <td style="background: plum;">Name</td>
            <td><?php echo $detail['name']; ?></td>
            <td style="background: plum;">Lotto bought</td>
            <td><?php echo $count; ?></td>
        </tr>
        <tr>
            <td style="background: plum;">Address</td>
            <td><?php echo $detail['address']; ?></td>
            <td style="background: plum;">Total charge</td>
            <td><?php if($sum){echo $sum;}else{echo 0;} ?></td>
        </tr>
        <tr>
            <td style="background: plum;">Gender</td>
            <td colspan="3"><?php if($detail['age']==1){echo 'Male';}else{echo 'Female';} ?></td>
        </tr>
        <tr>
            <td style="background: plum;">Contact for</td>
            <td colspan="3"><?php echo $detail['content']; ?></td>
        </tr>
        <tr>
            <td style="background: plum;">Status</td>
            <td colspan="3"><?php if($detail['status']==1){echo 'Pending';}else{echo 'Processed';} ?></td>
        </tr>
        </tbody>
    </table>
<?php } ?>
<br/>
<?php if($hitory){ ?>
    <table border="1" >
        <tbody>
        <tr>
            <th colspan="4" style="background: rosybrown;"> Purchase history</th>
        </tr>
        <tr >
            <th style="background: red;">Code.</th>
            <th style="background: red;">Amount</th>
            <th style="background: red;">Type</th>
            <th style="background: red;">Date Time</th>
        </tr>
        <?php foreach($hitory as $h): ?>
            <tr>
                <td><?php echo $h['code']; ?></td>
                <td><?php echo $h['money']; ?></td>
                <td>IVR</td>
                <td><?php echo date('d/m/Y H:i',strtotime($h['created_datetime'])); ?></td>
            </tr>
        <?php endforeach; ?>
        <?php if($sms){ ?>
            <?php
            for($i=0;$i< count($sms,COUNT_NORMAL);$i++){
                $item = explode(';',$sms[$i]);
                $amount = explode('|',$item[1]);
                ?>
                <tr>
                    <td><?php echo $item[0]; ?></td>
                    <td><?php echo $amount[0]; ?></td>
                    <td>SMS</td>
                    <td><?php echo date('d/m/Y H:i'); ?></td>
                </tr>
            <?php } ?>
        <?php } ?>
        </tbody>
    </table>
<?php } ?>