<?php
    $limit=10;
    $currentPage = isset($_GET['page']) ? $_GET['page'] : 1;
    $startRecord = ($currentPage - 1) * $limit;
    $msisdn = isset($_REQUEST['m_search'])?$_REQUEST['m_search']:NULL;
    $callCenter = CallCenter::model();

    if(isset($_POST['missed'])){
        $count = $callCenter->countMissCall($msisdn);
        $numberPage = ceil($count / $limit);
        $miss = $callCenter->findMissCall($msisdn,$startRecord,$limit);
    }else{
        $count = $callCenter->countMissCall(NULL);
        $numberPage = ceil($count / $limit);
        $miss = $callCenter->findMissCall(NULL,$startRecord,$limit);
    }
?>
<div id="missed">
    <table id="list2" class="table" style="width: 100%;border:1px solid #CCC;">
        <tbody>
        <tr class="border_bottom tr">
            <th class="th">Phone No.</th>
            <th class="th">Member Information</th>
            <th class="th" style="border-right: 0px;background: #FDFDC8;">Time</th>
        </tr>
        <?php if($miss){ ?>
            <?php foreach($miss as $m): ?>
                <tr class="td-border tr">
                    <td class="td"><a href="<?php echo Yii::app()->createUrl('callCenter/note',array('id'=>$m['id'])); ?>"><?php echo $m['msisdn']; ?></a></td>
                    <td class="td" style="font-weight: bold;"><?php if($m['name']){echo $m['name'];} ?></td>
                    <?php $date = new DateTime($m['created']); ?>
                    <td class="td" style="border-right: none;background: #FDFDC8;"><?php echo $date->format('H:i - d/m/Y'); ?></td>
                </tr>
            <?php endforeach; ?>
        <?php }else{ ?>
            <tr class="td-border tr">
                <td class="td" colspan="3" style="color: red;">No data result</td>
            </tr>
        <?php } ?>
        <?php $path = Yii::app()->createUrl('callCenter/admin').'&'; ?>
        <?php if($numberPage>1){ ?>
            <tr class="td-border tr" style="border-collapse: collapse;border-top: 1px solid #8c8c8c;">
                <td colspan="5" class="td" style="font-weight: bold;">
                    <div style="float: left;width: 100%;">
                        <ul class="paging">
                            <a href="<?php echo $path; ?>"><span><?php if($numberPage >1) {echo Paging::showPageNavigation($currentPage,$numberPage,$path);} ?></span></a>
                        </ul>
                    </div>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>