<?php
    $msisdn = isset($_REQUEST['m_search'])?$_REQUEST['m_search']:NULL;
    $callCenter = CallCenter::model();
    //connect DB
    if(isset($_POST['current'])){$current = $callCenter->findOnlineCall($msisdn);}else{$current = $callCenter->findOnlineCall();}
?>
<div id="caller">
    <table class="table" id="list1" style="width: 100%;border:1px solid #CCC;">
        <tbody>
        <tr class="border_bottom tr">
            <th class="th">Phone No.</th>
            <th class="th">Member Information</th>
            <th class="th" style="border-right: 0px;background: #FDFDC8;">Time</th>
        </tr>
        <?php if($current){ ?>
            <?php foreach($current as $m): ?>
                <tr class="td-border tr">
                    <td class="td"><?php echo $m['msisdn']; ?></td>
                    <td class="td" style="font-weight: bold;"><?php if($m['name']){echo $m['name'];} ?></td>
                    <?php $date = new DateTime($m['created']); ?>
                    <td class="td" style="border-right: none;background: #FDFDC8;"><?php echo $date->format('H:i - d/m/Y'); ?></td>
                </tr>
            <?php endforeach; ?>
        <?php }else{ ?>
            <tr class="td-border tr">
                <td class="td" colspan="3" style="color: red;">No data result</td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>