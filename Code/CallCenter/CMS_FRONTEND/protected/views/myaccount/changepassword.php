<?php
/* @var $this AccountController */
/* @var $model Account */

$this->breadcrumbs = array(
    'User' => array('/user/admin'),
    $model->username => array('/user/view', 'id' => $model->id),
    'Change Password',
);
?>

<h4>Change Password Account <?php echo $model->username; ?></h4>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>