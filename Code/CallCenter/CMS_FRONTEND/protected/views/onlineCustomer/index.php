<?php
/* @var $this OnlineCustomerController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Online Customers',
);

$this->menu=array(
	array('label'=>'Create OnlineCustomer', 'url'=>array('create')),
	array('label'=>'Manage OnlineCustomer', 'url'=>array('admin')),
);
?>

<h1>Online Customers</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
