<?php
/* @var $this OnlineCustomerController */
/* @var $model OnlineCustomer */

$this->breadcrumbs=array(
	'Online Customers'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List OnlineCustomer', 'url'=>array('index')),
	array('label'=>'Create OnlineCustomer', 'url'=>array('create')),
	array('label'=>'View OnlineCustomer', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage OnlineCustomer', 'url'=>array('admin')),
);
?>

<h1>Update OnlineCustomer <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>