<?php
/* @var $this OnlineCustomerController */
/* @var $model OnlineCustomer */

$this->breadcrumbs=array(
	'Online Customers'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List OnlineCustomer', 'url'=>array('index')),
	array('label'=>'Manage OnlineCustomer', 'url'=>array('admin')),
);
?>

<h1>Create OnlineCustomer</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>