<?php
/* @var $this OnlineCustomerController */
/* @var $model OnlineCustomer */

$this->breadcrumbs=array(
	'Online Customers'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List OnlineCustomer', 'url'=>array('index')),
	array('label'=>'Create OnlineCustomer', 'url'=>array('create')),
	array('label'=>'Update OnlineCustomer', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete OnlineCustomer', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage OnlineCustomer', 'url'=>array('admin')),
);
?>

<h1>View OnlineCustomer #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'msisdn',
		'status',
		'created',
	),
)); ?>
