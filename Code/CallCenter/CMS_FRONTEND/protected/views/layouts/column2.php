<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>
<div id="wrapper">
    <div id="content">
        <?php echo $content; ?>
    </div>
    <div class="clear"></div>
</div>
<?php $this->endContent(); ?>