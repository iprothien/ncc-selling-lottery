<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="language" content="en" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/styles.css" />
	    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/datepickr.css" />

        <script type="text/javascript">
            function selectDistrict(provinceID){
                if(provinceID!="-1"){
                    loadData("district",provinceID);
                    $("#ward_dropdown").html("<option value='-1'>-- Select Ward --</option>");
                    $("#village_dropdown").html("<option value='-1'>-- Select Village --</option>");
                }else{
                    $("#district_dropdown").html("<option value='-1'>-- Select District --</option>");
                    $("#ward_dropdown").html("<option value='-1'>-- Select Ward --</option>");
                    $("#village_dropdown").html("<option value='-1'>-- Select Village --</option>");
                }
            }

            function selectWard(districtID){
                if(districtID!="-1"){
                    loadData("ward",districtID);
                    $("#village_dropdown").html("<option value='-1'>-- Select Village --</option>");
                }else{
                    $("#ward_dropdown").html("<option value='-1'>-- Select Ward --</option>");
                    $("#village_dropdown").html("<option value='-1'>-- Select Village --</option>");
                }
            }

            function selectVillage(wardID){
                if(wardID!="-1"){
                    loadData("village",wardID);
                }else{
                    $("#village_dropdown").html("<option value='-1'>-- Select Village --</option>");
                }
            }

            function loadData(loadType,loadId){
                var dataString = 'loadType='+ loadType +'&loadId='+ loadId;
                $.ajax({
                    type: "POST",
                    url: "<?php echo Yii::app()->createUrl('callCenter/loadProvince'); ?>",
                    data: dataString,
                    cache: false,
                    success: function(result){
                        $("#"+loadType+"_dropdown").html("<option value='-1'>-- Select "+loadType+" --</option>");
                        $("#"+loadType+"_dropdown").append(result);
                    }
                });
            }
        </script>
    </head>
    <body>
        <div class="container" id="page">
            <?php if(Yii::app()->user->id): ?>
            <div id="header">
                <?php if(Yii::app()->user->id) { ?>
                <span style="float:right; display: block; padding:20px;"> 
                    Welcome <b><?php echo Yii::app()->user->name ?></b>, 
                    <a style="color:#000;" href="<?php echo Yii::app()->createUrl('/myaccount/changepassword'); ?>">Change Password</a> | 
                    <a style="color:#000;" href="<?php echo Yii::app()->createUrl('/site/logout'); ?>">Logout</a>
                </span>
                <?php } ?>
                <?php $menu = Yii::app()->user->getGroup1(); ?>
                <div id="topmenu">
                    <ul>
                        <?php foreach($menu as $item) { ?>
                        <li <?php if(isset($_GET['r']) && strlen(strstr($item['check'],$_GET['r']))>0) { ?> class="active" <?php } ?>>
                            <a <?php if(isset($_GET['r']) && strlen(strstr($item['check'],$_GET['r']))>0) { ?> class="active" <?php } ?> href="<?php echo $item['url']; ?>"><?php echo $item['label']; ?></a>
                        </li>
                        <?php } ?>
                    </ul>
                </div>
                <div class="clear"></div>
                <div style="background: #078241; height:3px; width:100%;"></div>
            </div>
            <?php endif ?>
            <?php echo $content; ?>
        </div>
    </body>
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/datepickr.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/datepickr.min.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-1.4.1.min.js"></script>
    <script type="text/javascript">
       new datepickr('start', {
            'dateFormat': 'Y-m-d'
        });
       new datepickr('end', {
           'dateFormat': 'Y-m-d'
       });
    </script>

</html>
