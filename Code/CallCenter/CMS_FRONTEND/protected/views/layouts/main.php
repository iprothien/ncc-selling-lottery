<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="language" content="en" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/styles.css" />
	 <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
        <title>NCC CALL CENTER</title>
	 <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/datepickr.css" />
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-ui-1.10.0.custom.min.js"></script>
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/jquery-ui-1.10.0.custom.min.css" />
	 <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.0/jquery.min.js"></script>

    </head>
    <body>
	 <?php //echo Yii::app()->language; ?>
        <div class="container" id="page">
            <?php if(Yii::app()->user->id): ?>
            <div id="header">
                <?php if(Yii::app()->user->id) { ?>
                <span style="float:right; display: block; padding:20px;"> 
                    Welcome <b><?php echo Yii::app()->user->name ?></b>, 
                    <a style="color:#000;" href="<?php echo Yii::app()->createUrl('/myaccount/changepassword'); ?>">Change Password</a> | 
                    <a style="color:#000;" href="<?php echo Yii::app()->createUrl('/site/logout'); ?>">Logout</a>
		      <p style="padding-top: 10px; text-align: right;">
                        Language <a href="<?php echo Yii::app()->createUrl('site/setlang',array('lang'=>'en')); ?>"> English </a> | <a href="<?php echo Yii::app()->createUrl('site/setlang',array('lang'=>'lo')); ?>"> Laos </a>
                    </p>
                </span>
                <?php } ?>
                <?php $menu2 = Yii::app()->user->getGroup2(); ?>
                <div id="topmenu">
                    <ul>
                        <?php foreach($menu2 as $item) { ?>
                        <li <?php if($this->menuActive==$item['alias']): ?> class="active" <?php endif; ?>>
                            <a <?php if($this->menuActive==$item['alias']): ?> class="active" <?php endif; ?> href="<?php echo $item['url']; ?>"><?php echo $item['label']; ?></a>
                        </li>
                        <?php } ?>
                    </ul>
                </div>
                <div class="clear"></div>
                <div style="background: #078241; height:3px; width:100%;"></div>
            </div>
            <?php endif ?>
            <?php echo $content; ?>
        </div>
    </body>
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/datepickr.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/datepickr.min.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-1.4.1.min.js"></script>
    <script type="text/javascript">
       new datepickr('start', {
            'dateFormat': 'Y-m-d'
        });
       new datepickr('end', {
           'dateFormat': 'Y-m-d'
       });
    </script>
    <script type="text/javascript">
        function selectDistrict(provinceID){
            if(provinceID!=""){
                loadData("district",provinceID);
                $("#ward_dropdown").html("<option value='-1'>-- Select Ward --</option>");
                $("#village_dropdown").html("<option value='-1'>-- Select Village --</option>");
            }else{
                $("#district_dropdown").html("<option value='-1'>-- Select District --</option>");
                $("#ward_dropdown").html("<option value='-1'>-- Select Ward --</option>");
                $("#village_dropdown").html("<option value='-1'>-- Select Village --</option>");
            }
        }

        function selectWard(districtID){
            if(districtID!=""){
                loadData("ward",districtID);
                $("#village_dropdown").html("<option value='-1'>-- Select Village --</option>");
            }else{
                $("#ward_dropdown").html("<option value='-1'>-- Select Ward --</option>");
                $("#village_dropdown").html("<option value='-1'>-- Select Village --</option>");
            }
        }

        function selectVillage(wardID){
            if(wardID!=""){
                loadData("village",wardID);
            }else{
                $("#village_dropdown").html("<option value='-1'>-- Select Village --</option>");
            }
        }

        function loadData(loadType,loadId){
            var dataString = 'loadType='+ loadType +'&loadId='+ loadId;
            $.ajax({
                type: "POST",
                url: "<?php echo Yii::app()->createUrl('callCenter/loadProvince'); ?>",
                data: dataString,
                cache: false,
                success: function(result){
                    $("#"+loadType+"_dropdown").html("<option value=''>-- Select "+loadType+" --</option>");
                    $("#"+loadType+"_dropdown").append(result);
                }
            });
        }
    </script>
</html>
