<?php
class Service {
    public function SaleHistory($mobileNo) {
        $query=new ArrayObject();
        $dateF = new DateTime(date('Y-m-d H:i:s', strtotime("-30 day",strtotime(date('Y-m-d H:i:s')))));
        $dateT = new DateTime(date('Y-m-d H:i:s', strtotime("+1 day",strtotime(date('Y-m-d H:i:s')))));
        try {
            $soapClient = new SoapClient('http://ncclaos.net:8208/lldserviceIVR.asmx?WSDL', array('trace' => true));
        } catch (Exception $e) {
            var_dump($e);
        }
        $query->PassKey = 'mo246d0262c3925617b0c72bb20aac2d';
        $query->MobileNo = $mobileNo;
        $query->TraID = 0;
        $query->DateF = $dateF->format('d/m/Y');
        $query->DateT = $dateT->format('d/m/Y');
        try {
            $result = $soapClient->SaleHistory($query);
        } catch (Exception $e) {
            var_dump($e);
        }
        return $result;
    }

    public function SaleHistoryDetail($billNumber) {
        $query=new ArrayObject();
        try {
            $soapClient = new SoapClient('http://ncclaos.net:8208/lldserviceIVR.asmx?WSDL', array('trace' => true));
        } catch (Exception $e) {
            var_dump($e);
        }
        $query->PassKey= 'mo246d0262c3925617b0c72bb20aac2d';
        $query->BillNumber=$billNumber;
        try {
            $result = $soapClient->SaleHistoryDetail($query);
        } catch (Exception $e) {
            var_dump($e);
        }
        return $result;
    }

    public function getSmsAmount($mobileNo){
        $arrObj = array();
        $saleHistory = $this->SaleHistory($mobileNo)->SaleHistoryResult;
        $listHistory = explode('|',$saleHistory);
        for($i=0; $i<count($listHistory,COUNT_NORMAL)-1; $i++){
            $billNumber=explode(';',$listHistory[$i]);
            $arrObj[] = $this->SaleHistoryDetail(floatval($billNumber[2]))->SaleHistoryDetailResult;
        }
        return $arrObj;
    }


    function addOrder($MobileNo, $digits, $amount) {
        try {
            $soapClient = new SoapClient('http://ncclaos.net:8208/lldserviceIVR.asmx?WSDL', array('trace' => true));
        } catch (Exception $e) {
            var_dump($e);
        }

        $QueryUserEvt=new ArrayObject();
        $QueryUserEvt->PassKey = 'mo246d0262c3925617b0c72bb20aac2d';
        $QueryUserEvt->AccountNo = '1448';
        $QueryUserEvt->MobileNo = $MobileNo;
        $QueryUserEvt->mDigit = $digits;
        $QueryUserEvt->mAmount = $amount;
        $QueryUserEvt->isFirstRecord = true;

        try {
            $resutl = $soapClient->AddOrder($QueryUserEvt);
            var_dump($resutl);
        } catch (Exception $e) {
            var_dump($e);
        }
        return $resutl;
    }

    public function SubmitBuying($MobileNo,$PaymentNo,$TotalAmount){

        try {
            $soapClient = new SoapClient('http://ncclaos.net:8208/lldserviceIVR.asmx?WSDL', array('trace' => true));
        } catch (Exception $e) {
            var_dump($e);
        }
        $submitOrder=new ArrayObject();
        $submitOrder->PassKey = 'mo246d0262c3925617b0c72bb20aac2d';
        $submitOrder->ProviderID = '10';
        $submitOrder->MobileNo = $MobileNo;
        $submitOrder->PaymentNo = $PaymentNo;
        $submitOrder->TotalAmount = $TotalAmount;
        $submitOrder->isSuccess = true;

        try {
            $resut = $soapClient->SubmitBuying($submitOrder);
            var_dump($resut);
        } catch (Exception $e) {
            var_dump($e);
        }
        return $resut;
    }
} 