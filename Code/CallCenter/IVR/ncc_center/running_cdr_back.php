<?php
    require('connect/sbMysqlPDO.class.php');
    require('connect/sbMssql.class.php');
    require('connect/config.php');
    $conn = new sbMysqlPDO($server, $user, $password, $db);
    $sqlCheck ="SELECT `id`,`src`,`dstchannel`,`duration` FROM `cdr` WHERE `status`=0 and accountcode='callcenter' ";
    while(1){
        if(date("H",time())=="1") break;
        $cdrCheck = $conn->doSelect($sqlCheck);
        foreach($cdrCheck as $cdr){
            if($cdr['dstchannel']){
                $agentNum = explode("/",$cdr['dstchannel']);
                $agentAnswer = explode("-",$agentNum[1]);

                $sql1 = "UPDATE ncc_agent_answer SET msisdn_agent=".$agentAnswer[0].",duration=".$cdr['duration'].",updated='".date('Y-m-d H:i:s')."' WHERE msisdn_member=".$cdr['src']." AND as_status=0";
                $conn->doUpdate($sql1);

                $idAgent = "SELECT a.id FROM ncc_agent AS a INNER JOIN ncc_agent_answer AS ncc ON a.phonenumber=ncc.msisdn_agent WHERE ncc.msisdn_agent=".$agentAnswer[0]." AND ncc.msisdn_member=".$cdr['src']." AND ncc.as_status=0";
                $id = $conn->doSelectOne($idAgent);

                $sql2 = "UPDATE ncc_agent_answer SET agent_id=".$id['id']." WHERE msisdn_agent=".$agentAnswer[0]." AND msisdn_member=".$cdr['src']." AND as_status=0";
                $conn->doUpdate($sql2);

                $sql3 = "UPDATE ncc_agent_answer SET as_status=1 WHERE as_status=0 AND `msisdn_member`=".$cdr['src'];
                $conn->doUpdate($sql3);

                $sqlDelete = "DELETE FROM `online_customer` WHERE msisdn=".$cdr['src'];
                $conn->doUpdate($sqlDelete);

                $sqlUpdate = "UPDATE cdr SET status=1 WHERE id=".$cdr['id'];
                $conn->doUpdate($sqlUpdate);

            }else{
                $sqlAdd = "INSERT INTO miss_call SET msisdn=".$cdr['src'];
                $conn->doUpdate($sqlAdd);

                $sqlDelete = "DELETE FROM `online_customer` WHERE msisdn=".$cdr['src'];
                $conn->doUpdate($sqlDelete);

                $sql3 = "UPDATE ncc_agent_answer SET as_status=1 WHERE as_status=0 AND `msisdn_member`=".$cdr['src'];
                $conn->doUpdate($sql3);

                $sqlUpdate = "UPDATE cdr SET status=1 WHERE id=".$cdr['id'];
                $conn->doUpdate($sqlUpdate);
            }
        }
        sleep(5);
    }
