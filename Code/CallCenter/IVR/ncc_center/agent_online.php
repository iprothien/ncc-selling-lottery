#!/usr/bin/php -q
<?php
    set_time_limit(30);
    require_once('connect/phpagi.php');
    require_once('connect/sbMysqlPDO.class.php');
    require_once('connect/sbMssql.class.php');
    require_once('connect/config.php');
    $conn = new sbMysqlPDO($server, $user, $password, $db);
    $agi = new AGI();

    $agentNumber = $agi->get_variable("CALLERID(num)",true);
    $path = "/var/www/backend/uploads/system/config/";
    $agi->stream_file($path.'agent_login');
    /*Lay SDT cua Agent*/
    $sqlOnline = "SELECT `id`,`status` FROM `ncc_agent` WHERE `phonenumber`=".$agentNumber;
    $online = $conn->doSelectOne($sqlOnline);

    if($online['status']=1){
        $sql1 = "UPDATE `ncc_agent` SET `status`=0 WHERE `phonenumber`=".$agentNumber;
        $conn->doUpdate($sql1);
        $sql2 = "INSERT INTO `ncc_agent_status` SET `agent_id`=".$online['id'].",`agent_login_time`='".date('Y-m-d H:i:s')."'";
        $conn->doUpdate($sql2);
    }