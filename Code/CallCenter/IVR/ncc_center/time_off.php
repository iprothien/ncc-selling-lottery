#!/usr/bin/php -q
<?php
    set_time_limit(30);
    require_once('connect/phpagi.php');
    require_once('connect/sbMysqlPDO.class.php');
    require_once('connect/config.php');
    $conn = new sbMysqlPDO($server, $user, $password, $db);
    $agi = new AGI();
    
    $path = '/var/www/backend/uploads/system/config/';
    $msisdn = $agi->get_variable("CALLERID(num)",true);
    $checkTime = strtotime(date('H:i'),time());
    $toDay = date('D');

    $sql = "SELECT `day_of_week`, `start_time`, `end_time` FROM `time_off` WHERE `status`=1 AND `day_of_week` = '{$toDay}'";
    $dayOfWeek = $conn->doSelectOne($sql);

    if($dayOfWeek['day_of_week']==$toDay){
        $timeStart = strtotime($dayOfWeek['start_time'],time());
        $timeEnd = strtotime($dayOfWeek['end_time'],time());
        if($checkTime >= $timeStart && $checkTime <= $timeEnd){
            exit;
        }else{
            $agi->stream_file($path.'time_off');
            $agi->hangup();
            exit;
        }
    }