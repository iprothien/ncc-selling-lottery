#!/usr/bin/php -q
<?php
    set_time_limit(30);
    require_once('connect/phpagi.php');
    require_once('connect/sbMysqlPDO.class.php');
    require_once('connect/sbMssql.class.php');
    require_once('connect/config.php');
    $conn = new sbMysqlPDO($server, $user, $password, $db);
    $agi = new AGI();

    $member = $agi->get_variable("CALLERID(num)",true);
    $phone = $agi->get_variable("EXTEN",true);
    
    $sqlCheckAgent = " SELECT * FROM ncc_agent WHERE phonenumber='{$member}' ";
    $agent = $conn->doSelectOne($sqlCheckAgent);
    if($agent){
        $agi->exec_goto("ncc_ipbx", $phone, 1);
        exit;
    }
    
    $path_record = $agi->get_variable("MONITOR_FILENAME", true);
    $file = explode("/", $path_record);
    if(strlen($member)>=9){
    	$sqlAnswer = "INSERT INTO `ncc_agent_answer` SET `path_record`='".$file[5]."/".$file[6]."',`msisdn_member`=".$member.",`call_time`='".date('Y-m-d H:i:s')."'";
    	$conn->doUpdate($sqlAnswer);
    	$sqlOnline = "INSERT INTO `online_customer` SET msisdn=".$member;
    	$conn->doUpdate($sqlOnline);
    }