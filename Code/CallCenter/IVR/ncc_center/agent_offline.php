#!/usr/bin/php -q
<?php
    set_time_limit(30);
    require_once('connect/phpagi.php');
    require_once('connect/sbMysqlPDO.class.php');
    require_once('connect/sbMssql.class.php');
    require_once('connect/config.php');
    $conn = new sbMysqlPDO($server, $user, $password, $db);
    $agi = new AGI();
    $agentNumber = $agi->get_variable("CALLERID(num)",true);
    $path = "/var/www/backend/uploads/system/config/";
    $agi->stream_file($path.'agent_logout');
    /*Get SDT cua Agent*/
    $sqlOff = "SELECT `id`,`status` FROM `ncc_agent` WHERE `phonenumber`=".$agentNumber;
    $offline = $conn->doSelectOne($sqlOff);

    if($offline['status']==0){
        $sqlUp = "UPDATE `ncc_agent` SET status=1 WHERE `phonenumber`=".$agentNumber;
        $conn->doUpdate($sqlUp);

        $sqlTime = "UPDATE `ncc_agent_status` SET `agent_logout_time`='".date('Y:m:d H:i:s')."' WHERE `agent_id`=".$offline['id']." AND `duration`=0";
        $conn->doUpdate($sqlTime);

        $sqlCountTime = "SELECT `agent_login_time`, `agent_logout_time` FROM `ncc_agent_status` WHERE `agent_id`=".$offline['id']." AND `duration`=0";
        $time = $conn->doSelectOne($sqlCountTime);

        $timeLogoff = strtotime($time['agent_logout_time']) - strtotime($time['agent_login_time']);

        $sqlDuration = "UPDATE  `ncc_agent_status` SET `duration`=".$timeLogoff." WHERE `agent_id`=".$offline['id']." AND `duration`=0";
        $conn->doUpdate($sqlDuration);
    }