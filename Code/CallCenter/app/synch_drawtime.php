<?php
require('lib/config.php');
require('lib/functions.php');
require('lib/sbMysqlPDO.class.php');
$conn = new sbMysqlPDO($server, $user, $password, $db);


$connEtl = new sbMysqlPDO($serverEtl, $userEtl, $passEtl, $dbEtl);
$connUnitel = new sbMysqlPDO($serverUnitel, $userEtl, $passEtl, $dbEtl);
$connBeeline = new sbMysqlPDO($serverBeeline, $userEtl, $passEtl, $dbEtl);

$sqlSelectDrawTime = 'SELECT * FROM draw_time WHERE status=1 order by id DESC';
$rsDraw = $conn->doSelectOne($sqlSelectDrawTime);

if ($rsDraw) {
    $sqlCheck = 'SELECT * FROM draw_time WHERE start_time="' . $rsDraw['start_time'] . '" && 
                            end_time="' . $rsDraw['end_time'] . '" && draw_no="' . $rsDraw['draw_no'] . '" && 
                            status="' . $rsDraw['status'] . '" && ref_id="' . $rsDraw['id'] . '"';
    $dataEtl = $connEtl->doSelectOne($sqlCheck);
    
    $sqlInsert = 'INSERT INTO draw_time(
                            start_time,end_time,draw_no,three_digit,two_digit,status,ref_id
                                ) VALUES(
                            "' . $rsDraw['start_time'] . '","' . $rsDraw['end_time'] . '","' . $rsDraw['draw_no'] . '","' . $rsDraw['three_digit'] . '","' . $rsDraw['two_digit'] . '","' . $rsDraw['status'] . '","' . $rsDraw['id'] . '"
                                )';
    if (!$dataEtl)
        $connEtl->doUpdate($sqlInsert);

    $dataUnitel = $connUnitel->doSelectOne($sqlCheck);
    if(!$dataUnitel)
        $connUnitel->doUpdate($sqlInsert);
    
    $dataBeeline = $connBeeline->doSelectOne($sqlCheck);
    if(!$dataBeeline)
        $connBeeline->doUpdate($sqlInsert);
}
exit;
?>