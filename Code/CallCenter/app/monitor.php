<?php
/**
 * User: dovv<dovv1987@gmail.com>
 * DateTime: 7/21/14 3:02 PM
 */

require_once('lib/config.php');
require_once('lib/sbMysqlPDO.class.php');

// Check Connecting
//$conn_beeline = new sbMysqlPDO($serverBeeline,$userEtl,$passEtl,$dbEtl);
//$checkConnBeeline = $conn_beeline->getConnection();
$conn_etl = new sbMysqlPDO($serverEtl,$userEtl,$passEtl,$dbEtl);
$checkConnEtl = $conn_etl->getConnection();
$conn_unitel = new sbMysqlPDO($serverUnitel,$userEtl,$passEtl,$dbEtl);
$checkConnUnitel = $conn_unitel->getConnection();

$msg = 'Error message box : <br /><br />';

/*
if(empty($checkConnBeeline)){
    $msg .= 'Can not connect to Beeline mysql server <br />';
}else{
    echo 'Ok Mysql Beeline'.'<br />';
}
*/

if(empty($checkConnEtl)){
    $msg .= 'Can not connect to Etl mysql server <br />';
}else{
    echo 'Ok Mysql Etl'.'<br />';
}

if(empty($checkConnUnitel)){
    $msg .= 'Can not connect to Unitel mysql server <br />';
}else{
    echo 'Ok Mysql Unitel'.'<br />';
}

// Check Asterisk
$statusAsterisk = exec('/etc/init.d/asterisk status');
$strAs = "is running";
if (strpos($statusAsterisk,$strAs) > 0) {
    echo 'Ok Asterisk';
}else{
    $msg .= 'Error Asterisk or Stop Asterisk.'.'<br />';
}

// Ping server Etl
$pingEtl = exec('ping -c 3 202.62.111.233');
if(empty($pingEtl)){
    $msg .= 'Can not ping to Etl server <br />';
}else{
    echo 'Ok'.'<br />';
}

// Ping server Beeline
/*
$pingBeeline = exec('ping -c 3 115.84.105.43');
if(empty($pingBeeline)){
    $msg .= 'Can not ping to Beeline server <br />';
}else{
    echo 'Ok Beeline'.'<br />';
}
*/

// Ping server Unitel
$pingUnitel = exec('ping -c 3 183.182.100.131');
if(empty($pingUnitel)){
    $msg .= 'Can not ping to Unitel server <br />';
}else{
    echo 'Ok Unitel'.'<br />';
}
// || empty($pingBeeline)    empty($checkConnBeeline) ||
if(empty($checkConnEtl) || empty($checkConnUnitel) || strpos($statusAsterisk,$strAs) < 0 || empty($pingEtl) || empty($pingUnitel)){
    sendEmail($_email,$msg);

    // Reset bien
    unset($checkConnBeeline);
    unset($checkConnEtl);
    unset($checkConnUnitel);
    unset($statusAsterisk);
    unset($strAs);
    unset($pingEtl);
    unset($pingBeeline);
    unset($pingUnitel);
}else{
    echo 'Ok';
    // Reset bien
    unset($checkConnBeeline);
    unset($checkConnEtl);
    unset($checkConnUnitel);
    unset($statusAsterisk);
    unset($strAs);
    unset($pingEtl);
    unset($pingBeeline);
    unset($pingUnitel);
}

function sendEmail($mailList =array(),$body){

    require_once('mailer/class.phpmailer.php');
    require_once('mailer/class.smtp.php');
    try {
        $mail = new PHPMailer(true);                //New instance, with exceptions enabled
        $mail->IsSMTP();                            // tell the class to use SMTP
        $mail->SMTPAuth   = true;                   // enable SMTP authentication
        $mail->Port       = 465;                    // set the SMTP server port
        $mail->Host       = "smtp.gmail.com";       // SMTP server
	 $mail->SMTPSecure = 'ssl'; 
        $mail->Username   = "dovv@iprotech.vn";     // SMTP server username
        $mail->Password   = "Do1987@123";           // SMTP server password

        $mail->From       = "dovv@iprotech.vn";
        $mail->FromName   = "NCC monitoring";

        foreach($mailList as $to){
            $mail->AddAddress($to);
        }
     
        $mail->Subject  = "NCC monitoring Call Center";
        $mail->MsgHTML($body);
        $mail->IsHTML(true);
        $mail->Send();
	 echo 'Email Ok';
    } catch (phpmailerException $e) {
        echo $e->errorMessage();
    }
    return true;
}
