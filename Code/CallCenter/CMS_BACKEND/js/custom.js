/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

jQuery.noConflict();
(function($, window, document, undefined) {
    $(document).ready(function() {
        $('#btn-export').on('click', function(e) {
            e.preventDefault();
            var url = $(this).attr('url');
            window.location = url + '?export=true&' + $(this).parents('form').serialize();
            return false;
        });
    });
})(jQuery, window, document);