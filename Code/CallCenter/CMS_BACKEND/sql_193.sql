-- MySQL dump 10.11
--
-- Host: localhost    Database: ncc
-- ------------------------------------------------------
-- Server version	5.0.95-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `account`
--

DROP TABLE IF EXISTS `account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(80) NOT NULL default '',
  `canreinvite` char(3) default 'yes',
  `context` varchar(80) default NULL,
  `dtmfmode` varchar(7) default NULL,
  `host` varchar(31) NOT NULL default '',
  `port` varchar(5) NOT NULL default '',
  `secret` varchar(80) default NULL,
  `type` varchar(6) NOT NULL default 'friend',
  `username` varchar(80) NOT NULL default '',
  `ipaddr` varchar(1024) default NULL,
  `regseconds` varchar(1024) default NULL,
  `useragent` varchar(1024) default NULL,
  `lastms` varchar(1024) default NULL,
  `fullcontact` varchar(1024) default NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `name_2` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account`
--

LOCK TABLES `account` WRITE;
/*!40000 ALTER TABLE `account` DISABLE KEYS */;
INSERT INTO `account` VALUES (21,'900','yes','ncc_ipbx','inband','dynamic','5060','900','friend','900',NULL,NULL,NULL,NULL,NULL),(22,'800','yes','call_center','inband','dynamic','27162','800','friend','800','192.168.0.107','1395218960','X-Lite release 1104o stamp 56125','0','sip:800@192.168.0.107:27162;rinstance=ef7296c6ebbf6c54'),(23,'8080','yes','call_center','inband','dynamic','','8080','friend','8080','','0','','',''),(20,'700','yes','call_center','inband','dynamic','0','700','friend','700','0.0.0.0','1395054383','3CXPhone 6.0.18815.0','0','sip:700@192.168.0.201:58433;rinstance=ebed05067b973785'),(24,'9000','yes','call_center','inband','dynamic','5060','9000','friend','9000',NULL,NULL,NULL,NULL,NULL),(25,'9100','yes','ncc_ipbx','inband','dynamic','5060','9100','friend','9100',NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `account1`
--

DROP TABLE IF EXISTS `account1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account1` (
  `id` int(10) NOT NULL auto_increment,
  `username` varchar(255) collate utf8_unicode_ci NOT NULL,
  `password` varchar(255) collate utf8_unicode_ci NOT NULL,
  `fullname` varchar(255) collate utf8_unicode_ci NOT NULL,
  `module` varchar(255) collate utf8_unicode_ci NOT NULL,
  `service` varchar(255) collate utf8_unicode_ci NOT NULL,
  `status` tinyint(2) NOT NULL,
  `group_id` tinyint(2) NOT NULL,
  `user_type` tinyint(1) NOT NULL default '0',
  `total_sms` int(10) NOT NULL,
  `remain_sms` int(10) NOT NULL,
  `unlimit_sms` tinyint(2) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=30 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account1`
--

LOCK TABLES `account1` WRITE;
/*!40000 ALTER TABLE `account1` DISABLE KEYS */;
INSERT INTO `account1` VALUES (1,'admin','21232f297a57a5a743894a0e4a801fc3','hung nguyen','user,category,member,memberCheck,content,chargingHistory,reporting,smsMo,smsMt,service,commandCode,shortcode,sendSms,emailReport,votingKeyword','[1][2][17][19][20][21][22][23][24]',1,1,0,10000000,9998191,0),(16,'pardthana','96e79218965eb72c92a549dd5a330112','pardthana','content','[1][2][17][19][20][21][22][23][24]',1,1,0,1000,998,0),(28,'vtnews2','e10adc3949ba59abbe56e057f20f883e','VT NEWS','member,content,chargingHistory,smsMo,smsMt','[2]',1,2,0,0,-64699,1),(27,'vttnews','5a405ecd7682252e0f457487942ca69e','VTT NEWS','member,content,chargingHistory,smsMo,smsMt','[2]',1,2,0,0,0,1),(26,'laolotto','e451a3482d63d2624d8d43c805eaaa94','Lao Lottery','member,content,chargingHistory,smsMo,smsMt','[1]',1,2,0,0,-66468,1),(25,'news','5a405ecd7682252e0f457487942ca69e','News','member,content,chargingHistory,smsMo,smsMt','[2]',1,2,1,1000,1000,1),(29,'dovv','827ccb0eea8a706c4c34a16891f84e7b','Vo Van Do','user,member,content,reporting,smsMt,commandCode,sendSms,votingKeyword','[1][2][19][21][23][24]',1,1,2,0,0,1);
/*!40000 ALTER TABLE `account1` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin` (
  `id` int(4) NOT NULL auto_increment,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `module` varchar(255) NOT NULL,
  `permision` tinyint(2) NOT NULL default '1',
  `status` tinyint(2) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin`
--

LOCK TABLES `admin` WRITE;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
INSERT INTO `admin` VALUES (1,'admin','202cb962ac59075b964b07152d234b70','user,staff,departments,config,drawTime,nccAgent,nccAgentAnswer,nccAgentStatus,nccGroup,nccNote,cdr',1,1),(9,'123','827ccb0eea8a706c4c34a16891f84e7b','nccAgentAnswer,nccNote',0,1),(10,'199','c4ca4238a0b923820dcc509a6f75849b','nccAgentAnswer,nccNote',0,1),(11,'800','7a53928fa4dd31e82c6ef826f341daec','nccAgentAnswer,nccNote',0,1),(12,'800','7a53928fa4dd31e82c6ef826f341daec','nccAgentAnswer,nccNote',0,1),(13,'8080','d41d8cd98f00b204e9800998ecf8427e','nccAgentAnswer,nccNote',0,1),(14,'9000','d5ab8dc7ef67ca92e41d730982c5c602','nccAgentAnswer,nccNote',0,1),(15,'1223','d41d8cd98f00b204e9800998ecf8427e','nccAgentAnswer,nccNote',0,1);
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `call_note`
--

DROP TABLE IF EXISTS `call_note`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `call_note` (
  `id` int(11) NOT NULL auto_increment,
  `msisdn` varchar(20) NOT NULL,
  `name` varchar(150) NOT NULL,
  `address` varchar(255) NOT NULL,
  `age` tinyint(1) NOT NULL,
  `content` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `call_note`
--

LOCK TABLES `call_note` WRITE;
/*!40000 ALTER TABLE `call_note` DISABLE KEYS */;
INSERT INTO `call_note` VALUES (1,'2023047852','Duong Thien','',2,'',2,'2014-03-19 03:29:16');
/*!40000 ALTER TABLE `call_note` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cdr`
--

DROP TABLE IF EXISTS `cdr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cdr` (
  `id` int(11) NOT NULL auto_increment,
  `start` datetime NOT NULL default '0000-00-00 00:00:00',
  `callerid` varchar(80) NOT NULL default '',
  `src` varchar(80) NOT NULL default '',
  `dst` varchar(80) NOT NULL default '',
  `dcontext` varchar(80) NOT NULL default '',
  `channel` varchar(80) NOT NULL default '',
  `dstchannel` varchar(80) NOT NULL default '',
  `lastapp` varchar(80) NOT NULL default '',
  `lastdata` varchar(80) NOT NULL default '',
  `duration` int(11) NOT NULL default '0',
  `billsec` int(11) NOT NULL default '0',
  `disposition` varchar(45) NOT NULL default '',
  `amaflags` int(11) NOT NULL default '0',
  `accountcode` varchar(20) NOT NULL default '',
  `userfield` varchar(255) NOT NULL default '',
  `uniqueid` varchar(255) NOT NULL default '',
  `status` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `src` (`src`),
  KEY `channel` (`channel`),
  KEY `dcontext` (`dcontext`),
  KEY `start` (`start`)
) ENGINE=MyISAM AUTO_INCREMENT=40 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cdr`
--

LOCK TABLES `cdr` WRITE;
/*!40000 ALTER TABLE `cdr` DISABLE KEYS */;
INSERT INTO `cdr` VALUES (1,'2014-03-19 09:37:01','','2023047852','2023047852','call_center','SIP/Etl-000007d1','','Queue','callcenter,twh',25,25,'ANSWERED',3,'','','1395196621.2001',1),(2,'2014-03-19 09:55:50','','6009','00972598267299','default','SIP/27.118.30.2-000007d2','','SayDigits','123',1,1,'ANSWERED',3,'','','1395197750.2002',0),(3,'2014-03-19 09:55:53','','6009','900972598267299','default','SIP/27.118.30.2-000007d3','','Hangup','',1,1,'ANSWERED',3,'','','1395197753.2003',0),(4,'2014-03-19 10:28:02','','2023047852','2023047852','call_center','SIP/Etl-000007d4','','Queue','callcenter,twh',15,15,'ANSWERED',3,'','','1395199682.2004',1),(5,'2014-03-19 10:45:53','','7009','00972598267299','default','SIP/27.118.30.2-000007d5','','Answer','',1,1,'ANSWERED',3,'','','1395200753.2005',0),(6,'2014-03-19 10:45:56','','7009','900972598267299','default','SIP/27.118.30.2-000007d6','','Hangup','',1,1,'ANSWERED',3,'','','1395200756.2006',0),(7,'2014-03-19 11:35:17','','8009','00972598267299','default','SIP/27.118.30.2-000007d7','','Answer','',1,1,'ANSWERED',3,'','','1395203717.2007',0),(8,'2014-03-19 11:35:21','','8009','900972598267299','default','SIP/27.118.30.2-000007d8','','Answer','',0,0,'ANSWERED',3,'','','1395203721.2008',0),(9,'2014-03-19 12:25:00','','9009','00972598267299','default','SIP/27.118.30.2-000007d9','','Answer','',0,0,'ANSWERED',3,'','','1395206700.2009',0),(10,'2014-03-19 12:25:04','','9009','900972598267299','default','SIP/27.118.30.2-000007da','','Answer','',0,0,'ANSWERED',3,'','','1395206704.2010',0),(11,'2014-03-19 13:03:12','','100','000972598601063','music','SIP/100-000007db','','Answer','',1,1,'ANSWERED',3,'','','1395208992.2011',0),(12,'2014-03-19 13:07:49','','2003','00972598267299','default','SIP/27.118.30.2-000007dc','','Answer','',0,0,'ANSWERED',3,'','','1395209269.2012',0),(13,'2014-03-19 13:07:50','','2003','900972598267299','default','SIP/27.118.30.2-000007dd','','GotoIf','0?Label1448',1,1,'ANSWERED',3,'','','1395209270.2013',0),(14,'2014-03-19 13:09:43','','101','00972592659883','thiendv','SIP/101-000007de','','Answer','',0,0,'ANSWERED',3,'','','1395209383.2014',0),(15,'2014-03-19 13:09:44','','101','000972592659883','thiendv','SIP/101-000007df','','Answer','',1,1,'ANSWERED',3,'','','1395209384.2015',0),(16,'2014-03-19 13:09:46','','101','900972592659883','thiendv','SIP/101-000007e0','','Answer','',1,1,'ANSWERED',3,'','','1395209386.2016',0),(17,'2014-03-19 13:28:13','','300','00972592659883','default','SIP/27.118.30.2-000007e1','','Answer','',0,0,'ANSWERED',3,'','','1395210493.2017',0),(18,'2014-03-19 13:28:14','','300','000972592659883','default','SIP/27.118.30.2-000007e2','','Answer','',0,0,'ANSWERED',3,'','','1395210494.2018',0),(19,'2014-03-19 13:28:15','','300','900972592659883','default','SIP/27.118.30.2-000007e3','','Set','CHANNEL(language)=lao',1,1,'ANSWERED',3,'','','1395210495.2019',0),(20,'2014-03-19 13:46:29','','200','00972592659883','default','SIP/27.118.30.2-000007e4','','Answer','',1,1,'ANSWERED',3,'','','1395211589.2020',0),(21,'2014-03-19 13:46:31','','200','000972592659883','default','SIP/27.118.30.2-000007e5','','Answer','',0,0,'ANSWERED',3,'','','1395211591.2021',0),(22,'2014-03-19 13:46:32','','200','900972592659883','default','SIP/27.118.30.2-000007e6','','Answer','',0,0,'ANSWERED',3,'','','1395211592.2022',0),(23,'2014-03-19 13:48:09','','300','00972598267299','default','SIP/27.118.30.2-000007e7','','Hangup','',0,0,'ANSWERED',3,'','','1395211689.2023',0),(24,'2014-03-19 13:48:12','','300','900972598267299','default','SIP/27.118.30.2-000007e8','','Hangup','',1,0,'ANSWERED',3,'','','1395211692.2024',0),(25,'2014-03-19 14:04:45','','6001','00972592659883','default','SIP/27.118.30.2-000007e9','','Answer','',1,1,'ANSWERED',3,'','','1395212685.2025',0),(26,'2014-03-19 14:04:47','','6001','000972592659883','default','SIP/27.118.30.2-000007ea','','Hangup','',0,0,'ANSWERED',3,'','','1395212687.2026',0),(27,'2014-03-19 14:04:48','','6001','900972592659883','default','SIP/27.118.30.2-000007eb','','Answer','',0,0,'ANSWERED',3,'','','1395212688.2027',0),(28,'2014-03-19 14:22:57','','1001','00972592659883','default','SIP/27.118.30.2-000007ec','','Answer','',1,1,'ANSWERED',3,'','','1395213777.2028',0),(29,'2014-03-19 14:22:59','','1001','000972592659883','default','SIP/27.118.30.2-000007ed','','Answer','',0,0,'ANSWERED',3,'','','1395213779.2029',0),(30,'2014-03-19 14:23:00','','1001','900972592659883','default','SIP/27.118.30.2-000007ee','','Hangup','',0,0,'ANSWERED',3,'','','1395213780.2030',0),(31,'2014-03-19 14:31:30','','221','00972598267299','default','SIP/27.118.30.2-000007ef','','Set','CHANNEL(language)=lao',1,1,'ANSWERED',3,'','','1395214290.2031',0),(32,'2014-03-19 14:31:33','','221','900972598267299','default','SIP/27.118.30.2-000007f0','','Answer','',0,0,'ANSWERED',3,'','','1395214293.2032',0),(33,'2014-03-19 14:40:48','','150','00972592659883','default','SIP/27.118.30.2-000007f1','','Answer','',0,0,'ANSWERED',3,'','','1395214848.2033',0),(34,'2014-03-19 14:40:49','','150','000972592659883','default','SIP/27.118.30.2-000007f2','','Answer','',0,0,'ANSWERED',3,'','','1395214849.2034',0),(35,'2014-03-19 14:40:50','','150','900972592659883','default','SIP/27.118.30.2-000007f3','','Answer','',1,1,'ANSWERED',3,'','','1395214850.2035',0),(36,'2014-03-19 14:58:27','','120','00972592659883','default','SIP/27.118.30.2-000007f4','','Answer','',0,0,'ANSWERED',3,'','','1395215907.2036',0),(37,'2014-03-19 14:58:28','','120','000972592659883','default','SIP/27.118.30.2-000007f5','','SayDigits','12345',1,1,'ANSWERED',3,'','','1395215908.2037',0),(38,'2014-03-19 14:58:29','','120','900972592659883','default','SIP/27.118.30.2-000007f6','','Answer','',1,1,'ANSWERED',3,'','','1395215909.2038',0),(39,'2014-03-19 15:02:06','','100','972598601063','music','SIP/100-000007f7','','Answer','',0,0,'ANSWERED',3,'','','1395216126.2039',0);
/*!40000 ALTER TABLE `cdr` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `config`
--

DROP TABLE IF EXISTS `config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `config` (
  `id` int(4) NOT NULL auto_increment,
  `key` varchar(255) collate utf8_unicode_ci NOT NULL,
  `value` varchar(255) collate utf8_unicode_ci NOT NULL,
  `note` varchar(255) collate utf8_unicode_ci NOT NULL,
  `convert_status` tinyint(2) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `config`
--

LOCK TABLES `config` WRITE;
/*!40000 ALTER TABLE `config` DISABLE KEYS */;
INSERT INTO `config` VALUES (1,'Welcome','welcome.mp3','Welcome to NCC callcenter',1),(2,'Require enter','require_enter.mp3','Require enter phone number',1),(3,'Require re-enter','require_re_enter.mp3','Require re-enter phone number',1),(4,'Not connect','not_connect.mp3','Can not connect to phone number',1),(5,'Agent Login','agent_login.mp3','Agent Login File',1),(6,'Agent Logout','agent_logout.mp3','Agent Logout File',1),(7,'Agent busy','agent_busy.mp3','Sorry all of agent busy, pls try again later',1),(8,'No phone','no_phone.mp3','The phone number does not exist, pls try another',1);
/*!40000 ALTER TABLE `config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `departments`
--

DROP TABLE IF EXISTS `departments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `departments` (
  `id` int(11) NOT NULL auto_increment,
  `shortcode` int(10) unsigned NOT NULL,
  `name` varchar(50) NOT NULL,
  `path_wellcome_file` varchar(50) NOT NULL,
  `convert_status` tinyint(2) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `departments`
--

LOCK TABLES `departments` WRITE;
/*!40000 ALTER TABLE `departments` DISABLE KEYS */;
INSERT INTO `departments` VALUES (1,8090,'iprotech','1_month.mp3',1),(2,444,'Phong Kinh Doanh','2_month.mp3',1),(10,3333,'test','10_month.mp3',1);
/*!40000 ALTER TABLE `departments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `draw_time`
--

DROP TABLE IF EXISTS `draw_time`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `draw_time` (
  `id` int(11) NOT NULL auto_increment,
  `time_start` datetime NOT NULL,
  `time_end` datetime NOT NULL,
  `status` tinyint(1) NOT NULL default '1',
  PRIMARY KEY  (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `draw_time`
--

LOCK TABLES `draw_time` WRITE;
/*!40000 ALTER TABLE `draw_time` DISABLE KEYS */;
INSERT INTO `draw_time` VALUES (1,'2014-03-12 00:00:00','2014-03-13 12:56:00',1),(2,'2014-03-12 00:00:00','2014-03-13 00:00:00',0);
/*!40000 ALTER TABLE `draw_time` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `extensions`
--

DROP TABLE IF EXISTS `extensions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `extensions` (
  `id` int(11) NOT NULL auto_increment,
  `context` varchar(20) NOT NULL default '',
  `exten` varchar(20) NOT NULL default '',
  `priority` tinyint(4) NOT NULL default '0',
  `app` varchar(20) NOT NULL default '',
  `appdata` varchar(128) NOT NULL default '',
  PRIMARY KEY  (`id`),
  KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `extensions`
--

LOCK TABLES `extensions` WRITE;
/*!40000 ALTER TABLE `extensions` DISABLE KEYS */;
/*!40000 ALTER TABLE `extensions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `miss_call`
--

DROP TABLE IF EXISTS `miss_call`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `miss_call` (
  `id` int(11) NOT NULL auto_increment,
  `msisdn` varchar(20) NOT NULL,
  `status` tinyint(1) NOT NULL default '1',
  `created` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `miss_call`
--

LOCK TABLES `miss_call` WRITE;
/*!40000 ALTER TABLE `miss_call` DISABLE KEYS */;
INSERT INTO `miss_call` VALUES (1,'2023047852',2,'2014-03-19 03:29:16'),(2,'2023047852',1,'2014-03-19 03:29:01');
/*!40000 ALTER TABLE `miss_call` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ncc_agent`
--

DROP TABLE IF EXISTS `ncc_agent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ncc_agent` (
  `id` int(11) NOT NULL auto_increment,
  `group_id` int(11) NOT NULL COMMENT 'Biết Agent thuộc group nào',
  `phonenumber` varchar(15) NOT NULL COMMENT 'Số ĐT trong bảng SIP',
  `name` varchar(128) NOT NULL COMMENT 'Tên Agent',
  `status` tinyint(1) NOT NULL default '0' COMMENT '0: Online; 1: Offline',
  PRIMARY KEY  (`id`),
  KEY `id` (`id`),
  KEY `status` (`status`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='Table lưu thông tin Agent';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ncc_agent`
--

LOCK TABLES `ncc_agent` WRITE;
/*!40000 ALTER TABLE `ncc_agent` DISABLE KEYS */;
INSERT INTO `ncc_agent` VALUES (3,1,'700','Duong Van Thien',0),(4,1,'800','800',1),(5,2,'8080','dovv',1),(6,1,'9000','9000',0),(7,1,'1223','Hoai',0);
/*!40000 ALTER TABLE `ncc_agent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ncc_agent_answer`
--

DROP TABLE IF EXISTS `ncc_agent_answer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ncc_agent_answer` (
  `id` int(11) NOT NULL auto_increment,
  `agent_id` int(11) NOT NULL COMMENT 'Mã Agent',
  `path_record` varchar(50) NOT NULL COMMENT 'File ghi âm Agent trả lời khách hàng  ',
  `cv_status` tinyint(4) NOT NULL default '0',
  `msisdn_agent` varchar(15) default NULL COMMENT 'Sá»‘ Ä‘iá»‡n thoáº¡i cá»§a Agent',
  `msisdn_member` varchar(15) NOT NULL COMMENT 'Số điện thoại Member gọi đến',
  `call_time` datetime NOT NULL COMMENT 'Thời điểm gọi đến của Member tới Agent',
  `duration` int(11) default NULL COMMENT 'Thời gian đàm thoại',
  `in_status` tinyint(1) NOT NULL default '0',
  `as_status` tinyint(1) NOT NULL default '0',
  `status` tinyint(1) NOT NULL default '0',
  `note` varchar(255) NOT NULL,
  `customer` varchar(150) default NULL,
  `created` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `updated` datetime default NULL,
  PRIMARY KEY  (`id`),
  KEY `id` (`id`,`agent_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='Table này ghi log cuộc gọi';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ncc_agent_answer`
--

LOCK TABLES `ncc_agent_answer` WRITE;
/*!40000 ALTER TABLE `ncc_agent_answer` DISABLE KEYS */;
INSERT INTO `ncc_agent_answer` VALUES (1,0,'',0,NULL,'2023047852','2014-03-19 09:37:01',NULL,1,0,0,'',NULL,'2014-03-19 02:38:01',NULL),(2,0,'',0,NULL,'2023047852','2014-03-19 10:28:02',NULL,1,0,0,'',NULL,'2014-03-19 03:29:01',NULL),(3,22,'NULL',0,'800','2023047852','2014-03-19 10:29:16',0,0,0,1,'','Duong Thien','2014-03-19 03:29:16','2014-03-19 10:29:16');
/*!40000 ALTER TABLE `ncc_agent_answer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ncc_agent_status`
--

DROP TABLE IF EXISTS `ncc_agent_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ncc_agent_status` (
  `id` int(11) NOT NULL auto_increment,
  `agent_id` int(11) NOT NULL COMMENT 'Mã của Agent',
  `agent_login_time` datetime default NULL COMMENT 'Thời gian Agent nhập',
  `agent_logout_time` datetime default NULL COMMENT 'Thời gian Agent Logout',
  `duration` int(6) NOT NULL COMMENT 'Thời Agent Login trởi lại',
  PRIMARY KEY  (`id`),
  KEY `id` (`id`,`agent_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COMMENT='Table theo dõi Agent login, logout';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ncc_agent_status`
--

LOCK TABLES `ncc_agent_status` WRITE;
/*!40000 ALTER TABLE `ncc_agent_status` DISABLE KEYS */;
INSERT INTO `ncc_agent_status` VALUES (1,1,'2013-10-11 11:13:10','2013-10-12 11:13:14',130),(2,2,'2014-01-22 14:51:35','2014-01-23 11:43:16',75101),(3,1,'2014-01-23 11:27:27','2014-01-23 11:30:24',177),(4,1,'2014-01-23 11:31:15','2014-01-23 11:31:31',16),(5,2,'2014-01-23 11:43:30','2014-01-24 15:14:53',99083),(6,2,'2014-01-24 15:24:50',NULL,0),(7,1,'2014-01-24 15:24:55','2014-01-25 10:30:15',68720),(8,1,'2014-01-25 10:42:17','2014-01-25 11:12:29',1812),(9,5,'2014-03-11 11:42:29','2014-03-11 15:53:31',15062),(10,5,'2014-03-11 16:32:06','2014-03-11 16:36:56',290),(11,4,'2014-03-11 17:27:10','2014-03-11 17:34:44',454),(12,3,'2014-03-17 12:06:29','2014-03-17 14:14:32',7683),(13,3,'2014-03-17 12:37:29','2014-03-17 14:14:32',7683),(14,4,'2014-03-17 12:43:40','2014-03-17 14:14:15',5435),(15,3,'2014-03-17 12:43:49','2014-03-17 14:14:32',7683),(16,3,'2014-03-17 14:21:20','2014-03-17 14:42:23',1263),(17,4,'2014-03-17 14:38:13','2014-03-17 14:42:08',235),(18,3,'2014-03-17 14:38:31','2014-03-17 14:42:23',1263),(19,3,'2014-03-17 14:48:13','2014-03-17 14:49:03',50),(20,3,'2014-03-17 14:49:05',NULL,0);
/*!40000 ALTER TABLE `ncc_agent_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ncc_group`
--

DROP TABLE IF EXISTS `ncc_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ncc_group` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(128) NOT NULL COMMENT 'Tên group',
  `status` tinyint(4) NOT NULL default '0' COMMENT '0: Active; 1: Disable',
  PRIMARY KEY  (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='Table này phân nhóm hỗ trợ khách hàng (Sales, Support, QA...';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ncc_group`
--

LOCK TABLES `ncc_group` WRITE;
/*!40000 ALTER TABLE `ncc_group` DISABLE KEYS */;
INSERT INTO `ncc_group` VALUES (1,'Sales',0),(2,'Support',0),(3,'Q&A',0);
/*!40000 ALTER TABLE `ncc_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ncc_note`
--

DROP TABLE IF EXISTS `ncc_note`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ncc_note` (
  `id` int(11) NOT NULL auto_increment,
  `agent_id` int(11) NOT NULL,
  `agent_answer_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL COMMENT '0: Đã xử lý, 1: Đang xử lý, 2: Chưa xử lý',
  `name` varchar(255) NOT NULL COMMENT 'Tên note',
  `timer` datetime NOT NULL COMMENT 'Thời gian',
  `content` varchar(128) NOT NULL COMMENT 'Nội dung',
  `process_content` varchar(128) NOT NULL,
  `group_id` int(11) NOT NULL COMMENT 'Thuộc nhóm nội dung nào',
  PRIMARY KEY  (`id`),
  KEY `id` (`id`,`agent_id`),
  KEY `status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Table này ghi lại note cuộc gọi ';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ncc_note`
--

LOCK TABLES `ncc_note` WRITE;
/*!40000 ALTER TABLE `ncc_note` DISABLE KEYS */;
INSERT INTO `ncc_note` VALUES (1,9,1,0,'testthoi','2013-10-12 09:46:20','abc','xyz',2);
/*!40000 ALTER TABLE `ncc_note` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ncc_queue`
--

DROP TABLE IF EXISTS `ncc_queue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ncc_queue` (
  `id` int(11) NOT NULL auto_increment,
  `msisdn_member` varchar(15) NOT NULL COMMENT 'Số điện thoại gọi đến',
  `timer` datetime NOT NULL COMMENT 'Gọi đến lúc nào',
  `status` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table này lưu số điện thoại gọi đến khi tất cả các Agent đều';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ncc_queue`
--

LOCK TABLES `ncc_queue` WRITE;
/*!40000 ALTER TABLE `ncc_queue` DISABLE KEYS */;
/*!40000 ALTER TABLE `ncc_queue` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `online_customer`
--

DROP TABLE IF EXISTS `online_customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `online_customer` (
  `id` int(11) NOT NULL auto_increment,
  `msisdn` varchar(20) NOT NULL,
  `status` tinyint(1) NOT NULL default '1',
  `created` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `online_customer`
--

LOCK TABLES `online_customer` WRITE;
/*!40000 ALTER TABLE `online_customer` DISABLE KEYS */;
/*!40000 ALTER TABLE `online_customer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quocta`
--

DROP TABLE IF EXISTS `quocta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quocta` (
  `id` int(11) NOT NULL auto_increment,
  `max_two` int(11) NOT NULL,
  `max_three` int(11) NOT NULL,
  `time_start` datetime NOT NULL,
  `time_end` datetime NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `quocta`
--

LOCK TABLES `quocta` WRITE;
/*!40000 ALTER TABLE `quocta` DISABLE KEYS */;
/*!40000 ALTER TABLE `quocta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `staff`
--

DROP TABLE IF EXISTS `staff`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `staff` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `full_name` varchar(40) collate utf8_unicode_ci NOT NULL,
  `phone` varchar(11) collate utf8_unicode_ci NOT NULL,
  `departments_id` smallint(5) unsigned NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `staff`
--

LOCK TABLES `staff` WRITE;
/*!40000 ALTER TABLE `staff` DISABLE KEYS */;
INSERT INTO `staff` VALUES (1,'HUNG NGUYEN','2000',1),(2,'Thien DV','113',2),(3,'Nguyen Hoang Thu','114',2),(4,'THIENDV','900',1),(5,'Do','9100',2);
/*!40000 ALTER TABLE `staff` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-03-19 15:10:23
