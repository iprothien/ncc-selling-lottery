<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AccessController
 *
 * @author BUI VAN UY
 */
class AccessController extends Controller {

    public $layout = '//layouts/column2';
    public $defaultAction = 'admin';
    public $module;
    public $account;

    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            //'postOnly + delete', // we only allow deletion via POST request
        );
    }
    
    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('view', 'create','update', 'delete', 'admin', 'export','draw','delallaction','delaction','delAllinOnlyTest','import','paynow'),
                'users' => array('@'),
                'expression' => (string) $this->authenticate($this->module)
            ),
/*
            array('allow',
                'actions' => array('delete', 'update'),
                'users' => array('@'),
                'expression' => (string) $this->full()
            ),
*/
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function authenticate($module) {
        $account = $this->getAccount();
        if ($account) {
            $accountModules = explode(',', $account->module);
            return in_array($module, $accountModules);
        }
        return FALSE;
    }
    
    public function full() {
        $account = $this->getAccount();
        if ($account) {
            if ($account->permision==2)
                return TRUE;
        }
        return FALSE;
    }

    public function getAccount() {
        if (!$this->account)
            $this->account = User::model()->findByAttributes(array('username' => Yii::app()->user->name, 'status' => 1));
        return $this->account;
    }
    
    public function behaviors() {
        return array(
            'eexcelview' => array(
                'class' => 'application.extensions.eexcelview.EExcelBehavior',
            )
        );
    }

    public function exportData($model, $attributes, $filename) {
        $data = $model->search();
        $data->setPagination(array('pageSize' => '10000'));
        $attributesList = array();
        foreach ($attributes as $item) {
            $attributesList[] = $item . '::' . $model->getAttributeLabel($item);
        }
        $this->toExcel($data, $attributesList, $filename, array('creator' => Yii::app()->user->name), 'CSV');
    }
}

?>