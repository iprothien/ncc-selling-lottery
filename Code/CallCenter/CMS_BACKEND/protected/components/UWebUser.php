<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CWebUser
 *
 */
class UWebUser extends CWebUser
{

    private $account;
    public $returnUrl = 'index.php?r=service/admin';

    public function getGroup1()
    {
        $account = $this->getAccount();
        $menu = array();
        if ($account) {
            if (strlen(strstr($account->module, 'account')) > 0)
                $menu[] = array('label' => 'Agent', 'url' => Yii::app()->createUrl('/account/admin'));
            if (strlen(strstr($account->module, 'nccAgentAnswer')) > 0)
                $menu[] = array('label' => 'Call Note', 'url' => Yii::app()->createUrl('/nccAgentAnswer/admin'));
            if (strlen(strstr($account->module, 'staff')) > 0)
                $menu[] = array('label' => 'Staff', 'url' => Yii::app()->createUrl('/staff/admin'));
            if (strlen(strstr($account->module, 'config')) > 0)
                $menu[] = array('label' => 'Configuration', 'url' => Yii::app()->createUrl('/config/admin'));
            if (strlen(strstr($account->module, 'timeOff')) > 0)
                $menu[] = array('label' => 'Working Time', 'url' => Yii::app()->createUrl('/timeOff/admin'));
            if (strlen(strstr($account->module, 'province')) > 0)
                $menu[] = array('label' => 'Province', 'url' => Yii::app()->createUrl('/province2/admin'));
        }
        return $menu;
    }

    public function getGroup2()
    {
        $account = $this->getAccount();
        $menu = array();
        if ($account) {
            if (strlen(strstr($account->module, 'statistic')) > 0)
                $menu[] = array('label' => 'Statistic', 'url' => Yii::app()->createUrl('/statistic/admin'));
            if (strlen(strstr($account->module, 'memberDraw')) > 0)
                $menu[] = array('label' => 'History', 'url' => Yii::app()->createUrl('memberDraw/admin'));
            if (strlen(strstr($account->module, 'drawTime')) > 0)
                $menu[] = array('label' => 'Draw time', 'url' => Yii::app()->createUrl('drawTime/admin'));
            if (strlen(strstr($account->module, 'drawResult')) > 0)
                $menu[] = array('label' => 'Result', 'url' => Yii::app()->createUrl('drawResult/admin'));
            if (strlen(strstr($account->module, 'winner')) > 0)
                $menu[] = array('label' => 'Winner', 'url' => Yii::app()->createUrl('winner/admin'));
            if (strlen(strstr($account->module, 'cdr')) > 0)
                $menu[] = array('label' => 'Cdr', 'url' => Yii::app()->createUrl('cdr/admin'));
            //if (strlen(strstr($account->module, 'winner')) > 0)
            //    $menu[] = array('label' => 'Payment', 'url' => Yii::app()->createUrl('winner/paynow'));
            if (strlen(strstr($account->module, 'smsMt')) > 0)
                $menu[] = array('label' => 'SMS MT', 'url' => Yii::app()->createUrl('smsMt/admin'));
            if (strlen(strstr($account->module, 'transactionLog')) > 0)
                $menu[] = array('label' => 'Transaction', 'url' => Yii::app()->createUrl('transactionLog/admin'));
            if (strlen(strstr($account->module, 'chargingHistory')) > 0)
                $menu[] = array('label' => 'Charging', 'url' => Yii::app()->createUrl('chargingHistory/admin'));
            if (strlen(strstr($account->module, 'callService')) > 0)
                $menu[] = array('label' => 'Outbound', 'url' => Yii::app()->createUrl('/callService/admin'));

            /*
	     if (strlen(strstr($account->module, 'winner'))>0)
               $menu[] = array('label' => 'Payment', 'url' => Yii::app()->createUrl('winner/paynow'));
            if (strlen(strstr($account->module, 'processor'))>0)
               $menu[] = array('label' => 'Processor', 'url' => Yii::app()->createUrl('processor/admin'));
		*/
        }

        return $menu;
    }

    public function getGroup3()
    {
        $account = $this->getAccount();
        $menu = array();
        if ($account) {
            if (strlen(strstr($account->module, 'user')) > 0)
                $menu[] = array('label' => 'Admin & Accounts', 'url' => Yii::app()->createUrl('/user/admin'));
        }
        return $menu;
    }

    public function authenticate($module)
    {
        $account = $this->getAccount();
        if ($account) {
            $accountModules = explode(',', $account->module);
            //return array_key_exists($moduleId, $accountModules);
            return in_array($module, $accountModules);
        }
        return FALSE;
    }

    public function getAccount()
    {
        if (!$this->account)
            $this->account = User::model()->findByAttributes(array('username' => $this->name, 'status' => 1));
        return $this->account;
    }

}

?>