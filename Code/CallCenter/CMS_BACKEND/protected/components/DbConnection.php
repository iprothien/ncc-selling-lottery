<?php

class DbConnection extends CDbConnection {

    protected function open() {
        try {
            parent::open();
        } catch (CDbException $e) {
            Yii::app()->request->redirect(Yii::app()->createUrl("site/error"));
        }
    }

}
