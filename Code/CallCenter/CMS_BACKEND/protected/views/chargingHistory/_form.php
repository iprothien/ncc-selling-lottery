<?php
/* @var $this ChargingHistoryController */
/* @var $model ChargingHistory */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'charging-history-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'msisdn'); ?>
		<?php echo $form->textField($model,'msisdn',array('size'=>15,'maxlength'=>15)); ?>
		<?php echo $form->error($model,'msisdn'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'time_charge'); ?>
		<?php echo $form->textField($model,'time_charge'); ?>
		<?php echo $form->error($model,'time_charge'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'money'); ?>
		<?php echo $form->textField($model,'money'); ?>
		<?php echo $form->error($model,'money'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'member_id'); ?>
		<?php echo $form->textField($model,'member_id'); ?>
		<?php echo $form->error($model,'member_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'digit'); ?>
		<?php echo $form->textField($model,'digit'); ?>
		<?php echo $form->error($model,'digit'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'charging_type'); ?>
		<?php echo $form->textField($model,'charging_type'); ?>
		<?php echo $form->error($model,'charging_type'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'trans_id'); ?>
		<?php echo $form->textField($model,'trans_id'); ?>
		<?php echo $form->error($model,'trans_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'trans_date'); ?>
		<?php echo $form->textField($model,'trans_date'); ?>
		<?php echo $form->error($model,'trans_date'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->