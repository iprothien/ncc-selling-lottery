<?php
/* @var $this ChargingHistoryController */
/* @var $model ChargingHistory */

$this->breadcrumbs=array(
	'Charging Histories'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ChargingHistory', 'url'=>array('index')),
	array('label'=>'Manage ChargingHistory', 'url'=>array('admin')),
);
?>

<h1>Create ChargingHistory</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>