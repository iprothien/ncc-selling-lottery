<?php
/* @var $this ChargingHistoryController */
/* @var $model ChargingHistory */

$this->breadcrumbs=array(
	'Charging Histories'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List ChargingHistory', 'url'=>array('index')),
	array('label'=>'Create ChargingHistory', 'url'=>array('create')),
	array('label'=>'Update ChargingHistory', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete ChargingHistory', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ChargingHistory', 'url'=>array('admin')),
);
?>

<h1>View ChargingHistory #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'msisdn',
		'time_charge',
		'money',
		'member_id',
		'digit',
		'charging_type',
		'trans_id',
		'trans_date',
	),
)); ?>
