<?php
/* @var $this ChargingHistoryController */
/* @var $model ChargingHistory */

$this->breadcrumbs=array(
	'Charging Histories'=>array('index'),
	'Manage',
);

/*$this->menu=array(
	array('label'=>'List ChargingHistory', 'url'=>array('index')),
	array('label'=>'Create ChargingHistory', 'url'=>array('create')),
);*/

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#charging-history-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Charging Histories</h1>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php if($option){ ?>
    <div style="width: 100%;">
        <?php if($option == 1){ ?>
            Telco: <span style="color: red;">Beeline</span>
        <?php }elseif($option == 2){ ?>
            Telco: <span style="color: red;">ETL</span>
        <?php }else{ ?>
            Telco: <span style="color: red;">Unitel</span>
        <?php } ?>
    </div>
    <div style="width: 100%;margin: 5px 0px;background: skyblue!important;">
        <p style="float: left;width: 30%;margin: 10px 0px;">Total Amount: <span style="color: red;font-size: 15px;font-weight: bold;"><?php if($model->getSumAmount()){echo $model->getSumAmount(); }else{ echo 0; } ?></span> </p>
    </div>
<?php } ?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'charging-history-grid',
	'dataProvider'=>$model->search(),
	//'filter'=>$model,
	'columns'=>array(
		'id',
		'msisdn',
        'money',
        'charging_type',
        //array('name'=>'charging_type','value'=>'$data->getChargingType()'),
        'trans_id',
        'time_charge',
		/*
		'trans_date',
		*/
		/*array(
			'class'=>'CButtonColumn',
		),*/
	),
)); ?>
