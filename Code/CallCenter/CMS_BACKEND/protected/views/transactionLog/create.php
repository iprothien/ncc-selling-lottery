<?php
/* @var $this TransactionLogController */
/* @var $model TransactionLog */

$this->breadcrumbs=array(
	'Transaction Logs'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List TransactionLog', 'url'=>array('index')),
	array('label'=>'Manage TransactionLog', 'url'=>array('admin')),
);
?>

<h1>Create TransactionLog</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>