<?php
/* @var $this TransactionLogController */
/* @var $model TransactionLog */
/* @var $form CActiveForm */
?>

<div class="wide form">
<?php
    Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
    $form=$this->beginWidget('CActiveForm', array(
	    'action'=>Yii::app()->createUrl($this->route),
	    'method'=>'get',
    ));
?>
	<div class="row" style="width:48%;float:left">
		<?php echo $form->label($model,'msisdn'); ?>
		<?php echo $form->textField($model,'msisdn',array('size'=>20,'maxlength'=>20)); ?>
	</div>
	<div class="row" style="width:48%;float:left">
		<?php echo $form->label($model,'start'); ?>
        <?php
            $this->widget('CJuiDateTimePicker',array(
                'model'=>$model, 
                'attribute'=>'start', 
                'mode'=>'datetime',
                'options'=>array("dateFormat"=>'yy-mm-dd'),
                'language' => ''
            ));
        ?>
	</div>
       
        <div class="row" style="width:48%;float:left">
		<?php echo $form->label($model,'buy_type'); ?>
		<?php echo $form->textField($model,'buy_type'); ?>
	</div>
        
         <div class="row" style="width:48%;float:left">
            <?php echo $form->label($model,'end'); ?>
            <?php
                $this->widget('CJuiDateTimePicker',array(
                    'model'=>$model, 
                    'attribute'=>'end', 
                    'mode'=>'datetime', 
                    'options'=>array("dateFormat"=>'yy-mm-dd'),
                    'language' => ''
                ));
            ?>
        </div>
      <div class="row" style="width:48%;float:left">
                <?php echo $form->label($model,'option'); ?>
		<?php echo $form->dropDownlist($model,'option',array(''=>'Select',1=>'Beeline',2=>'ETL',3=>'Unitel')); ?>
	</div> 
        <div class="row" style="width:48%;float:left" >
		<?php echo $form->label($model,'status'); ?>
		<?php echo $form->dropDownlist($model,'status',array(''=>'Select',1=>'Buy successfully',2=>'Close for draw',3=>'Postpaid',4=>'Not enought money',5=>'Sold out',6=>'Cant charge',7=>'SMS lottery fail')); ?>
	</div>
	<div class="row buttons" style="width:48%;float:left">
		<?php echo CHtml::submitButton('Search'); ?>
                <?php echo CHtml::button('Export', array('url' => Yii::app()->createUrl(''), 'id' => 'btn-export')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->