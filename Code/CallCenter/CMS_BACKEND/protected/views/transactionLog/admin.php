<?php
/* @var $this TransactionLogController */
/* @var $model TransactionLog */

$this->breadcrumbs=array(
	'Transaction Logs'=>array('index'),
	'Manage',
);


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#transaction-log-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Transaction Log</h1>
<div class="search-form">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->
<?php if($option){ ?>

    <div style="width: 100%;float: left;margin-left: 20px;margin-bottom: 10px;">
        <?php if($option == 1){ ?>
            <b>Telco   :</b><span style="color: red;">  Beeline</span>
        <?php }elseif($option == 2){ ?>
            <b>Telco   :</b><span style="color: red;">  ETL</span>
        <?php }else{ ?>
            <b>Telco   :</b><span style="color: red;">  Unitel</span>
        <?php } ?>
    </div>
<br/><br/><br/><br/><br/><br/>
<div class="statistic_content" id="statistic_content">
   <div style="width: 300px;margin: 0px 10px">
	<fieldset id="address">
        <table>
            <tr>
            <label>Total Transaction</label>: <span><?php echo $model->getTotalRecord(); ?></span>
            </tr>
            <tr>
                <td><label> Buy successfully</label>: <span><?php echo $model->getTotalRecord1(); ?></td>
                <td><label>Sold out</label>: <span><?php echo $model->getTotalRecord5(); ?></span></td>
            </tr
            <tr>
                <td><label>Close for draw</label>: <span><?php echo $model->getTotalRecord2(); ?></span></td>
                <td><label>Cant charge</label>: <span><?php echo $model->getTotalRecord6(); ?></span></td>
            </tr>
            <tr>
                <td><label>Postpaid</label>: <span><?php echo $model->getTotalRecord3(); ?></span></td>
                <td><label>SMS lottery fail</label>: <span><?php echo $model->getTotalRecord7(); ?></span></td>
            </tr>
            <tr>
                <td><label>Not enought money</label>: <span><?php echo $model->getTotalRecord4(); ?></span></td>
            </tr>
        </table>
	 </fieldset>
    </div>
</div>
<?php } ?>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'transaction-log-grid',
	'dataProvider'=>$model->search(),
	'columns'=>array(
                'id',
                'msisdn',
                 //array('name'=>'buy_type','value'=>'$data->getBuyType()'),
		'buy_type',
		'status',
		'money',
		'code',
		'balance_before',
		'balance_after',
		 //array('name'=>'status','value'=>'$data->getStatus()'),
		'content',
		'created_datetime',
			),
)); ?> 