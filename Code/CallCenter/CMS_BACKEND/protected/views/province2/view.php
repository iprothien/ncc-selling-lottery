<?php
/* @var $this Province2Controller */
/* @var $model Province2 */

$this->breadcrumbs=array(
	'Province2s'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'Manage Province', 'url'=>array('admin')),
);
?>

<h1>View Province2 #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
        array(
            "name" => "type",
            "type" => "raw",
            "value" => $model->getTypeName()
        ),
		'name',
		'parent_id',
        array(
            "name" => "status",
            "type" => "raw",
            "value" => $model->getStatusName()
        ),
		'created',
	),
)); ?>
