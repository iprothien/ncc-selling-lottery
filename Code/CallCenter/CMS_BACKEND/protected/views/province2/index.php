<?php
/* @var $this Province2Controller */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Province2s',
);

$this->menu=array(
	array('label'=>'Manage Province', 'url'=>array('admin')),
);
?>

<h1>Province2s</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
