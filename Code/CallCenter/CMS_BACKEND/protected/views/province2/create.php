<?php
/* @var $this Province2Controller */
/* @var $model Province2 */

$this->breadcrumbs=array(
	'Province2s'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Manage Province', 'url'=>array('admin')),
);
?>

<h1>Create Province2</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>