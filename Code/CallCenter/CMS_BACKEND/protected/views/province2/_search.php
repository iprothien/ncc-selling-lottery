<?php
/* @var $this Province2Controller */
/* @var $model Province2 */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>255)); ?>
	</div>

    <div class="row">
        <?php echo $form->labelEx($model, 'province_id'); ?>
        <?php echo $form->dropDownList($model, 'province_id', $model->getProvinceList(1, 0), array("prompt" => "-- Select --")); ?>
        <?php echo $form->error($model, 'province_id'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'district_id'); ?>
        <?php echo $form->dropDownList($model, 'district_id', $model->getProvinceList(2, $model->province_id), array("prompt" => "-- Select --")); ?>
        <?php echo $form->error($model, 'district_id'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'ward_id'); ?>
        <?php echo $form->dropDownList($model, 'ward_id', $model->getProvinceList(3, $model->district_id), array("prompt" => "-- Select --")); ?>
        <?php echo $form->error($model, 'ward_id'); ?>
    </div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->

<script type="text/javascript">
    (function ($, document, undefined) {

        var province = function () {
        };

        province.prototype = {
            dom: {
                type: undefined,
                province: undefined,
                district: undefined,
                ward: undefined
            },
            init: function () {
                var that = this;
                that.dom.type = $("#Province2_type").first();
                that.dom.province = $("#Province2_province_id").first();
                that.dom.district = $("#Province2_district_id").first();
                that.dom.ward = $("#Province2_ward_id").first();

                that.dom.type.on("change", function (e) {
                    var type = $(this).val();

                    if (type > 1) {
                        that.dom.province.parent("div").show();
                    } else {
                        that.dom.province.parent("div").hide();
                    }

                    if (type > 2) {
                        that.dom.district.parent("div").show();
                    } else {
                        that.dom.district.parent("div").hide();
                    }

                    if (type > 3) {
                        that.dom.ward.parent("div").show();
                    } else {
                        that.dom.ward.parent("div").hide();
                    }

                });

                that.dom.province.on("change", function (e) {
                    var parentId = $(this).val();
                    that.dom.district.empty();
                    that.dom.ward.empty();
                    that.loadData(that.dom.district, parentId, 2);
                });

                that.dom.district.on("change", function (e) {
                    var parentId = $(this).val();
                    that.dom.ward.empty();
                    that.loadData(that.dom.ward, parentId, 3);
                });

                that.dom.ward.on("change", function (e) {
                });

            },
            loadData: function (dropList, parentId, type) {
                if (parentId != 0) {
                    $.ajax({
                        url: "<?php echo $this->createUrl("getProvince"); ?>",
                        type: "GET",
                        dataType: "json",
                        cache: false,
                        async: true,
                        data: {
                            parentId: parentId,
                            type: type
                        },
                        success: function (response) {
                            dropList.append(new Option("-- Select --", 0));
                            $.each(response, function (key, item) {
                                dropList.append(new Option(item, key));
                            });
                        }
                    });
                }
            }
        };

        $.province = new province();

        $(document).ready(function () {
            $.province.init();
        });
    })(jQuery, document);
</script>