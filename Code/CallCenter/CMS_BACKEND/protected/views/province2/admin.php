<?php
/* @var $this Province2Controller */
/* @var $model Province2 */

$this->breadcrumbs = array(
    'Province' => array('index'),
    'Manage',
);

$this->menu = array(
    array('label' => 'Create Province', 'url' => array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#province2-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Province</h1>

<div class="search-form">
    <?php $this->renderPartial('_search', array(
        'model' => $model,
    )); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'province2-grid',
    'dataProvider' => $model->search(),
    'columns' => array(
        'id',
        array(
            "name" => "type",
            "type" => "raw",
            "value" => "\$data->getTypeName()"
        ),
        'name',
        array(
            "name" => "status",
            "type" => "raw",
            "value" => "\$data->getStatusName()"
        ),
        'created',
        array(
            'class' => 'CButtonColumn',
        ),
    ),
)); ?>
