<?php
/* @var $this Province2Controller */
/* @var $model Province2 */

$this->breadcrumbs=array(
	'Province2s'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'Manage Province', 'url'=>array('admin')),
);
?>

<h1>Update Province2 <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>