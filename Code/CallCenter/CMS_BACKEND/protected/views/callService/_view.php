<?php
/* @var $this CallServiceController */
/* @var $data CallService */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('type')); ?>:</b>
	<?php echo CHtml::encode($data->type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('draw_no')); ?>:</b>
	<?php echo CHtml::encode($data->draw_no); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('time_call')); ?>:</b>
	<?php echo CHtml::encode($data->time_call); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('file_path')); ?>:</b>
	<?php echo CHtml::encode($data->file_path); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('created_datetime')); ?>:</b>
	<?php echo CHtml::encode($data->created_datetime); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('call_type')); ?>:</b>
	<?php echo CHtml::encode($data->call_type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('media_path')); ?>:</b>
	<?php echo CHtml::encode($data->media_path); ?>
	<br />

	*/ ?>

</div>