<?php
/* @var $this CallServiceController */
/* @var $model CallService */

$this->breadcrumbs=array(
	'Call Services'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Create CallService', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#call-service-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Call Services</h1>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none;">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'call-service-grid',
	'dataProvider'=>$model->search(),
	'columns'=>array(
		'id',
		'name',
		'sType',
		'draw_no',
		'sStatus',
		'file_path',
		'sCallType',
		'media_path',
        'time_call',
        array(
            'buttons' => array(
                'update' =>
                    array(
                        'label' => 'Edit',
                        'imageUrl' => FALSE,
                        'url' => 'Yii::app()->createUrl("callService/update", array("id"=>$data->id))',
                        'visible'=>'$data->status!=1',
                    ),
            ),
            'template' => '{update}',
            'class' => 'CButtonColumn',
            'htmlOptions' => array('style' => 'width:20px;text-align: center;')
        ),
		/*array(
			'class'=>'CButtonColumn',
		),*/
	),
)); ?>
