<?php
/* @var $this CallServiceController */
/* @var $model CallService */

$this->breadcrumbs=array(
	'Call Services'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'Manage CallService', 'url'=>array('admin')),
);
?>

<h1>View CallService #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'type',
		'draw_no',
		'time_call',
		'status',
		'file_path',
		'created_datetime',
		'call_type',
		'media_path',
	),
)); ?>
