<?php
/* @var $this CallServiceController */
/* @var $model CallService */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php
        Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'call-service-form',
            'enableAjaxValidation' => false,
            'htmlOptions' => array('enctype' => 'multipart/form-data'),
        ));
    ?>
    <?php  ?>
    <?php if($model->status == 1){ ?>
        <p style="margin: 5px 0px;width: 100%;color: red;font-size: 18px;">Cancel not Edit record</p>
    <?php }else{ ?>
        <p class="note">Fields with <span class="required">*</span> are required.</p>

        <?php echo $form->errorSummary($model); ?>

        <?php if (!$model->isNewRecord): ?>
            <div class="row" style="margin-top: 5px;float: left;width: 100%;">
                <div style="width: 17%;float: left;"><?php echo $form->labelEx($model, 'status'); ?></div>
                <div style="width: 70%;float: left;"><?php echo $form->dropDownlist($model, 'status', array(0 => 'Pending', 2 => 'Cancel')); ?></div>
            </div>
        <?php endif; ?>

        <div class="row" style="margin-top: 5px;float: left;width: 100%;">
            <div style="width: 17%;float: left;"><?php echo $form->labelEx($model, 'name'); ?></div>
            <div style="width: 70%;float: left;"><?php echo $form->textField($model, 'name', array('size' => 60, 'maxlength' => 256)); ?></div>
        </div>

        <?php if ($model->isNewRecord): ?>
            <div class="row" style="margin-top: 5px;float: left;width: 100%;">
                <div style="width: 17%;float: left;"><div style="width: 170px;float: left;"><?php echo $form->labelEx($model, 'type'); ?></div></div>
                <div style="width: 70%;float: left;"><div style="width: 700px;float: left;"><?php echo $form->dropDownlist($model, 'type', array('' => 'Selected', 1 => 'Draw No', 2 => 'File upload'), array('onchange' => 'checkSelect("CallService_type","CallService_call_type")')); ?></div></div>
            </div>

            <div class="row" style="width: 100%;float: left;margin-top: 10px;">
                <div style="width: 17%;float: left;"><?php echo $form->labelEx($model, 'call_type'); ?></div>
                <div style="width: 70%;float: left;"><?php echo $form->dropDownlist($model, 'call_type', array(1 => 'Inform results', 2 => 'Promotion', 3 => 'Inform winner'),array('onchange' => 'checkSelect("CallService_type","CallService_call_type")')); ?></div>
            </div>

            <div class="row" id="draw_no" style="display: none;width: 100%;float: left;margin-top: 5px;">
                <div style="width: 17%;float: left;"><?php echo $form->labelEx($model, 'draw_no'); ?></div>
                <div style="width: 70%;float: left;"><?php echo $form->dropDownlist($model, 'draw_no', $model->getDrawNo()); ?></div>
            </div>

            <div class="row" id="file_path" style="display: none;width: 100%;float: left;margin-top: 5px;">
                <div style="width: 17%;float: left;"><?php echo $form->labelEx($model, 'file_path'); ?></div>
                <div style="width: 70%;float: left;"><?php echo $form->fileField($model, 'file_path', array('size' => 60, 'maxlength' => 255)); ?></div>
            </div>

            <div class="row" id="media_path" style="display: none;width: 100%;float: left;margin-top: 5px;">
                <div style="width: 17%;float: left;"><?php echo $form->labelEx($model, 'media_path'); ?></div>
                <div style="width: 70%;float: left;"><?php echo $form->fileField($model, 'media_path', array('size' => 60, 'maxlength' => 255)); ?></div>
            </div>
        <?php endif; ?>

        <div class="row" style="width: 100%;float: left;margin-top: 5px;">
            <div style="width: 17%;float: left;"><?php echo $form->labelEx($model, 'time_call'); ?></div>
            <div style="width: 70%;float: left;">
                <?php
                $this->widget('CJuiDateTimePicker', array(
                    'model' => $model, //Model object
                    'attribute' => 'time_call', //attribute name
                    'mode' => 'datetime', //use "time","date" or "datetime" (default)
                    'options' => array("dateFormat" => 'yy/mm/dd'), // jquery plugin options
                    'language' => ''
                ));
                ?>
            </div>
        </div>

        <div class="row buttons" style="width: 100%;float: left;padding-left: 17%">
            <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
        </div>
    <?php } ?>
    <?php $this->endWidget(); ?>
</div><!-- form -->

<script type="text/javascript">
    function checkSelect(type,call_type){
        var idType = document.getElementById(type);
        var idCallType = document.getElementById(call_type);
        if(idType.value == 1 && idCallType.value == 1){
            document.getElementById('draw_no').style.display = 'block';
            document.getElementById('file_path').style.display = 'none';
            document.getElementById('media_path').style.display = 'none';
        }else if(idType.value == 1 && idCallType.value == 3){
            document.getElementById('draw_no').style.display = 'block';
            document.getElementById('file_path').style.display = 'none';
            document.getElementById('media_path').style.display = 'none';
        }else if(idType.value == 2 && idCallType.value == 3){
            document.getElementById('draw_no').style.display = 'block';
            document.getElementById('file_path').style.display = 'block';
            document.getElementById('media_path').style.display = 'none';
        } else if(idType.value == 1 && idCallType.value == 2){
            document.getElementById('draw_no').style.display = 'block';
            document.getElementById('file_path').style.display = 'none';
            document.getElementById('media_path').style.display = 'block';
        }else if(idType.value == 2 && idCallType.value == 1){
            document.getElementById('draw_no').style.display = 'block';
            document.getElementById('file_path').style.display = 'block';
            document.getElementById('media_path').style.display = 'none';
        }else if(idType.value == 2 && idCallType.value == 2){
            document.getElementById('draw_no').style.display = 'none';
            document.getElementById('file_path').style.display = 'block';
            document.getElementById('media_path').style.display = 'block';
        }else if(idType.value == ''){
            document.getElementById('draw_no').style.display = 'none';
            document.getElementById('media_path').style.display = 'none';
            document.getElementById('file_path').style.display = 'none';
        }
    }
</script>