<?php
/* @var $this CallServiceController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Call Services',
);

$this->menu=array(
	array('label'=>'Create CallService', 'url'=>array('create')),
	array('label'=>'Manage CallService', 'url'=>array('admin')),
);
?>

<h1>Call Services</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
