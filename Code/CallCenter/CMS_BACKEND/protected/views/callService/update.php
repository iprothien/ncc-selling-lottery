<?php
/* @var $this CallServiceController */
/* @var $model CallService */

$this->breadcrumbs=array(
	'Call Services'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'Manage CallService', 'url'=>array('admin')),
);
?>

<h1>Update CallService <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>