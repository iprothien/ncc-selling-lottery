<?php
/* @var $this CallServiceController */
/* @var $model CallService */

$this->breadcrumbs=array(
	'Call Services'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Manage CallService', 'url'=>array('admin')),
);
?>

<h1>Create CallService</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>