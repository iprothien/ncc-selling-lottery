<?php
/* @var $this AccountController */
/* @var $model Account */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'account-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

    <?php if($model->isNewRecord){?>
        <div class="row">
            <?php echo $form->labelEx($model,'user'); ?>
            <?php echo $form->textField($model,'user',array('size'=>25,'maxlength'=>80)); ?>
            <?php echo $form->error($model,'user'); ?>
        </div>
    <?php }else{ ?>
        <div class="row">
            <?php echo $form->labelEx($model,'user1'); ?>
            <span style="font-weight: bold;line-height: 2.2em;padding-left: 10px;"><?php echo $model->admin_user; ?></span>
        </div>
    <?php } ?><div class="clear">

    <?php if($model->isNewRecord){?>
        <div class="row">
            <?php echo $form->labelEx($model,'password'); ?>
            <?php echo $form->textField($model,'password',array('size'=>25,'maxlength'=>80)); ?>
            <?php echo $form->error($model,'password'); ?>
        </div>
    <?php }else{ ?>
        <div class="row">
            <?php echo $form->labelEx($model,'re_password'); ?>
            <?php echo $form->textField($model,'re_password',array('size'=>25,'maxlength'=>80,'placeholder'=>'Edit password')); ?>
            <?php echo $form->error($model,'re_password'); ?>
        </div>
    <?php } ?>

    <?php if($model->isNewRecord){?>
        <div class="row">
            <?php echo $form->labelEx($model,'username'); ?>
            <?php echo $form->textField($model,'username',array('size'=>25,'maxlength'=>80)); ?>
            <?php echo $form->error($model,'username'); ?>
        </div>
    <?php }else{ ?>
        <div class="row">
            <?php echo $form->labelEx($model,'oldUsername'); ?>
            <?php echo $form->textField($model,'oldUsername',array('size'=>25,'maxlength'=>80,'placeholder'=>$model->username)); ?>
            <?php echo $form->error($model,'oldUsername'); ?>
        </div>
    <?php } ?>

	<div class="row">
		<?php echo $form->labelEx($model,'secret'); ?>
		<?php echo $form->textField($model,'secret',array('size'=>25,'maxlength'=>80)); ?>
		<?php echo $form->error($model,'secret'); ?>
	</div>

    <div class="row">
        <?php echo $form->labelEx($model,'module'); ?>
        <?php echo $form->checkBoxList($model, 'moduleSelected', $model->getListModule(), array('template' => '<li>{input}{label}</li>', 'container' => 'ul class="module-list"', 'separator' => '')); ?>
        <?php echo $form->error($model,'module'); ?>
    </div><div class="clear">

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->