<?php
/* @var $this MemberDrawController */
/* @var $model MemberDraw */

$this->breadcrumbs=array(
	'Member Draws'=>array('admin'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Create MemberDraw', 'url'=>array('create')),
);

$provider = $model->search();
?>

<h1>Manage Member Draws</h1>

<div class="search-form">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->
<?php if(isset($option) && $option): ?>
<p>Total Money: <?php echo $model->totalmoney; ?></p>
<?php endif; ?>

<?php
if(isset($option) && $option):
    $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'member-draw-grid',
	'dataProvider'=>$provider,
        'ajaxUpdate'=>FALSE,
	'columns'=>array(
		'id',
		'msisdn',
		'code',
		'money',
                array('name'=>'draw_no','value'=>'$data->draw_no'),
		'created_datetime',
                array('name'=>'type','value'=>'$data->getType()'),
                array('name'=>'win_status','value'=>'$data->getWinStatus()'),
                array('name'=>'buy_type','value'=>'$data->buy_type==1?"By Agent":"By IVR"'),
                'agent_id'
		/*
		'status',
		*/
	),
)); endif; ?>
