<?php
/* @var $this DrawResultController */
/* @var $model DrawResult */
/* @var $form CActiveForm */
?>

<div class="form">

<?php
    Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
    $form=$this->beginWidget('CActiveForm', array(
	'id'=>'draw-result-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>
        
        <div class="row">
		<?php echo $form->labelEx($model,'draw_no'); ?>
		<?php echo $form->dropDownlist($model,'draw_no',$model->getDrawNo()); ?>
		<?php echo $form->error($model,'draw_no'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'code'); ?>
		<?php echo $form->textArea($model,'code',array('cols'=>40, 'rows'=>8)); ?>
		<?php echo $form->error($model,'code'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'created_datetime'); ?>
                <?php 
                        $this->widget('CJuiDateTimePicker',array(
                        'model'=>$model, //Model object
                        'attribute'=>'created_datetime', //attribute name
                        'mode'=>'datetime', //use "time","date" or "datetime" (default)
                        'options'=>array("dateFormat"=>'yy/mm/dd'), // jquery plugin options
                        'language' => ''
                )); ?>
		<?php echo $form->error($model,'created_datetime'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->