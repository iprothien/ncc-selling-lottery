<?php
/* @var $this DrawResultController */
/* @var $model DrawResult */

$this->breadcrumbs=array(
	'Draw Results'=>array('admin'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'Create DrawResult', 'url'=>array('create')),
	array('label'=>'View DrawResult', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage DrawResult', 'url'=>array('admin')),
);
?>

<h1>Update DrawResult <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>