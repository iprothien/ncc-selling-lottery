<?php
/* @var $this DrawResultController */
/* @var $model DrawResult */

$this->breadcrumbs=array(
	'Draw Results'=>array('admin'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Create DrawResult', 'url'=>array('create')),
	array('label'=>'Update DrawResult', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete DrawResult', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage DrawResult', 'url'=>array('admin')),
);
?>

<h1>View DrawResult #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'code',
		'created_datetime',
	),
)); ?>
