<?php
/* @var $this WinnerController */
/* @var $model Winner */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php
    $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'msisdn',array('style'=>'width:110px;')); ?>
		<?php echo $form->textField($model,'msisdn',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>

	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->