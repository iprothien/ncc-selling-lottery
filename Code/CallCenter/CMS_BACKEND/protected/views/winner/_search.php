<?php
/* @var $this WinnerController */
/* @var $model Winner */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php
    Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
    $form = $this->beginWidget('CActiveForm', array(
        'action' => Yii::app()->createUrl($this->route),
        'enableAjaxValidation' => FALSE,
        'method' => 'get',
    )); ?>

    <div class="row" style="width:48%;float:left">
        <?php echo $form->label($model, 'msisdn', array('style' => 'width:110px;')); ?>
        <?php echo $form->textField($model, 'msisdn', array('size' => 20, 'maxlength' => 20)); ?>
    </div>

    <div class="row" style="width:50%;float:left;">
        <label style='width:110px;'>Telco</label>
        <?php echo $form->dropDownlist($model, 'option', array('' => 'Select', 1 => 'Beeline', 2 => 'ETL', 3 => 'Unitel')); ?>
    </div>

    <div class="row" style="width:48%;float:left">
        <?php echo $form->label($model, 'time_start', array('style' => 'width:105px;')); ?>
        <?php
        $this->widget('CJuiDateTimePicker', array(
            'model' => $model, //Model object
            'attribute' => 'time_start', //attribute name
            'mode' => 'datetime', //use "time","date" or "datetime" (default)
            'options' => array("dateFormat" => 'yy/mm/dd'), // jquery plugin options
            'language' => ''
        )); ?>
    </div>

    <div class="row" style="width:50%;float:left;">
        <?php echo $form->label($model, 'time_end', array('style' => 'width:110px;')); ?>
        <?php
        $this->widget('CJuiDateTimePicker', array(
            'model' => $model, //Model object
            'attribute' => 'time_end', //attribute name
            'mode' => 'datetime', //use "time","date" or "datetime" (default)
            'options' => array("dateFormat" => 'yy/mm/dd'), // jquery plugin options
            'language' => ''
        )); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'draw_no'); ?>
        <?php echo $form->dropDownlist($model,'draw_no',Statistic::model()->getDrawNo(),array('empty'=>'Select')); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton('Search'); ?>
        <?php echo CHtml::button('Export', array('url' => Yii::app()->createUrl(''), 'id' => 'btn-export')); ?>

    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->