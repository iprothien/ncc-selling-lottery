<?php
/* @var $this WinnerController */
/* @var $model Winner */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'winner-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>
        
        <div class="row">
		<?php echo $form->labelEx($model,'msisdn'); ?>
		<?php echo $form->textField($model,'msisdn',array('disabled'=>TRUE,'style'=>'color:red;')); ?>
		<?php echo $form->error($model,'msisdn'); ?>
	</div>
        
        <div class="row">
		<?php echo $form->labelEx($model,'full_name'); ?>
		<?php echo $form->textField($model,'full_name',array('style'=>'color:red;')); ?>
		<?php echo $form->error($model,'full_name'); ?>
	</div>
        
        <div class="row">
		<?php echo $form->labelEx($model,'description'); ?>
		<?php echo $form->textArea($model,'description',array('cols'=>30,'rows'=>5)); ?>
		<?php echo $form->error($model,'description'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'total_money'); ?>
		<?php echo $form->textField($model,'total_money',array('disabled'=>TRUE,'style'=>'color:red;')); ?>
		<?php echo $form->error($model,'total_money'); ?>
	</div>
        
        <div class="row">
		<?php echo $form->labelEx($model,'payment_status'); ?>
		<?php echo $form->dropDownlist($model,'payment_status',array(''=>'Select...',0=>'No',1=>'Yes')); ?>
		<?php echo $form->error($model,'payment_status'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Pay'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->