<?php
/* @var $this WinnerController */
/* @var $model Winner */

$this->breadcrumbs=array(
	'Winners'=>array('admin'),
);

$this->menu=array(
	array('label'=>'Manage Winner', 'url'=>array('admin')),
);
?>

<h1>Update Winner <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>