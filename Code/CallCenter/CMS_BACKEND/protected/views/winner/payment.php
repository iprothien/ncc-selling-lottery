<?php
/* @var $this WinnerController */
/* @var $model Winner */

$this->breadcrumbs = array(
    'Payment Winners',
);
?>

<h1>Payment Winners</h1>

<div class="search-form">
    <?php
    $this->renderPartial('_searchPay', array(
        'model' => $model,
    ));
    ?>
</div><!-- search-form -->

<?php
$provider = $model->search();

$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'winner-grid',
    'dataProvider' => $provider,
    'ajaxUpdate' => FALSE,
    'columns' => array(
        'id',
        'msisdn',
        array('name' => 'drawNo', 'value' => '$data->drawNo'),
        array('name' => 'timeEnd', 'value' => '$data->timeEnd'),
        array('name' => 'code', 'value' => '$data->code'),
        array('name' => 'money', 'value' => '$data->money'),
        array('name' => 'payment_status', 'value' => '$data->getPayment()'),
        'total_money',
        array(
            'class' => 'CButtonColumn',
            'buttons' => array(
                'update' =>
                array(
                    'label' => 'Pay',
                    'imageUrl' => FALSE,
                    'url' => 'Yii::app()->createUrl("winner/update", array("id"=>$data->id,"msisdn"=>$data->msisdn))',
                    'visible' => '$data->payment_status == 0'
                ),
            ),
            'template' => '{update}'
        ),
    ),
));
unset($provider);
?>
