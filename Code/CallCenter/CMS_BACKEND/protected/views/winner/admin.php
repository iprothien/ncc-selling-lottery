<?php
/* @var $this WinnerController */
/* @var $model Winner */

$this->breadcrumbs = array(
    'Winners' => array('admin'),
    'Manage',
);
?>

<h1>Manage Winners</h1>

<div class="search-form">
    <?php
    $this->renderPartial('_search', array(
        'model' => $model,
    ));
    ?>
</div><!-- search-form -->

<?php
$provider = $model->search();

if(isset($option) && $option):
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'winner-grid',
    'dataProvider' => $provider,
    'ajaxUpdate' => FALSE,
    'columns' => array(
        'id',
        'msisdn',
        array('name' => 'drawNo', 'value' => '$data->drawNo'),
        array('name' => 'code', 'value' => '$data->code'),
        array('name' => 'money', 'value' => '$data->money'),
        'total_money',
        array('name' => 'timeStart', 'value' => '$data->timeStart'),
        array('name' => 'timeEnd', 'value' => '$data->timeEnd'),
    ),
));
endif;
?>
