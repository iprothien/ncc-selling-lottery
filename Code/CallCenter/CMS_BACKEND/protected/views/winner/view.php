<?php
/* @var $this WinnerController */
/* @var $model Winner */

$this->breadcrumbs = array(
    'Winners' => array('admin'),
);

$this->menu = array(
    array('label' => 'Manage Winner', 'url' => array('admin')),
);
?>

<h1>View Winner #<?php echo $model->id; ?></h1>

<?php
$this->widget('zii.widgets.CDetailView', array(
    'data' => $model,
    'attributes' => array(
        'id',
        'msisdn',
        'buyer_id',
        'transaction_id',
        array('name' => 'inform_status', 'value' => $model->getStatus()),
        array('name' => 'payment_status', 'value' => $model->getPayment()),
        'total_money',
    ),
));
?>
