<?php
/* @var $this WinnerController */
/* @var $model Winner */

$this->breadcrumbs=array(
	'Winners'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Winner', 'url'=>array('index')),
	array('label'=>'Manage Winner', 'url'=>array('admin')),
);
?>

<h1>Create Winner</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>