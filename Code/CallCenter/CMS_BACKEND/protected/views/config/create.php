<?php
/* @var $this ConfigController */
/* @var $model Config */

$this->breadcrumbs=array(
	'Configs'=>array('admin'),
	'Create',
);

$this->menu=array(
	array('label'=>'Manage Config', 'url'=>array('admin')),
);
?>

<h4>Create Config</h4>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>