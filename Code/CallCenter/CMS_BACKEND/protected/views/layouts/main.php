<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="language" content="en" />
        <!-- blueprint CSS framework -->
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/style.css" media="screen, projection" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/switch.css" media="print" />
        <!--[if lt IE 8]>
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie-sucks.css" media="screen, projection" />
        <![endif]-->

        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/theme.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/custom.css" />
        <title>Administrator</title>

	 <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-ui-1.10.0.custom.min.js"></script>
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/jquery-ui-1.10.0.custom.min.css" />
    </head>
    <body>
        <div class="container" id="page">
            <div id="header">
                <?php if(Yii::app()->user->id) { ?>
                <span style="float:right; display: block; padding:20px;"> 
                    Welcome <b><?php echo Yii::app()->user->name ?></b> 
                    <a style="color:#000;" href="<?php echo Yii::app()->createUrl('/myaccount/changepassword'); ?>">Change Password</a> | 
                    <a style="color:#000;" href="<?php echo Yii::app()->createUrl('/site/logout'); ?>">Logout</a>
                </span>
                <?php } ?>
                <h2><?php //echo CHtml::encode(Yii::app()->name); ?></h2>
                <?php
                    $menu1 = Yii::app()->user->getGroup1();
                    $menu2 = Yii::app()->user->getGroup2();
                    $menu3 = Yii::app()->user->getGroup3();
                ?>
                <div id="topmenu">
                    <?php if(Yii::app()->user->id) { ?>
                    <ul>
                        <li class="group1" style="float: left;">
                            <ul style="padding: 0 5px;background: #f8f8f8;">
                                <span style="font-weight: bold;color: #d2d2d2;display: block; font-size: 14px;">Call Center</span>
                                <?php foreach ($menu1 as $item) { ?>
                                <li class="itemgroup1">
                                    <a href="<?php echo $item['url']; ?>"><?php echo $item['label']; ?></a>
                                </li>
                                <?php } ?>
                            </ul>
                        </li>
                        <li class="group2" style="float: left; margin-left: 5px;">
                            <ul style="width:auto; padding: 0 5px;background: #f8f8f8;">
                                <span style="font-weight: bold;color: #d2d2d2;display: block; font-size: 14px;">Selling Lotto</span>
                                <?php foreach ($menu2 as $item2) { ?>
                                <li class="itemgroup2">
                                    <a href="<?php echo $item2['url']; ?>"><?php echo $item2['label']; ?></a>
                                </li>
                                <?php } ?>
                            </ul>
                        </li>
                        <li class="group3" style="float: right; margin-left: 5px;">
                            <ul style="padding: 0 10px;background: #f8f8f8;">
                                <span style="font-weight: bold;color: #d2d2d2;display: block; font-size: 14px;">Admin zone</span>
                                <?php foreach ($menu3 as $item3) { ?>
                                <li class="itemgroup3">
                                    <a href="<?php echo $item3['url']; ?>"><?php echo $item3['label']; ?></a>
                                </li>
                                <?php } ?>
                            </ul>
                        </li>
                    </ul>
                    <?php } else { ?>
                    <ul>
                        <li class="group1" style="float: left;">
                            <ul style="width:80px; padding: 0 10px;background: #f8f8f8;">
                                <span style="font-weight: bold;color: #d2d2d2;display: block; font-size: 14px;">Login zone</span>
                                <li class="itemgroup1">
                                    <a href="<?php echo Yii::app()->createUrl('/site/login'); ?>">Login</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    <?php } ?>
                </div>
                <div class="clear"></div>
                <div style="background: #A20000; height:5px; width:100%;"></div>
            </div>
            <?php if (isset($this->breadcrumbs)): ?>
                <div id="top-panel">
                    <?php
                    $this->widget('zii.widgets.CBreadcrumbs', array(
                        'links' => $this->breadcrumbs,
                    ));
                    ?>
                </div>
            <?php endif ?>

            <?php echo $content; ?>

            <div id="footer" style="padding-left: 20px;">
                Copyright &copy; <?php echo date('Y'); ?> by iProtech. All Rights Reserved.<br/>
            </div>
        </div>
    </body>
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.form.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/custom.js"></script>
</html>
