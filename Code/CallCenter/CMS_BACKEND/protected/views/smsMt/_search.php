<?php
/* @var $this SmsMtController */
/* @var $model SmsMt */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php
    Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
    $form=$this->beginWidget('CActiveForm', array(
        'action'=>Yii::app()->createUrl($this->route),
        'method'=>'get',
    ));
?>
	<div class="row" style="width:30%;float:left">
		<?php echo $form->label($model,'msisdn'); ?>
		<?php echo $form->textField($model,'msisdn',array('size'=>16,'maxlength'=>16)); ?>
	</div>

    <div class="row" style="width:70%;float:left;">
        <label style='width:110px;'>Telco</label>
        <?php echo $form->dropDownlist($model,'option',array(''=>'Select',1=>'Beeline',2=>'ETL',3=>'Unitel')); ?>
    </div>

    <div class="row" style="width:30%;float:left">
        <?php echo $form->label($model,'start'); ?>
        <?php
            $this->widget('CJuiDateTimePicker',array(
                'model'=>$model, //Model object
                'attribute'=>'start', //attribute name
                'mode'=>'datetime', //use "time","date" or "datetime" (default)
                'options'=>array("dateFormat"=>'yy/mm/dd'), // jquery plugin options
                'language' => ''
            ));
        ?>
    </div>

    <div class="row" style="width:70%;float:left;">
        <?php echo $form->label($model,'end',array('style'=>'width:110px;')); ?>
        <?php
            $this->widget('CJuiDateTimePicker',array(
                'model'=>$model, //Model object
                'attribute'=>'end', //attribute name
                'mode'=>'datetime', //use "time","date" or "datetime" (default)
                'options'=>array("dateFormat"=>'yy/mm/dd'), // jquery plugin options
                'language' => ''
            ));
        ?>
    </div>
    <div class="row">
        <?php echo $form->label($model, 'draw_no'); ?>
        <?php echo $form->dropDownlist($model,'draw_no',Statistic::model()->getDrawNo(),array('empty'=>'Select')); ?>
    </div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
        <?php echo CHtml::button('Export', array('url' => Yii::app()->createUrl(''), 'id' => 'btn-export')); ?>
    </div>

<?php $this->endWidget(); ?>
</div><!-- search-form -->