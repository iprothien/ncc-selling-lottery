<?php
/* @var $this SmsMtController */
/* @var $model SmsMt */

$this->breadcrumbs=array(
	'Sms Mts'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List SmsMt', 'url'=>array('index')),
	array('label'=>'Create SmsMt', 'url'=>array('create')),
	array('label'=>'View SmsMt', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage SmsMt', 'url'=>array('admin')),
);
?>

<h1>Update SmsMt <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>