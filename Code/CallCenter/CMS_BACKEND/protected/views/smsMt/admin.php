<?php
/* @var $this SmsMtController */
/* @var $model SmsMt */

$this->breadcrumbs=array(
	'Sms Mts'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Create SmsMt', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#sms-mt-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage SMS MT</h1>


<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php if($option){ ?>
    <div style="width: 100%;margin-left: 5px;">
        <?php if($option == 1){ ?>
            Telco: <span style="color: red;">Beeline</span>
        <?php }elseif($option == 2){ ?>
            Telco: <span style="color: red;">ETL</span>
        <?php }else{ ?>
            Telco: <span style="color: red;">Unitel</span>
        <?php } ?>
    </div>
    <div style="width: 100%;margin: 5px 0px;background: skyblue!important;">
        <p style="float: left;min-width: 5%;max-width: 30%;">Total Record: <span style="color: red;font-size: 15px;font-weight: bold;"><?php echo $model->getTotalRecord(); ?></span> </p>
        <p style="float: left;width: 30%;">Total SMS: <span style="color: red;font-size: 15px;font-weight: bold;"><?php if($model->getSumSms()){echo $model->getSumSms(); }else{ echo 0; } ?></span> </p>
    </div>
<?php } ?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'sms-mt-grid',
	'dataProvider'=>$model->search(),
	'columns'=>array(
		'id',
		'short_code',
		'msisdn',
        'status',
        //array('name'=>'status','value'=>'$data->getStatus()'),
		'content',
		'created_datetime',
		/*array(
			'class'=>'CButtonColumn',
		),*/
	),
)); ?>
