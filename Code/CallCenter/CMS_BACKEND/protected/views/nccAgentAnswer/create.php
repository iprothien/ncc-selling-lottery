<?php
/* @var $this NccAgentAnswerController */
/* @var $model NccAgentAnswer */

$this->breadcrumbs=array(
	'Ncc Agent Answers'=>array('admin'),
	'Create',
);

$this->menu=array(
	array('label'=>'Manage NccAgentAnswer', 'url'=>array('admin')),
);
?>

<h1>Create NccAgentAnswer</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>