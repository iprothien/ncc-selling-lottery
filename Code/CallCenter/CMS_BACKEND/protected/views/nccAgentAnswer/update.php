<?php
/* @var $this NccAgentAnswerController */
/* @var $model NccAgentAnswer */

$this->breadcrumbs=array(
	'Ncc Agent Answers'=>array('admin'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'Create NccAgentAnswer', 'url'=>array('create')),
	array('label'=>'View NccAgentAnswer', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage NccAgentAnswer', 'url'=>array('admin')),
);
?>

<h1>Update NccAgentAnswer <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>