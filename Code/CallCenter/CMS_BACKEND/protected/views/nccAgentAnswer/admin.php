<?php
/* @var $this NccAgentAnswerController */
/* @var $model NccAgentAnswer */

$this->breadcrumbs=array(
	'Ncc Agent Answers'=>array('admin'),
	'Manage',
);

/*
$this->menu=array(
	array('label'=>'Create NccAgentAnswer', 'url'=>array('create')),
);
 * 
 */

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('ncc-agent-answer-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Ncc Agent Answers</h1>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'ncc-agent-answer-grid',
	'dataProvider'=>$model->search(),
	'columns'=>array(
		'id',
        array('name'=>'agent_id','value'=>'$data->agent'),
		//'path_record',
        array(
            'type' => 'raw',
            'header' => 'MP3 Answer Record',
            'value' => '$data->getMp3player()'
        ),
		'msisdn_agent',
		'msisdn_member',
        'call_time',
		'duration',
        'note',
		array(
			'class'=>'CButtonColumn',
                        'template' => '{view}{delete}',
		),
	),
)); ?>
