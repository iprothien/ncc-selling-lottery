<?php
/* @var $this NccAgentAnswerController */
/* @var $model NccAgentAnswer */

$this->breadcrumbs=array(
	'Ncc Agent Answers'=>array('admin'),
	$model->id,
);

$this->menu=array(
	//array('label'=>'Create NccAgentAnswer', 'url'=>array('create')),
	//array('label'=>'Update NccAgentAnswer', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete NccAgentAnswer', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage NccAgentAnswer', 'url'=>array('admin')),
);
?>

<h1>View NccAgentAnswer #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'agent_id',
		'path_record',
		'msisdn_agent',
		'msisdn_member',
		'call_time',
		'duration',
		'created',
		'updated',
	),
)); ?>
