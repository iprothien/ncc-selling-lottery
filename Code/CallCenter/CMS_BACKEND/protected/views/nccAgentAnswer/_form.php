<?php
/* @var $this NccAgentAnswerController */
/* @var $model NccAgentAnswer */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'ncc-agent-answer-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'agent_id'); ?>
		<?php echo $form->textField($model,'agent_id'); ?>
		<?php echo $form->error($model,'agent_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'path_record'); ?>
		<?php echo $form->textField($model,'path_record',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'path_record'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'msisdn_agent'); ?>
		<?php echo $form->textField($model,'msisdn_agent',array('size'=>15,'maxlength'=>15)); ?>
		<?php echo $form->error($model,'msisdn_agent'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'msisdn_member'); ?>
		<?php echo $form->textField($model,'msisdn_member',array('size'=>15,'maxlength'=>15)); ?>
		<?php echo $form->error($model,'msisdn_member'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'call_time'); ?>
		<?php echo $form->textField($model,'call_time'); ?>
		<?php echo $form->error($model,'call_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'duration'); ?>
		<?php echo $form->textField($model,'duration'); ?>
		<?php echo $form->error($model,'duration'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'created'); ?>
		<?php echo $form->textField($model,'created'); ?>
		<?php echo $form->error($model,'created'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'updated'); ?>
		<?php echo $form->textField($model,'updated'); ?>
		<?php echo $form->error($model,'updated'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->