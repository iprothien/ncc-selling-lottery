<?php
/* @var $this NccAgentAnswerController */
/* @var $data NccAgentAnswer */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('agent_id')); ?>:</b>
	<?php echo CHtml::encode($data->agent_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('path_record')); ?>:</b>
	<?php echo CHtml::encode($data->path_record); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('msisdn_agent')); ?>:</b>
	<?php echo CHtml::encode($data->msisdn_agent); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('msisdn_member')); ?>:</b>
	<?php echo CHtml::encode($data->msisdn_member); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('call_time')); ?>:</b>
	<?php echo CHtml::encode($data->call_time); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('duration')); ?>:</b>
	<?php echo CHtml::encode($data->duration); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created')); ?>:</b>
	<?php echo CHtml::encode($data->created); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated')); ?>:</b>
	<?php echo CHtml::encode($data->updated); ?>
	<br />

	*/ ?>

</div>