<?php
/* @var $this NccAgentAnswerController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Ncc Agent Answers',
);

$this->menu=array(
	array('label'=>'Create NccAgentAnswer', 'url'=>array('create')),
	array('label'=>'Manage NccAgentAnswer', 'url'=>array('admin')),
);
?>

<h1>Ncc Agent Answers</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
