<?php
/* @var $this NccAgentAnswerController */
/* @var $model NccAgentAnswer */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php 
Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
$form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>
   <?php if(User::model()->findByPk(Yii::app()->user->id)->permision==1) { ?>

	<div class="row">
		<?php echo $form->label($model,'agent'); ?>
		<?php echo $form->textField($model,'agent'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'msisdn_agent'); ?>
		<?php echo $form->textField($model,'msisdn_agent',array('size'=>15,'maxlength'=>15)); ?>
	</div>
        <?php } ?>

	<div class="row">
		<?php echo $form->label($model,'msisdn_member'); ?>
		<?php echo $form->textField($model,'msisdn_member',array('size'=>15,'maxlength'=>15)); ?>
	</div>

      <div class="row" style="float: left;width: 30%">
        <?php echo $form->label($model,'start'); ?>
        <?php
        $this->widget('CJuiDateTimePicker',array(
            'model'=>$model, //Model object
            'attribute'=>'start', //attribute name
            'mode'=>'datetime', //use "time","date" or "datetime" (default)
            'options'=>array("dateFormat"=>'yy-mm-dd'), // jquery plugin options
            'language' => ''
        ));
        ?>
    </div>
    <div class="row">
        <?php echo $form->label($model,'end'); ?>
        <?php
        $this->widget('CJuiDateTimePicker',array(
            'model'=>$model, //Model object
            'attribute'=>'end', //attribute name
            'mode'=>'datetime', //use "time","date" or "datetime" (default)
            'options'=>array("dateFormat"=>'yy-mm-dd'), // jquery plugin options
            'language' => ''
        ));
        ?>
    </div>

	<div class="row">
        <?php echo $form->label($model,'call_status'); ?>
        <?php echo $form->dropDownList($model,'duration',array(0=>'Miss Call',1=>'Caller'),array('empty'=>'Selected')); ?>
    </div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
        <?php echo CHtml::button('Export', array('url' => Yii::app()->createUrl(''), 'id' => 'btn-export')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->