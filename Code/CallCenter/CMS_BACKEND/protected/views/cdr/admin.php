<?php
/* @var $this CdrController */
/* @var $model Cdr */

$this->breadcrumbs = array(
    'Cdrs' => array('admin'),
    'Manage',
);

$this->menu = array(
    array('label' => 'Create Cdr', 'url' => array('create')),
);
?>

<h4>Manage Cdrs</h4>

<?php // echo CHtml::link('#','#',array('class'=>'search-button')); ?>
<div class="search-form">
    <?php
    $this->renderPartial('_search', array(
        'model' => $model,
    ));
    ?>
</div><!-- search-form -->

<?php if($option){ ?>
    <div style="width: 100%;margin-left: 5px;">
        <?php if($option == 1){ ?>
            Telco: <span style="color: red;">Beeline</span>
        <?php }elseif($option == 2){ ?>
            Telco: <span style="color: red;">ETL</span>
        <?php }else{ ?>
            Telco: <span style="color: red;">Unitel</span>
        <?php } ?>
    </div>
    <div style="width: 100%;margin: 5px 0px;background: skyblue!important;">
        <p style="float: left;min-width: 5%;max-width: 30%;">Total Record: <span style="color: red;font-size: 15px;font-weight: bold;"><?php echo $model->getTotalRecord(); ?></span> </p>
        <p style="float: left;width: 30%;">Total Duration: <span style="color: red;font-size: 15px;font-weight: bold;"><?php if($model->getSumDuration()){echo $model->getSumDuration(); }else{ echo 0; } ?></span> </p>
    </div>
<?php } ?>

<?php if(isset($option) && $option): $provider  = $model->search(); ?>
<!--
<div class="thongke">
    <p>
        Total Call:
        <span><?php //echo $model->getTotal(); ?></span>
    </p>
    <p>
        Total Block:
        <span><?php //echo $model->getCountBlock(); ?></span>
    </p>
</div>
-->

<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'cdr-grid',
    'dataProvider' => $provider,
    'ajaxUpdate' => FALSE,
    'columns' => array(
        'id',
        'src',
        'accountcode',
        'duration',
        'start',
    ),
));
endif;
?>
