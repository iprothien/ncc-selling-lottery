<?php
/* @var $this CdrController */
/* @var $model Cdr */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'cdr-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'start'); ?>
		<?php echo $form->textField($model,'start'); ?>
		<?php echo $form->error($model,'start'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'callerid'); ?>
		<?php echo $form->textField($model,'callerid',array('size'=>60,'maxlength'=>80)); ?>
		<?php echo $form->error($model,'callerid'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'src'); ?>
		<?php echo $form->textField($model,'src',array('size'=>60,'maxlength'=>80)); ?>
		<?php echo $form->error($model,'src'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'dst'); ?>
		<?php echo $form->textField($model,'dst',array('size'=>60,'maxlength'=>80)); ?>
		<?php echo $form->error($model,'dst'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'dcontext'); ?>
		<?php echo $form->textField($model,'dcontext',array('size'=>60,'maxlength'=>80)); ?>
		<?php echo $form->error($model,'dcontext'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'channel'); ?>
		<?php echo $form->textField($model,'channel',array('size'=>60,'maxlength'=>80)); ?>
		<?php echo $form->error($model,'channel'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'dstchannel'); ?>
		<?php echo $form->textField($model,'dstchannel',array('size'=>60,'maxlength'=>80)); ?>
		<?php echo $form->error($model,'dstchannel'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'lastapp'); ?>
		<?php echo $form->textField($model,'lastapp',array('size'=>60,'maxlength'=>80)); ?>
		<?php echo $form->error($model,'lastapp'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'lastdata'); ?>
		<?php echo $form->textField($model,'lastdata',array('size'=>60,'maxlength'=>80)); ?>
		<?php echo $form->error($model,'lastdata'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'duration'); ?>
		<?php echo $form->textField($model,'duration'); ?>
		<?php echo $form->error($model,'duration'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'billsec'); ?>
		<?php echo $form->textField($model,'billsec'); ?>
		<?php echo $form->error($model,'billsec'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'disposition'); ?>
		<?php echo $form->textField($model,'disposition',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'disposition'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'amaflags'); ?>
		<?php echo $form->textField($model,'amaflags'); ?>
		<?php echo $form->error($model,'amaflags'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'accountcode'); ?>
		<?php echo $form->textField($model,'accountcode',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'accountcode'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'userfield'); ?>
		<?php echo $form->textField($model,'userfield',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'userfield'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'uniqueid'); ?>
		<?php echo $form->textField($model,'uniqueid',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'uniqueid'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->