<?php
/* @var $this CdrController */
/* @var $model Cdr */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php
    Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
    $form = $this->beginWidget('CActiveForm', array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
    )); ?>
    <div class="row">
        <?php echo $form->label($model, 'accountcode'); ?>
        <?php echo $form->dropDownlist($model, 'accountcode', array(1448 => '1448', 8899 => '8899'),array('empty'=>'Selected')); ?>
    </div>

    <div class="row" style="width:35%;float:left">
        <?php echo $form->label($model, 'src', array('style' => 'width:80px;')); ?>
        <?php echo $form->textField($model, 'src', array('size' => 20, 'maxlength' => 80)); ?>
    </div>

    <div class="row" style="width:50%;float:left;">
        <label style='width:110px;'>Telco</label>
        <?php echo $form->dropDownlist($model, 'option', array('' => 'Select', 1 => 'Beeline', 2 => 'ETL', 3 => 'Unitel')); ?>
    </div>

    <div class="row" style="width:35%;float:left">
        <?php echo $form->label($model, 'time_start', array('style' => 'width:80px;')); ?>
        <?php
        $this->widget('CJuiDateTimePicker', array(
            'model' => $model, //Model object
            'attribute' => 'time_start', //attribute name
            'mode' => 'datetime', //use "time","date" or "datetime" (default)
            'options' => array("dateFormat" => 'yy/mm/dd'), // jquery plugin options
            'language' => ''
        )); ?>
    </div>

    <div class="row" style="width:50%;float:left;">
        <?php echo $form->label($model, 'time_end', array('style' => 'width:100px;')); ?>
        <?php
        $this->widget('CJuiDateTimePicker', array(
            'model' => $model, //Model object
            'attribute' => 'time_end', //attribute name
            'mode' => 'datetime', //use "time","date" or "datetime" (default)
            'options' => array("dateFormat" => 'yy/mm/dd'), // jquery plugin options
            'language' => ''
        )); ?>
    </div>

    <div style="clear:both;"></div>

    <!--	<div class="row">
		<?php // echo $form->label($model,'start'); ?>
		<?php // echo $form->textField($model,'start'); ?>
	</div>
-->
    <div class="row buttons">
        <?php echo CHtml::submitButton('Search'); ?>
        <?php echo CHtml::button('Export', array('url' => Yii::app()->createUrl(''), 'id' => 'btn-export')); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->