<?php
/* @var $this CdrController */
/* @var $model Cdr */

$this->breadcrumbs=array(
	'Cdrs'=>array('admin'),
	'Create',
);

$this->menu=array(
	array('label'=>'Manage Cdr', 'url'=>array('admin')),
);
?>

<h1>Create Cdr</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>