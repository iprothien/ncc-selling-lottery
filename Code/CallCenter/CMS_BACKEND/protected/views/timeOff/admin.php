<?php
/* @var $this TimeOffController */
/* @var $model TimeOff */

$this->breadcrumbs=array(
	'Time Offs'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List TimeOff', 'url'=>array('index')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#time-off-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Time Offs</h1>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'time-off-grid',
	'dataProvider'=>$model->search(),
	'columns'=>array(
		'id',
		'day_of_week',
		'start_time',
		'end_time',
		'status',
		'created',
        array(
            'class'=>'CButtonColumn',
            'template' => '{update}'
        ),
		/*array(
			'class'=>'CButtonColumn',
		),*/
	),
)); ?>
