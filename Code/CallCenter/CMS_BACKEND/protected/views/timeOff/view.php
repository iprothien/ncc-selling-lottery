<?php
/* @var $this TimeOffController */
/* @var $model TimeOff */

$this->breadcrumbs=array(
	'Time Offs'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Manage TimeOff', 'url'=>array('admin')),
);
?>

<h1>View TimeOff #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'day_of_week',
		'start_time',
		'end_time',
		'status',
		'created',
	),
)); ?>
