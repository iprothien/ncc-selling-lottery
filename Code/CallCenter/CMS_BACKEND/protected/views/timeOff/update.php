<?php
/* @var $this TimeOffController */
/* @var $model TimeOff */

$this->breadcrumbs=array(
	'Time Offs'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'Manage TimeOff', 'url'=>array('admin')),
);
?>

<h1>Update TimeOff <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>