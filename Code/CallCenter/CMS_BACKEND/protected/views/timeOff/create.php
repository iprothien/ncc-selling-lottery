<?php
/* @var $this TimeOffController */
/* @var $model TimeOff */

$this->breadcrumbs=array(
	'Time Offs'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Manage TimeOff', 'url'=>array('admin')),
);
?>

<h1>Create TimeOff</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>