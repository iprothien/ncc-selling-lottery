<?php
/* @var $this TimeOffController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Time Offs',
);

$this->menu=array(
	array('label'=>'Create TimeOff', 'url'=>array('create')),
	array('label'=>'Manage TimeOff', 'url'=>array('admin')),
);
?>

<h1>Time Offs</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
