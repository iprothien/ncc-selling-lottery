<?php
/* @var $this TimeOffController */
/* @var $model TimeOff */
/* @var $form CActiveForm */
?>

<div class="form">

<?php
    $form=$this->beginWidget('CActiveForm', array(
        'id'=>'time-off-form',
        'enableAjaxValidation'=>false,

    ));
?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php if($model->isNewRecord){ ?>
        <div class="row">
            <?php echo $form->labelEx($model,'day_of_week'); ?>
            <?php echo $form->textField($model,'day_of_week',array('size'=>20,'maxlength'=>20)); ?>
            <?php echo $form->error($model,'day_of_week'); ?>
        </div>
    <?php } ?>

	<div class="row">
		<?php echo $form->labelEx($model,'start_time'); ?>
        <?php echo $form->textField($model,'start_time',array('size'=>20)); ?>
		<?php echo $form->error($model,'start_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'end_time'); ?>
        <?php echo $form->textField($model,'end_time',array('size'=>20)); ?>
		<?php echo $form->error($model,'end_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
        <?php echo $form->dropDownList($model,'status',array(1=>'Enable',0=>'Disable')); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->