<?php
/* @var $this CallDetailController */
/* @var $model CallDetail */

$this->breadcrumbs=array(
	'Call Details'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Create CallDetail', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#call-detail-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Call Details</h1>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'call-detail-grid',
	'dataProvider'=>$model->search(),
	'columns'=>array(
		'id',
		'call_service_id',
		'msisdn',
		'time_call',
		'status',
		'time_called',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
