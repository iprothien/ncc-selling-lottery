<?php
/* @var $this CallDetailController */
/* @var $model CallDetail */

$this->breadcrumbs=array(
	'Call Details'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List CallDetail', 'url'=>array('index')),
	array('label'=>'Create CallDetail', 'url'=>array('create')),
	array('label'=>'Update CallDetail', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete CallDetail', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage CallDetail', 'url'=>array('admin')),
);
?>

<h1>View CallDetail #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'call_service_id',
		'msisdn',
		'time_call',
		'status',
		'time_called',
	),
)); ?>
