<?php
/* @var $this CallDetailController */
/* @var $model CallDetail */

$this->breadcrumbs=array(
	'Call Details'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List CallDetail', 'url'=>array('index')),
	array('label'=>'Manage CallDetail', 'url'=>array('admin')),
);
?>

<h1>Create CallDetail</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>