<?php
/* @var $this CallDetailController */
/* @var $model CallDetail */

$this->breadcrumbs=array(
	'Call Details'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List CallDetail', 'url'=>array('index')),
	array('label'=>'Create CallDetail', 'url'=>array('create')),
	array('label'=>'View CallDetail', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage CallDetail', 'url'=>array('admin')),
);
?>

<h1>Update CallDetail <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>