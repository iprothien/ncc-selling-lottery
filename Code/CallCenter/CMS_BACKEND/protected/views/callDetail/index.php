<?php
/* @var $this CallDetailController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Call Details',
);

$this->menu=array(
	array('label'=>'Create CallDetail', 'url'=>array('create')),
	array('label'=>'Manage CallDetail', 'url'=>array('admin')),
);
?>

<h1>Call Details</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
