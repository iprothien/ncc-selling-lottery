<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'user-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'username'); ?>
		<?php echo $form->textField($model,'username',array('size'=>30,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'username'); ?>
	</div>
        
        <div class="row">
		<?php echo $form->labelEx($model,'fullname'); ?>
		<?php echo $form->textField($model,'fullname',array('size'=>30,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'fullname'); ?>
	</div>
        
    <?php if($model->isNewRecord){ ?>
        <div class="row">
            <?php echo $form->labelEx($model,'password'); ?>
            <?php echo $form->textField($model,'password',array('size'=>30,'maxlength'=>255)); ?>
            <?php echo $form->error($model,'password'); ?>
        </div>
    <?php }else{ ?>
        <div class="row">
            <?php echo $form->labelEx($model,'rePassword'); ?>
            <?php echo $form->textField($model,'rePassword',array('size'=>30,'maxlength'=>255)); ?>
        </div>
    <?php } ?>

	<div class="row">
		<?php echo $form->labelEx($model,'module'); ?>
		<?php echo $form->checkBoxList($model, 'moduleSelected', $model->getListModule(), array('template' => '<li>{input}{label}</li>', 'container' => 'ul class="module-list"', 'separator' => '')); ?>
		<?php echo $form->error($model,'module'); ?>
	</div>
	
    <div class="clear"></div>
        <div class="row">
		<?php echo $form->labelEx($model,'type'); ?>
		<?php echo $form->dropDownList($model,'type',array(''=>'Select...',1=>'NCC',2=>'Bank')); ?>
		<?php echo $form->error($model,'type'); ?>
	</div>
    
        <div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->dropDownList($model,'status',array(1=>'Enable',0=>'Disable')); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->