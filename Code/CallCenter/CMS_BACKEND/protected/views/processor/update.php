<?php
/* @var $this ProcessorController */
/* @var $model Processor */

$this->breadcrumbs=array(
	'Processors'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Processor', 'url'=>array('index')),
	array('label'=>'Create Processor', 'url'=>array('create')),
	array('label'=>'View Processor', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Processor', 'url'=>array('admin')),
);
?>

<h1>Update Processor <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>