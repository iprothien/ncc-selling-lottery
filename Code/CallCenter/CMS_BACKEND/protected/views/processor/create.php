<?php
/* @var $this ProcessorController */
/* @var $model Processor */

$this->breadcrumbs=array(
	'Processors'=>array('admin'),
	'Create',
);

$this->menu=array(
	array('label'=>'Manage Processor', 'url'=>array('admin')),
);
?>

<h1>Create Processor</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>