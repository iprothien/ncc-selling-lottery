<?php
/* @var $this ProcessorController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Processors',
);

$this->menu=array(
	array('label'=>'Create Processor', 'url'=>array('create')),
	array('label'=>'Manage Processor', 'url'=>array('admin')),
);
?>

<h1>Processors</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
