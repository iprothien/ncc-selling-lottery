<?php
/* @var $this ProcessorController */
/* @var $model Processor */

$this->breadcrumbs=array(
	'Processors'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Processor', 'url'=>array('index')),
	array('label'=>'Create Processor', 'url'=>array('create')),
	array('label'=>'Update Processor', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Processor', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Processor', 'url'=>array('admin')),
);
?>

<h1>View Processor #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'account_id',
		'full_name',
		'type',
		'status',
	),
)); ?>
