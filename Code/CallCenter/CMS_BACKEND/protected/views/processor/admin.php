<?php
/* @var $this ProcessorController */
/* @var $model Processor */

$this->breadcrumbs=array(
	'Processors'=>array('admin'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Create Processor', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-form form').submit(function(){
	$('#processor-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Processors</h1>

<div class="search-form">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'processor-grid',
	'dataProvider'=>$model->search(),
	'columns'=>array(
		'id',
		'full_name',
                array('name'=>'account_id','value'=>'$data->AccountName'),
		array('name'=>'type', 'value'=>'$data->getType()'),
		array('name'=>'status', 'value'=>'$data->getStatus()'),
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
