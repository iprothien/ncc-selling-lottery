<?php
/* @var $this DrawTimeController */
/* @var $model DrawTime */

$this->breadcrumbs=array(
	'Draw Times'=>array('admin'),
	'Create',
);

$this->menu=array(
	array('label'=>'Manage DrawTime', 'url'=>array('admin')),
);
?>

<h1>Create DrawTime</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>