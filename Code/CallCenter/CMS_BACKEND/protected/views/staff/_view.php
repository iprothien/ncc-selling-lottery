<?php
/* @var $this StaffController */
/* @var $data Staff */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('full_name')); ?>:</b>
	<?php echo CHtml::encode($data->full_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('phone')); ?>:</b>
	<?php echo CHtml::encode($data->phone); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('departments_id')); ?>:</b>
	<?php echo CHtml::encode($data->departments_id); ?>
	<br />
        
        <b><?php echo CHtml::encode($data->getAttributeLabel('sip_username')); ?>:</b>
	<?php echo CHtml::encode($data->sip_username); ?>
	<br />
        
        <b><?php echo CHtml::encode($data->getAttributeLabel('sip_password')); ?>:</b>
	<?php echo CHtml::encode($data->sip_password); ?>
	<br />


</div>