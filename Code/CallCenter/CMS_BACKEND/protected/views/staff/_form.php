<?php
/* @var $this StaffController */
/* @var $model Staff */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'staff-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'full_name'); ?>
		<?php echo $form->textField($model,'full_name',array('size'=>20,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'full_name'); ?>
	</div>

    <?php if($model->isNewRecord){ ?>
        <div class="row">
            <?php echo $form->labelEx($model,'username'); ?>
            <?php echo $form->textField($model,'username',array('size'=>20,'maxlength'=>50)); ?>
            <?php echo $form->error($model,'username'); ?>
        </div>
    <?php }else{ ?>
        <div class="row">
            <?php echo $form->labelEx($model,'username1'); ?>
            <span style="font-weight: bold;line-height: 2.2em;padding-left: 10px;"><?php echo $model->admin_user; ?></span>
        </div>
    <?php } ?>

    <?php if($model->isNewRecord){ ?>
        <div class="row">
            <?php echo $form->labelEx($model,'password'); ?>
            <?php echo $form->textField($model,'password',array('size'=>20,'maxlength'=>50)); ?>
            <?php echo $form->error($model,'password'); ?>
        </div>
    <?php }else{ ?>
        <div class="row">
            <?php echo $form->labelEx($model,'pwd'); ?>
            <?php echo $form->textField($model,'pwd',array('size'=>20,'maxlength'=>50,'placeholder'=>'New password')); ?>
            <?php echo $form->error($model,'pwd'); ?>
        </div>
    <?php } ?>

    <?php if($model->isNewRecord){ ?>
        <div class="row">
            <?php echo $form->labelEx($model,'phone'); ?>
            <?php echo $form->textField($model,'phone',array('size'=>20,'maxlength'=>11)); ?>
            <?php echo $form->error($model,'phone'); ?>
        </div>
    <?php }else{ ?>
        <div class="row">
            <?php echo $form->labelEx($model,'oldPhone'); ?>
            <?php echo $form->textField($model,'oldPhone',array('size'=>20,'maxlength'=>11,'placeholder'=>$model->phone)); ?>
            <?php echo $form->error($model,'oldPhone'); ?>
        </div>
    <?php } ?>

    <?php if($model->isNewRecord){ ?>
        <div class="row">
            <?php echo $form->labelEx($model,'sip_password'); ?>
            <?php echo $form->textField($model,'sip_password',array('size'=>20,'maxlength'=>40)); ?>
            <?php echo $form->error($model,'sip_password'); ?>
        </div>
    <?php }else{ ?>
        <div class="row">
            <?php echo $form->labelEx($model,'sip_password'); ?>
            <?php echo $form->textField($model,'sip_password',array('size'=>20,'maxlength'=>40,'placeholder'=>$model->sip_pwd)); ?>
            <?php echo $form->error($model,'sip_password'); ?>
        </div>
    <?php } ?>


    <div class="row">
        <?php echo $form->labelEx($model,'module'); ?>
        <?php echo $form->checkBoxList($model, 'moduleSelected', $model->getListModule(), array('template' => '<li>{input}{label}</li>', 'container' => 'ul class="module-list"', 'separator' => '')); ?>
        <?php echo $form->error($model,'module'); ?>
    </div><div class="clear">

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->