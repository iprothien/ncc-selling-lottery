<?php
/* @var $this MemberDrawController */
/* @var $model MemberDraw */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php
    Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
    $form = $this->beginWidget('CActiveForm', array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
    )); ?>

    <div class="row" style="width:400px;float:left">
        <?php echo $form->label($model, 'From Draw No '); ?>
        <?php echo $form->dropDownlist($model,'draw_no_start',$model->getDrawNo(),array('empty'=>'Select')); ?>
    </div>
    <div class="row" style="float:left;">
        <?php echo $form->label($model, 'To Draw No'); ?>
        <?php echo $form->dropDownlist($model,'draw_no_end',$model->getDrawNo(),array('empty'=>'Select')); ?>
    </div>
    <div style="clear:both"></div>
    <div class="row" style="width:400px;float:left">
        <?php echo $form->label($model, 'time_start'); ?>
        <?php
        $this->widget('CJuiDateTimePicker', array(
            'model' => $model, //Model object
            'attribute' => 'time_start', //attribute name
            'mode' => 'datetime', //use "time","date" or "datetime" (default)
            'options' => array("dateFormat" => 'yy/mm/dd'), // jquery plugin options
            'language' => ''
        )); ?>
    </div>

    <div class="row" style="float:left;">
        <?php echo $form->label($model, 'time_end', array('style' => 'width:110px;')); ?><?php
        $this->widget('CJuiDateTimePicker', array(
            'model' => $model, //Model object
            'attribute' => 'time_end', //attribute name
            'mode' => 'datetime', //use "time","date" or "datetime" (default)
            'options' => array("dateFormat" => 'yy/mm/dd'), // jquery plugin options
            'language' => ''
        )); ?>
    </div>
    <div style="clear:both"></div>
    <div class="row" style="width:400px;float:left;">
        <?php echo $form->label($model, 'month'); ?>
        <?php echo $form->dropDownlist($model,'month',array("1"=>"1","2"=>"2","3"=>"3","4"=>"4","5","6","7","8","9","10","11","12"),array('empty'=>'Select')); ?>
    </div>
    <div class="row" style="width:400px;float:left;">
        <?php echo $form->label($model, 'year'); ?>
        <?php echo $form->dropDownlist($model,'year',array("2014"=>"2014","2015"=>"2015","2016"=>"2016","2017"=>"2017","2018"=>"2018"),array('empty'=>'Select')); ?>
    </div>
    <div style="clear:both"></div>
    <div class="row buttons">
        <?php echo CHtml::submitButton('View'); ?>
    </div>

    <?php $this->endWidget(); ?>
</div><!-- search-form -->