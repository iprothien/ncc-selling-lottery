<?php
/* @var $this MemberDrawController */
/* @var $model MemberDraw */
$this->breadcrumbs=array(
	'Statistic'=>array('admin'),
	'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('statistic_content', {
		data: $(this).serialize()
	});
	return false;
});
");

?>

<h1 style="margin-left: 20px;">Manage Statistic</h1>
<style>

</style>
<div class="search-form">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->
<table>
    <tr  valign="top">
        <td width="300px;">
            <div class="statistic_content" id="statistic_content" style="with=500px,float: left">
                <div style="width: 300px;margin: 0px 10px">
                    <fieldset id="address">
                        <legend><b>Total revenue</b></legend>
                        <?php echo number_format($model->total_etl + $model->total_beeline + $model->total_unitel); ?>
                    </fieldset>
                </div>
                <div style="float:left;width: 300px;margin: 0px 10px">
                    <fieldset id="address">
                    <legend><b>ETL</b></legend>
                           <label>Total</label>: <span><?php echo number_format($model->total_etl); ?></span> <br/>
                     <label>2 Digits</label>: <span><?php echo number_format($model->etl_2); ?> <br />
                            <label>3 Digits</label>: <span><?php echo number_format($model->etl_3); ?>       
                     </fieldset>
                </div>
                <div style="clear:both"></div>
                <div style="float:left;width: 300px;margin: 0px 10px">
                    <fieldset id="address">
                    <legend><b>Unitel</b></legend>
                           <label>Total</label>: <span><?php echo number_format($model->total_unitel); ?></span> <br/>
                           <label>2 Digits</label>: <span><?php echo number_format($model->unitel_2); ?></span> <br />
                           <label>3 Digits</label>: <span><?php echo number_format($model->unitel_3); ?></span>     
                     </fieldset>
                </div>
                <div style="float:left;width: 300px;margin: 0px 10px">
                    <fieldset id="address">
                    <legend><b>LTC</b></legend>
                           <label>Total</label>: <span>0</span> <br/>
                           <label>2 Digits</label>: <span>0</span> <br />
                           <label>3 Digits</label>: <span>0</span>     
                     </fieldset>
                </div>
                <div class="clear"></div>
            </div>

        </td>
        <td width="600px;">
            <div class="statistic_content" id="statistic_content" style="with=500px">
                <div style="float:left;width: 600px;margin: 0px 10px">
                        <?php
                            $arrDraw = array();
                            $arrMoney = array();
                            foreach($charTotal as $char){
                                $arrDraw[] = "Draw No ".$char['draw_no'];
                                $arrMoney[] = $char['total_money'];

                            }
                            $graphTitle = "";
                            if($month&&$year){
                                $graphTitle .=$month."/".$year;
                            }
                            if($model->draw_no_start&&$model->draw_no_end){
                                $graphTitle .= " DRAW RANGE: {$model->draw_no_start} to {$model->draw_no_end} ";
                            }
                            $this->Widget('HighchartsWidget', array(
                                            'options'=>array(
                                               'chart'=>array('type'=>'bar'),
                                               'title' => array('text' => 'GRAPH LOTTO '.$graphTitle),
                                               'xAxis' => array(
                                                  'categories' => $arrDraw
                                               ),
                                               'yAxis' => array(
                                                  'title' => array('text' => 'NCC LOTTO IVR')
                                               ),
                                               'plotOptions'=> array('bar'=>array('dataLabels'=>array('enabled'=>'true'))),
                                               'series' => array(
                                                  array('name'=>$month."/".$year,'data' => $arrMoney),
                                               )
                                            )
                                         ));
                        ?>
                </div>
                <div class="clear"></div>                
            </div>
        </td>
            
    </tr>
</table>


