<?php

class UserController extends AccessController {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';
    public $module = 'user';

    public function accessRules() {
        return array(
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('admin', 'index', 'create', 'update', 'delete', 'view'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new User;
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['User'])) {
            $model->attributes = $_POST['User'];
            $model->moduleSelected = $_POST['User']['moduleSelected'];
            $model->password = $_POST['User']['password'];
            $model->fullname = $_POST['User']['fullname'];
            $model->type = $_POST['User']['type'];
            if ($model->save()) {
                $model->password = md5($_POST['User']['password']);

                $sqlInsert = 'INSERT INTO processor(account_id,full_name,type,status) 
                                    VALUES("' . $model->id . '","' . $model->fullname . '","' . $model->type . '","1")';
                Yii::app()->db->createCommand($sqlInsert)->execute();
                $this->redirect(array('admin'));
            }
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['User'])) {
            $model->attributes = $_POST['User'];
            $model->rePassword = $_POST['User']['rePassword'];

            if (isset($model->rePassword) && $model->rePassword) {
                $model->password = md5($_POST['User']['rePassword']);
            }
            $model->moduleSelected = $_POST['User']['moduleSelected'];

            $model->fullname = $_POST['User']['fullname'];
            $model->type = $_POST['User']['type'];
            if ($model->save()) {
                if (Processor::model()->findByAttributes(array('account_id' => $model->id))) {
                    $sqlUpdate = 'UPDATE processor SET full_name="' . $model->fullname . '",type="' . $model->type . '",
                                    status="' . $model->status . '" WHERE account_id=' . $model->id;
                    Yii::app()->db->createCommand($sqlUpdate)->execute();
                } else {
                    $sqlInsert = 'INSERT INTO processor(account_id,full_name,type,status) 
                                    VALUES("' . $model->id . '","' . $model->fullname . '","' . $model->type . '","1")';
                    Yii::app()->db->createCommand($sqlInsert)->execute();
                }

                $this->redirect(array('admin'));
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new User('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['User']))
            $model->attributes = $_GET['User'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = User::model()->findByPk($id);
        if(Processor::model()->findByAttributes(array('account_id'=>$id))) {
            $model->fullname    = Processor::model()->findByAttributes(array('account_id'=>$id))->full_name;
            $model->type        = Processor::model()->findByAttributes(array('account_id'=>$id))->type;
        }else{
            $model->fullname    = '';
            $model->type        = '';
        }
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'user-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
