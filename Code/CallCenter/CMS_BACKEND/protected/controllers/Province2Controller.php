<?php

class Province2Controller extends AccessController
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';
    public $module = 'province';

    public function accessRules() {
        return array(
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('view', 'create', 'admin', 'export','draw', 'getProvince'),
                'users' => array('@'),
                'expression' => (string) $this->authenticate($this->module)
            ),
            array('allow',
                'actions' => array('delete', 'update'),
                'users' => array('@'),
                'expression' => (string) $this->full()
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model = new Province2;
        $model->unsetAttributes();

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Province2'])) {
            //$post = $_POST['Province2'];

            $model->setAttributes($_POST['Province2']);
            if (isset($_POST['Province2']["province_id"]) && $_POST['Province2']["province_id"]) {
                $model->parent_id = $_POST['Province2']["province_id"];
            }
            if (isset($_POST['Province2']["district_id"]) && $_POST['Province2']["district_id"]) {
                $model->parent_id = $_POST['Province2']["district_id"];
            }
            if (isset($_POST['Province2']["ward_id"]) && $_POST['Province2']["ward_id"]) {
                $model->parent_id = $_POST['Province2']["ward_id"];
            }

            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);
        $model->loadParents();

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Province2'])) {
            //$post = $_POST['Province2'];

            $model->setAttributes($_POST['Province2']);
            if (isset($_POST['Province2']["province_id"])) {
                $model->parent_id = $_POST['Province2']["province_id"];
            }
            if (isset($_POST['Province2']["district_id"])) {
                $model->parent_id = $_POST['Province2']["district_id"];
            }
            if (isset($_POST['Province2']["ward_id"])) {
                $model->parent_id = $_POST['Province2']["ward_id"];
            }
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $dataProvider = new CActiveDataProvider('Province2');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model = new Province2('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['Province2']))
            $model->attributes = $_GET['Province2'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Province2 the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Province2::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Province2 $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'province2-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionGetProvince($type, $parentId)
    {
        echo json_encode(Province2::model()->getProvinceList($type, $parentId));
    }
}
