<?php

class CallServiceController extends AccessController
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';
    public $module = 'callService';

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model = new CallService;
        $model1 = new CallService;
        $model2 = new CallService;
        $model3 = new CallService;
        if (isset($_POST['CallService'])) {
            $model->attributes = $_POST['CallService'];
            $model->time_call = $_POST['CallService']['time_call'];
            $model->created_datetime = date('Y-m-d H:i:s');
            if ($model->time_call) {
                $model->time_call = $_POST['CallService']['time_call'];
            } else {
                $model->time_call = date('Y-m-d H:i:s');
            }


            /*$model1->connect = 1;
            $model1->attributes = $_POST['CallService'];
            $model1->time_call = $_POST['CallService']['time_call'];
            $model1->created_datetime = date('Y-m-d H:i:s');
            if ($model1->time_call) {
                $model1->time_call = $_POST['CallService']['time_call'];
            } else {
                $model1->time_call = date('Y-m-d H:i:s');
            }*/

            $model2->connect = 2;
            $model2->attributes = $_POST['CallService'];
            $model2->time_call = $_POST['CallService']['time_call'];
            $model2->created_datetime = date('Y-m-d H:i:s');
            if ($model2->time_call) {
                $model2->time_call = $_POST['CallService']['time_call'];
            } else {
                $model2->time_call = date('Y-m-d H:i:s');
            }

            $model3->connect = 3;
            $model3->attributes = $_POST['CallService'];
            $model3->time_call = $_POST['CallService']['time_call'];
            $model3->created_datetime = date('Y-m-d H:i:s');
            if ($model3->time_call) {
                $model3->time_call = $_POST['CallService']['time_call'];
            } else {
                $model3->time_call = date('Y-m-d H:i:s');
            }

            /*$model->save();
            $model1->save();
            $model2->save();
            $model3->save();*/

            if ($model->type == 1) {
                // File media
                $model->fileMedia = CUploadedFile::getInstance($model, 'media_path');
                //$dirMedia = 'C:/xampp/htdocs/ncc1/file_mp3/';
                $dirMedia = '/var/www/backend/uploads/upload_file_mp3/';

                if ($model->fileMedia) {
                    $fileMedia = preg_replace('/\s+/', '', $model->fileMedia->getName());
                    $rename = explode('.', $fileMedia);
                    $mName = time() . '.' . $rename[1];
                    $model->fileMedia->saveAs($dirMedia . $mName);
                    $model->media_path = $mName;
                    //$model1->media_path = $mName;
                    $model2->media_path = $mName;
                    $model3->media_path = $mName;
                }

                $model->save();
                //$model1->save();
                $model2->save();
                $model3->save();

                //$sql_beeline = "INSERT INTO call_detail (msisdn,call_service_id,time_call)
                //        SELECT DISTINCT(msisdn),{$model1->id},'{$model1->time_call}' FROM member_draw WHERE draw_no=" . $model1->draw_no;
                //Yii::app()->ncc_beeline->createCommand($sql_beeline)->execute();

                $sql_etl = "INSERT INTO call_detail (msisdn,call_service_id,time_call)
                        SELECT DISTINCT(msisdn),{$model2->id},'{$model2->time_call}' FROM member_draw WHERE draw_no=" . $model2->draw_no;
                Yii::app()->ncc_etl->createCommand($sql_etl)->execute();

                $sql_unitel = "INSERT INTO call_detail (msisdn,call_service_id,time_call)
                        SELECT DISTINCT(msisdn),{$model3->id},'{$model3->time_call}' FROM member_draw WHERE draw_no=" . $model3->draw_no;
                Yii::app()->ncc_unitel->createCommand($sql_unitel)->execute();

            } elseif ($model->type == 2) {
                // File media
                $model->fileMedia = CUploadedFile::getInstance($model, 'media_path');
                //$dirMedia = 'C:/xampp/htdocs/ncc1/file_mp3/';
                $dirMedia = '/var/www/backend/uploads/upload_file_mp3/';

                if ($model->fileMedia) {
                    $fileMedia = preg_replace('/\s+/', '', $model->fileMedia->getName());
                    $rename = explode('.', $fileMedia);
                    $mName = time() . '.' . $rename[1];
                    $model->fileMedia->saveAs($dirMedia . $mName);
                    $model->media_path = $mName;
                    //$model1->media_path = $mName;
                    $model2->media_path = $mName;
                    $model3->media_path = $mName;
                }

                // File Txt
                $model->fileName = CUploadedFile::getInstance($model, 'file_path');
                //$dir = 'C:/xampp/htdocs/ncc1/file_txt/';
                $dir = '/var/www/backend/uploads/upload_file_txt/';
                if ($model->fileName) {
                    $fileName = preg_replace('/\s+/', '', $model->fileName->getName());
                    $model->fileName->saveAs($dir . $fileName);
                    $model->file_path = $fileName;
                    //$model1->file_path = $fileName;
                    $model2->file_path = $fileName;
                    $model3->file_path = $fileName;
                }

                $fileUpload = file("/var/www/backend/uploads/upload_file_txt/" . $fileName, FILE_SKIP_EMPTY_LINES);

                $model->save();
                //$model1->save();
                $model2->save();
                $model3->save();

                for ($i = 0; $i < count($fileUpload); $i++) {
                    $item = explode(',',$fileUpload[$i]);
                    if (substr($item[0], 0, 3) == 207 || substr($item[0], 0, 3) == 307) {
                        //$cmd = Yii::app()->ncc_beeline->createCommand();
                        //$cmd->insert('call_detail', array('call_service_id' => $model1->id, 'msisdn' => $item[0],'prize' => $item[1], 'digit' => $item[2], 'time_call' => $model1->time_call));
                    } elseif (substr($item[0], 0, 3) == 202 || substr($item[0], 0, 3) == 302) {
                        $cmd = Yii::app()->ncc_etl->createCommand();
                        $cmd->insert('call_detail', array('call_service_id' => $model2->id, 'msisdn' => $item[0],'prize' => $item[1], 'digit' => $item[2], 'time_call' => $model2->time_call));
                    } elseif (substr($item[0], 0, 3) == 209 || substr($item[0], 0, 3) == 309) {
                        $cmd = Yii::app()->ncc_unitel->createCommand();
                        $cmd->insert('call_detail', array('call_service_id' => $model3->id, 'msisdn' => $item[0],'prize' => $item[1], 'digit' => $item[2], 'time_call' => $model3->time_call));
                    } else {
                        $cmd = Yii::app()->db->createCommand();
                        $cmd->insert('call_detail', array('call_service_id' => $model->id, 'msisdn' => $item[0],'prize' => $item[1], 'digit' => $item[2], 'time_call' => $model->time_call));
                    }
                }
            }
            $this->redirect(array('admin'));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        //$model1 = $this->loadModel($id);
        //$model1->connect = 1;
        $model2 = $this->loadModel($id);
        $model2->connect = 2;
        $model3 = $this->loadModel($id);
        $model3->connect = 3;

        if (isset($_POST['CallService'])) {
            $model->attributes = $_POST['CallService'];
            $model->name = $_POST['CallService']['name'];
            $model->status = $_POST['CallService']['status'];
            $model->time_call = $_POST['CallService']['time_call'];

            /*$model1->attributes = $_POST['CallService'];
            $model1->name = $_POST['CallService']['name'];
            $model1->status = $_POST['CallService']['status'];
            $model1->time_call = $_POST['CallService']['time_call'];
	    */
            $model2->attributes = $_POST['CallService'];
            $model2->name = $_POST['CallService']['name'];
            $model2->status = $_POST['CallService']['status'];
            $model2->time_call = $_POST['CallService']['time_call'];

            $model3->attributes = $_POST['CallService'];
            $model3->name = $_POST['CallService']['name'];
            $model3->status = $_POST['CallService']['status'];
            $model3->time_call = $_POST['CallService']['time_call'];

            $model->save();
            //$model1->save();
            $model2->save();
            $model3->save();

            if ($model->status == 1) {
                $sql = "UPDATE call_service SET status = " . $model->status . " WHERE created_datetime='{$model->created_datetime}'";
                //ncc_beeline, ncc_etl, ncc_unitel
                Yii::app()->ncc_beeline->createCommand($sql)->execute();
                Yii::app()->ncc_etl->createCommand($sql)->execute();
                Yii::app()->ncc_unitel->createCommand($sql)->execute();

            } elseif ($model->status == 2) {
                $sql = "UPDATE call_service SET status = " . $model->status . " WHERE created_datetime='{$model->created_datetime}' AND status = 0";

               // Yii::app()->ncc_beeline->createCommand($sql)->execute();
                Yii::app()->ncc_etl->createCommand($sql)->execute();
                Yii::app()->ncc_unitel->createCommand($sql)->execute();
            }

            $this->redirect(array('admin'));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $dataProvider = new CActiveDataProvider('CallService');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model = new CallService('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['CallService']))
            $model->attributes = $_GET['CallService'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return CallService the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = CallService::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CallService $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'call-service-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
