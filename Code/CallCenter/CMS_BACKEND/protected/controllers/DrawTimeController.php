<?php

class DrawTimeController extends AccessController {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';
    public $module = 'drawTime';

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new DrawTime;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['DrawTime'])) {
            $model->attributes = $_POST['DrawTime'];
            $model->draw_no     = $_POST['DrawTime']['draw_no'];
            $model->three_digit = $_POST['DrawTime']['three_digit'];
            $model->two_digit   = $_POST['DrawTime']['two_digit'];
            if ($model->save()) {
                //INSERT INTO ETL SERVER
                $sql_etl = 'INSERT INTO draw_time(start_time, end_time, ref_id, draw_no, three_digit, two_digit) 
                                    VALUES("' . $model->start_time . '","' . $model->end_time . '","' . $model->id . '",
                                        "'.$model->draw_no.'","'.$model->three_digit.'","'.$model->two_digit.'")';
                Yii::app()->ncc_etl->createCommand($sql_etl)->execute();

                //INSERT INTO BEELINE SERVER
                //Yii::app()->ncc_beeline->createCommand($sql_etl)->execute();
                
                //INSERT INTO BEELINE SERVER
                Yii::app()->ncc_unitel->createCommand($sql_etl)->execute();

                $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['DrawTime'])) {
            $model->attributes = $_POST['DrawTime'];
            $model->status = $_POST['DrawTime']['status'];
            $model->draw_no     = $_POST['DrawTime']['draw_no'];
            $model->three_digit = $_POST['DrawTime']['three_digit'];
            $model->two_digit   = $_POST['DrawTime']['two_digit'];
            if ($model->save()) {
                //UPDATE ETL
                $sql_update = 'UPDATE draw_time SET start_time="' . $model->start_time . '",end_time="' . $model->end_time . '",
                                            status="' . $model->status . '", draw_no="'.$model->draw_no.'", 
                                            three_digit="'.$model->three_digit.'", two_digit="'.$model->two_digit.'" 
                                                WHERE ref_id=' . $model->id;
                Yii::app()->ncc_etl->createCommand($sql_update)->execute();
                //UPDATE BEELINE
                //Yii::app()->ncc_beeline->createCommand($sql_update)->execute();
                
                //UPDATE BEELINE
                Yii::app()->ncc_unitel->createCommand($sql_update)->execute();


                $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        /* DELETE ETL */
        $sql_update = 'UPDATE draw_time SET status=3, delete_status=1  
                                                WHERE ref_id=' . $id;
        Yii::app()->ncc_etl->createCommand($sql_update)->execute();
        //DELETE BEELINE
        //Yii::app()->ncc_beeline->createCommand($sql_update)->execute();
        
        //DELETE UNITEL
        Yii::app()->ncc_unitel->createCommand($sql_update)->execute();
        
        //$this->loadModel($id)->delete();
        $cmdUpdate  = Yii::app()->db->createCommand('UPDATE draw_time SET status=3,delete_status=1 WHERE id='.$id)->execute();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new DrawTime('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['DrawTime']))
            $model->attributes = $_GET['DrawTime'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return DrawTime the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = DrawTime::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param DrawTime $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'draw-time-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
