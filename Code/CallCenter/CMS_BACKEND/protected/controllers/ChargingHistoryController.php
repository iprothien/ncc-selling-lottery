<?php

class ChargingHistoryController extends AccessController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */

	public $layout='//layouts/column1';
    public $module  = 'chargingHistory';
    public $filename = 'Report_chargingHistory';

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	/*public function actionCreate()
	{
		$model=new ChargingHistory;
		if(isset($_POST['ChargingHistory']))
		{
			$model->attributes=$_POST['ChargingHistory'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}*/

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	/*public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['ChargingHistory']))
		{
			$model->attributes=$_POST['ChargingHistory'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}*/

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	/*public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}*/

	/**
	 * Lists all models.
	 */
	/*public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('ChargingHistory');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}*/

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new ChargingHistory('search');

        $option = isset($_GET['ChargingHistory']['option']) ? $_GET['ChargingHistory']['option'] : NULL;
        $msisdn = isset($_GET['ChargingHistory']['msisdn']) ? $_GET['ChargingHistory']['msisdn'] : NULL;
        $start = isset($_GET['ChargingHistory']['start']) ? $_GET['ChargingHistory']['start'] : NULL;
        $end = isset($_GET['ChargingHistory']['end']) ? $_GET['ChargingHistory']['end'] : NULL;

        if($option == 1){
            $model->conn = 1;
            $model->option = 1;
            if(empty($_GET['ChargingHistory']['msisdn'])){
                $_GET['ChargingHistory']['msisdn'] = $msisdn;
            }
            if(empty($_GET['ChargingHistory']['start'])){
                $_GET['ChargingHistory']['start'] = $start;
            }
            if(empty($_GET['ChargingHistory']['end'])){
                $_GET['ChargingHistory']['end'] = $end;
            }
            if(isset($_GET['ChargingHistory'])){
                $model->attributes = $_GET['ChargingHistory'];
            }

            if(Yii::app()->request->getParam('export')) {
                $this->exportData($model, array('id','msisdn','money','charging_type','trans_id','time_charge'), $this->filename);
                Yii::app()->end();
            }
        }else if($option == 2){
            $model->conn = 2;
            $model->option = 2;
            if(empty($_GET['ChargingHistory']['msisdn'])){
                $_GET['ChargingHistory']['msisdn'] = $msisdn;
            }
            if(empty($_GET['ChargingHistory']['start'])){
                $_GET['ChargingHistory']['start'] = $start;
            }
            if(empty($_GET['ChargingHistory']['end'])){
                $_GET['ChargingHistory']['end'] = $end;
            }
            if(isset($_GET['ChargingHistory'])){
                $model->attributes = $_GET['ChargingHistory'];
            }

            if(Yii::app()->request->getParam('export')) {
                $this->exportData($model, array('id','msisdn','money','charging_type','trans_id','time_charge'), $this->filename);
                Yii::app()->end();
            }
        }else if($option == 3){
            $model->conn = 3;
            $model->option = 3;
            if(empty($_GET['ChargingHistory']['msisdn'])){
                $_GET['ChargingHistory']['msisdn'] = $msisdn;
            }
            if(empty($_GET['ChargingHistory']['start'])){
                $_GET['ChargingHistory']['start'] = $start;
            }
            if(empty($_GET['ChargingHistory']['end'])){
                $_GET['ChargingHistory']['end'] = $end;
            }

            if(isset($_GET['ChargingHistory'])){
                $model->attributes = $_GET['ChargingHistory'];
            }

            if(Yii::app()->request->getParam('export')) {
                $this->exportData($model, array('id','msisdn','money','charging_type','trans_id','time_charge'), $this->filename);
                Yii::app()->end();
            }
        }else{
            $model->charging_type = 9;
            if(isset($_GET['ChargingHistory'])){
                $model->option = 3;
                $model->attributes = $_GET['ChargingHistory'];
            }
        }
		$this->render('admin',array(
			'model'=>$model,
            'option' => $option,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return ChargingHistory the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=ChargingHistory::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param ChargingHistory $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='charging-history-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
