<?php

class SmsMtController extends AccessController
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column1';
    public $module  = 'smsMt';
    public $filename = 'Report_Sms';

    /*public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    public function accessRules()
    {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }*/

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model = new SmsMt;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['SmsMt'])) {
            $model->attributes = $_POST['SmsMt'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        if (isset($_POST['SmsMt'])) {
            $model->attributes = $_POST['SmsMt'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $dataProvider = new CActiveDataProvider('SmsMt');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model = new SmsMt('search');
        $option = isset($_GET['SmsMt']['option']) ? $_GET['SmsMt']['option'] : NULL;
        $msisdn = isset($_GET['SmsMt']['msisdn']) ? $_GET['SmsMt']['msisdn'] : NULL;
        $start = isset($_GET['SmsMt']['start']) ? $_GET['SmsMt']['start'] : NULL;
        $end = isset($_GET['SmsMt']['end']) ? $_GET['SmsMt']['end'] : NULL;

        if($option == 1){
            $model->conn = 1;
            $model->option = 1;
            if(empty($_GET['SmsMt']['msisdn'])){
                $_GET['SmsMt']['msisdn'] = $msisdn;
            }
            if(empty($_GET['SmsMt']['start'])){
                $_GET['SmsMt']['start'] = $start;
            }
            if(empty($_GET['SmsMt']['end'])){
                $_GET['SmsMt']['end'] = $end;
            }
            if(isset($_GET['SmsMt'])){
                $model->attributes = $_GET['SmsMt'];
            }

            if(Yii::app()->request->getParam('export')) {
                $this->exportData($model, array('id','short_code','msisdn','status','type','content','created_datetime','updated_datetime','total_sms'), $this->filename);
                Yii::app()->end();
            }
        }elseif($option == 2){
            $model->conn = 2;
            $model->option = 2;
            if(empty($_GET['SmsMt']['msisdn'])){
                $_GET['SmsMt']['msisdn'] = $msisdn;
            }
            if(empty($_GET['SmsMt']['start'])){
                $_GET['SmsMt']['start'] = $start;
            }
            if(empty($_GET['SmsMt']['end'])){
                $_GET['SmsMt']['end'] = $end;
            }
            if(isset($_GET['SmsMt'])){
                $model->attributes = $_GET['SmsMt'];
            }

            if(Yii::app()->request->getParam('export')) {
                $this->exportData($model, array('id','short_code','msisdn','status','type','content','created_datetime','updated_datetime','total_sms'), $this->filename);
                Yii::app()->end();
            }
        }elseif($option == 3){
            $model->conn = 3;
            $model->option = 3;
            if(empty($_GET['SmsMt']['msisdn'])){
                $_GET['SmsMt']['msisdn'] = $msisdn;
            }
            if(empty($_GET['SmsMt']['start'])){
                $_GET['SmsMt']['start'] = $start;
            }
            if(empty($_GET['SmsMt']['end'])){
                $_GET['SmsMt']['end'] = $end;
            }

            if(isset($_GET['SmsMt'])){
                $model->attributes = $_GET['SmsMt'];
            }

            if(Yii::app()->request->getParam('export')) {
                $this->exportData($model, array('id','short_code','msisdn','status','type','content','created_datetime','updated_datetime','total_sms'), $this->filename);
                Yii::app()->end();
            }
        }else{
            $model->status = 4;
            if(isset($_GET['SmsMt'])){
                $model->attributes = $_GET['SmsMt'];
            }
        }

        $this->render('admin', array(
            'model' => $model,
            'option' => $option,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return SmsMt the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = SmsMt::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param SmsMt $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'sms-mt-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
