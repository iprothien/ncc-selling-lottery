<?php

class ConfigController extends AccessController {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';
    public $module = 'config';

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Config;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Config'])) {
            $model->attributes = $_POST['Config'];
            $file = CUploadedFile::getInstanceByName('Config[value]');
            $cmd = Yii::app()->db->createCommand();
            $cmd->select('id')
                    ->where('value=:value', array(':value' => $file->name))
                    ->from('config');

            $data = $cmd->queryAll();
            $filearray = array();
            $filearray = explode('.', $file->name);
            if ($data)
                $file->name = $filearray[0] . '_01' . $filearray[1];
            $file->saveAs('uploads/cms/config/' . $file->name);
            $file_rename = preg_replace('/\s{1}+/', '', $file->name);
            rename('uploads/cms/config/' . $file->name, 'uploads/cms/config/' . $file_rename);
            $model->value = $file_rename;
            $model->convert_status = '0';

            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Config'])) {
            $model->attributes = $_POST['Config'];
            $file = CUploadedFile::getInstanceByName('Config[value]');
            if (!isset($file)) {
                $model->value = $_POST['old_value'];
            } else {
                $file->saveAs('uploads/cms/config/' . $file->name);
                $filearray = array();
                $filearray = explode('.', $file->name);
                rename('uploads/cms/config/' . $file->name, 'uploads/cms/config/'.$model->value);
                $model->convert_status = '0';
            }
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
/*
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
*/
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('Config');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Config('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Config']))
            $model->attributes = $_GET['Config'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = Config::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'config-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
