<?php

class StaffController extends AccessController {
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */

    public $layout = '//layouts/column2';
    public $module = 'staff';

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Staff;

        if (isset($_POST['Staff'])) {
            $model->attributes = $_POST['Staff'];

            $model->sip_password = $_POST['Staff']['sip_password'];
            $model->username = $_POST['Staff']['username'];
            $model->password = $_POST['Staff']['password'];
            $model->moduleSelected = $_POST['Staff']['moduleSelected'];

            $cmd = Yii::app()->db->createCommand();
            if ($model->save()) {
                if ($model->sip_password) {
                    $cmd->insert('account', array('name' => $model->phone,'canreinvite' => 'yes','context' => 'ncc_ipbx','dtmfmode' => 'inband','host' => 'dynamic','port' => '5060','secret' => $model->sip_password,'type' => 'friend','username' => $model->phone,));
		            //$idAccount = Yii::app()->db->createCommand("SELECT id FROM account WHERE username=".$model->phone)->queryRow();
                    //$cmd->insert('ncc_agent', array('account_id' => $idAccount['id'],'phonenumber' => $model->phone,'name'=>$model->full_name,'status'=>0));
    		    }

                $id = Yii::app()->db->createCommand("SELECT id FROM account WHERE username=".$model->phone)->queryRow();
                if($model->username && $model->password){
                    $cmd->insert('admin', array('account_id' => $id['id'],'username' => $model->username,'password' => md5($model->password),'module' => implode(',', array_values($model->moduleSelected)),'permision' => '0','status' => '1'));
                }
                $this->redirect(array('admin'));
            }
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
        $idAcc = Yii::app()->db->createCommand("SELECT id,secret FROM account WHERE username=".$model->phone)->queryRow();
        $adminUser = Yii::app()->db->createCommand("SELECT username FROM admin WHERE account_id=".$idAcc['id'])->queryRow();
        $model->admin_user = $adminUser['username'];
        $model->sip_pwd = $idAcc['secret'];

        if (isset($_POST['Staff'])) {
            $model->attributes = $_POST['Staff'];
            
            $model->sip_password = $_POST['Staff']['sip_password'];
            $model->pwd = $_POST['Staff']['pwd'];
            $model->oldPhone = $_POST['Staff']['oldPhone'];
            $model->moduleSelected = $_POST['Staff']['moduleSelected'];

            if ($model->save()) {
                $cmd    = Yii::app()->db->createCommand();
                if(isset($model->oldPhone) && $model->oldPhone){
                    $idAccount = Yii::app()->db->createCommand("SELECT id FROM account WHERE username=".$model->phone)->queryRow();
                    if(empty($idAccount)){
                        $this->redirect(array('admin'));
                    }else{
                        $cmd->update('account',array('username'=>$model->oldPhone,'name'=> $model->oldPhone),'id=:id',array(':id' => $idAccount['id']));
                        //$cmd->update('ncc_agent',array('phonenumber'=>$model->oldPhone),'account_id=:account_id',array(':account_id' => $idAccount['id']));
			            $cmd->update('staff',array('phone' => $model->oldPhone),'id=:id',array(':id'=>$id));
                    }
                }

                if(isset($model->sip_password) && $model->sip_password){
                    $idAccount = Yii::app()->db->createCommand("SELECT id FROM account WHERE username=".$model->phone)->queryRow();
                    if(empty($idAccount)){
                        $this->redirect(array('admin'));
                    }else{
                        $cmd->update('account',array('secret'=>$model->sip_password),'id=:id',array(':id' => $idAccount['id']));
                    }

                }

                if(isset($model->pwd) && $model->pwd){
                    $idAccount = Yii::app()->db->createCommand("SELECT id FROM account WHERE username=".$model->phone)->queryRow();
                    if(empty($idAccount)){
                        $this->redirect(array('admin'));
                    }else{
                        $cmd->update('admin',array('password'=>md5($model->pwd)),'account_id=:account_id',array(':account_id' => $idAccount['id']));
                    }
                }

                if(isset($model->moduleSelected) && $model->moduleSelected){
                    $idAccount = Yii::app()->db->createCommand("SELECT id FROM account WHERE username=".$model->phone)->queryRow();
                    if(empty($idAccount)){
                        $this->redirect(array('admin'));
                    }else{
                        $cmd->update('admin',array('module'=>implode(',', array_values($model->moduleSelected))),'account_id=:account_id',array(':account_id' => $idAccount['id']));
                    }
                }
                $this->redirect(array('admin'));
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $model = $this->loadModel($id);
        $idAccount = Yii::app()->db->createCommand("SELECT id FROM account WHERE username=".$model->phone)->queryRow();
        if($idAccount){
            Yii::app()->db->CreateCommand("DELETE FROM admin WHERE account_id=".$idAccount['id'])->execute();
            Yii::app()->db->CreateCommand("DELETE FROM account WHERE id=".$idAccount['id'])->execute();
            //Yii::app()->db->createCommand("DELETE FROM ncc_agent WHERE account_id=".$idAccount['id'])->execute();
        }
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Staff('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Staff']))
            $model->attributes = $_GET['Staff'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = Staff::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'staff-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
