<?php

class StatisticController extends AccessController
{

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column1';
    public $module = 'statistic';

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model = new Statistic();
        //$model->unsetAttributes();  // clear any default values);
        $time_start = isset($_GET['Statistic']['time_start']) ? $_GET['Statistic']['time_start'] : NULL;
        $time_end = isset($_GET['Statistic']['time_end']) ? $_GET['Statistic']['time_end'] : NULL;
        $drawEnd = isset($_GET['Statistic']['draw_no_end']) ? $_GET['Statistic']['draw_no_end'] : NULL;
        $drawStart = isset($_GET['Statistic']['draw_no_start']) ? $_GET['Statistic']['draw_no_start'] : NULL;
        $month = isset($_GET['Statistic']['month']) ? $_GET['Statistic']['month'] : NULL;
        $year = isset($_GET['Statistic']['year']) ? $_GET['Statistic']['year'] : NULL;
        $model->month = $month;
        $model->year = $year;
        $extMonth = "";
        $ext = 'WHERE md.status=1';
        if ($time_start)
            $ext .= ' AND md.created_datetime > "' . $time_start . '"';
        if ($time_end)
            $ext .= ' AND md.created_datetime < "' . $time_end . '"';
        if ($drawStart&&!$drawEnd) {
            $ext .= ' AND dt.draw_no =' . $drawStart;
            $model->draw_no_start = $drawStart;
        }
        if ($drawStart&&$drawEnd) {
            $extMonth .= " AND dt.draw_no >=$drawStart AND dt.draw_no <=$drawEnd" ;
            $model->draw_no_start = $drawStart;
            $model->draw_no_end = $drawEnd;
        }
       
        $charTotal = array();
        if($month&&$year){
            $model->month = $month;
            $model->year = $year;
            $timeStart = "$year-$month-01";
            $timeEnd = "$year-$month-31";
            $extMonth .= ' AND dt.start_time < "' . $timeEnd . '"';
            $extMonth .= ' AND dt.start_time > "' . $timeStart . '"';
            
        }
        if(($month&&$year)||($drawStart&&$drawEnd)){
            $sqlDraw = "SELECT dt.draw_no , SUM( money ) AS total_money "
                    . " FROM `member_draw` as md INNER JOIN draw_time as dt ON md.draw_id = dt.id "
                    . " WHERE 1=1 {$extMonth} AND (md.type=1 || md.type=2) GROUP BY md.draw_no ORDER BY md.draw_no DESC";
            //exit();
            $cmdCharEtl = Yii::app()->ncc_etl->createCommand($sqlDraw);
            $charEtl = $cmdCharEtl->queryAll();
            $cmdCharUnitel = Yii::app()->ncc_unitel->createCommand($sqlDraw);
            $charUnitel = $cmdCharUnitel->queryAll();
            $charTotal = array();
            foreach($charEtl as $char){
                $charTotal[$char['draw_no']]['draw_no'] = $char['draw_no'];
                $charTotal[$char['draw_no']]['total_money'] = $char['total_money'];
            }
            foreach($charUnitel as $char){
                $charTotal[$char['draw_no']]['draw_no'] = $char['draw_no'];
                $charTotal[$char['draw_no']]['total_money'] += $char['total_money'];
            }
        }

        $sql_etl = 'SELECT type , SUM( money ) AS total_money '
                . ' FROM `member_draw` as md INNER JOIN draw_time as dt ON md.draw_id = dt.id ' . $ext . $extMonth. ' AND (md.type=1 || md.type=2) GROUP BY md.type ORDER BY md.type';
        $cmd_etl = Yii::app()->ncc_etl->createCommand($sql_etl);
        $data_etl = $cmd_etl->queryAll();


        foreach ($data_etl as $data) {
            if ($data['type'] == 1) $model->etl_2 = $data['total_money'];
            else  $model->etl_3 = $data['total_money'];
        }

        $model->total_etl = $model->etl_2 + $model->etl_3;

        /* BEELINE
        $cmd_bee = Yii::app()->ncc_beeline->createCommand($sql_etl);
        $data_bee = $cmd_bee->queryAll();

        foreach ($data_bee as $data) {
            if ($data['type'] == 1) $model->beeline_2 = $data['total_money'];
            else  $model->beeline_3 = $data['total_money'];
        }

        $model->total_beeline = $model->beeline_2 + $model->beeline_3;
	  */
        /* UNITEL */
        $cmd_unitel = Yii::app()->ncc_unitel->createCommand($sql_etl);
        $data_unitel = $cmd_unitel->queryAll();

        foreach ($data_unitel as $data) {
            if ($data['type'] == 1) $model->unitel_2 = $data['total_money'];
            else  $model->unitel_3 = $data['total_money'];
        }
        $model->total_unitel = $model->unitel_2 + $model->unitel_3;

        if (isset($_GET['Statistic'])) {
            $model->time_start = $_GET['Statistic']['time_start'];
            $model->time_end = $_GET['Statistic']['time_end'];
        }

        $this->render('admin', array(
            'model' => $model,
            'charTotal'=>$charTotal,
            'month'=>$month,
            'year'=>$year,
        ));
    }


    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'statistic-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}