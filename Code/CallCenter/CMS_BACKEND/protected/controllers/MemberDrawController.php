<?php

class MemberDrawController extends AccessController {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column1';
    public $module = 'memberDraw';
    public $filename = 'Member_Draw';

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        if (isset($_GET['MemberEtl']['option']))
            $option = $_GET['MemberEtl']['option'];
        else if (isset($_GET['MemberBeeline']['option']))
            $option = $_GET['MemberBeeline']['option'];
        else if (isset($_GET['MemberUnitel']['option']))
            $option = $_GET['MemberUnitel']['option'];
        else
            $option = NULL;
        $winstatus = isset($_GET['MemberEtl']['win_status']) ? $_GET['MemberEtl']['win_status'] : NULL;
        $msisdn = isset($_GET['MemberEtl']['msisdn']) ? $_GET['MemberEtl']['msisdn'] : NULL;
        $time_start = isset($_GET['MemberEtl']['time_start']) ? $_GET['MemberEtl']['time_start'] : NULL;
        $time_end = isset($_GET['MemberEtl']['time_end']) ? $_GET['MemberEtl']['time_end'] : NULL;
        $agent_id = isset($_GET['MemberEtl']['agent_id']) ? $_GET['MemberEtl']['agent_id'] : NULL;

        if ($option == '1') {
            if (empty($_GET['MemberBeeline']['msisdn'])) {
                $_GET['MemberBeeline']['msisdn'] = $msisdn;
            }
            
            if (empty($_GET['MemberBeeline']['win_status'])) {
                $_GET['MemberBeeline']['win_status'] = $winstatus;
            }

            if (empty($_GET['MemberBeeline']['time_end'])) {
                $_GET['MemberBeeline']['time_end'] = $time_end;
            }

            if (empty($_GET['MemberBeeline']['time_start'])) {
                $_GET['MemberBeeline']['time_start'] = $time_start;
            }
            
            if (empty($_GET['MemberBeeline']['agent_id'])) {
                $_GET['MemberBeeline']['agent_id'] = $agent_id;
            }

            $model = new MemberBeeline('search');
            $model->unsetAttributes();  // clear any default values
            if (isset($_GET['MemberBeeline'])) {
                $model->option = $option;
                $model->attributes = $_GET['MemberBeeline'];
            }

            if (Yii::app()->request->getParam('export')) {
                $this->exportData($model, array('msisdn', 'code', 'money', 'created_datetime'), $this->filename);
                Yii::app()->end();
            }
        } elseif ($option == '2') {
            if (empty($_GET['MemberEtl']['msisdn'])) {
                $_GET['MemberEtl']['msisdn'] = $msisdn;
            }
            
            if (empty($_GET['MemberEtl']['win_status'])) {
                $_GET['MemberEtl']['win_status'] = $winstatus;
            }

            if (empty($_GET['MemberEtl']['time_end'])) {
                $_GET['MemberEtl']['time_end'] = $time_end;
            }

            if (empty($_GET['MemberEtl']['time_start'])) {
                $_GET['MemberEtl']['time_start'] = $time_start;
            }
            
            if (empty($_GET['MemberEtl']['agent_id'])) {
                $_GET['MemberEtl']['agent_id'] = $agent_id;
            }

            $model = new MemberEtl('search');
            $model->unsetAttributes();  // clear any default values
            if (isset($_GET['MemberEtl'])) {
                $model->option = $option;
                $model->attributes = $_GET['MemberEtl'];
            }

            if (Yii::app()->request->getParam('export')) {
                $this->exportData($model, array('msisdn', 'code', 'money', 'created_datetime'), $this->filename);
                Yii::app()->end();
            }
        } else if ($option == '3') {
            if (empty($_GET['MemberUnitel']['msisdn'])) {
                $_GET['MemberUnitel']['msisdn'] = $msisdn;
            }
            
            if (empty($_GET['MemberUnitel']['win_status'])) {
                $_GET['MemberUnitel']['win_status'] = $winstatus;
            }

            if (empty($_GET['MemberUnitel']['time_end'])) {
                $_GET['MemberUnitel']['time_end'] = $time_end;
            }

            if (empty($_GET['MemberUnitel']['time_start'])) {
                $_GET['MemberUnitel']['time_start'] = $time_start;
            }
            
            if (empty($_GET['MemberUnitel']['agent_id'])) {
                $_GET['MemberUnitel']['agent_id'] = $agent_id;
            }

            $model = new MemberUnitel('search');
            $model->unsetAttributes();  // clear any default values
            if (isset($_GET['MemberUnitel'])) {
                $model->option = $option;
                $model->attributes = $_GET['MemberUnitel'];
            }

            if (Yii::app()->request->getParam('export')) {
                $this->exportData($model, array('msisdn', 'code', 'money', 'created_datetime'), $this->filename);
                Yii::app()->end();
            }
        } else {
            $model = new MemberEtl('search');
            $model->unsetAttributes();  // clear any default values
        }

        $this->render('admin', array(
            'model' => $model,
            'option' => $option
        ));
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'member-draw-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}