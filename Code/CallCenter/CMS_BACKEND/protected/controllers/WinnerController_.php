<?php

class WinnerController extends AccessController {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';
    public $module = 'winner';
    public $filename = 'Winner';

    public function actionView($id, $msisdn) {
        if (substr($msisdn, 0, 3) == '207' || substr($msisdn, 0, 3) == '307') {
            $model = $this->loadModel($id);
            $model->connect = 1;
        } else if (substr($msisdn, 0, 3) == '202' || substr($msisdn, 0, 3) == '302') {
            $model = $this->loadModel($id);
            $model->connect = 2;
        } else if (substr($msisdn, 0, 3) == '209' || substr($msisdn, 0, 3) == '309') {
            $model = $this->loadModel($id);
            $model->connect = 3;
        }

        $this->render('view', array(
            'model' => $model,
        ));
    }

    public function actionUpdate($id, $msisdn) {
        $processor = Processor::model()->findByAttributes(array('account_id' => Yii::app()->user->id));
        if (!$processor)
            $this->redirect('site/error');
        if (substr($msisdn, 0, 3) == '207' || substr($msisdn, 0, 3) == '307') {
            $model = $this->loadModel($id);
            $model->connect = 1;

            if (isset($_POST['Winner'])) {
                $model->attributes = $_POST['Winner'];
                $model->processor_id = $processor->id;
                $model->payment_date = date('Y-m-d H:i:s', time());
                $model->payment_status = $_POST['Winner']['payment_status'];
                $model->full_name = $_POST['Winner']['full_name'];
                $model->description = $_POST['Winner']['description'];
                if ($model->save())
                    $this->redirect(array('admin'));
            }
        }else if (substr($msisdn, 0, 3) == '202' || substr($msisdn, 0, 3) == '302') {
            $model = $this->loadModel($id);
            $model->connect = 1;

            if (isset($_POST['Winner'])) {
                $model->attributes = $_POST['Winner'];
                $model->processor_id = $processor->id;
                $model->payment_date = date('Y-m-d H:i:s', time());
                $model->payment_status = $_POST['Winner']['payment_status'];
                $model->full_name = $_POST['Winner']['full_name'];
                $model->description = $_POST['Winner']['description'];
                if ($model->save())
                    $this->redirect(array('admin'));
            }
        } else if (substr($msisdn, 0, 3) == '209' || substr($msisdn, 0, 3) == '309') {
            $model = $this->loadModel($id);
            $model->connect = 1;

            if (isset($_POST['Winner'])) {
                $model->attributes = $_POST['Winner'];
                $model->processor_id = $processor->id;
                $model->payment_date = date('Y-m-d H:i:s', time());
                $model->payment_status = $_POST['Winner']['payment_status'];
                $model->full_name = $_POST['Winner']['full_name'];
                $model->description = $_POST['Winner']['description'];
                if ($model->save())
                    $this->redirect(array('admin'));
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Payment by BANK
     */
    public function actionPaynow() {
        $model = new Winner('search');

        $msisdn = isset($_GET['Winner']['msisdn']) ? $_GET['Winner']['msisdn'] : NULL;

        if (substr($msisdn, 0, 3) == '207' || substr($msisdn, 0, 3) == '207') {
            if (empty($_GET['Winner']['msisdn'])) {
                $_GET['Winner']['msisdn'] = $msisdn;
            }

            $model = new Winner('search');
            $model->unsetAttributes();  // clear any default values
            if (isset($_GET['Winner'])) {
                $model->attributes = $_GET['Winner'];
            }
        } else if (substr($msisdn, 0, 3) == '202' || substr($msisdn, 0, 3) == '302') {
            if (empty($_GET['WinnerEtl']['msisdn'])) {
                $_GET['WinnerEtl']['msisdn'] = $msisdn;
            }

            $model = new WinnerEtl('search');
            $model->unsetAttributes();  // clear any default values
            if (isset($_GET['WinnerEtl'])) {
                $model->attributes = $_GET['WinnerEtl'];
            }
        } else if (substr($msisdn, 0, 3) == '209' || substr($msisdn, 0, 3) == '309') {
            if (empty($_GET['WinnerUnitel']['msisdn'])) {
                $_GET['WinnerUnitel']['msisdn'] = $msisdn;
            }

            $model = new WinnerUnitel('search');
            $model->unsetAttributes();  // clear any default values
            if (isset($_GET['WinnerUnitel'])) {
                $model->attributes = $_GET['WinnerUnitel'];
            }
        } else {
            $model = new Winner('search');
            $model->unsetAttributes();
        }

        $this->render('payment', array(
            'model' => $model,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        if (isset($_GET['Winner']['option']) && $_GET['Winner']['option'])
            $option = $_GET['Winner']['option'];
        else if (isset($_GET['WinnerEtl']['option']) && $_GET['WinnerEtl']['option'])
            $option = $_GET['WinnerEtl']['option'];
        else if (isset($_GET['WinnerUnitel']['option']) && $_GET['WinnerUnitel']['option'])
            $option = $_GET['WinnerUnitel']['option'];
        else
            $option = NULL;

        $msisdn = isset($_GET['Winner']['msisdn']) ? $_GET['Winner']['msisdn'] : NULL;
        $time_start = isset($_GET['Winner']['time_start']) ? $_GET['Winner']['time_start'] : NULL;
        $time_end = isset($_GET['Winner']['time_end']) ? $_GET['Winner']['time_end'] : NULL;

        if (isset($option) && $option == '1') {
            if (empty($_GET['Winner']['msisdn'])) {
                $_GET['Winner']['msisdn'] = $msisdn;
            }

            if (empty($_GET['Winner']['time_end'])) {
                $_GET['Winner']['time_end'] = $time_end;
            }

            if (empty($_GET['Winner']['time_start'])) {
                $_GET['Winner']['time_start'] = $time_start;
            }

            $model = new Winner('search');
            $model->unsetAttributes();  // clear any default values
            if (isset($_GET['Winner'])) {
                $model->option = $option;
                $model->attributes = $_GET['Winner'];
            }

            if (Yii::app()->request->getParam('export')) {
                $this->exportData($model, array('msisdn', 'drawNo', 'timeStart', 'timeEnd', 'code', 'money'), $this->filename);
                Yii::app()->end();
            }
        } else if (isset($option) && $option == '2') {
            if (empty($_GET['WinnerEtl']['msisdn'])) {
                $_GET['WinnerEtl']['msisdn'] = $msisdn;
            }

            if (empty($_GET['WinnerEtl']['time_end'])) {
                $_GET['WinnerEtl']['time_end'] = $time_end;
            }

            if (empty($_GET['WinnerEtl']['time_start'])) {
                $_GET['WinnerEtl']['time_start'] = $time_start;
            }

            $model = new WinnerEtl('search');
            $model->unsetAttributes();  // clear any default values
            if (isset($_GET['WinnerEtl'])) {
                $model->option = $option;
                $model->attributes = $_GET['WinnerEtl'];
            }

            if (Yii::app()->request->getParam('export')) {
                $this->exportData($model, array('msisdn', 'drawNo', 'timeStart', 'timeEnd', 'code', 'money'), $this->filename);
                Yii::app()->end();
            }
        } else if (isset($option) && $option == '3') {
            if (empty($_GET['WinnerUnitel']['msisdn'])) {
                $_GET['WinnerUnitel']['msisdn'] = $msisdn;
            }

            if (empty($_GET['WinnerUnitel']['time_end'])) {
                $_GET['WinnerUnitel']['time_end'] = $time_end;
            }

            if (empty($_GET['WinnerUnitel']['time_start'])) {
                $_GET['WinnerUnitel']['time_start'] = $time_start;
            }

            $model = new WinnerUnitel('search');
            $model->unsetAttributes();  // clear any default values
            if (isset($_GET['WinnerUnitel'])) {
                $model->option = $option;
                $model->attributes = $_GET['WinnerUnitel'];
            }

            if (Yii::app()->request->getParam('export')) {
                $this->exportData($model, array('msisdn', 'drawNo', 'timeStart', 'timeEnd', 'code', 'money'), $this->filename);
                Yii::app()->end();
            }
        } else {
            $model = new Winner('search');
            $model->unsetAttributes();
        }

        $this->render('admin', array(
            'model' => $model,
            'option' => $option
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Winner the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Winner::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Winner $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'winner-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
