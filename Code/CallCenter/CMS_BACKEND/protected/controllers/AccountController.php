<?php

class AccountController extends AccessController {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';
    public $module  = 'account';

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Account;
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Account'])) {
            $model->attributes = $_POST['Account'];
            $model->name = $_POST['Account']['username'];
            $model->canreinvite = 'yes';
            $model->context = 'call_center';
            $model->dtmfmode = 'inband';
            $model->host = 'dynamic';
            $model->port = '5060';
            $model->secret = $_POST['Account']['secret'];
            $model->type = 'friend';
            $model->username = $_POST['Account']['username'];

            $model->user = $_POST['Account']['user'];
            $model->password = $_POST['Account']['password'];
            $model->moduleSelected = $_POST['Account']['moduleSelected'];

            $cmd = Yii::app()->db->createCommand();
            if ($model->save()){
                $cmd->insert('ncc_agent', array('account_id' => $model->id,'phonenumber' => $model->username,'name'=>$model->user,'status'=>0));
                if($model->user && $model->password){
                    $cmd->insert('admin', array(
                        'account_id' => $model->id,
                        'username' => $model->user,
                        'password' => md5($model->password),
                        'module' => implode(',', array_values($model->moduleSelected)),
                        'permision' => '0',
                        'status' => '1')
                    );
                }
                $this->redirect(array('admin'));
            }
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
        $userAdmin = Yii::app()->db->createCommand("SELECT username FROM admin WHERE account_id=".$id)->queryRow();
        $model->admin_user = $userAdmin['username'];
        //$getStaffAccount = Yii::app()->db->createCommand("SELECT username FROM account WHERE id=".$id)->queryRow();
        //$getStaff = Yii::app()->db->createCommand("SELECT id,phone FROM staff WHERE phone=".$getStaffAccount['username'])->queryRow();

        if (isset($_POST['Account'])) {
            $model->attributes = $_POST['Account'];
            $model->secret = $_POST['Account']['secret'];
            $model->oldUsername = $_POST['Account']['oldUsername'];

            $model->re_password = $_POST['Account']['re_password'];
            $model->moduleSelected = $_POST['Account']['moduleSelected'];

            if ($model->save()){
                $cmd    = Yii::app()->db->createCommand();
                if(isset($model->oldUsername) && $model->oldUsername){
                    $cmd->update('account',array('name'=>$model->oldUsername,'username' =>$model->oldUsername),'id=:id',array(':id'=>$id));
                    $cmd->update('ncc_agent',array('phonenumber'=>$model->oldUsername),'account_id=:account_id',array(':account_id' => $id));
	            }

                if(isset($model->re_password) && $model->re_password){
                    $cmd->update('admin',array('password'=>md5($model->re_password)),'account_id=:account_id',array(':account_id' => $id));
                }

                if(isset($model->moduleSelected) && $model->moduleSelected){
                    $cmd->update('admin',array('module'=>implode(',', array_values($model->moduleSelected))),'account_id=:account_id',array(':account_id' => $id));
                }
                $this->redirect(array('admin'));
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $getStaffAccount = Yii::app()->db->createCommand("SELECT username FROM account WHERE id=".$id)->queryRow();
        $getStaff = Yii::app()->db->createCommand("SELECT id,phone FROM staff WHERE phone=".$getStaffAccount['username'])->queryRow();

        Yii::app()->db->createCommand("DELETE FROM admin WHERE account_id=".$id)->execute();
        Yii::app()->db->createCommand("DELETE FROM ncc_agent WHERE account_id=".$id)->execute();
        if($getStaffAccount['username']==$getStaff['phone']){
            Yii::app()->db->createCommand("DELETE FROM staff WHERE id=".$getStaff['id'])->execute();
        }
        Yii::app()->db->createCommand("DELETE FROM account WHERE id=".$id)->execute();
        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('Account');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Account('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Account']))
            $model->attributes = $_GET['Account'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = Account::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'account-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
