<?php

class CdrController extends AccessController {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column1';
    public $module  = 'cdr';
    public $filename = 'Smsmo';

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        if(isset($_GET['Cdr']['option']))
            $option = $_GET['Cdr']['option'];
        else if(isset($_GET['CdrEtl']['option']))
            $option = $_GET['CdrEtl']['option'];
        else if(isset($_GET['CdrUnitel']['option']))
            $option = $_GET['CdrUnitel']['option'];
        else
            $option = NULL;

        if(isset($_GET['Cdr']['accountcode']))
            $accountcode = $_GET['Cdr']['accountcode'];
        else if(isset($_GET['CdrEtl']['accountcode']))
            $accountcode = $_GET['CdrEtl']['accountcode'];
        else if(isset($_GET['CdrUnitel']['accountcode']))
            $accountcode = $_GET['CdrUnitel']['accountcode'];
        
        if(isset($_GET['Cdr']['src']))
            $src = $_GET['Cdr']['src'];
        else if(isset($_GET['CdrEtl']['src']))
            $src = $_GET['CdrEtl']['src'];
        else if(isset($_GET['CdrUnitel']['src']))
            $src = $_GET['CdrUnitel']['src'];
        
        if(isset($_GET['Cdr']['time_start']))
            $time_start = $_GET['Cdr']['time_start'];
        else if(isset($_GET['CdrEtl']['time_start']))
            $time_start = $_GET['CdrEtl']['time_start'];
        else if(isset($_GET['CdrUnitel']['time_start']))
            $time_start = $_GET['CdrUnitel']['time_start'];
        
        if(isset($_GET['Cdr']['time_end']))
            $time_end = $_GET['Cdr']['time_end'];
        else if(isset($_GET['CdrEtl']['time_end']))
            $time_end = $_GET['CdrEtl']['time_end'];
        else if(isset($_GET['CdrUnitel']['time_end']))
            $time_end = $_GET['CdrUnitel']['time_end'];
        
        if ($option == '1') {
            if (empty($_GET['Cdr']['accountcode'])) {
                $_GET['Cdr']['accountcode']     = $accountcode;
            }

            if (empty($_GET['Cdr']['msisdn'])) {
                $_GET['Cdr']['src']     = $src;
            }

            if (empty($_GET['Cdr']['time_end'])) {
                $_GET['Cdr']['time_end'] = $time_end;
            }

            if (empty($_GET['Cdr']['time_start'])) {
                $_GET['Cdr']['time_start'] = $time_start;
            }

            $model = new Cdr('search');
            $model->unsetAttributes();  // clear any default values
            if (isset($_GET['Cdr'])) {
                $model->option  = $option;
                $model->attributes = $_GET['Cdr'];
            }
            
            if (Yii::app()->request->getParam('export')) {
                $this->exportData($model, array('start', 'src', 'dst', 'duration','accountcode'), $this->filename);
                Yii::app()->end();
            }
        } elseif ($option == '2') {
            if (empty($_GET['CdrEtl']['accountcode'])) {
                $_GET['CdrEtl']['accountcode']     = $accountcode;
            }

            if (empty($_GET['CdrEtl']['msisdn'])) {
                $_GET['CdrEtl']['src']  = $src;
            }

            if (empty($_GET['CdrEtl']['time_end'])) {
                $_GET['CdrEtl']['time_end'] = $time_end;
            }

            if (empty($_GET['CdrEtl']['time_start'])) {
                $_GET['CdrEtl']['time_start'] = $time_start;
            }

            $model = new CdrEtl('search');
            $model->unsetAttributes();  // clear any default values
            if (isset($_GET['CdrEtl'])) {
                $model->option  = $option;
                $model->attributes = $_GET['CdrEtl'];
            }
            
            if (Yii::app()->request->getParam('export')) {
                $this->exportData($model, array('start', 'src', 'dst', 'duration','accountcode'), $this->filename);
                Yii::app()->end();
            }
        } elseif ($option == '3') {
            if (empty($_GET['CdrUnitel']['accountcode'])) {
                $_GET['CdrUnitel']['accountcode']     = $accountcode;
            }

            if (empty($_GET['CdrUnitel']['msisdn'])) {
                $_GET['CdrUnitel']['src']  = $src;
            }

            if (empty($_GET['CdrUnitel']['time_end'])) {
                $_GET['CdrUnitel']['time_end'] = $time_end;
            }

            if (empty($_GET['CdrUnitel']['time_start'])) {
                $_GET['CdrUnitel']['time_start'] = $time_start;
            }

            $model = new CdrUnitel('search');
            $model->unsetAttributes();  // clear any default values
            if (isset($_GET['CdrUnitel'])) {
                $model->option  = $option;
                $model->attributes = $_GET['CdrUnitel'];
            }
            
            if (Yii::app()->request->getParam('export')) {
                $this->exportData($model, array('start', 'src', 'dst', 'duration','accountcode'), $this->filename);
                Yii::app()->end();
            }
        } else {
            $model = new Cdr('search');
            $model->unsetAttributes();  // clear any default values
        }

        $this->render('admin', array(
            'model' => $model,
            'option' => $option
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = Cdr::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'cdr-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
