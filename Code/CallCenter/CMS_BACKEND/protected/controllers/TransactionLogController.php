<?php

class TransactionLogController extends AccessController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';
        public $module  = 'transactionLog';
	public $filename = 'Report_transactionLog';

	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	public function actionIndex()
    {
        $dataProvider = new CActiveDataProvider('TransactionLog');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }
	public function actionAdmin() 
       {
        $model=new TransactionLog('search');

        $option = isset($_GET['TransactionLog']['option']) ? $_GET['TransactionLog']['option'] : NULL;
        $msisdn = isset($_GET['TransactionLog']['msisdn']) ? $_GET['TransactionLog']['msisdn'] : NULL;
        $buy_type = isset($_GET['TransactionLog']['buy_type']) ? $_GET['TransactionLog']['buy_type'] : NULL;
        $status = isset($_GET['TransactionLog']['status']) ? $_GET['TransactionLog']['status'] : NULL;
        $start = isset($_GET['TransactionLog']['start']) ? $_GET['TransactionLog']['start'] : NULL;
        $end = isset($_GET['TransactionLog']['end']) ? $_GET['TransactionLog']['end'] : NULL;

        if($option == 1){
            $model->connect = 1;
            $model->option = 1;
            if(empty($_GET['TransactionLog']['msisdn'])){
                $_GET['TransactionLog']['msisdn'] = $msisdn;
            }
            if(empty($_GET['TransactionLog']['buy_type'])){
                $_GET['TransactionLog']['buy_type'] = $buy_type;
            }
            if(empty($_GET['TransactionLog']['status'])){
                $_GET['TransactionLog']['status'] = $status;
            }
            if(empty($_GET['TransactionLog']['start'])){
                $_GET['TransactionLog']['start'] = $start;
            }
            if(empty($_GET['TransactionLog']['end'])){
                $_GET['TransactionLog']['end'] = $end;
            }
            if(isset($_GET['TransactionLog'])){
                $model->attributes = $_GET['TransactionLog'];
            }

             if(Yii::app()->request->getParam('export')) {
                $this->exportData($model, array('id','msisdn','status','buy_type','content','created_datetime'), $this->filename);
                Yii::app()->end();
            }
        }else if($option == 2){
            $model->connect = 2;
            $model->option = 2;
            if(empty($_GET['TransactionLog']['msisdn'])){
                $_GET['TransactionLog']['msisdn'] = $msisdn;
            }
             if(empty($_GET['TransactionLog']['buy_type'])){
                $_GET['TransactionLog']['buy_type'] = $buy_type;
            }
            if(empty($_GET['TransactionLog']['status'])){
                $_GET['TransactionLog']['status'] = $status;
            }
            if(empty($_GET['TransactionLog']['start'])){
                $_GET['TransactionLog']['start'] = $start;
            }
            if(empty($_GET['TransactionLog']['end'])){
                $_GET['TransactionLog']['end'] = $end;
            }
            if(isset($_GET['TransactionLog'])){
                $model->attributes = $_GET['TransactionLog'];
            }

             if(Yii::app()->request->getParam('export')) {
                $this->exportData($model, array('id','msisdn','status','buy_type','content','created_datetime'), $this->filename);
                Yii::app()->end();
            }
        }else if($option == 3){
            $model->connect = 3;
            $model->option = 3;
            if(empty($_GET['TransactionLog']['msisdn'])){
                $_GET['TransactionLog']['msisdn'] = $msisdn;
            }
             if(empty($_GET['TransactionLog']['buy_type'])){
                $_GET['TransactionLog']['buy_type'] = $buy_type;
            }
            if(empty($_GET['TransactionLog']['status'])){
                $_GET['TransactionLog']['status'] = $status;
            }
            if(empty($_GET['TransactionLog']['start'])){
                $_GET['TransactionLog']['start'] = $start;
            }
            if(empty($_GET['TransactionLog']['end'])){
                $_GET['TransactionLog']['end'] = $end;
            }

            if(isset($_GET['TransactionLog'])){
                $model->attributes = $_GET['TransactionLog'];
            }

            if(Yii::app()->request->getParam('export')) {
                $this->exportData($model, array('id','msisdn','status','buy_type','content','created_datetime'), $this->filename);
                Yii::app()->end();
            }
        }else{
            $model->status = 9;
            if(isset($_GET['TransactionLog'])){
                $model->option = 3;
                $model->attributes = $_GET['TransactionLog'];
            }
        }
		$this->render('admin',array(
			'model'=>$model,
                        'option' => $option,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return TransactionLog the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model= TransactionLog::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param TransactionLog $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='transaction-log-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
