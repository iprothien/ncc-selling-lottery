<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');
// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'NCC SELLING LOTTERY',
    // preloading 'log' component
    'preload' => array('log'),
    // autoloading model and component classes
    'import' => array(
        'application.models.*',
        'application.components.*',
        'application.extensions.highcharts.*',
    ),
    'modules' => array(
        // uncomment the following to enable the Gii tool

        'gii' => array(
            'class' => 'system.gii.GiiModule',
            'password' => '12345',
            // If removed, Gii defaults to localhost only. Edit carefully to taste.
            'ipFilters' => array('127.0.0.1', '::1'),
        ),
    ),
    // application components
    'components' => array(
	 'urlManager' => array(
            'rules' => array(
                'member/admin/<export>' => 'member/admin',
		'winner/update/<id:\d+>-<msisdn:\w+>' => 'winner/update',
                'winner/view/<id:\d+>-<msisdn:\w+>' => 'winner/view',
            ),
        ),
        'user' => array(
            // enable cookie-based authentication
            'class' => 'UWebUser'
        ),
        'db' => array(
            'connectionString' => 'mysql:host=localhost;dbname=ncc',
            'emulatePrepare' => true,
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8',
        ),
        'ncc_etl' => array(
            'class'=>'CDbConnection',
            'connectionString' => 'mysql:host=202.62.111.233;dbname=lotto',
            'emulatePrepare' => true,
            'username' => 'iprodev',
            'password' => 'ipro@2014',
            'charset' => 'utf8',
        ),
        'ncc_beeline' => array(
            'class'=>'CDbConnection',
            'connectionString' => 'mysql:host=115.84.105.43;dbname=lotto',
            'emulatePrepare' => true,
            'username' => 'ipro',
            'password' => 'ipro@2014',
            'charset' => 'utf8',
        ),
	 'ncc_unitel' => array(
            'class'=>'CDbConnection',
            'connectionString' => 'mysql:host=183.182.100.131;dbname=lotto',
            'emulatePrepare' => true,
            'username' => 'iprodev',
            'password' => 'ipro@2014',
            'charset' => 'utf8',
        ),
        'errorHandler' => array(
            // use 'site/error' action to display errors
            'errorAction' => 'site/error',
        ),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning',
                ),
            // uncomment the following to show log messages on web pages
            /*
              array(
              'class'=>'CWebLogRoute',
              ),
             */
            ),
        ),
    ),
    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params' => array(
        // this is used in contact page
        'adminEmail' => 'webmaster@example.com',
        'module' => array(
            'user' => array(
                'name' => 'User',
                'url' => array('/user/admin')
            ),
            'account' => array(
                'name' => 'Agent',
                'url' => array('/account/admin')
            ),
	        'nccAgentAnswer' => array(
                'name' => 'Call Note',
                'url' => array('/nccAgentAnswer/admin')
            ),
            'staff' => array(
                'name' => 'Staff',
                'url' => array('/staff/admin')
            ),

            'config' => array(
                'name' => 'Configuration',
                'url' => array('/config/admin')
            ),
	     'timeOff' => array(
                'name' => 'Working Time',
                'url' => array('/timeOff/admin')
            ),

            'statistic' => array(
                'name' => 'Statitic',
                'url' => array('/statistic/admin')
            ),
            'memberDraw' => array(
                'name' => 'History',
                'url' => array('memberDraw/admin')
            ),
            'drawTime' => array(
                'name' => 'Raw time',
                'url' => array('drawTime/admin')
            ),
            'drawResult' => array(
                'name' => 'Result',
                'url' => array('drawResult/admin')
            ),
	     'smsMt' => array(
                'name' => 'SMS MT',
                'url' => array('smsMt/admin')
            ),
            'transactionLog' => array(
                'name' => 'Transaction Log',
                'url' => array('transactionLog/admin')
            ),
            'chargingHistory' => array(
                'name' => 'Charging',
                'url' => array('chargingHistory/admin')
            ),
	     /*
	     'processor' => array(
                'name' => 'Processor',
                'url' => array('/processor/admin')
            ),
	    */
	     'winner' => array(
                'name' => 'Winner',
                'url' => array('winner/admin')
            ),
	     'serviceStatus'=>array(
                'name' => 'Service',
            ),
	     'cdr'=>array(
                'name' => 'Cdr',
	         'url' => array('cdr/admin')
            ),

            'callCenter' => array(
                'name' => 'Call Center',
            ),
	     'province'=>array(
                'name' => 'Province',
                'url' => array('province2/admin')
            ),
	     'callService' => array(
                'name' => 'Outbound',
                'url' => array('callService/admin')
            ),
        ),
	'service' => array(
            'serviceStatus'=>array(
                'name' => 'Service',
            ),
            'callCenter' => array(
                'name' => 'Call Center',
            ),
        ),
    ),
);