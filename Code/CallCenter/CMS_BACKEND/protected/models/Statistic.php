<?php

/**
 * This is the model class for table "member_draw".
 *
 * The followings are the available columns in table 'member_draw':
 * @property integer $id
 * @property string $msisdn
 * @property string $code
 * @property integer $money
 * @property string $created_datetime
 * @property integer $type
 * @property integer $status
 */
class Statistic extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return MemberDraw the static model class
     */
    public $time_start,$time_end,$draw_no,$month,$year,$draw_no_start,$draw_no_end;
    public $etl_2, $etl_3, $beeline_2, $beeline_3, $unitel_2, $unitel_3, $total_etl, $total_beeline, $total_unitel;
    
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'member_draw';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('time_start, time_end, draw_no', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'time_start' => 'Time Start',
            'time_end' => 'Time End',
            'draw_no' => 'Draw No'
        );
    }

    public function getDrawNo(){
        $drawNo = array();
        $sql = 'SELECT draw_no FROM draw_time WHERE delete_status = 0 ORDER BY draw_no DESC';
        $data = Yii::app()->db->createCommand($sql)->queryAll();
        foreach ($data as $item) {
            $drawNo[$item['draw_no']] = $item['draw_no'];
        }
        return $drawNo;
    }
}