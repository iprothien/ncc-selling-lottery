<?php

/**
 * This is the model class for table "cdr".
 *
 * The followings are the available columns in table 'cdr':
 * @property integer $id
 * @property string $start
 * @property string $callerid
 * @property string $src
 * @property string $dst
 * @property string $dcontext
 * @property string $channel
 * @property string $dstchannel
 * @property string $lastapp
 * @property string $lastdata
 * @property integer $duration
 * @property integer $billsec
 * @property string $disposition
 * @property integer $amaflags
 * @property string $accountcode
 * @property string $userfield
 * @property string $uniqueid
 */
class CdrUnitel extends CActiveRecord {

    public $time_start;
    public $time_end;
    public $count_total;
    public $count_block;
    public $option;
    
    public function getDbConnection() {
            return Yii::app()->ncc_unitel;
    }
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Cdr the static model class
     */
    public function getCountBlock() {
        $data = array();
        $sql = 'SELECT sum(ceil(duration/30)) as count FROM cdr where accountcode like "%1448%"';
        $cmd = Yii::app()->db->createCommand($sql);
        $data = $cmd->queryRow();
        return $data['count'];
    }

    public function getTotal() {
        $data = array();
        $sql = 'SELECT count(*) as total FROM cdr where accountcode like "%1448%"';
        $cmd = Yii::app()->db->createCommand($sql);
        $data = $cmd->queryRow();
        return $data['total'];
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'cdr';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('duration, billsec, amaflags', 'numerical', 'integerOnly' => true),
            array('callerid, src, dst, dcontext, channel, dstchannel, lastapp, lastdata', 'length', 'max' => 80),
            array('disposition', 'length', 'max' => 45),
            array('accountcode', 'length', 'max' => 20),
            array('userfield, uniqueid', 'length', 'max' => 255),
            array('start', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, src, dst, option, time_start, duration, billsec, time_end, accountcode', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'start' => 'Start',
            'callerid' => 'Callerid',
            'src' => 'Src',
            'dst' => 'Dst',
            'dcontext' => 'Dcontext',
            'channel' => 'Channel',
            'dstchannel' => 'Dstchannel',
            'lastapp' => 'Lastapp',
            'lastdata' => 'Lastdata',
            'duration' => 'Duration',
            'billsec' => 'Billsec',
            'disposition' => 'Disposition',
            'amaflags' => 'Amaflags',
            'accountcode' => 'Shortcode',
            'userfield' => 'Userfield',
            'uniqueid' => 'Uniqueid',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('src', $this->src, true);
        $criteria->compare('CHAR_LENGTH(src)','>='.'7',true);
        $criteria->addCondition('(SUBSTR(src,1,3) = "209" || SUBSTR(src,1,3) = "309")');

        if($this->accountcode){
            $criteria->compare('accountcode',$this->accountcode,true);
        }

        if ($this->time_start) {
            $criteria->compare('start', '> ' . $this->time_start);
        }

        if ($this->time_end) {
            $criteria->compare('start', '< ' . $this->time_end);
        }
        $criteria->order = 'id DESC';

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                    'pagination' => array('pageSize' => '25'),
                ));
    }

    public $totalRecord,$totalDuration;
    public function getTotalRecord(){
        $criteria=new CDbCriteria;
        $criteria->compare('src', $this->src, true);
        $criteria->compare('CHAR_LENGTH(src)','>='.'7',true);
        $criteria->addCondition('(SUBSTR(src,1,3) = "209" || SUBSTR(src,1,3) = "309")');

        if($this->accountcode){
            $criteria->compare('accountcode',$this->accountcode,true);
        }

        if ($this->time_start) {
            $criteria->compare('start', '> ' . $this->time_start);
        }
        if ($this->time_end) {
            $criteria->compare('start', '< ' . $this->time_end);
        }
        $totalRecord = $this->count($criteria);
        return $totalRecord;
    }

    public function getSumDuration(){
        $criteria=new CDbCriteria;
        $criteria->select = "SUM(duration) AS totalDuration";
        $criteria->compare('src', $this->src, true);
        $criteria->compare('CHAR_LENGTH(src)','>='.'7',true);
        $criteria->addCondition('(SUBSTR(src,1,3) = "209" || SUBSTR(src,1,3) = "309")');

        if($this->accountcode){
            $criteria->compare('accountcode',$this->accountcode,true);
        }

        if ($this->time_start) {
            $criteria->compare('start', '> ' . $this->time_start);
        }
        if ($this->time_end) {
            $criteria->compare('start', '< ' . $this->time_end);
        }
        $total = $this->find($criteria);
        return $total->totalDuration;
    }

}