<?php

/**
 * This is the model class for table "account".
 *
 * The followings are the available columns in table 'account':
 * @property integer $id
 * @property string $name
 * @property string $canreinvite
 * @property string $context
 * @property string $dtmfmode
 * @property string $host
 * @property string $port
 * @property string $secret
 * @property string $type
 * @property string $username
 * @property string $module
 */
class Account extends CActiveRecord
{
    public $user;
    public $password;
    public $re_password;
    public $moduleSelected;
    public $module;
    public $oldUsername;
    public $admin_user;
    public $user1;

    public function afterFind() {
        $this->moduleSelected = explode(',', User::model()->findByAttributes(array('account_id'=>$this->id))->module);
        parent::afterFind();
    }

    public function getListModule() {
        $moduleList = Yii::app()->params['service'];
        $result = array();
        foreach ($moduleList as $key => $value) {
            $result[$key] = $value['name'];
        }
        return $result;
    }

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Account the static model class
	 */
	public static function model($className=__CLASS__){
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'account';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('secret,moduleSelected', 'required'),
            array('user','validateUser'),
            array('password','validatePassword'),
            array('username','validateUsername'),
			array('secret, username', 'length', 'max'=>20),
			array('canreinvite', 'length', 'max'=>3),
			array('dtmfmode', 'length', 'max'=>7),
			array('host', 'length', 'max'=>31),
			array('port', 'length', 'max'=>5),
			array('type', 'length', 'max'=>6),
            array('user','checkUsername'),
            array('username','checkSipUser'),
			array('id, name, canreinvite, context, dtmfmode, host, port, secret, type, username', 'safe', 'on'=>'search'),
		);
	}

    public function validateUser($attribute) {
        if($this->isNewRecord) {
            if(empty($this->user)) {
                $this->addError($attribute, 'User not empty');
            }
        }
    }

    public function validatePassword($attribute) {
        if($this->isNewRecord) {
            if(empty($this->password)) {
                $this->addError($attribute, 'Password not empty');
            }
        }
    }

    public function validateUsername($attribute) {
        if($this->isNewRecord) {
            if(empty($this->username)) {
                $this->addError($attribute, 'Password not empty');
            }
        }
    }


    public function checkUsername($attribute){
        if(User::model()->findAllByAttributes(array('username' => $this->user))){
            $this->addError($attribute, 'Username "' . $this->user . '" has already been taken.');
        }
    }

    public function checkSipUser($attribute){
        if($this->isNewRecord){
            if(Account::model()->findAllByAttributes(array('username' => $this->username))){
                $this->addError($attribute, 'Sip name "' . $this->username . '" has already been taken.');
            }
        }else{
            if(Account::model()->findAllByAttributes(array('username' => $this->oldUsername))){
                $this->addError($attribute, 'Re sip name "' . $this->oldUsername . '" has already been taken.');
            }
        }
    }

    /**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'canreinvite' => 'Canreinvite',
			'context' => 'Context',
			'dtmfmode' => 'Dtmfmode',
			'host' => 'Host',
			'port' => 'Port',
			'secret' => 'Sip Password',
			'type' => 'Type',
			'username' => 'Sip Username',
            'module' => 'Module',
            'user' => 'Username',
            'user1' => 'Username',
            're_password' => 'New Password',
            'oldUsername' => 'Sip Username',
            'admin_user' => 'Username',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
        $criteria->alias = 'acc';
        $criteria->select   = 'acc.*, ad.username as admin_user';
        $criteria->join     = 'INNER JOIN admin as ad ON acc.id = ad.account_id INNER JOIN ncc_agent as ag ON acc.username = ag.phonenumber';

        $criteria->compare('acc.id',$this->id);
		$criteria->compare('acc.name',$this->name,true);
		$criteria->compare('acc.canreinvite',$this->canreinvite,true);
		$criteria->compare('acc.context',$this->context,true);
		$criteria->compare('acc.dtmfmode',$this->dtmfmode,true);
		$criteria->compare('acc.host',$this->host,true);
		$criteria->compare('acc.port',$this->port,true);
		$criteria->compare('acc.secret',$this->secret,true);
		$criteria->compare('acc.type',$this->type,true);
		$criteria->compare('acc.username',$this->username,true);
        $criteria->order = 'acc.id DESC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}