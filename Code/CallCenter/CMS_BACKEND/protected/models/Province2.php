<?php

/**
 * This is the model class for table "province".
 *
 * The followings are the available columns in table 'province':
 * @property integer $id
 * @property integer $type
 * @property string $name
 * @property integer $parent_id
 * @property integer $status
 * @property string $created
 */
class Province2 extends CActiveRecord
{

    public $province_id = 0, $district_id = 0, $ward_id = 0;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'province';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name', 'required'),
            array('type, parent_id, status', 'numerical', 'integerOnly' => true),
            array('name', 'length', 'max' => 255),
            array('province_id, district_id, ward_id', 'provinceValidate'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, type, name, parent_id, status, created', 'safe', 'on' => 'search'),
        );
    }

    public function provinceValidate($attribute, $params)
    {
        if ($attribute == "province_id" && $this->type == 2 && !$this->province_id) {
            $this->addError("province_id", "Province cannot be blank");
        }

        if ($attribute == "district_id" && $this->type == 3 && !$this->district_id) {
            $this->addError("district_id", "District cannot be blank");
        }

        if ($attribute == "ward_id" && $this->type == 4 && !$this->ward_id) {
            $this->addError("ward_id", "Ward cannot be blank");
        }
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'type' => 'Type',
            'name' => 'Name',
            'parent_id' => 'Parent',
            'status' => 'Status',
            'created' => 'Created',
            'province_id' => "Province",
            'district_id' => "District",
            'ward_id' => "Ward",
            'village_id' => "Village"
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('name', $this->name, true);

        if ($this->province_id) {
            $criteria->condition = "id = " . $this->province_id . " OR parent_id" . $this->province_id;
        }

        if ($this->district_id && $this->province_id) {
            $criteria->condition = "id = " . $this->province_id . " OR parent_id = " . $this->district_id;
        }

        if ($this->district_id && $this->province_id && $this->ward_id) {
            $criteria->condition = "id = " . $this->province_id . " OR parent_id = " . $this->district_id . " OR parent_id = " . $this->ward_id;
        }

        $criteria->order = 'id DESC';

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Province2 the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    const Type1 = "Province";
    const Type2 = "District";
    const Type3 = "Ward";
    const Type4 = "Village";

    public function getTypeName($type = NULL){
        if ($type == NULL) {
            $type = $this->type;
        }
        $list = $this->getTypeList();
        if (isset($list[$type])) {
            return $list[$type];
        }
        return NULL;
    }

    public function getTypeList()
    {
        return array(
            1 => self::Type1,
            2 => self::Type2,
            3 => self::Type3,
            4 => self::Type4
        );
    }

    const Status_Disable = "Disable";
    const Status_Enable = "Enable";

    public function getStatusName($status = NULL)
    {
        if ($status == NULL) {
            $status = $this->status;
        }
        $list = $this->getStatusList();
        if (isset($list[$status])) {
            return $list[$status];
        }
        return NULL;
    }

    public function  getStatusList()
    {
        return array(
            0 => self::Status_Disable,
            1 => self::Status_Enable,
        );
    }

    public function getProvinceList($type, $parentId)
    {
        $result = array();

        $criteria = new CDbCriteria;
        $criteria->compare("type", $type);
        $criteria->compare("parent_id", $parentId);
        $data = $this->findAll($criteria);
        if ($data) {
            foreach ($data as $item) {
                $result[$item->id] = $item->name;
            }
        }
        return $result;
    }

    public function afterFind()
    {
        return parent::afterFind();
    }

    public function loadParents()
    {
        $parentId = $this->parent_id;
        for ($i = $this->type; $i > 1; $i--) {
            $item = $this->findByPk($parentId);
            if ($item) {
                if ($i == 4) {
                    $this->ward_id = $item->id;
                } else if ($i == 3) {
                    $this->district_id = $item->id;
                } else if ($i == 2) {
                    $this->province_id = $item->id;
                }
                $parentId = $item->parent_id;
            } else {
                break;
            }
        }
    }
}
