<?php

/**
 * This is the model class for table "charging_history".
 *
 * The followings are the available columns in table 'charging_history':
 * @property integer $id
 * @property string $msisdn
 * @property string $time_charge
 * @property integer $money
 * @property integer $member_id
 * @property integer $digit
 * @property integer $charging_type
 * @property integer $trans_id
 * @property string $trans_date
 */
class ChargingHistory extends CActiveRecord
{
    public $start, $end, $option, $conn, $str_status, $draw_no;
    public $totalAmount;

    public function afterFind (){
        if($this->charging_type ==0 ){
            $this->charging_type = 'Buy Lotto';
        }else{
            $this->charging_type = 'Info Sms';
        }
        parent::afterFind ();
    }

    public function getDbConnection(){
        if ($this->conn == 1) {
            return Yii::app()->ncc_beeline;
        } elseif ($this->conn == 2) {
            return Yii::app()->ncc_etl;
        } else {
            return Yii::app()->ncc_unitel;
        }
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'charging_history';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('msisdn, time_charge, money, member_id, digit', 'required'),
            array('money, member_id, digit, charging_type, trans_id', 'numerical', 'integerOnly' => true),
            array('msisdn', 'length', 'max' => 15),
            array('trans_date', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, msisdn, time_charge, start, end, money, member_id, digit, charging_type, draw_no, trans_id, trans_date', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'msisdn' => 'Msisdn',
            'time_charge' => 'Time Charge',
            'money' => 'Money',
            'member_id' => 'Member',
            'digit' => 'Digit',
            'charging_type' => 'Charging Type',
            'trans_id' => 'Trans',
            'trans_date' => 'Trans Date',
            'start' => 'Start Date',
            'end' => 'End Date',
            'draw_no' => 'Draw No',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;
        $criteria->alias = 'c';
        $criteria->select = 'c.*';
        $criteria->join = "INNER JOIN member_draw as m ON c.member_id = m.id INNER JOIN draw_time as dt ON m.draw_id = dt.id";

        $criteria->compare('c.id', $this->id);
        $criteria->compare('c.msisdn', $this->msisdn, true);
        $criteria->compare('c.charging_type', $this->charging_type,true);
        $criteria->compare('dt.draw_no', $this->draw_no,true);
        if($this->start){
            $criteria->compare('c.time_charge','>='.$this->start, true);
        }
        if($this->end){
            $criteria->compare('c.time_charge','<='.$this->end, true);
        }
        $criteria->order = 'c.id DESC';
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array('pageSize'=>'35'),
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ChargingHistory the static model class
     */
    public static function model($className = __CLASS__){
        return parent::model($className);
    }

    public function getSumAmount(){
        $criteria=new CDbCriteria;
        $criteria->alias = 'c';
        $criteria->select = "SUM(c.money) AS totalAmount";
        $criteria->join = "INNER JOIN member_draw as m ON c.member_id = m.id INNER JOIN draw_time as dt ON m.draw_id = dt.id";

        $criteria->compare('c.msisdn',$this->msisdn,true);
        $criteria->compare('c.charging_type', $this->charging_type);
        $criteria->compare('dt.draw_no', $this->draw_no,true);

        if($this->start){
            $criteria->compare('c.time_charge','>='.$this->start,true);
        }
        if($this->end){
            $criteria->compare('c.time_charge','<='.$this->end,true);
        }
        $total = $this->find($criteria);
        return $total->totalAmount;
    }
}
