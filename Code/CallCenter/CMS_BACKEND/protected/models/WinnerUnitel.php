<?php

/**
 * This is the model class for table "winner".
 *
 * The followings are the available columns in table 'winner':
 * @property integer $id
 * @property string $msisdn
 * @property integer $buyer_id
 * @property integer $transaction_id
 * @property integer $inform_status
 * @property string $created_datetime
 * @property string $full_name
 * @property string $description
 */
class WinnerUnitel extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public $option;
    public $time_start, $time_end;
    
    public function getDbConnection() {
            return Yii::app()->ncc_unitel;
    }
    
    public function getStatus() {
        switch ($this->inform_status) {
            case 1:
                return 'Complete';
                break;
            case 0:
                return 'Processing';
                break;
            default:
                return '';
                break;
        }
    }
    
    public function getPayment() {
        switch($this->payment_status) {
            case 1:
                return 'Paid';
                break;
            case 0:
                return 'Not Paid';
                break;
            default:
                return '';
        }
    }
    
    public function tableName() {
        return 'winner';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, msisdn, option, time_start, time_end, buyer_id, transaction_id, inform_status, created_datetime', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'msisdn' => 'Msisdn',
            'buyer_id' => 'Buyer',
            'transaction_id' => 'Transaction',
            'inform_status' => 'Inform Status',
            'created_datetime' => 'Created Datetime',
            'drawNo' => 'Draw No',
            'time_start' => 'Start Time',
            'time_end' => 'End Time',
            'code' => 'Number Buy',
            'money' => 'Amount Buy'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public $timeStart, $timeEnd, $drawNo, $code, $money;
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;
        $criteria->alias    = 'a';
        $criteria->select   = 'a.*,b.draw_no as drawNo,b.start_time as timeStart,b.end_time as timeEnd,c.code,c.money';
        $criteria->join = 'INNER JOIN draw_time AS b ON a.transaction_id=b.id
                                INNER JOIN member_draw AS c ON a.buyer_id=c.id';

        $criteria->compare('a.id', $this->id);
        $criteria->compare('a.msisdn', $this->msisdn, true);
        $criteria->compare('a.buyer_id', $this->buyer_id);
        $criteria->compare('a.transaction_id', $this->transaction_id);
        $criteria->compare('a.inform_status', $this->inform_status);
        
        if($this->time_start)
            $criteria->compare ('b.end_time', '>'.$this->time_start);
        if($this->time_end)
            $criteria->compare ('b.end_time', '<'.$this->time_end);

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Winner the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
