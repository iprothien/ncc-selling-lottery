<?php

/**
 * This is the model class for table "departments".
 *
 * The followings are the available columns in table 'departments':
 * @property integer $id
 * @property string $shortcode
 * @property string $name
 * @property string $path_wellcome_file
 */
class Departments extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Departments the static model class
     */
    public $old_value;
    
    public function afterFind() {
        $this->old_value    = $this->path_wellcome_file;
        parent::afterFind();
    }
    
    public function getMp3Player() {
        $file = array();
        $file = explode('.', $this->path_wellcome_file);
        $mp3 = $file[0] . '.mp3';
        return '<object type="application/x-shockwave-flash" data="mp3player/dewplayer.swf?mp3=uploads/cms/departments/' . $mp3 . '" width="200" height="20" id="dewplayer">
                        <param name="wmode" value="transparent" />
                        <param name="movie" value="mp3player/dewplayer.swf?mp3=uploads/cms/departments/' . $mp3 . '" />
                    </object>';
    }
    
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'departments';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('shortcode, name', 'required'),
            array('shortcode', 'length', 'max' => 10),
            array('name', 'length', 'max' => 50),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, shortcode, name, path_wellcome_file', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'shortcode' => 'Shortcode',
            'name' => 'Name',
            'path_wellcome_file' => 'Path Wellcome File',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('shortcode', $this->shortcode, true);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('path_wellcome_file', $this->path_wellcome_file, true);

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                ));
    }

}