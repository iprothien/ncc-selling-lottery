<?php

/**
 * This is the model class for table "ncc_agent_answer".
 *
 * The followings are the available columns in table 'ncc_agent_answer':
 * @property integer $id
 * @property integer $agent_id
 * @property string $path_record
 * @property string $msisdn_agent
 * @property string $msisdn_member
 * @property string $call_time
 * @property integer $duration
 * @property string $created
 * @property string $updated
 */
class NccAgentAnswer extends CActiveRecord
{
    public $note, $start, $end, $call_status;

    public function afterFind()
    {
        if ($this->duration) {
            $this->duration = $this->duration;
        } else {
            $this->duration = 'miss call';
        }
        parent::afterFind();
    }

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return NccAgentAnswer the static model class
     */

    //public $path_mp3   = '/var/www/html/ncc/convert/';

    public function getMp3Player()
    {
        $file = array();
        $file = explode('.', $this->path_record);
        $mp3 = $file[0] . '.mp3';
        return '<object type="application/x-shockwave-flash" data="mp3player/dewplayer.swf?mp3=/convert_to_mp3/' . $mp3 . '" width="200" height="20" id="dewplayer">
                        <param name="wmode" value="transparent" />
                        <param name="movie" value="mp3player/dewplayer.swf?mp3=/convert_to_mp3/' . $mp3 . '" />
                    </object>';
    }


    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'ncc_agent_answer';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('agent_id, path_record, msisdn_agent, msisdn_member, call_time, created, updated', 'required'),
            array('agent_id, duration', 'numerical', 'integerOnly' => true),
            array('msisdn_agent, msisdn_member', 'length', 'max' => 15),
            array('path_record', 'length', 'max' => 50),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, agent_id, agent, msisdn_agent, msisdn_member, call_time, duration, start, end', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'agent_id' => 'Agent',
            'path_record' => 'Path Record',
            'msisdn_agent' => 'Msisdn Agent',
            'msisdn_member' => 'Msisdn Member',
            'call_time' => 'Call Time',
            'duration' => 'Duration',
            'created' => 'Created',
            'updated' => 'Updated',
            'note' => 'Note',
            'start' => 'Time Start',
            'end' => 'Time End',
            'call_status' => 'Call Status',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public $agent;

    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $cmd = Yii::app()->db->createCommand();
        $cmd->select('id')->from('ncc_agent')->where('phonenumber=:phonenumber', array(':phonenumber' => Yii::app()->user->name));
        $data = $cmd->queryRow();

        $criteria = new CDbCriteria;
        $criteria->alias = 'a';
        $criteria->select = 'a.*,b.name as agent,note.content as note';
        $criteria->join = 'LEFT JOIN ncc_agent as b ON a.agent_id=b.id LEFT JOIN customer as note ON a.msisdn_member = note.msisdn';

        $criteria->compare('id', $this->id);
        if (User::model()->findByPk(Yii::app()->user->id)->permision == 1) {
            $criteria->compare('agent_id', $this->agent_id);
        } else {
            $criteria->compare('agent_id', $data['id'], FALSE);
            $criteria->compare('agent_id', 0, FALSE, 'OR');
        }
        $criteria->compare('a.msisdn_agent', $this->msisdn_agent, true);
        $criteria->compare('a.msisdn_member', $this->msisdn_member, true);
        $criteria->compare('b.name', $this->agent, true);

        if ($this->duration <= 0) {
            $criteria->compare('a.duration', '<=' . $this->duration, true);
        } else {
            $criteria->compare('a.duration', '>' . $this->duration, true);
        }

        if ($this->start) {
            $criteria->compare('a.call_time', '>=' . $this->start, true);
        }
        if ($this->end) {
            $criteria->compare('a.call_time', '<=' . $this->end, true);
        }
        $criteria->order = 'a.id DESC';

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array('pageSize' => 25)
        ));
    }

}