<?php

/**
 * This is the model class for table "staff".
 *
 * The followings are the available columns in table 'staff':
 * @property string $id
 * @property string $full_name
 * @property string $phone
 * @property integer $departments_id
 */
class Staff extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Staff the static model class
     */

    public $sip_password;
    public $oldPhone;
    public $username,$admin_user;
    public $password,$pwd;
    public $moduleSelected;
    public $module;
    public $sip_pwd;
    public $username1;

    public function afterFind() {
        $id = Yii::app()->db->createCommand("SELECT id FROM account WHERE username=".$this->phone)->queryRow();
        if($id){
            $this->moduleSelected = explode(',', User::model()->findByAttributes(array('account_id'=>$id['id']))->module);
        }
        parent::afterFind();
    }

    public function getListModule() {
        $moduleList = Yii::app()->params['service'];
        $result = array();
        foreach ($moduleList as $key => $value) {
            $result[$key] = $value['name'];
        }
        return $result;
    }

    public function getListDp() {
        $list   = array();
        $cmd    = Yii::app()->db->createCommand();
        $cmd->select('id,name')->from('departments')->where('1');
        $data   = $cmd->queryAll();
        $list['']   = 'Select...';
        foreach($data as $item) {
            $list[$item['id']]  = $item['name'];
        }
        return $list;
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'staff';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return array(
            array('full_name, phone,, moduleSelected', 'required'),
            array('username','validateUsername'),
            array('sip_password','validateSippassword'),
            array('password','validatePassword'),
            array('departments_id, sip_password', 'numerical', 'integerOnly' => true),
            array('full_name', 'length', 'max' => 40),
            array('phone','checkAccount'),
            array('username','checkUsername'),
            array('id, full_name, phone, departments_id', 'safe', 'on' => 'search'),
        );
    }

    public function validateUsername($attribute) {
        if($this->isNewRecord) {
            if(empty($this->username)) {
                $this->addError($attribute, 'Username not empty');
            }
        }
    }

    public function validateSippassword($attribute) {
        if($this->isNewRecord) {
            if(empty($this->sip_password)) {
                $this->addError($attribute, 'Sip password not empty');
            }
        }
    }

    public function validatePassword($attribute) {
        if($this->isNewRecord) {
            if(empty($this->password)) {
                $this->addError($attribute, 'Password not empty');
            }
        }
    }
    
    public function checkAccount($attribute) {
        if($this->isNewRecord){
            if(Staff::model()->findAllByAttributes(array('phone' => $this->phone))){
                $this->addError($attribute, 'Phone "' . $this->phone . '" has already been taken.');
            }elseif(Account::model()->findAllByAttributes(array('username' => $this->phone))){
                $this->addError($attribute, 'Username sip "' . $this->phone . '" has already been taken.');
            }
        }else{
            if(Staff::model()->findAllByAttributes(array('phone' => $this->oldPhone))){
                $this->addError($attribute, 'Re-phone "' . $this->oldPhone . '" has already been taken.');
            }elseif(Account::model()->findAllByAttributes(array('username' => $this->oldPhone))){
                $this->addError($attribute, 'Re-username sip "' . $this->oldPhone . '" has already been taken.');
            }
        }
    }

    public function checkUsername($attribute){
        if(User::model()->findAllByAttributes(array('username' => $this->username))){
            $this->addError($attribute, 'Username "' . $this->username . '" has already been taken.');
        }
    }

    /*public function checkModule($attribute){
        $module = User::model()->findAllByAttributes(array('module' => $this->moduleSelected));
        if(empty($module)){
            $this->addError($attribute, 'Module not Empty');
        }
    }*/

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'full_name' => 'Full Name',
            'username' => 'Username',
            'username1' => 'Username',
            'password' => 'Password',
            'phone' => 'Sip Username',
            'sip_password' => 'Sip Password',
            'pwd' => 'New Password',
            'oldPhone' => 'Sip Username',
            'departments_id' => 'Departments',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public $Department;
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;
        //$criteria->alias    = 'a';
        //$criteria->select   = 'a.*, b.name as Department';
        //$criteria->join     = 'INNER JOIN departments as b ON a.departments_id=b.id';

        $criteria->compare('id', $this->id, true);
        $criteria->compare('full_name', $this->full_name, true);
        $criteria->compare('phone', $this->phone, true);
        //$criteria->compare('departments_id', $this->departments_id);

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
            ));
    }

}