<?php

/**
 * This is the model class for table "transaction_log".
 *
 * The followings are the available columns in table 'transaction_log':
 * @property integer $id
 * @property string $msisdn
 * @property string $code
 * @property integer $money
 * @property string $created_datetime
 * @property integer $type
 * @property integer $status
 * @property integer $draw_id
 * @property integer $buy_type
 * @property integer $agent_id
 * @property string $content
 * @property string $balance_before
 * @property string $balance_after
 * @property integer $member_id
 */
class TransactionEtl extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
         public $start_time;
         public $end_time;
         public $stt_1, $stt_2, $stt_3, $stt_4, $stt_5, $stt_6, $stt_7;
         public $stt;
         public $option;
         public function tableName()
	{
		return 'transaction_log';
	}
         public function getDbConnection() {
            return Yii::app()->ncc_etl;
    }

        public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('msisdn, code, money, created_datetime, type, status', 'required'),
			array('money, type, status, draw_id, buy_type, agent_id, member_id', 'numerical', 'integerOnly'=>true),
			array('msisdn, balance_before, balance_after', 'length', 'max'=>16),
			array('code', 'length', 'max'=>3),
			array('content', 'length', 'max'=>1024),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id,option, msisdn, code, money,start_time,end_time, type, status, draw_id, buy_type, agent_id, content, balance_before, balance_after, member_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'msisdn' => 'Msisdn',
			'code' => 'Code',
			'money' => 'Money',
			'created_datetime' => 'Created time',
			'type' => 'Type',
			'status' => 'Status',
			'draw_id' => 'Draw',
			'buy_type' => 'Buy Type',
			'agent_id' => 'Phone Number',
			'content' => 'Information',
			'balance_before' => 'Balance Before',
			'balance_after' => 'Balance After',
			'member_id' => 'Member',
                        'start_time' => 'Start Date',
                        'end_time' => 'End Date',
                        'option' => 'Telco'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('msisdn',$this->msisdn,true);
		$criteria->compare('code',$this->code,true);
		$criteria->compare('money',$this->money);
		$criteria->compare('created_datetime',$this->created_datetime,true);
		$criteria->compare('type',$this->type);
		$criteria->compare('status',$this->status);
		$criteria->compare('draw_id',$this->draw_id);
		$criteria->compare('buy_type',$this->buy_type);
		$criteria->compare('agent_id',$this->agent_id);
		$criteria->compare('content',$this->content,true);
		$criteria->compare('balance_before',$this->balance_before,true);
		$criteria->compare('balance_after',$this->balance_after,true);
		$criteria->compare('member_id',$this->member_id);
                if ($this->start_time)
                    $criteria->compare('created_datetime', '>=' . $this->start_time);
                if ($this->end_time)
                    $criteria->compare('created_datetime', '<=' . $this->end_time);
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        public function getBuyType() {
        switch ($this->buy_type) {
            case 0:
                return 'IVR';
                break;
            case 1:
                return 'Call Center';
                break;
            case 2:
                return 'Query Result';
                break;
            default:
                break;
        }
    }
    public function getStatus(){
        switch ($this->status) {
            case 1:
                return 'Buy successfully';
                break;
            case 2:
                return 'Close for draw';
                break;
            case 3:
                return ' Postpaid';
                break;
            case 4:
                return 'Not enought money';
                break;
            case 5:
                return 'Sold out';
                break;
            case 6:
                return ' Cant charge';
                break;
            case 7:
                return 'SMS lottery fail';
            default:
                break;
        }
    }
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TransactionLog the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
