<?php

/**
 * This is the model class for table "processor".
 *
 * The followings are the available columns in table 'processor':
 * @property integer $id
 * @property integer $account_id
 * @property string $full_name
 * @property integer $type
 * @property integer $status
 */
class Processor extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    
    public function getStatus() {
        switch ($this->status) {
            case 1:
                return 'Active';
                break;
            case 0:
                return 'Inactive';
                break;
            default:
                return '';
                break;
        }
    }
    
    public function getType() {
        switch ($this->type) {
            case 1:
                return 'NCC';
                break;
            case 2:
                return 'Bank';
                break;
            default:
                return '';
                break;
        }
    }
    
    public function getUser() {
        $list   = array();
        $cmd    = Yii::app()->db->createCommand('SELECT id,name FROM account');
        $data   = $cmd->queryAll();
        $list['']   = 'SELECT...';
        foreach($data as $i) {
            $list[$i['id']] = $i['name'];
        }
        return $list;
    }
    
    public function tableName() {
        return 'processor';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('account_id, type, status', 'required'),
            array('account_id, type, status', 'numerical', 'integerOnly' => true),
            array('full_name', 'length', 'max' => 255),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, account_id, full_name, type, status', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'account_id' => 'Account',
            'full_name' => 'Full Name',
            'type' => 'Type',
            'status' => 'Status',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public $AccountName;
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;
        $criteria->alias    = 'a';
        $criteria->select   = 'a.*,b.name as AccountName';
        $criteria->join = 'INNER JOIN account AS b ON a.account_id=b.id';

        $criteria->compare('id', $this->id);
        $criteria->compare('a.full_name', $this->full_name, true);
        $criteria->compare('a.type', $this->type);
        $criteria->compare('a.status', $this->status);

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Processor the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
