<?php

/**
 * This is the model class for table "sms_mt".
 *
 * The followings are the available columns in table 'sms_mt':
 * @property integer $id
 * @property integer $short_code
 * @property string $msisdn
 * @property integer $status
 * @property integer $type
 * @property string $content
 * @property string $created_datetime
 * @property string $updated_datetime
 * @property integer $draw_no
 */
class SmsMt extends CActiveRecord
{
    public $start,$end,$option,$conn, $str_status;
    public $totalRecord,$totalSms=0;

    public function afterFind (){
        if($this->status == 1){
            $this->status = 'Successfully';
        }else if($this->status == 0){
            $this->status = 'Pending';
        }else{
            $this->status = 'Error';
        }
        parent::afterFind ();
    }

    public function getDbConnection() {
        if($this->conn==1){
            return Yii::app()->ncc_beeline;
        }elseif($this->conn==2){
            return Yii::app()->ncc_etl;
        }else{
            return Yii::app()->ncc_unitel;
        }
    }

	/**
	 * @return string the associated database table name
	 */
	public function tableName(){
		return 'sms_mt';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('short_code, status, type', 'numerical', 'integerOnly'=>true),
			array('msisdn', 'length', 'max'=>16),
			array('content', 'length', 'max'=>512),
			array('created_datetime, updated_datetime', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, short_code, msisdn, status, type, draw_no,content, created_datetime, start, end, option', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'short_code' => 'Short Code',
			'msisdn' => 'Msisdn',
			'status' => 'Status',
			'type' => 'Type',
			'content' => 'Content',
			'created_datetime' => 'Created Datetime',
			'updated_datetime' => 'Updated Datetime',
            'start' => 'Start Date',
            'end' => 'End Date',
            'option' => 'Telco',
            'draw_no' => 'Draw No',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('short_code',$this->short_code);
		$criteria->compare('msisdn',$this->msisdn,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('type',$this->type);
		$criteria->compare('content',$this->content,true);
        $criteria->compare('draw_no', $this->draw_no,true);
        if($this->start){
            $criteria->compare('created_datetime','>='.$this->start,true);
        }
        if($this->end){
            $criteria->compare('created_datetime','<='.$this->end,true);
        }
        $criteria->order = 'id DESC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array('pageSize'=>'35'),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SmsMt the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function getTotalRecord(){
        $criteria=new CDbCriteria;
        $criteria->compare('msisdn',$this->msisdn,true);
        $criteria->compare('draw_no', $this->draw_no,true);
        if($this->start){
            $criteria->compare('created_datetime','>='.$this->start,true);
        }
        if($this->end){
            $criteria->compare('created_datetime','<='.$this->end,true);
        }
        $totalRecord = $this->count($criteria);
        return $totalRecord;
    }

    public function getSumSms(){
        $criteria=new CDbCriteria;
        $criteria->select = "SUM(total_sms) AS totalSms";
        $criteria->compare('msisdn',$this->msisdn,true);
        $criteria->compare('draw_no', $this->draw_no,true);
        if($this->start){
            $criteria->compare('created_datetime','>='.$this->start,true);
        }
        if($this->end){
            $criteria->compare('created_datetime','<='.$this->end,true);
        }
        $total = $this->find($criteria);
        return $total->totalSms;
    }

    }
