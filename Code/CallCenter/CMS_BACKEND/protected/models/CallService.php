<?php

/**
 * This is the model class for table "call_service".
 *
 * The followings are the available columns in table 'call_service':
 * @property integer $id
 * @property string $name
 * @property integer $type
 * @property string $draw_no
 * @property string $time_call
 * @property integer $status
 * @property string $file_path
 * @property string $created_datetime
 * @property integer $call_type
 * @property string $media_path
 */
class CallService extends CActiveRecord
{
    public $sType, $sStatus, $sCallType, $connect, $fileName, $fileMedia;

    public function getDbConnection()
    {
        if($this->connect==1){
            return Yii::app()->ncc_beeline;
        }elseif($this->connect==2){
            return Yii::app()->ncc_etl;
        }elseif($this->connect == 3){
            return Yii::app()->ncc_unitel;
        }else{
            return Yii::app()->db;
        }
    }

    /**
     * @return string the associated database table name
     */

    public function afterFind()
    {
        if ($this->status == 0) {
            $this->sStatus = 'Pending';
        }elseif($this->status == 1){
            $this->sStatus = 'Finish';
        }else{
            $this->sStatus = 'Cancel';
        }

        if($this->type == 1){
            $this->sType = 'Draw No';
        }elseif($this->type == 2){
            $this->sType = 'Upload File';
        }else{
            $this->sType = 'Service';
        }

        if($this->call_type == 1){
            $this->sCallType = 'Inform results';
        }elseif($this->call_type == 3){
            $this->sCallType = 'Inform winner';
        }else{
            $this->sCallType = 'Promotion';
        }
        return parent::afterFind();
    }

    public function tableName()
    {
        return 'call_service';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name, type', 'required'),
            array('type, status, call_type', 'numerical', 'integerOnly' => true),
            array('name, file_path, media_path', 'length', 'max' => 256),
            array('draw_no', 'length', 'max' => 5),
            array('file_path', 'file', 'allowEmpty' => true, 'types' => 'txt'),
            array('media_path', 'file', 'allowEmpty' => true, 'types' => 'mp3'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, name, type, draw_no, time_call, status, file_path, created_datetime, call_type, media_path', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'name' => 'Name',
            'type' => 'Get list phone number from',
            'sType' => 'Type',
            'draw_no' => 'Draw No',
            'time_call' => 'Time Call',
            'status' => 'Status',
            'sStatus' => 'Status',
            'file_path' => 'File Path',
            'created_datetime' => 'Created Datetime',
            'call_type' => 'Call Type',
            'sCallType' => 'Call Type',
            'media_path' => 'Media Path',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('type', $this->type);
        $criteria->compare('draw_no', $this->draw_no, true);
        $criteria->compare('time_call', $this->time_call, true);
        $criteria->compare('status', $this->status);
        $criteria->compare('file_path', $this->file_path, true);
        $criteria->compare('created_datetime', $this->created_datetime, true);
        $criteria->compare('call_type', $this->call_type);
        $criteria->compare('media_path', $this->media_path, true);

        $criteria->order = 'id DESC';

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array('pageSize' => '20'),
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return CallService the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function getDrawNo()
    {
        $list = array();
        $sql = "SELECT draw_no FROM draw_time WHERE status = 2 ORDER BY draw_no DESC";
        $data = Yii::app()->db->createCommand($sql)->queryAll();
        foreach ($data as $item) {
            $list[$item['draw_no']] = $item['draw_no'];
        }
        return $list;
    }
}
