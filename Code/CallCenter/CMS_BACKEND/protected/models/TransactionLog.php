<?php

/**
 * This is the model class for table "transaction_log".
 *
 * The followings are the available columns in table 'transaction_log':
 * @property integer $id
 * @property string $msisdn
 * @property string $code
 * @property integer $money
 * @property string $created_datetime
 * @property integer $type
 * @property integer $status
 * @property integer $draw_id
 * @property integer $buy_type
 * @property integer $agent_id
 * @property string $content
 * @property string $balance_before
 * @property string $balance_after
 * @property integer $member_id
 */
class TransactionLog extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
         public $start;
         public $end;
         public $option,$connect;
         public function tableName()
	{
		return 'transaction_log';
	}
         public function getDbConnection(){
        if ($this->connect == 1) {
            return Yii::app()->ncc_beeline;
        } elseif ($this->connect == 2) {
            return Yii::app()->ncc_etl;
        } else {
            return Yii::app()->ncc_unitel;
        }
    }


        public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('msisdn, code, money, created_datetime, type, status', 'required'),
			array('money, type, status, draw_id, buy_type, agent_id, member_id', 'numerical', 'integerOnly'=>true),
			array('msisdn, balance_before, balance_after', 'length', 'max'=>16),
			array('code', 'length', 'max'=>3),
			array('content', 'length', 'max'=>1024),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id,option, msisdn, code, money,start,end, type, status, draw_id, buy_type, agent_id, content, balance_before, balance_after, member_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'msisdn' => 'Phone Number',
			'code' => 'Code',
			'money' => 'Money',
			'created_datetime' => 'Created time',
			'type' => 'Type',
			'status' => 'Status',
			'draw_id' => 'Draw',
			'buy_type' => 'Buy Type',
			'agent_id' => 'Agent',
			'content' => 'Information',
			'balance_before' => 'Balance Before',
			'balance_after' => 'Balance After',
			'member_id' => 'Member',
                        'start' => 'Start Date',
                        'end' => 'End Date',
                        'option' => 'Telco'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('msisdn',$this->msisdn,true);
		$criteria->compare('code',$this->code,true);
		$criteria->compare('money',$this->money);
		$criteria->compare('created_datetime',$this->created_datetime,true);
		$criteria->compare('type',$this->type);
		$criteria->compare('status',$this->status);
		$criteria->compare('draw_id',$this->draw_id);
		$criteria->compare('buy_type',$this->buy_type);
		$criteria->compare('agent_id',$this->agent_id);
		$criteria->compare('content',$this->content,true);
		$criteria->compare('balance_before',$this->balance_before,true);
		$criteria->compare('balance_after',$this->balance_after,true);
		$criteria->compare('member_id',$this->member_id);
                if ($this->start)
                    $criteria->compare('created_datetime', '>=' . $this->start);
                if ($this->end)
                    $criteria->compare('created_datetime', '<=' . $this->end);
                $criteria->order = 'id DESC';
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination' => array('pageSize'=>'25'),
		));
	}
           
    
    public function afterFind (){
        if($this->buy_type == 0){
            $this->buy_type = 'IVR';
        }
        elseif($this->buy_type == 1){
            $this->buy_type = 'Call center';
        }elseif($this->buy_type == 2){
		$this->buy_type = 'Query Result';
	}else{
		$this->buy_type = '   ';
	}    
       if($this->status == 1){
            $this->status = 'Buy successfully';
       }elseif($this->status == 2){
            $this->status= 'Close for draw';
       }elseif($this->status == 3){
           $this->status = 'Postpaid';
       }elseif($this->status == 4){
           $this->status = 'Not enought money';
       }elseif ($this->status == 5) {
           $this->status = 'Sold out';
        }elseif ($this->status == 6) {
            $this->status= 'Cant charge';
        }elseif ($this->status == 7) {
            $this->status = 'SMS lottery fail';
        }
	  parent::afterFind ();
    }
    public function getTotalRecord(){
        $criteria=new CDbCriteria;
        $criteria->compare('msisdn',$this->msisdn,true);
        if($this->start){
            $criteria->compare('created_datetime','>='.$this->start,true);
        }
        if($this->end){
            $criteria->compare('created_datetime','<='.$this->end,true);
        }
        $totalRecord = $this->count($criteria);
        return $totalRecord;
    }
    public function getTotalRecord1(){
        $criteria=new CDbCriteria;
        $criteria -> condition='status=1';
        $criteria->compare('msisdn',$this->msisdn,true);
        if($this->start){
            $criteria->compare('created_datetime','>='.$this->start,true);
        }
        if($this->end){
            $criteria->compare('created_datetime','<='.$this->end,true);
        }
        $totalRecord1 = $this->count($criteria);
        return $totalRecord1;
    }
    public function getTotalRecord2(){
        $criteria=new CDbCriteria;
        $criteria -> condition='status=2';
        $criteria->compare('msisdn',$this->msisdn,true);
        if($this->start){
            $criteria->compare('created_datetime','>='.$this->start,true);
        }
        if($this->end){
            $criteria->compare('created_datetime','<='.$this->end,true);
        }
        $totalRecord2 = $this->count($criteria);
        return $totalRecord2;
    }
    public function getTotalRecord3(){
        $criteria=new CDbCriteria;
        $criteria -> condition='status=3';
        $criteria->compare('msisdn',$this->msisdn,true);
        if($this->start){
            $criteria->compare('created_datetime','>='.$this->start,true);
        }
        if($this->end){
            $criteria->compare('created_datetime','<='.$this->end,true);
        }
        $totalRecord3 = $this->count($criteria);
        return $totalRecord3;
    }
    public function getTotalRecord4(){
        $criteria=new CDbCriteria;
        $criteria -> condition='status=4';
        $criteria->compare('msisdn',$this->msisdn,true);
        if($this->start){
            $criteria->compare('created_datetime','>='.$this->start,true);
        }
        if($this->end){
            $criteria->compare('created_datetime','<='.$this->end,true);
        }
        $totalRecord4 = $this->count($criteria);
        return $totalRecord4;
    }
    public function getTotalRecord5(){
        $criteria=new CDbCriteria;
        $criteria -> condition='status=5';
        $criteria->compare('msisdn',$this->msisdn,true);
        if($this->start){
            $criteria->compare('created_datetime','>='.$this->start,true);
        }
        if($this->end){
            $criteria->compare('created_datetime','<='.$this->end,true);
        }
        $totalRecord5 = $this->count($criteria);
        return $totalRecord5;
    }
    public function getTotalRecord6(){
        $criteria=new CDbCriteria;
        $criteria -> condition='status=6';
        $criteria->compare('msisdn',$this->msisdn,true);
        if($this->start){
            $criteria->compare('created_datetime','>='.$this->start,true);
        }
        if($this->end){
            $criteria->compare('created_datetime','<='.$this->end,true);
        }
        $totalRecord6 = $this->count($criteria);
        return $totalRecord6;
    }
    public function getTotalRecord7(){
        $criteria=new CDbCriteria;
        $criteria -> condition='status=7';
        $criteria->compare('msisdn',$this->msisdn,true);
        if($this->start){
            $criteria->compare('created_datetime','>='.$this->start,true);
        }
        if($this->end){
            $criteria->compare('created_datetime','<='.$this->end,true);
        }
        $totalRecord7 = $this->count($criteria);
        return $totalRecord7;
    }
    
   
    /**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TransactionLog the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
