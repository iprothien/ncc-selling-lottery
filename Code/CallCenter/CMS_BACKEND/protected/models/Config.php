<?php

/**
 * This is the model class for table "config".
 *
 * The followings are the available columns in table 'config':
 * @property integer $id
 * @property string $key
 * @property string $value
 * @property integer $convert_status
 */
class Config extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Config the static model class
     */
    public function getMp3Player() {
        $file = array();
        $file = explode('.', $this->value);
        $mp3 = $file[0] . '.mp3';
        return '<object type="application/x-shockwave-flash" data="mp3player/dewplayer.swf?mp3=uploads/cms/config/' . $mp3 . '" width="200" height="20" id="dewplayer">
                        <param name="wmode" value="transparent" />
                        <param name="movie" value="mp3player/dewplayer.swf?mp3=uploads/cms/config/' . $mp3 . '" />
                    </object>';
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'config';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('key, convert_status', 'required'),
            array('convert_status', 'numerical', 'integerOnly' => true),
            array('value', 'file', 'allowEmpty' => true, 'types' => Yii::app()->params['audioType']),
            array('key, value, note', 'length', 'max' => 255),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, key, value, convert_status', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'key' => 'Key',
            'value' => 'Value',
            'note' => 'Note',
            'convert_status' => 'Convert Status',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('key', $this->key, true);
        $criteria->compare('value', $this->value, true);
        $criteria->compare('note', $this->note);
        $criteria->compare('convert_status', $this->convert_status);

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                ));
    }

}