<?php
require('lib/config.php');
require('lib/functions_beeline.php');
require('lib/sbMysqlPDO.class.php');
require('lib/sbMssql.class.php');
$conn = new sbMysqlPDO($server, $user, $password, $db);

$return = array();
$ip = $_SERVER['REMOTE_ADDR'];

$logInfo = $ip."|".date("Y-m-d H:i:s",time());
foreach($_REQUEST as $key=>$value){
    $logInfo .= "|$key<>$value";
}
$logInfo .= "\n";
file_put_contents("data/".date("Y_m_d").".log",$logInfo,FILE_APPEND);


if ($ip == '103.240.242.59' || $ip == '27.118.30.2') {
    if (isset($_GET['msisdn']) && $_GET['msisdn'])
        $msisdn = $_GET['msisdn'];

    if (isset($_GET['code']) && $_GET['code'])
        $listcode = $_GET['code'];

    if (isset($_GET['money']) && $_GET['money'])
        $listmoney = $_GET['money'];

    if (isset($_GET['draw_id']) && $_GET['draw_id'])
        $draw_id = $_GET['draw_id'];

    if (isset($_GET['agent_id']) && $_GET['agent_id'])
        $agent_id = $_GET['agent_id'];

    if ($msisdn && $listcode && $listmoney && $draw_id && $agent_id) {

        $code = explode(':', $listcode);
        $money = explode(':', $listmoney);

        $i = 0;
        $count = count($code);
        $msgMt = array();
        for ($i; $i < $count; $i++) {
            $memberId = 0;
            $submitResultContent = '';

            $sqlgetTime = "SELECT * FROM draw_time WHERE ref_id=" . $draw_id;
            $getTime = $conn->doSelectOne($sqlgetTime);

            if (!$getTime) {
                //Invalid Time
                $status = 2;
                $statusError[] = $code[$i] . ':' . $money[$i] . ':' . $status;
            } else {
                $info = viewInfo($msisdn);
                if (checkType($msisdn)) {
                    if ($info >= $money[$i]) {
                        $rsAddOrder = addOrder($msisdn, $code[$i], $money[$i]);

                        if ($rsAddOrder == $money[$i]) {
                            $result = charge($msisdn, $money[$i]);
                            
                            $logCharging = date('Y-m-d H:i:s',time())."|".$msisdn."|".$money[$i];
                            foreach($result as $key=>$value){
                                $logCharging .= "|$key<>$value";
                            }
                            $logCharging .= "\n";
                            file_put_contents("data/".date("Y_m_d")."_charging.log",$logCharging,FILE_APPEND);
                            
                            if ($result) {
                                
                                $sqlInsertMember = 'INSERT INTO member_draw(msisdn,code,money,created_datetime,type,status,draw_no,draw_id,buy_type,agent_id) VALUES ("' . $msisdn . '", "' . $code[$i] . '", "' . $money[$i] . '", now(), "' . (strlen($code[$i]) - 1) . '", "1", "' . $getTime['draw_no'] . '", "' . $getTime['id'] . '", "1", "' . $agent_id . '")';

                                $memberId = $conn->doUpdate($sqlInsertMember);

                                if ($memberId) {
                                    $submitResult = SubmitBuying($msisdn, $money[$i], $code[$i]);
                                    if (!substr_count(@$submitResult->SubmitBuyingResult, "Successfull-IVR")) {
                                        //SUBMIT NOT OK
                                        $status = 7;
                                        $statusError[] = $code[$i] . ':' . $money[$i] . ':' . $status;
                                    } else {
                                        //TRU TIEN VA MUA THANH CONG
                                        $status = 1;
                                        $statusError[] = $code[$i] . ':' . $money[$i] . ':' . $status;
                                    }
                                    $submitResultContent = htmlentities(@$submitResult->SubmitBuyingResult, ENT_QUOTES);

                                    //InsertCharging
                                    $sqlInsertCharging = "INSERT INTO charging_history(msisdn,time_charge,money,member_id,digit,trans_id,trans_date,draw_no) VALUES ('{$msisdn}',now(),'{$money[$i]}','{$memberId}','{$code[$i]}','{$result['trans_id']}','{$result['trans_date']}', '{$getTime['draw_no']}')";
                                    $conn->doUpdate($sqlInsertCharging);

                                    $msgMt[] = 'Digit: ' . $code[$i] . ', Amount: ' . $money[$i] . " KIP";
                                }
                            } else {
                                //TRU TIEN THAT BAI
                                $status = 6;
                                $statusError[] = $code[$i] . ':' . $money[$i] . ':' . $status;
                            }
                        } else {
                            //FULL BOUGHT
                            $status = 5;
                            $statusError[] = $code[$i] . ':' . $money[$i] . ':' . $status;
                        }
                    } else {
                        //Not Enough
                        $status = 4;
                        $statusError[] = $code[$i] . ':' . $money[$i] . ':' . $status;
                    }
                } else {
                    //POSTPAID
                    $status = 3;
                    $statusError[] = $code[$i] . ':' . $money[$i] . ':' . $status;
                }
            }

            $inforAfter = viewInfo($msisdn);
            $tBalance[] = $info;
            $sBalance[] = $inforAfter;
            $submitContent[] = $submitResultContent;

            if (isset($status) && $status) {
                $sqlInsertTrans = 'INSERT INTO transaction_log
                                    (
                                    msisdn,code,money,created_datetime,type,status,draw_id,buy_type,agent_id,content,balance_before,balance_after,member_id
                                    ) 
                                    VALUES ("' . $msisdn . '",
                                            "' . $code[$i] . '",
                                            "' . $money[$i] . '",
                                            "' . date('Y-m-d H:i:s', time()) . '",
                                            "' . (strlen($code[$i]) - 1) . '",
                                            "' . $status . '",
                                            "' . $getTime['id'] . '",
                                            "1",
                                            "' . $agent_id . '",
                                            "' . $submitResultContent . '",
                                            "' . $info . '",
                                            "' . $inforAfter . '",
                                            "' . $memberId . '"
                                            )';

                $conn->doUpdate($sqlInsertTrans);
            }
        }

        /* INSERT INTO SMS_MT MULTI LOTTO  */
        if (isset($msgMt) && count($msgMt) > 0) {
            $smsInform = "You successfuly bought lottery with the following information:" . "\n"
                . "Draw No: " . $getTime['draw_no'] . "\n"
                . "Draw Date " . date('d/m/Y', strtotime($getTime['end_time'])) . "\n"
                . "Timing buy: " . date('H:i d/m/Y', time()) . "\n"
                . implode("\n", $msgMt);
            $totalSMS = ceil(strlen($smsInform) / 160);
            $sqlInsertSMS = "INSERT INTO sms_mt(short_code,msisdn,status,content,created_datetime,total_sms, draw_no) VALUES('8899','{$msisdn}','0','{$smsInform}',now(),'{$totalSMS}', '{$getTime['draw_no']}')";
            $conn->doUpdate($sqlInsertSMS);
        }

        if (isset($statusError) && $statusError)
            $statusReturn = implode(',', $statusError);
        $return[] = array('status' => $statusReturn, 'content' => implode('|', $submitContent), 'balance_before' => implode(',', $tBalance), 'balance_after' => implode(',', $sBalance));
        echo json_encode($return);
    }
}
?>