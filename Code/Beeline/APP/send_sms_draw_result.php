<?php
    require_once("lib/config.php");
    require_once("lib/sbMysqlPDO.class.php");

    try {
        $conn = new sbMysqlPDO($server, $user, $password, $db);
    } catch (Exception $e) {
        file_put_contents($_real_path . "log/connect.txt", "\nError when connect to database server|" . date("Y-m-d H:i:s", time()));
        sleep(5);
    }

    $sql = "INSERT INTO `sms_mt` (msisdn,short_code,status,content,created_datetime,updated_datetime,total_sms,draw_no)
    SELECT distinct(m.msisdn), '8899', 0, res.code, now(), now(), CEIL(LENGTH(res.code)/160),m.draw_no
    FROM `member_draw` as m INNER JOIN `draw_time` as dt ON m.draw_id = dt.id INNER JOIN `draw_result` as res ON dt.draw_no = res.draw_no
    WHERE res.info_status = 0 OR res.info_status IS NULL";
    $resultContent = $conn->doSelect($sql);

    // UPDATE info_status = 1 FROM TABLE draw_result
    $sqlUpdateDrawResult = "UPDATE `draw_result` SET `info_status` = 1 WHERE `info_status` = 0 OR `info_status` IS NULL";
    $conn->doUpdate($sqlUpdateDrawResult);
?>