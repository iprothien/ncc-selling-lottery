<?php
require_once("lib/config.php");
require_once("lib/functions.php");
require_once("lib/sbMysqlPDO.class.php");
$conn = NULL;

try {
    if (!$conn) {
        try {
            $conn = new sbMysqlPDO($server, $user, $password, $db);
        } catch (Exception $e) {
            file_put_contents($realPath . "log/{$mod}.txt", "\nError when connect to database server|" . date("Y-m-d H:i:s", time()));
            sleep(5);
        }
    } {
        $sqlSelectQuestion = " SELECT * FROM config WHERE convert_status=0 ORDER BY id ASC LIMIT 10 ";
        $questions = $conn->doSelect($sqlSelectQuestion);
        foreach ($questions as $question) {
            if (!strpos($item['value'], 'mp3')) {
                $OrgPath = "/var/www/lotto/uploads/cms/config/" . $question['value'];
                $fileMp3 = "/var/www/lotto/uploads/cms/config/" . getFileName($question['value']) . ".mp3";
                exec("/usr/bin/ffmpeg -i $OrgPath -y -vn -ar 44100 -ac 2 -ab 192 -f  mp3 $fileMp3");
            }

            $fileOrgPath = "/var/www/lotto/uploads/cms/config/" . $question['value'];
            $fileDestPath = "/var/www/lotto/uploads/system/config/" . $question['value'];
            exec("/usr/bin/ffmpeg -i $fileOrgPath -acodec pcm_alaw -ab 64k -ar 8000 -ac 1 $fileDestPath");
            rename("/var/www/lotto/uploads/system/config/" . $question['value'], "/var/www/lotto/uploads/system/config/" . getFileName($question['value']) . ".alaw");

            $sqlUpdate = " UPDATE config SET convert_status=1 WHERE id='{$question['id']}' ";
            $conn->doUpdate($sqlUpdate);

            sleep(5);
        }
        sleep(5);
        $conn = null;
    }
} catch (Exception $e) {
    
}
?>