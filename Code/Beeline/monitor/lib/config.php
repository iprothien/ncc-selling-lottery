<?php

$server 	= "localhost";
$user 		= "root";
$password 	= "";
$db 		= "lotto";
$dial           = '1448';
$shortcode      = '1448';
$feeType        = '1448_daily_paid';
$amout          = 1000000;
$max_timeout    = 5000;

$_max_two_digit = 2500000;
$_max_three_digit = 500000;

$_real_path = '/var/www/lotto/uploads/system/config/';
$_system_path = array(
    'welcome' => $_real_path.'welcome',
    'main_menu' => $_real_path.'main_menu',
    'promotion' => $_real_path.'promotion',
    'method' => $_real_path.'method',
    'invalid' => $_real_path.'invalid',
    'youwant' => $_real_path.'youwant',
    'with' => $_real_path.'with',
    'confirm' => $_real_path.'confirm',
    'youbuy' => $_real_path.'youbuy',
    'buysuccess' => $_real_path.'buysuccess',
    'notenough' => $_real_path.'notenough',
    'close' => $_real_path.'close',
    'result' => $_real_path.'result',
    'full_bought' => $_real_path.'full_bought',
    'number_buy' => $_real_path.'number_buy',
    'amount_buy' => $_real_path.'amount_buy',
    'continue_buy'=>$_real_path.'continue_buy',
    'postpaid'=>$_real_path.'postpaid',
    'cant_charge'=>$_real_path.'cant_charge',
    'invalid_amount'=>$_real_path.'invalid_amount',
    'not_press'=>$_real_path.'not_press',
    'success_amount'=>$_real_path.'success_amount',
    'success_digit'=>$_real_path.'success_digit',
    'sub_menu'=>$_real_path.'sub_menu',
    'millionaire'=>$_real_path.'millionaire',
);
?>