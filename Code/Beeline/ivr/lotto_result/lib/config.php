<?php

$server 	= "localhost";
$user 		= "root";
$password 	= "";
$db 		= "lotto";
$dial           = '1448';
$shortcode      = '1448';
$feeType        = '1448_daily_paid';
$amout          = 1000000;
$max_timeout    = 5000;

$_real_path = '/var/www/lotto/uploads/system/config/';
$_system_path = array(
    'welcome' => $_real_path.'welcome',
    'main_result' => $_real_path.'main_result',
    'promotion' => $_real_path.'promotion',
    'method' => $_real_path.'method',
    'invalid' => $_real_path.'invalid',
    'notenough' => $_real_path.'notenough',
    'close' => $_real_path.'close',
    'menu_result' => $_real_path.'menu_result',
    'month' => $_real_path.'month',
    'input_date' => $_real_path.'input_date',
    'no_draw' => $_real_path.'no_draw',
    'result' => $_real_path.'result',
    'charge1000' => $_real_path.'charge1000',
    'postpaid'=>$_real_path.'postpaid',
    'cant_charge'=>$_real_path.'cant_charge',
    'invalid_amount'=>$_real_path.'invalid_amount',
    'not_press'=>$_real_path.'not_press',
    'invalid_date'=>$_real_path.'invalid_date',
);
?>