#!/usr/bin/php -q
<?php
set_time_limit(30);
require('lib/phpagi.php');
require('lib/sbMysqlPDO.class.php');
require('lib/sbMssql.class.php');
require('lib/config.php');
require('lib/functions_beeline.php');
$agi = new AGI();
$conn = new sbMysqlPDO($server, $user, $password, $db);

$caller = $agi->get_variable("CALLERID(num)");
$caller = $caller['data'];
//$agi->say_digits($caller);
$today = date("Y-m-d H:i:s", time());
$sqlgetTime = "SELECT * FROM draw_time WHERE NOW() > start_time && NOW() < end_time && status=1";

$getTime = $conn->doSelectOne($sqlgetTime);
if (!$getTime) {
    //Invalid Time
    $agi->stream_file($_system_path['close']);
    $agi->exec_goto('lotto-menu', $dial, 1);
    exit;
} else {
    $j = 1;
    while (1) {
        $data = $agi->get_data($_system_path['number_buy'], 5000, 3);
        $pressKey = $data['result'];
        if (!$pressKey) {
            if ($j++ > 2) {
                $agi->stream_file($_system_path['not_press']);
                exit;
            }
        } else {
            if ($pressKey == '0') {
                $agi->exec_goto('lotto-menu', $dial, 1);
                exit;
            }

            $data2 = $agi->get_data($_system_path['amount_buy'], 5000, 9);
            $pressAmount = $data2['result'];

            $money = intval($pressAmount);
            if (strlen($pressKey) >= 2 && strlen($pressKey) <= 3 && $money<=100000) {

                if ($money % 1000 == 0 && $money != '0') {
                    $agi->set_variable('code', $pressKey);
                    $agi->set_variable('money', $money);

                    $Time_Buy = date('Y-m-d H:i:s', time());
                    //$DigitAvailable = checkDigitAvailable($pressKey,$amout);
                    $DigitAvailable = true;
                    if ($DigitAvailable) {
                        $i = 1;
                        $dtConfirm2 = $agi->stream_file($_system_path['youwant']);
                        $agi->say_digits($pressKey);
                        $dtConfirm3 = $agi->stream_file($_system_path['with']);
                        $agi->say_number($money);
                        while (1) {
                            $dtConfirm = $agi->get_data($_system_path['confirm'], 1);
                            $keyConfirm = $dtConfirm['result'];
                            if ($keyConfirm) {
                                $agi->set_variable('keyConfirm', $keyConfirm);
                                $agi->exec_goto('loto-confirm', $dial, 1);
                                exit;
                            } else {
                                if ($i++ > 2) {
                                    $agi->stream_file($_system_path['not_press']);
                                    exit;
                                }
                            }
                        }
                        exit;
                    } else {
                        $agi->stream_file($_system_path['invalid']);
                        $agi->exec_goto('lotto-buy', $dial, 1);
                        exit;
                    }
                } else {
                    $agi->stream_file($_system_path['invalid_amount']);
                    $agi->exec_goto('lotto-buy', $dial, 1);
                    exit;
                }
            } elseif ($pressAmount == '0') {
                $agi->exec_goto('lotto-menu', $dial, 1);
                break;
            } else {
                //Invalid. Wrong Press
                $agi->stream_file($_system_path['invalid']);
                $agi->exec_goto('lotto-buy', $dial, 1);
                break;
            }
        }
    }
    exit;
}
?>